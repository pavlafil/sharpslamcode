# SharpSLAM

> This repository is part (submodule) of https://gitlab.fit.cvut.cz/pavlafil/sharpslam. The superior repository contains all files related with this project. This repository is reserved only for code versioning.

This program is heavily based on original https://github.com/JakobEngel/dso so maybe you want check this repository first. Used (slightly modified) version for debugging on windows is copied in superior repository.

## Goals for this project

 - **Port existing `C++` program to "higher" language** with better abstraction capabilities while maintaining performance (in this case `C# & C++/CLI`)
 - **Get rid of dependencies hell** - only few `NuGet` packages are used and they will be included in repository (just in case).
 - **Partial but easy to use platofrm portability**
 - **Find out if there is any possibility of improvement using algorithms of artificial intelligence or data mining**

## Structore of project

 - **App.Net461** Console application containing entry point of project. Run under full `.Net461` framework. It is not easy platform portable but can use full framework and therefore `SharpSLAM` modules that are not portable (e.g. `WPF` Viewer).
 - **App.NetCore** Console application containing entry point of project. Run under portable `.NetCore` but can not use all `SharpSLAM` modules.
 - **Configuration** Define structured input for application. 
 - **Core** Contains ported application (written in `C#`).
 - **Core.Unmanaged** `C++/CLI` project containing code that can not be easily ported. Usually it is because specific library is used and there is no good enough alternative or because program can be more optimized in this language.
 - **Output.Abstractions** Define abstract interface between `Core` and `Output` layer and some code that can be use in Viewers (specific type of output).   
 - **Output.Viewer.WPF** Viewer implemented for debugging purposes. It is written in `WPF` so it can be used only with `Net461` and Windows.
 - **Shared** Contains common code used on multiple places across project. It contains abstraction for logging, abstraction for images and 3D structures
 - **KDTree** Implementation of KDTRee (third party)