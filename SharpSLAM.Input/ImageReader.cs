﻿using SharpSLAM.Output.Abstractions;
using SharpSLAM.Shared;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace SharpSLAM.Input
{
    public class ImageReader
    {
        private List<string> _images;
        private List<string> _imagesIds;
        private Configuration.Configuration _config;
        private Output.Abstractions.Output _output;
        private float[] _undisotrtCacheX;
        private float[] _undisotrtCacheY;
        

        private Bitmap loadImage(int index)
        {
            var image = new Bitmap(_images[index]);
            if (_config != null && (image.Height != _config.Input.GeometricCalibration.OriginalHeight || image.Width != _config.Input.GeometricCalibration.OriginalWidth))
            {
                throw new Exception($"Dimensions in calibration file and input image \"{_images[index]}\" does not match!");
            }
            return image;
        }

        private void ensureUndistortCacheLoaded()
        {
            if ((_undisotrtCacheX == null || _undisotrtCacheY == null) && _config.Input.GeometricCalibration?.CameraModel != null && _config.Input.GeometricCalibration.OutputCameraModel != null)
            {

                var size = _config.Input.GeometricCalibration.OutputWidth * _config.Input.GeometricCalibration.OutputHeight;
                _undisotrtCacheX = new float[size];
                _undisotrtCacheY = new float[size];
#if (PAR)
                Parallel.For(0, size, (i) =>
#else
                for (int i = 0; i < size; i++)
#endif
                {
                    float x = (float)(i % _config.Input.GeometricCalibration.OutputWidth);
                    float y = (float)(i / _config.Input.GeometricCalibration.OutputWidth);

                    //no distord
                    //_config.Input.Calibration.CameraModel = new Configuration.PinholeCameraModel() { cx = _config.Input.Calibration.OriginalWidth/2.0f, cy = _config.Input.Calibration.OriginalHeight/2.0f,
                    //fx=540,fy= 540};
                    //------------------
                    _config.Input.GeometricCalibration.CameraModel.Distort(x, y, out _undisotrtCacheX[i], out _undisotrtCacheY[i], _config.Input.GeometricCalibration.OutputCameraModel);
                }
#if (PAR)
                );
#endif
            }
        }

        private byte[] correctionsB(byte[] data, ref int width, ref int height)
        {
            byte[] output = new byte[width * height];
            float maxColor;
            var floats = correctionsF(data, ref width, ref height, out maxColor);
            if (maxColor <= 0) maxColor = 255;
#if (PAR)
            Parallel.For(0, height * width, (i) =>
#else
            for (int i = 0; i < height * width; i++)
#endif
            {
                output[i] = (byte)Math.Round((floats[i] / maxColor) * 255, 0, MidpointRounding.AwayFromZero);
            }
#if (PAR)
                );
#endif
            return output;
        }
        private float[] correctionsF(byte[] data, ref int width, ref int height)
        {
            float max;//not used - dummy value so specific signature of method can be used
            return correctionsF(data, ref width, ref height, out max);
        }

        //Original
        //        private float[] correctionsF(byte[] data, ref int width, ref int height, out float maxColor)
        //        {
        //            float lMaxColor = -1;
        //            float[] output = null;
        //            if (_config?.Input.Calibration?.OutputCameraModel == null || _undisotrtCacheX == null || _undisotrtCacheY == null) //there is no camera model or reader initialized without distort
        //            {
        //                //No distort only convert to one channel gray scale
        //                output = new float[height * width];
        //#if (PAR)
        //                        Parallel.For(0, height * width, (i) =>
        //                        {
        //                           var color = data[i * 3];
        //                            //if (color > lMaxColor)//no normalization 0..255 input is already in this interval i don't want stretch out smaller intervals (it is useful for wild float data)
        //                            //    lMaxColor = color;
        //                            output[i] = color;
        //                        });
        //#else
        //                for (int i = 0; i < height * width; i++)
        //                {
        //                    var color = data[i * 3];
        //                    //if (color > lMaxColor)//no normalization 0..255 input is already in this interval i don't want stretch out smaller intervals (it is useful for wild float data)
        //                    //    lMaxColor = color;
        //                    output[i] = color;
        //                }
        //#endif

        //            }
        //            else
        //            {
        //                //Distord
        //                height = _config.Input.Calibration.OutputHeight;
        //                width = _config.Input.Calibration.OutputWidth;
        //                var lOriginalWidth = _config.Input.Calibration.OriginalWidth;
        //                output = new float[height * width];

        //                var vignette = false;
        //                if (_config?.Input?.Vignette?.Values != null)
        //                {
        //                    if (_config.Input.Vignette.Values.Length * 3 == data.Length)
        //                    {
        //                        vignette = true;
        //                    }
        //                    else
        //                    {
        //                        throw new Exception("Dimensions mismatch - original image and vignette!");
        //                    }
        //                }
        //                else
        //                {
        //                    Log.LogInfo("Vignette was not provided - skipping!");
        //                }

        //#if (PAR)
        //                        Parallel.For(0, height * width, (i) =>
        //#else
        //                for (int i = 0; i < height * width; i++)
        //#endif
        //                {
        //                    double color = 0;
        //                    double vignetteColor = 0;
        //                    #region Undistort
        //                    float ox = _undisotrtCacheX[i], oy = _undisotrtCacheY[i];
        //                    for (int lr = 0; lr < 2; lr++)
        //                    {
        //                        //0=right, 1=left
        //                        var lrP = Math.Abs(lr - (ox - Math.Floor(ox))); //% of left or right pixel

        //                        for (int bt = 0; bt < 2; bt++)
        //                        {
        //                            //0=bottom, 1=top
        //                            var btP = Math.Abs(bt - (oy - Math.Floor(oy)));//% of top or bttom pixel

        //                            int dataIndex = (((int)Math.Floor(oy) + Math.Abs(bt - 1)) * lOriginalWidth) + ((int)Math.Floor(ox) + Math.Abs(lr - 1));
        //                            int bigDataIndex = 3 * dataIndex;
        //                            if (bigDataIndex > 0 && bigDataIndex < data.Length)
        //                            {
        //                                var p = lrP * btP;
        //                                color += data[bigDataIndex] * p;
        //                                if (vignette)
        //                                    vignetteColor += _config.Input.Vignette.InvertValues[dataIndex] * p;
        //                            }
        //                            else
        //                            {
        //                                Log.LogInfo($"Source coordinates out of range - index:{bigDataIndex}");
        //                            }
        //                        }
        //                    }

        //                    //color = Math.Sqrt(Math.Pow(((_config.Input.Calibration.OriginalWidth / 2) - ox), 2) + Math.Pow((_config.Input.Calibration.OriginalHeight / 2 - oy), 2));
        //                    //color = color / Math.Sqrt(Math.Pow(_config.Input.Calibration.OriginalWidth / 2, 2) + Math.Pow(_config.Input.Calibration.OriginalHeight / 2, 2)) * 255;

        //                    #endregion
        //                    #region Gamma
        //                    if (_config.Input.Gamma != null)
        //                    {
        //                        //this is original - do not know why bilinear is not required yet because i work with byte - discrete color values
        //                        //if (color < 1e-3)
        //                        //    BinvC = 0.0f;
        //                        //else if (color > GDepth - 1.01f)
        //                        //    BinvC = GDepth - 1.1;
        //                        //else
        //                        //{
        //                        //    int c = color;
        //                        //    float a = color - c;
        //                        //    BinvC = G[c] * (1 - a) + G[c + 1] * a;
        //                        //}

        //                        var gammaVals = _config.Input.Gamma.Values;
        //                        var index = Math.Floor(color);
        //                        var indexDiff = (color - index);
        //                        var indexC = (int)Math.Round(index * (gammaVals.Length - 1) / 255f, 0, MidpointRounding.AwayFromZero);
        //                        var indexN = (int)Math.Round((index + 1) * (gammaVals.Length - 1) / 255f, 0, MidpointRounding.AwayFromZero);
        //                        color = 0;
        //                        for (int lr = 0; lr < 2; lr++)
        //                        {
        //                            //0=next 1=prew
        //                            var p = Math.Abs(lr - indexDiff);
        //                            if (p != 0)
        //                            {
        //                                if (lr == 0)
        //                                {
        //                                    color += gammaVals[indexN] * p;
        //                                }
        //                                else
        //                                {
        //                                    color += gammaVals[indexC] * p;
        //                                }
        //                            }
        //                        }

        //                    }
        //                    #endregion
        //                    #region Vignette
        //                    if (vignette)
        //                    {
        //                        color = color * vignetteColor;
        //                    }
        //                    #endregion
        //                    if (color > lMaxColor)
        //                        lMaxColor = (float)color;
        //                    output[i] = (float)color;
        //                }
        //#if (PAR)
        //                        );
        //#endif
        //            }
        //            maxColor = lMaxColor;
        //            return output;
        //        }

        //Rewrite
        private float[] correctionsF(byte[] data, ref int width, ref int height, out float maxColor)
        {
            float lMaxColor = -1;
            float[] workingData = null;
            float[] output = null;
            if (_config?.Input.GeometricCalibration?.OutputCameraModel == null || _undisotrtCacheX == null || _undisotrtCacheY == null) //there is no camera model or reader initialized without distort
            {
                //No distort only convert to one channel gray scale
                output = new float[height * width];
#if (PAR)
                        Parallel.For(0, height * width, (i) =>
                        {
                           var color = data[i * 3];
                            //if (color > lMaxColor)//no normalization 0..255 input is already in this interval i don't want stretch out smaller intervals (it is useful for wild float data)
                            //    lMaxColor = color;
                            output[i] = color;
                        });
#else
                for (int i = 0; i < height * width; i++)
                {
                    var color = data[i * 3];
                    //if (color > lMaxColor)//no normalization 0..255 input is already in this interval i don't want stretch out smaller intervals (it is useful for wild float data)
                    //    lMaxColor = color;
                    output[i] = color;
                }
#endif

            }
            else
            {
                //Distord
                height = _config.Input.GeometricCalibration.OutputHeight;
                width = _config.Input.GeometricCalibration.OutputWidth;
                var lOriginalWidth = _config.Input.GeometricCalibration.OriginalWidth;
                workingData = new float[data.Length / 3];
                output = new float[height * width];

                var vignette = false;
                if (_config?.Input?.PhotometricCalibration?.Vignette != null)
                {
                    if (_config.Input.PhotometricCalibration.Vignette.Values.Length * 3 == data.Length)
                    {
                        vignette = true;
                    }
                    else
                    {
                        throw new Exception("Dimensions mismatch - original image and vignette!");
                    }
                }
                else
                {
                    Log.LogInfo("Vignette was not provided - skipping!");
                }


                //copy original
#if (PAR)
                        Parallel.For(0, workingData.Length, (i) =>
#else
                for (int i = 0; i < workingData.Length; i++)
#endif
                {
                    workingData[i] = data[i * 3];
                }
#if (PAR)
                );
#endif


                #region Gamma
                if (_config.Input.PhotometricCalibration?.Gamma != null)
                {
#if (PAR)
                        Parallel.For(0, workingData.Length, (i) =>
#else
                    for (int i = 0; i < workingData.Length; i++)
#endif
                    {
                        //this is original - do not know why bilinear is not required yet because i work with byte - discrete color values
                        //if (color < 1e-3)
                        //    BinvC = 0.0f;
                        //else if (color > GDepth - 1.01f)
                        //    BinvC = GDepth - 1.1;
                        //else
                        //{
                        //    int c = color;
                        //    float a = color - c;
                        //    BinvC = G[c] * (1 - a) + G[c + 1] * a;
                        //}

                        var gammaVals = _config.Input.PhotometricCalibration.Gamma.Values;
                        var index = Math.Floor(workingData[i]);
                        var indexDiff = (workingData[i] - index);
                        var indexC = (int)Math.Round(index * (gammaVals.Length - 1) / 255f, 0, MidpointRounding.AwayFromZero);
                        var indexN = (int)Math.Round((index + 1) * (gammaVals.Length - 1) / 255f, 0, MidpointRounding.AwayFromZero);
                        float color = 0;
                        for (int lr = 0; lr < 2; lr++)
                        {
                            //0=next 1=prew
                            var p = (float)Math.Abs(lr - indexDiff);
                            if (p != 0)
                            {
                                if (lr == 0)
                                {
                                    color += gammaVals[indexN] * p;
                                }
                                else
                                {
                                    color += gammaVals[indexC] * p;
                                }
                            }
                        }

                        workingData[i] = color;

                    }
#if (PAR)
                    );
#endif
                }
                #endregion


                if (vignette)
                {
#if (PAR)
                        Parallel.For(0, workingData.Length, (i) =>
#else
                    for (int i = 0; i < workingData.Length; i++)
#endif
                    {
                        //workingData[i] = workingData[i] * _config.Input.Vignette.Values[i];
                        workingData[i] = workingData[i] * _config.Input.PhotometricCalibration.Vignette.InvertValues[i];
                        if (float.IsNaN(workingData[i]) || float.IsInfinity(workingData[i]) || workingData[i] >= 800)
                        { workingData[i] = 0; }
                    }
#if (PAR)
                    );
#endif

                }


                #region Undistort
#if (PAR)
                        Parallel.For(0, height * width, (i) =>
#else
                for (int i = 0; i < height * width; i++)
#endif
                {
                    double color = 0;
                    float ox = _undisotrtCacheX[i], oy = _undisotrtCacheY[i];
                    for (int lr = 0; lr < 2; lr++)
                    {
                        //0=right, 1=left
                        var lrP = Math.Abs(lr - (ox - Math.Floor(ox))); //% of left or right pixel

                        for (int bt = 0; bt < 2; bt++)
                        {
                            //0=bottom, 1=top
                            var btP = Math.Abs(bt - (oy - Math.Floor(oy)));//% of top or bttom pixel

                            int dataIndex = (((int)Math.Floor(oy) + Math.Abs(bt - 1)) * lOriginalWidth) + ((int)Math.Floor(ox) + Math.Abs(lr - 1));
                            //int bigDataIndex = 3 * dataIndex;
                            if (dataIndex > 0 && dataIndex < workingData.Length)
                            {
                                var p = lrP * btP;
                                color += workingData[dataIndex] * p;
                            }
                            else
                            {
                                Log.LogInfo($"Source coordinates out of range - index:{dataIndex}");
                            }
                        }
                    }

                    //color = Math.Sqrt(Math.Pow(((_config.Input.Calibration.OriginalWidth / 2) - ox), 2) + Math.Pow((_config.Input.Calibration.OriginalHeight / 2 - oy), 2));
                    //color = color / Math.Sqrt(Math.Pow(_config.Input.Calibration.OriginalWidth / 2, 2) + Math.Pow(_config.Input.Calibration.OriginalHeight / 2, 2)) * 255;

                    if (color > lMaxColor)
                        lMaxColor = (float)color;
                    output[i] = (float)color;
                }
#if (PAR)
                        );
#endif
            }
            #endregion
            maxColor = lMaxColor;
            return output;
        }


        public bool Plot { get; set; } = true;

        public ImageReader(Configuration.Configuration config, Output.Abstractions.Output output = null, bool notLoadUndistordInfo = false)
        {
            _images = Directory.GetFiles(config.Input.WorkingDirectory + config.Input.ImagesPath).ToList();
            _imagesIds = _images.Select(x =>
            {
                var fi = new FileInfo(x);
                return fi.Name.Substring(0, fi.Name.Length - fi.Extension.Length);
            }).ToList();
            _config = config;
            _output = output;
            if (!notLoadUndistordInfo)
                ensureUndistortCacheLoaded();
        }
        public ImageReader(string[] fileNames, Configuration.Configuration config, Output.Abstractions.Output output = null, bool notLoadUndistordInfo = false)
        {
            _images = fileNames.Select(x => new FileInfo(x).FullName).ToList();
            _imagesIds = _images.Select(x =>
            {
                var fi = new FileInfo(x);
                return fi.Name.Substring(0, fi.Name.Length - fi.Extension.Length);
            }).ToList();
            _config = config;
            _output = output;
            if (!notLoadUndistordInfo)
                ensureUndistortCacheLoaded();
        }
        public ImageReader(string fileName, Configuration.Configuration config, Output.Abstractions.Output output = null, bool notLoadUndistordInfo = false) : this(new string[] { fileName }, config, output, notLoadUndistordInfo)
        { }


        public int Count { get { return _images.Count; } }

        public MinimalImageB3 GetRawImage(int index)
        {
            var image = loadImage(index);
            var bytes = DrawingHelper.MakeArray(image);
            var ret = new MinimalImageB3(index.ToString(), image.Width, image.Height, bytes);
            image.Dispose();
            if (_output != null && Plot)
            {
                _output.UpdateImage("Original", ret);
            }
            return ret;
        }
        public MinimalImageB GetImage(int index)
        {
            //if (_undisotrtCacheX == null || _undisotrtCacheY == null)
            //{
            //    Log.LogInfo("Reader initialized without distort info - distort will not be applied. (Can be caused by missing camera model or distort is explicitly disabled)");
            //}

            var image = loadImage(index);
            {
                var grayScale = DrawingHelper.MakeGrayscale3(image);
                image.Dispose();
                image = grayScale;
            }
            Byte[] bytes;
            int height = image.Height, width = image.Width;
            {
                var triBytes = DrawingHelper.MakeArray(image);
                if (_output != null && Plot)
                {
                    _output.UpdateImage("Original", new MinimalImageB3(index.ToString(), width, height, triBytes));
                }
                bytes = correctionsB(triBytes, ref width, ref height);
            }
            var ret = new MinimalImageB(index.ToString(), width, height, bytes);
            image.Dispose();
            if (_output != null && Plot)
            {
                _output.UpdateImage("Undistorted", ret);
            }
            return ret;
        }
        public MinimalImageF GetImageF(int index)
        {
            //if (_undisotrtCacheX == null || _undisotrtCacheY == null)
            //{
            //    Log.LogInfo("Reader initialized without distort info - distort will not be applied. (Can be caused by missing camera model or distort is explicitly disabled)");
            //}

            var image = loadImage(index);
            {
                var grayScale = DrawingHelper.MakeGrayscale3(image);
                image.Dispose();
                image = grayScale;
            }
            float[] bytes;
            int height = image.Height, width = image.Width;
            {
                var triBytes = DrawingHelper.MakeArray(image);
                if (_output != null && Plot)
                {
                    _output.UpdateImage("Original", new MinimalImageB3(index.ToString(), width, height, triBytes));
                }
                bytes = correctionsF(triBytes, ref width, ref height);
            }
            var ret = new MinimalImageF(index.ToString(), width, height, bytes);
            image.Dispose();
            if (_output != null && Plot)
            {
                _output.UpdateImage("Undistorted", ret);
            }
            return ret;
        }
        public ImageAndExposure<MinimalImageF, float> GetImageAndExposureF(int index)
        {
            var img = GetImageF(index);
            var imgWrap = new ImageAndExposure<MinimalImageF, float>() { Image = img };
            if (_config.Input.Timing?.Exposures != null && _config.Input.Timing?.Times != null)
            {
                var name = _imagesIds[index];
                imgWrap.ExposureTime = _config.Input.Timing.Exposures[name];
                imgWrap.TimeStamp = _config.Input.Timing.Times[name];
                if (_config.Input.GroundTruth.Truth.ContainsKey(imgWrap.TimeStamp))
                    imgWrap.GroundTruth = _config.Input.GroundTruth.Truth[imgWrap.TimeStamp];
            }
            else
            {
                imgWrap.ExposureTime = 1.0f / 30;
                imgWrap.TimeStamp = index * imgWrap.ExposureTime;
            }
            return imgWrap;
        }

        public LazyList<ImageAndExposure<MinimalImageF, float>> GetAllImagesAndExposuresFLazy(Action before, Action after)
        {
            LazyList<ImageAndExposure<MinimalImageF, float>> result = null;
                result = new LazyList<ImageAndExposure<MinimalImageF, float>>(_images.Count, (i) =>
                {
                    if (before != null)
                        before();
                    var r = GetImageAndExposureF(i);
                    if (after != null)
                        after();
                    return r;
                });
            return result;
        }

        public LazyList<ImageAndExposure<MinimalImageF, float>> GetAllImagesAndExposuresFLazy()
        {
            return new LazyList<ImageAndExposure<MinimalImageF, float>>(_images.Count, GetImageAndExposureF);
        }
    }
}
