﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace SharpSLAM.Input
{
    public class LazyCollectionEnumerator<T> : IEnumerator<T>, IEnumerator
    {
        private int _current = 0;
        private int _count;
        private Func<int, T> _getter;

        public LazyCollectionEnumerator(int count, Func<int, T> getter)
        {
            _count = count;
            _getter = getter;
        }

        public T Current => _getter(_current);

        object IEnumerator.Current => _getter(_current);

        public void Dispose()
        {
            Reset();
        }

        public bool MoveNext()
        {
            _current++;
            return _current < _count;
        }

        public void Reset()
        {
            _current = 0;
        }
    }

    public class LazyList<T> : IList<T>
    {
        private int _count;
        private Func<int, T> _getter;
        Dictionary<int, T> _cache;

        public LazyList(int count, Func<int,T> getter)
        {
            _cache = new Dictionary<int, T>();
            _count = count;
            _getter = (i)=> {
                if (_cache.ContainsKey(i))
                    return _cache[i];
                else
                    return getter(i);
            };
        }

        public T this[int index] { get {
                if (index < 0 || index >= _count)
                    throw new IndexOutOfRangeException();
                return _getter(index);
            }
            set => throw new NotSupportedException(); }

        public IEnumerator<T> GetEnumerator()
        {
            return new LazyCollectionEnumerator<T>(_count, _getter);
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return new LazyCollectionEnumerator<T>(_count, _getter);
        }

        public int Count => _count;

        public bool IsReadOnly => true;

        #region NotSupported
        public void Add(T item)
        {
            throw new NotSupportedException();
        }

        public void Clear()
        {
            throw new NotSupportedException();
        }

        public bool Contains(T item)
        {
            throw new NotSupportedException();
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            throw new NotSupportedException();
        }

        public int IndexOf(T item)
        {
            throw new NotSupportedException();
        }

        public void Insert(int index, T item)
        {
            throw new NotSupportedException();
        }

        public bool Remove(T item)
        {
            throw new NotSupportedException();
        }

        public void RemoveAt(int index)
        {
            throw new NotSupportedException();
        }
        #endregion
    }
}
