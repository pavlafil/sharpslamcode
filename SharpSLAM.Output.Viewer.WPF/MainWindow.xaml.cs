﻿using SharpSLAM.Shared;
using SharpSLAM.Output.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Windows.Media.Media3D;
using HelixToolkit.Wpf;
using Microsoft.Win32;

namespace SharpSLAM.Output.Viewer.WPF
{
    /// <summary>
    /// Interakční logika pro MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private HashSet<string> _activeCats { get; set; }
        private Dictionary<string, OutputContextMedium> _activeCatsMedium { get; set; }

        private Dictionary<string, Image> _imagesControls;//image controls per category
        private Dictionary<string, ModelVisual3D> _3DControlModels;//mesh controls per category
        private Grid _3DControl;//3d root control

        private bool _checkedCheckDownDir = true;
        private bool _firstEvent = true;

        private void draw2D<T, S>(string name, T image) where T : MinimalImage<S>
        {
            var pixelFormat = image.ChannelsCount == 1 ? PixelFormats.Gray8 : PixelFormats.Bgr24;

            Array data = image.Data;

            if (typeof(S) == typeof(float))
            {
                byte[] output = new byte[image.Width * image.Height * image.ChannelsCount];
                float[] maxColor = new float[image.ChannelsCount];
                for (int i = 0; i < data.Length; i++)
                {
                    if (i < image.ChannelsCount)
                    {
                        maxColor[i] = (float)data.GetValue(i);
                    }
                    else
                    {
                        var chI = i % image.ChannelsCount;
                        var val = (float)data.GetValue(i);
                        if (maxColor[chI] < val)
                        {
                            maxColor[chI] = val;
                        }
                    }
                }
#if (PAR)
                Parallel.For(0, image.Width * image.Height* image.ChannelsCount, (i) =>
#else
                for (int i = 0; i < image.Width * image.Height * image.ChannelsCount; i++)
#endif
                {
                    var chI = i % image.ChannelsCount;
                    output[i] = (byte)Math.Round((((float)data.GetValue(i)) / maxColor[chI]) * 255, 0, MidpointRounding.AwayFromZero);
                }
#if (PAR)
                                );
#endif
                data = output;


            }

            lock (_activeCats)
            {
                Dispatcher.InvokeAsync(() =>
                {
                    WriteableBitmap wb = new WriteableBitmap(
                                        image.Width,
                                        image.Height,
                                        50, 50, pixelFormat, null);

                    int stride = image.ChannelsCount * image.Width;
                    wb.WritePixels(new Int32Rect(0, 0, image.Width, image.Height),
                        data, stride, 0);

                    lock (_imagesControls) //not set while controls are mannipulated
                    {
                        if (_imagesControls.ContainsKey(name))
                        {
                            _imagesControls[name].Source = wb; // an Image class instance from XAML.
                        }
                    }
                }); //want run synced dispatcher only for control ownership - caller know when finished
            }
        }

        private void SavePoints(object sender, RoutedEventArgs e)
        {
            var sd = new SaveFileDialog();
            sd.AddExtension = true;
            sd.DefaultExt = ".obj";
            sd.FileName = "Model";
            var ok = sd.ShowDialog();
            if (ok.HasValue && ok.Value)
            {
                var name = sd.FileName;
                using (var s = new FileStream(name, FileMode.OpenOrCreate))
                {
                    var ex = new ObjExporter();
                    ex.MaterialsFile = System.IO.Path.GetFileNameWithoutExtension(name) + ".mtl";
                    foreach (var m in _3DControlModels.Values)
                    {
                        if (m.Content != null && m.Content is Model3DGroup g)
                        {
                            //ex.Export(g, s);
                           ex.Export(_3DControl.Children[0] as Viewport3D, s);
                        }
                    }
                }

            }
        }

        private void InitViewPort()
        {
            _3DControl = new Grid();
            // Declare scene objects.
            var viewPort = new Viewport3D();
            _3DControl.Children.Add(viewPort);


            var button = new Button() { Content = "Save points", Name = "SaveToFile" };
            _3DControl.ColumnDefinitions.Add(new ColumnDefinition());
            _3DControl.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(80, GridUnitType.Pixel) });
            button.Height = 20;
            Grid.SetColumn(button, 1);
            button.Click += SavePoints;
            _3DControl.Children.Add(button);






            ModelVisual3D myModelVisual3D = new ModelVisual3D();
            Model3DGroup myModel3DGroup = new Model3DGroup();

            // Defines the camera used to view the 3D object. In order to view the 3D object,
            // the camera must be positioned and pointed such that the object is within view 
            // of the camera.
            PerspectiveCamera myPCamera = new PerspectiveCamera();

            // Specify where in the 3D scene the camera is.
            myPCamera.Position = new Point3D(0.5, 1, 2);

            // Specify the direction that the camera is pointing.
            myPCamera.LookDirection = new Vector3D(-0.5, -1, -2);

            // Define camera's horizontal field of view in degrees.
            myPCamera.FieldOfView = 60;

            // Asign the camera to the viewport
            viewPort.Camera = myPCamera;





            // Define the lights cast in the scene. Without light, the 3D object cannot 
            // be seen. Note: to illuminate an object from additional directions, create 
            // additional lights.
            var ambLight = new AmbientLight(Colors.DarkGray);
            myModel3DGroup.Children.Add(ambLight);

            DirectionalLight myDirectionalLight = new DirectionalLight();
            myDirectionalLight.Color = Colors.White;
            myDirectionalLight.Direction = new Vector3D(0, 0, -1);
            myModel3DGroup.Children.Add(myDirectionalLight);






            ////Points
            //GeometryModel3D pointsGeometryModel = new GeometryModel3D();
            //MeshGeometry3D pointsMesh = new MeshGeometry3D();

            ////var points = new Point3D[] { new Point3D(-2, 0, -2), new Point3D(0, 0, -2), new Point3D(2, 0, -2) };
            ////var points = new Point3D[] { new Point3D(-1, 0, -1)};
            //var rnd = new Random(10);
            //var points = new List<Point3D>();
            //for (int i = 0; i < 2000; i++)
            //{
            //    points.Add(new Point3D(rnd.Next(100) - 50, rnd.Next(100) - 50, rnd.Next(100) - 50));
            //}
            //foreach (Point3D point in points)
            //{
            //    add3DPoint(point, pointsMesh, 0.25);
            //}
            //pointsGeometryModel.Geometry = pointsMesh;
            //pointsGeometryModel.Material = new DiffuseMaterial(new SolidColorBrush(Colors.Red) { Opacity = 0.5 });
            //myModel3DGroup.Children.Add(pointsGeometryModel);

            myModelVisual3D.Content = myModel3DGroup;
            viewPort.Children.Add(myModelVisual3D);






            //Pipes
            //for (int i = 1; i < points.Count; i++)
            //{
            //    var line = new PipeVisual3D();
            //    line.InnerDiameter = 0;
            //    line.Diameter = 0.02;
            //    line.Point1 = points[i - 1];
            //    line.Point2 = points[i];
            //    viewPort.Children.Add(line);
            //}

            _3DControl.Background = new SolidColorBrush(new Color() { R = 25, G = 25, B = 25, A = 255 });
            _3DControl.Background = new SolidColorBrush(new Color() { R = 0, G = 0, B = 0, A = 255 });

            //Events
            Point mousePos = default(Point);
            _3DControl.MouseDown += (s, e) =>
            {
                mousePos = e.GetPosition(null);
            };
            _3DControl.MouseWheel += (s, e) =>
            {
                var speed = 0.0005;
                if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift))
                {
                    speed = 0.005;
                }
                var zDir = myPCamera.LookDirection * speed * e.Delta;
                TranslateTransform3D translate = new TranslateTransform3D(zDir);
                myPCamera.Position = translate.Transform(myPCamera.Position);
            };
            _3DControl.PreviewMouseMove += (s, e) =>
            {
                if (e.LeftButton == MouseButtonState.Pressed || e.MiddleButton == MouseButtonState.Pressed)
                {
                    var diff = mousePos - e.GetPosition(null);
                    //Relative to viewport
                    diff.X /= _3DControl.ActualWidth;
                    diff.Y /= _3DControl.ActualHeight;

                    if (e.MiddleButton == MouseButtonState.Pressed)/*(Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl) || e.RightButton == MouseButtonState.Pressed)*/
                    {
                        var speed = 90;
                        if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift))
                        {
                            speed = 360;
                        }
                        speed = -speed;
                        RotateTransform3D rotateTransofrm = new RotateTransform3D();
                        AxisAngleRotation3D angleRotateTransform = new AxisAngleRotation3D();
                        rotateTransofrm.Rotation = angleRotateTransform;

                        var camRelUp = Vector3D.CrossProduct(Vector3D.CrossProduct(myPCamera.LookDirection, myPCamera.UpDirection), myPCamera.LookDirection);
                        //var camRelUp = myPCamera.UpDirection;

                        angleRotateTransform.Axis = Vector3D.CrossProduct(myPCamera.LookDirection, camRelUp);
                        angleRotateTransform.Angle = speed * diff.Y;
                        myPCamera.LookDirection = rotateTransofrm.Transform(myPCamera.LookDirection);
                        myPCamera.UpDirection = rotateTransofrm.Transform(myPCamera.UpDirection);


                        angleRotateTransform.Angle = speed * diff.X;
                        if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
                        {
                            angleRotateTransform.Axis = myPCamera.LookDirection;
                            myPCamera.LookDirection = rotateTransofrm.Transform(myPCamera.LookDirection);
                            myPCamera.UpDirection = rotateTransofrm.Transform(myPCamera.UpDirection);
                        }
                        else
                        {
                            angleRotateTransform.Axis = camRelUp;
                            myPCamera.LookDirection = rotateTransofrm.Transform(myPCamera.LookDirection);
                            //myPCamera.UpDirection = rotateTransofrm.Transform(myPCamera.UpDirection);//in uncomented camera is rotating relative to itself better to understand but cause roll drift after pitch & yaw (naturally occurs with pitch & yaw but it is annoying)
                        }

                    }
                    else
                    {
                        var speedX = 1;
                        var speedY = 0.5;
                        if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift))
                        {
                            speedX *= 10;
                            speedY *= 10;
                        }
                        var camRelUp = Vector3D.CrossProduct(Vector3D.CrossProduct(myPCamera.LookDirection, myPCamera.UpDirection), myPCamera.LookDirection);
                        var upDir = camRelUp * -speedY * diff.Y;
                        var rightDir = Vector3D.CrossProduct(myPCamera.LookDirection, myPCamera.UpDirection) * speedX * diff.X;
                        TranslateTransform3D translate = new TranslateTransform3D(upDir);
                        myPCamera.Position = translate.Transform(myPCamera.Position);
                        translate = new TranslateTransform3D(rightDir);
                        myPCamera.Position = translate.Transform(myPCamera.Position);
                    }
                    mousePos = e.GetPosition(null);
                }
            };
        }


        private void clear3DMesh(MeshGeometry3D pointsMeshGeometry3D)
        {
            pointsMeshGeometry3D.Positions.Clear();
            pointsMeshGeometry3D.TriangleIndices.Clear();
            pointsMeshGeometry3D.Normals.Clear();
            pointsMeshGeometry3D.TextureCoordinates.Clear();
        }
        private void add3DPoint(Point3D point, MeshGeometry3D pointsMesh, double sideSize = 0.025)
        {
            double x = point.X;
            double y = point.Y;
            double z = point.Z;

            double v = Math.Sqrt((sideSize * sideSize) - Math.Pow(sideSize / 2.0, 2));
            sideSize /= 2;
            v /= 2;

            var indexS = pointsMesh.Positions.Count;

            Point3D p0 = new Point3D(x, y, z + v);
            Point3D p1 = new Point3D(x - sideSize, y - v, z - v);
            Point3D p2 = new Point3D(x + sideSize, y - v, z - v);
            Point3D p3 = new Point3D(x, y + v, z - v);

            //Smooth
            pointsMesh.Positions.Add(p0);
            pointsMesh.Positions.Add(p1);
            pointsMesh.Positions.Add(p2);
            pointsMesh.Positions.Add(p3);

            pointsMesh.TriangleIndices.Add(indexS + 0);
            pointsMesh.TriangleIndices.Add(indexS + 2);
            pointsMesh.TriangleIndices.Add(indexS + 3);

            pointsMesh.TriangleIndices.Add(indexS + 0);
            pointsMesh.TriangleIndices.Add(indexS + 1);
            pointsMesh.TriangleIndices.Add(indexS + 2);

            pointsMesh.TriangleIndices.Add(indexS + 0);
            pointsMesh.TriangleIndices.Add(indexS + 3);
            pointsMesh.TriangleIndices.Add(indexS + 1);

            pointsMesh.TriangleIndices.Add(indexS + 1);
            pointsMesh.TriangleIndices.Add(indexS + 3);
            pointsMesh.TriangleIndices.Add(indexS + 2);


            //Not smooth
            //pointsMesh.Positions.Add(p0);
            //pointsMesh.Positions.Add(p2);
            //pointsMesh.Positions.Add(p3);
            //
            //pointsMesh.Positions.Add(p0);
            //pointsMesh.Positions.Add(p1);
            //pointsMesh.Positions.Add(p2);
            //
            //pointsMesh.Positions.Add(p0);
            //pointsMesh.Positions.Add(p3);
            //pointsMesh.Positions.Add(p1);
            //
            //pointsMesh.Positions.Add(p1);
            //pointsMesh.Positions.Add(p3);
            //pointsMesh.Positions.Add(p2);
        }

        private void addLine(Point3D start, Point3D end, bool renderAll, bool keyFrame, Visual3DCollection collection, Color? color = null)
        {
            var l = new PipeVisual3D();
            l.InnerDiameter = 0;
            l.Point1 = start;
            l.Point2 = end;

            if (color.HasValue)
            {
                l.Fill = new SolidColorBrush(color.Value);
            }

            if (renderAll)
            {
                l.Diameter = 0.001;
                if (!color.HasValue)
                {
                    if (keyFrame)
                        l.Fill = new SolidColorBrush(Colors.Orange);
                    else
                        l.Fill = new SolidColorBrush(Colors.Blue);
                }
            }
            else
            {
                l.Diameter = 0.005;
                if (!color.HasValue)
                    l.Fill = new SolidColorBrush(Colors.Red);
            }
            var ambLight = new AmbientLight(Colors.LightGray);
            collection.Add(l);
        }

        private Dictionary<string, List<CamPose>> _camLinesAllKHisotry = new Dictionary<string, List<CamPose>>();
        private Dictionary<string, List<CamPose>> _camLinesAllNKHisotry = new Dictionary<string, List<CamPose>>();
        private void draw3D(string name, CamPose pose)
        {
            if (_3DControlModels.ContainsKey(name))
            {
                lock (_activeCats)
                {
                    Dispatcher.InvokeAsync(() =>
                    {
                        ModelVisual3D model = _3DControlModels[name];

                        //if (model.Content == null)
                        //    model.Content = new Model3DGroup();

                        //Model3DGroup rootGroup = model.Content as Model3DGroup;

                        //rootGroup.ch
                        //if (model.Children != 2)
                        //{
                        //    model.Children.Clear();
                        //    model.
                        //    model.Children.Add(new Model3DGroup);
                        //    model.Children.Add(new Model3DGroup());
                        //}

                        var posesToRender = new List<CamPose>();

                        if (pose.RenderAll)
                        {
                            if (!_camLinesAllNKHisotry.ContainsKey(name))
                            {
                                _camLinesAllNKHisotry.Add(name, new List<CamPose>());
                            }
                            var lnesHistory = _camLinesAllNKHisotry[name];
                            var delay = 0;//not render last
                            if (pose.IsKeyFrame)
                            {
                                if (!_camLinesAllKHisotry.ContainsKey(name))
                                {
                                    _camLinesAllKHisotry.Add(name, new List<CamPose>());
                                }
                                lnesHistory = _camLinesAllKHisotry[name];
                                delay = 0;//render last too
                            }

                            if (model.Children.Count / 8 != lnesHistory.Count - (delay + 1))//if already rendered parts does not match render all else only increment render
                            {
                                if (lnesHistory.Count > 1)
                                {
                                    model.Children.Clear();
                                    posesToRender.AddRange(lnesHistory.Take(lnesHistory.Count - delay));
                                }
                            }
                            else
                            {
                                //render one missing
                                posesToRender.Add(lnesHistory[lnesHistory.Count - (delay + 1)]);
                            }
                        }
                        else
                        {
                            model.Children.Clear();
                            posesToRender.Add(pose);
                        }

                        foreach (var lpose in posesToRender)
                        {
                            var p0 = new Point3D(lpose.Points[0][0], lpose.Points[0][1], lpose.Points[0][2]);
                            var p1 = new Point3D(lpose.Points[1][0], lpose.Points[1][1], lpose.Points[1][2]);
                            var p2 = new Point3D(lpose.Points[2][0], lpose.Points[2][1], lpose.Points[2][2]);
                            var p3 = new Point3D(lpose.Points[3][0], lpose.Points[3][1], lpose.Points[3][2]);
                            var p4 = new Point3D(lpose.Points[4][0], lpose.Points[4][1], lpose.Points[4][2]);

                            var lPoseColor = new Color() { R = lpose.Color[0], G = lpose.Color[1], B = lpose.Color[2], A = 255 };

                            addLine(p0, p1, lpose.RenderAll, lpose.IsKeyFrame, model.Children, lPoseColor);
                            addLine(p0, p2, lpose.RenderAll, lpose.IsKeyFrame, model.Children, lPoseColor);
                            addLine(p0, p3, lpose.RenderAll, lpose.IsKeyFrame, model.Children, lPoseColor);
                            addLine(p0, p4, lpose.RenderAll, lpose.IsKeyFrame, model.Children, lPoseColor);

                            addLine(p1, p2, lpose.RenderAll, lpose.IsKeyFrame, model.Children, lPoseColor);
                            addLine(p2, p3, lpose.RenderAll, lpose.IsKeyFrame, model.Children, lPoseColor);
                            addLine(p3, p4, lpose.RenderAll, lpose.IsKeyFrame, model.Children, lPoseColor);
                            addLine(p4, p1, lpose.RenderAll, lpose.IsKeyFrame, model.Children, lPoseColor);
                        }
                    });
                }
            }
        }

        private void draw3D(string name, PointsCloud pointsCloud)
        {
            if (_3DControlModels.ContainsKey(name))
            {
                lock (_activeCats)
                {
                    Dispatcher.InvokeAsync(() =>
                    {
                        var model = _3DControlModels[name];
                        if (model.Content == null)
                            model.Content = new Model3DGroup();

                        Model3DGroup pointsModel3DGroup = model.Content as Model3DGroup;

                        var flattenOrdered = pointsCloud.Frames.Where(x => x.Value?.Points != null).SelectMany(x => x.Value.Points).Where(x => x != null && x.Color != null && x.Coords != null).OrderBy(x => x.Color, Comparer<double[]>.Create((x, y) =>
                        {
                            for (int it = 0; it < x.Length; it++)
                            {
                                if (x[it] != y[it])
                                {
                                    return (int)(x[it] - y[it]);
                                }
                            }
                            return 0;
                        })).ToList();

                        GeometryModel3D lastGeometry = null;
                        Color? lastColor = null;
                        PointsCloudPoint lastPoint = null;

                        pointsModel3DGroup.Children.Clear();
                        foreach (var p in flattenOrdered)
                        {
                            MeshGeometry3D mesh = null;
                            if (lastGeometry == null || !lastColor.HasValue || (lastPoint.Color[0] != p.Color[0] || lastPoint.Color[1] != p.Color[1] || lastPoint.Color[2] != p.Color[2]))
                            {
                                lastGeometry = new GeometryModel3D();
                                lastColor = new Color() { R = (byte)p.Color[0], G = (byte)p.Color[1], B = (byte)p.Color[2], A = 255 };
                                lastGeometry.Material = new DiffuseMaterial(new SolidColorBrush(lastColor.Value));
                                mesh = new MeshGeometry3D();
                                lastGeometry.Geometry = mesh;
                                pointsModel3DGroup.Children.Add(lastGeometry);
                            }
                            else
                            {
                                mesh = lastGeometry.Geometry as MeshGeometry3D;
                            }
                            add3DPoint(new Point3D(p.Coords[0], p.Coords[1], p.Coords[2]), mesh, 0.01);
                            lastPoint = p;
                        }
                    });
                }
            }
        }

        private void draw(string cat, OutputViewerContextImgeHolder media)
        {
            if (media.Medium is MinimalImageB imageB)
                draw2D<MinimalImageB, byte>(cat, imageB);
            else if (media.Medium is MinimalImageB3 imageB3)
                draw2D<MinimalImageB3, byte>(cat, imageB3);
            else if (media.Medium is MinimalImageF imageF)
                draw2D<MinimalImageF, float>(cat, imageF);
            else if (media.Medium is MinimalImageF3 imageF3)
                draw2D<MinimalImageF3, float>(cat, imageF3);
            else if (media.Medium is CamPose pose)
                draw3D(cat, pose);
            else if (media.Medium is PointsCloud pointsCloud)
                draw3D(cat, pointsCloud);
        }

        private void updated(string cat, OutputViewerContextImgeHolder media)
        {
            if (media.Medium is CamPose pose)
            {
                if (pose.RenderAll)
                {
                    if (pose.IsKeyFrame)
                    {
                        if (!_camLinesAllKHisotry.ContainsKey(cat))
                        {
                            _camLinesAllKHisotry.Add(cat, new List<CamPose>());
                        }
                        _camLinesAllKHisotry[cat].Add(pose);
                    }
                    else
                    {
                        if (!_camLinesAllNKHisotry.ContainsKey(cat))
                        {
                            _camLinesAllNKHisotry.Add(cat, new List<CamPose>());
                        }
                        _camLinesAllNKHisotry[cat].Add(pose);
                    }
                }
            }
        }

        private bool is3DType(string category)
        {
            return _activeCatsMedium[category].Is3D;
        }

        private void UpdateCatDependantControls()
        {
            var controls3DCount = _activeCats.Count(is3DType);

            var controlsCount = _activeCats.Count;
            if (controls3DCount != 0)
            {
                controlsCount -= controls3DCount - 1;
            }


            var d = (int)Math.Ceiling(Math.Sqrt(controlsCount));
            var rc = d == 0 ? 0 : d - ((d * d) - controlsCount) / d;

            imagesGrid.ColumnDefinitions.Clear();
            imagesGrid.RowDefinitions.Clear();

            for (int i = 0; i < d; i++)
            {
                imagesGrid.ColumnDefinitions.Add(new ColumnDefinition());
                if (i < rc)
                    imagesGrid.RowDefinitions.Add(new RowDefinition());
            }

            foreach (var i in _imagesControls.Keys.ToList())
            {
                if (!_activeCats.Contains(i))
                    _imagesControls.Remove(i);
            }
            foreach (var i in _3DControlModels.Keys.ToList())
            {
                if (!_activeCats.Contains(i))
                {
                    if (_3DControl != null && _3DControl.Children.Count > 0)
                    {
                        if (_3DControl.Children[0] is Viewport3D vp)
                        {
                            _3DControlModels[i].Children.Clear();//to detach all lines and can be reused potentionally
                            vp.Children.Remove(_3DControlModels[i]);
                        }
                    }
                    _3DControlModels.Remove(i);
                }
            }

            imagesGrid.Children.Clear();

            lock (_activeCats)
            {
                int i = 0;
                var alreadyDone3D = false;
                foreach (var category in _activeCats)
                //for (int i = 0; i < _activeCats.Count; i++)
                {
                    var r = i / d;
                    var c = i % d;
                    if (is3DType(category))
                    {
                        if (!_3DControlModels.ContainsKey(category))
                        {
                            if (_3DControl != null && _3DControl.Children.Count > 0)
                            {
                                if (_3DControl.Children[0] is Viewport3D vp)
                                {
                                    var model = new ModelVisual3D();
                                    _3DControlModels.Add(category, model); //create for later adding in draw (do not know what type of mesh will be used till draw)
                                    vp.Children.Add(model);
                                }
                            }

                        }
                        if (!alreadyDone3D)
                        {
                            imagesGrid.Children.Add(_3DControl);
                            Grid.SetRow(_3DControl, r);
                            Grid.SetColumn(_3DControl, c);
                            alreadyDone3D = true;
                            i++;
                        }
                    }
                    else
                    {
                        lock (_imagesControls)
                        {
                            Image img = null;
                            if (!_imagesControls.ContainsKey(category))
                            {
                                img = new Image();
                                _imagesControls.Add(category, img);
                            }
                            else
                            {
                                img = _imagesControls[category];
                            }
                            imagesGrid.Children.Add(img);
                            Grid.SetRow(img, r);
                            Grid.SetColumn(img, c);
                            i++;
                        }
                    }
                }
            }


            //check check box check if necessary from bottom to top
            Func<ItemsControl, bool> checkOneLevel = null;
            checkOneLevel = (item) =>
                    {
                        if (item == null) return true;

                        var allChecked = true;

                        foreach (TreeViewItem i in item.Items)
                        {
                            if (!checkOneLevel(i))
                            {
                                if (i.Header is CheckBox sch && i.Items.Count > 0)
                                {
                                    _checkedCheckDownDir = false;
                                    sch.IsChecked = false;
                                    _checkedCheckDownDir = true;
                                }
                            }
                            else
                            {
                                if (i.Header is CheckBox sch && i.Items.Count > 0)
                                {
                                    _checkedCheckDownDir = false;
                                    sch.IsChecked = true;
                                    _checkedCheckDownDir = true;
                                }
                            }

                            if (i.Header is CheckBox ch && ch.IsChecked.HasValue && !ch.IsChecked.Value)
                            {
                                allChecked = false;
                            }
                        }
                        return allChecked;
                    };

            checkOneLevel(menu);
        }

        private void ChangeActiveCats(string cat, bool state, OutputContextMedium medium)
        {
            lock (_activeCats)
            {
                if (state)
                {
                    _activeCats.Add(cat);
                    _activeCatsMedium.Add(cat, medium);
                }
                else
                {
                    _activeCats.Remove(cat);
                    _activeCatsMedium.Remove(cat);
                    _context.SetDirty(cat);//control will be removed so rendered image lost set dirty to be sure will be rerendered after activated agin
                }
            }
        }

        private void catUpdate(IDictionary<string, OutputViewerContextImgeHolder> categories)
        {
            var l = categories.Keys.ToList();
            l.Sort();
            var sl = l.Select(x => x.Split('.'));

            int deep = 0;
            menu.Items.Clear();
            ItemsControl currentItem = menu;
            string[] prew = null;
            foreach (var c in sl)
            {
                //go up
                while (deep > (c.Length - 1) || (deep > 0 && prew != null && prew[deep - 1] != c[deep - 1]))
                {
                    currentItem = currentItem.Parent as ItemsControl;
                    deep--;
                }
                //go down
                for (; deep < c.Length; deep++)
                {
                    var item = new TreeViewItem();

                    var ch = new CheckBox();
                    ch.Content = c[deep];
                    ch.IsChecked = _activeCats.Contains(string.Join(".", c));
                    ch.Checked += (s, e) =>
                    {
                        var lFirst = _firstEvent;
                        _firstEvent = false;
                        if (item.Items.Count != 0)
                        {
                            if (_checkedCheckDownDir)
                            {
                                foreach (var i in item.Items)
                                {
                                    if (i is TreeViewItem tvi)
                                    {
                                        if (tvi.Header is CheckBox ihch)
                                        {
                                            ihch.IsChecked = true;
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            var currentCat = string.Join(".", c);
                            ChangeActiveCats(currentCat, true, categories[currentCat].Medium);
                        }

                        if (lFirst)
                        {
                            _firstEvent = true;
                            UpdateCatDependantControls();
                            _context.SetActiveCategires();//draw all needed
                        }
                    };
                    ch.Unchecked += (s, e) =>
                    {
                        var lFirst = _firstEvent;
                        _firstEvent = false;
                        if (item.Items.Count != 0)
                        {
                            if (_checkedCheckDownDir)
                            {
                                foreach (var i in item.Items)
                                {
                                    if (i is TreeViewItem tvi)
                                    {
                                        if (tvi.Header is CheckBox ihch)
                                        {
                                            ihch.IsChecked = false;
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            var currentCat = string.Join(".", c);
                            ChangeActiveCats(string.Join(".", c), false, categories[currentCat].Medium);
                        }
                        if (lFirst)
                        {
                            _firstEvent = true;
                            UpdateCatDependantControls();
                        }
                    };

                    item.Header = c[deep];
                    item.Header = ch;

                    item.IsExpanded = true;
                    currentItem.Items.Add(item);
                    currentItem = item;
                }
                prew = c;
            }

            menu.Items.Refresh();
        }


        private void handleAsync(string cat, OutputContextMedium medium, Action<string, OutputContextMedium> callBack)
        {
            Dispatcher.InvokeAsync(() =>
            {
                callBack(cat, medium);
            });
        }


        private OutputViewer _context;
        private Timer _renderTimer;
        public void Free()
        {
            _context.DeInitializeViewer();
            _renderTimer?.Dispose();
            _renderTimer = null;
        }

        public MainWindow(OutputViewer context)
        {
            _activeCats = new HashSet<string>();
            _activeCatsMedium = new Dictionary<string, OutputContextMedium>();
            _imagesControls = new Dictionary<string, Image>();
            InitViewPort();
            _3DControlModels = new Dictionary<string, ModelVisual3D>();
            InitializeComponent();
            lock (context)
            {
                _context = context;
                context.InitializeViewer(handleAsync, draw, updated, catUpdate);
                context.SetActiveCategires(_activeCats);

                _renderTimer = new Timer((s) =>
                {
                    lock (_activeCats)
                    {
                        _context.SetActiveCategires();
                    }
                }, null, 0, 1000 / 1);

                Monitor.Pulse(context);
            }

            PlayPause.IsChecked = true;

        }

        private void ToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            if (_context != null && _context.Configuration != null)
                _context.Configuration.Core.PausingMode = Configuration.PausingMode.NoPause;
        }

        private void ToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            if (_context != null && _context.Configuration != null)
                _context.Configuration.Core.PausingMode = Configuration.PausingMode.OnEachFrame | Configuration.PausingMode.Debugging;
        }
    }
}
