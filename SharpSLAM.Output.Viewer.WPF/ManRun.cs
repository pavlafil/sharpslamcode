﻿using SharpSLAM.Output.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SharpSLAM.Output.Viewer.WPF
{
    public static class ManRun
    {
        internal static void Run(Abstractions.Output context)
        {
            Thread t = new Thread(ThreadStart);
            t.SetApartmentState(ApartmentState.STA);
            t.Start(context);
        }

        private static void ThreadStart(object parametr)
        {
            if (parametr is OutputViewer context)
            {
                App app = new App();
                app.ShutdownMode = System.Windows.ShutdownMode.OnExplicitShutdown;
                var mv = new MainWindow(context);

                EventHandler closeHandler = null;
                closeHandler = (s, e) => {
                    mv.Free();
                    var reopen = context.ViewerClosed();
                    if (reopen)
                    {
                        mv = new MainWindow(context);
                        mv.Closed += closeHandler;
                        mv.Show();
                    }
                    else
                    {
                        app.Shutdown();
                    }
                };
                mv.Closed += closeHandler;
                app.Run(mv);
            }
        }
    }
}
