﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SharpSLAM.Output.Viewer.WPF
{
    public class OutputViewerWpf : SharpSLAM.Output.Abstractions.OutputViewer
    {
        public OutputViewerWpf()
        {
            ViewerClosedEvent += () =>
            {
                Console.WriteLine("========================");
                Console.WriteLine("Viewer was closed press ENTER to reopen, press any key to continue without viewer");
                Console.WriteLine("========================");

                var k = Console.ReadKey();

                if (k.Key == ConsoleKey.Enter)
                {
                    return true;
                }
                return false;
            };

            //Run WPF viewer
            SharpSLAM.Output.Viewer.WPF.ManRun.Run(this);

            //Wait for viewer
            lock (this)
            {
                Monitor.Wait(this);
            }
        }
    }
}
