#include "SE3.hpp"
#include "Mat.cpp"//to fully define mat33
#include "NumType.hpp"
#include "UnmanagedClass.cpp"
using namespace SharpSLAM::Core::Unmanaged;

SE3::SE3()
{
	_se3 = new Types::SE3();
}

SE3::SE3(Types::SE3* data)
{
	_se3 = data;
}

SE3::SE3(double w, double x, double y, double z, double tx, double ty, double tz)
{
	_se3 = new Types::SE3(Sophus::Quaterniond(w, x, y, z), Types::Vec3(tx, ty, tz));
}

SE3::~SE3()
{
	this->!SE3();
}

SE3::!SE3()
{
	delete _se3;
}

void SE3::TranslationSetZero()
{
	_se3->translation().setZero();
}

Mat33^ SE3::RotationMatrix()
{
	return gcnew Mat33(new Types::Mat33(_se3->rotationMatrix()));
}

Mat33f^ SE3::RotationMatrixF()
{
	return gcnew Mat33f(new Types::Mat33f(_se3->rotationMatrix().cast<float>()));
}

array<float>^ SE3::TranslationF()
{
	auto arr = gcnew array<float>(3);
	auto transl = _se3->translation().cast<float>();
	for (int i = 0; i < 3; i++)
	{
		arr[i] = transl[i];
	}
	return arr;
}

array<double>^ SE3::TranslationD()
{
	auto arr = gcnew array<double>(3);
	auto transl = _se3->translation();
	for (int i = 0; i < 3; i++)
	{
		arr[i] = transl[i];
	}
	return arr;
}

void SE3::TranslationMul(double fac)
{
	_se3->translation() *= fac;
}

void SE3::TranslationDivide(double fac)
{
	_se3->translation() /= fac;
}

System::String^ SE3::PrintLog()
{
	std::ostringstream stream;
	UnmanagedClass::PrintSe3LogTranspone(_se3, &stream);
	return gcnew System::String(stream.str().c_str());
}

SE3^ SE3::Inverse()
{
	return gcnew SE3(new Types::SE3(_se3->inverse()));
}

SE3^ SE3::Clone()
{
	return gcnew SE3(new Types::SE3(*_se3));
}

SE3^ SE3::Exp(array<double>^ in)
{
	if (in->Length < 6)
		throw gcnew System::Exception("Input vector has to have at leas 6 items!");

	Types::Vec6 vec6;

	for (int i = 0; i < 6; i++)
	{
		vec6[i] = in[i];
	}
	return gcnew SE3(new Types::SE3(Types::SE3::exp(vec6)));
}

SE3^ SE3::operator *(SE3^ a, SE3^ b)
{
	return gcnew SE3(new Types::SE3((*a->_se3) * (*b->_se3)));
}

array<double>^ SE3::operator *(SE3^ a, array<double>^ b)
{
	if (b->Length != 3)
		throw gcnew System::Exception("Input vector has to have 3 items!");
	Types::Vec3 vec;
	for (int i = 0; i < 3; i++)
	{
		vec[i] = b[i];
	}
	auto ret = gcnew array<double>(3);
	auto res = ((*a->_se3) * vec);
	for (int i = 0; i < 3; i++)
	{
		ret[i] = res[i];
	}
	return ret;
}
array<double>^ SE3::Log()
{
	auto log = _se3->log();
	auto oLog = gcnew array<double>(6);
	for (int i = 0; i < 6; i++)
	{
		oLog[i] = log[i];
	}
	return oLog;
}

array<double>^ SE3::Quaternion()
{
	auto quaternion = _se3->unit_quaternion();
	auto oQuaternion = gcnew array<double>(4);
	oQuaternion[0] = quaternion.w();
	oQuaternion[1] = quaternion.x();
	oQuaternion[2] = quaternion.y();
	oQuaternion[3] = quaternion.z();
	return oQuaternion;
}