#include "EFResidualU.h"

namespace SharpSLAM
{
	namespace Core
	{
		namespace Unmanaged
		{
			void EFResidualU::FixLinearizationF(int patternNum, array<float>^ efAddHTdeltaFI,
				array<float>^ Jpdxi0, array<float>^ Jpdxi1,
				array<float>^ Jpdc0, array<float>^ Jpdc1,
				array<float>^ Jpdd, array<float>^ efCdeltaF, 
				array<float>^ JResF, array<float>^ resToZeroF,
				array<float>^ JJIdx0, array<float>^ JJIdx1, array<float>^ JJabF0, array<float>^ JJabF1,
				float pointDeltaF)
			{
				pin_ptr<float> dpPointer = &efAddHTdeltaFI[0];
				pin_ptr<float> Jpdxi0Pointer = &Jpdxi0[0];
				pin_ptr<float> Jpdxi1Pointer = &Jpdxi1[0];
				pin_ptr<float> Jpdc0Pointer = &Jpdc0[0];
				pin_ptr<float> Jpdc1Pointer = &Jpdc1[0];
				pin_ptr<float> JpddPointer = &Jpdd[0];
				pin_ptr<float> efCdeltaFPointer = &efCdeltaF[0];
				pin_ptr<float> JResFPointer = &JResF[0];
				pin_ptr<float> resToZeroFPointer = &resToZeroF[0];
				pin_ptr<float> JJIdx0Pointer = &JJIdx0[0];
				pin_ptr<float> JJIdx1Pointer = &JJIdx1[0];
				pin_ptr<float> JJabF0Pointer = &JJabF0[0];
				pin_ptr<float> JJabF1Pointer = &JJabF1[0];

				Types::Vec8f dp(dpPointer);
				Types::Vec6f Jpdxi0X(Jpdxi0Pointer);
				Types::Vec6f Jpdxi1X(Jpdxi1Pointer);
				Types::VecCf Jpdc0X(Jpdc0Pointer);
				Types::VecCf Jpdc1X(Jpdc1Pointer);
				Types::Vec2f JpddX(JpddPointer);
				Types::VecCf efCdeltaFX(efCdeltaFPointer);
				Types::VecNRf JResFX(JResFPointer);
				Types::VecNRf resToZeroFX(resToZeroFPointer);
				Types::VecNRf JJIdx0X(JJIdx0Pointer);
				Types::VecNRf JJIdx1X(JJIdx1Pointer);
				Types::VecNRf JJabF0X(JJabF0Pointer);
				Types::VecNRf JJabF1X(JJabF1Pointer);

				UnmanagedClass::EFResidualUFixLinearizationF(dp,Jpdxi0X,Jpdxi1X,Jpdc0X,Jpdc1X,JpddX,efCdeltaFX,JResFX,resToZeroFX,JJIdx0X,JJIdx1X,JJabF0X,JJabF1X,pointDeltaF,patternNum);
			}
		}
	}
}