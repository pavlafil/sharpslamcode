#pragma once
#include "NumType.hpp"
#include "MatrixAccumulators.h"

namespace SharpSLAM
{
	namespace Core
	{
		namespace Unmanaged
		{
			class UnmanagedClass
			{
			public:
				static void PrintSe3LogTranspone(Types::SE3* se3, std::ostringstream* stream)
				{
					(*stream) << se3->log().transpose();
				}

				static Types::Vec3f* Se3Log(Types::SE3* se3)
				{
					return new Types::Vec3f(se3->log().head<3>().cast<float>());
				}

				static void SetZero(Types::VecNRf* vec)
				{
					vec->setZero();
				}

				static Types::Mat88f* SubMat88f(Types::Mat88f* a, Types::Mat88f* b)
				{
					return new Types::Mat88f((*a) - (*b));
				}

				static void FixAffineAndSE3Exp(bool fixAffine, Eigen::DiagonalMatrix<float, 8>* wM, Types::Mat88f* Hl, Types::Vec8f* bl, Types::SE3* refToNew_current, Types::Vec8f** out, Types::SE3** refToNew_new_out)
				{
					Types::Vec8f* inc = new Types::Vec8f;
					if (fixAffine)
					{

						inc->head<6>() = -(wM->toDenseMatrix().topLeftCorner<6, 6>() * (Hl->topLeftCorner<6, 6>().ldlt().solve(bl->head<6>())));
						inc->tail<2>().setZero();
					}
					else
						(*inc) = -((*wM) * (Hl->ldlt().solve(*bl)));    //=-H^-1 * b.


					(*refToNew_new_out) = new Types::SE3(Types::SE3::exp(inc->head<6>().cast<double>()) * (*refToNew_current));
					(*out) = inc;
				}


				static void CalcLEnergyIterUnmanaged(int patternNum, float Jp_delta_x_1, float Jp_delta_y_1, float dp6, float dp7,
					float* JIdx0, float* JIdx1, float * JabF0, float * JabF1, float * resToZeroF, Accumulator11* E)
				{
					__m128 Jp_delta_x = _mm_set1_ps(Jp_delta_x_1);
					__m128 Jp_delta_y = _mm_set1_ps(Jp_delta_y_1);
					__m128 delta_a = _mm_set1_ps((float)(dp6));
					__m128 delta_b = _mm_set1_ps((float)(dp7));

					for (int i = 0; i + 3 < patternNum; i += 4)
					{
						//// PATTERN: E = (2*res_toZeroF + J*delta) * J*delta.
						__m128 Jdelta = _mm_mul_ps(_mm_load_ps(JIdx0 + i), Jp_delta_x);
						Jdelta = _mm_add_ps(Jdelta, _mm_mul_ps(_mm_load_ps(JIdx1 + i), Jp_delta_y));
						Jdelta = _mm_add_ps(Jdelta, _mm_mul_ps(_mm_load_ps(JabF0 + i), delta_a));
						Jdelta = _mm_add_ps(Jdelta, _mm_mul_ps(_mm_load_ps(JabF1 + i), delta_b));


						__m128 r0 = _mm_load_ps(resToZeroF + i);
						r0 = _mm_add_ps(r0, r0);
						r0 = _mm_add_ps(r0, Jdelta);
						Jdelta = _mm_mul_ps(Jdelta, r0);
						E->updateSSENoShift(Jdelta);
					}
				}

				static void AccTopHessAddPointIter(float fJp_delta_x, float fJp_delta_y, float dp6, float dp7, const float * ResToZeroFI,
					const float * JIdx0I, const float * JIdx1I, const float * JabF0I, const float * JabF1I,
					float * resApproxI)
				{
					//compute Jp*delta
					__m128 Jp_delta_x = _mm_set1_ps(fJp_delta_x);
					__m128 Jp_delta_y = _mm_set1_ps(fJp_delta_y);
					__m128 delta_a = _mm_set1_ps(dp6);
					__m128 delta_b = _mm_set1_ps(dp7);

					// PATTERN: rtz = resF - [JI*Jp Ja]*delta.
					__m128 rtz = _mm_load_ps(ResToZeroFI);
					rtz = _mm_add_ps(rtz, _mm_mul_ps(_mm_load_ps(JIdx0I), Jp_delta_x));
					rtz = _mm_add_ps(rtz, _mm_mul_ps(_mm_load_ps(JIdx1I), Jp_delta_y));
					rtz = _mm_add_ps(rtz, _mm_mul_ps(_mm_load_ps(JabF0I), delta_a));
					rtz = _mm_add_ps(rtz, _mm_mul_ps(_mm_load_ps(JabF1I), delta_b));
					_mm_store_ps(resApproxI, rtz);
				}

				static void EFResidualUFixLinearizationF(
					Types::Vec8f& dp,
					Types::Vec6f& Jpdxi0X,Types::Vec6f& Jpdxi1X,
					Types::VecCf& Jpdc0X,Types::VecCf& Jpdc1X,
					Types::Vec2f& JpddX,
					Types::VecCf& efCdeltaFX,
					Types::VecNRf& JResFX,
					Types::VecNRf& resToZeroFX,
					Types::VecNRf& JJIdx0X,Types::VecNRf& JJIdx1X,
					Types::VecNRf& JJabF0X,Types::VecNRf& JJabF1X,
					float& pointDeltaF, int& patternNum
					)
				{
					// compute Jp*delta
					__m128 Jp_delta_x = _mm_set1_ps(Jpdxi0X.dot(dp.head<6>())
						+ Jpdc0X.dot(efCdeltaFX)
						+ JpddX[0] * pointDeltaF);
					__m128 Jp_delta_y = _mm_set1_ps(Jpdxi1X.dot(dp.head<6>())
						+ Jpdc1X.dot(efCdeltaFX)
						+ JpddX[1] * pointDeltaF);
					__m128 delta_a = _mm_set1_ps((float)(dp[6]));
					__m128 delta_b = _mm_set1_ps((float)(dp[7]));

					for (int i = 0; i < patternNum; i += 4)
					{
						// PATTERN: rtz = resF - [JI*Jp Ja]*delta.
						__m128 rtz = _mm_load_ps(((float*)&JResFX) + i);
						rtz = _mm_sub_ps(rtz, _mm_mul_ps(_mm_load_ps(((float*)(&JJIdx0X)) + i), Jp_delta_x));
						rtz = _mm_sub_ps(rtz, _mm_mul_ps(_mm_load_ps(((float*)(&JJIdx1X)) + i), Jp_delta_y));
						rtz = _mm_sub_ps(rtz, _mm_mul_ps(_mm_load_ps(((float*)(&JJabF0X)) + i), delta_a));
						rtz = _mm_sub_ps(rtz, _mm_mul_ps(_mm_load_ps(((float*)(&JJabF1X)) + i), delta_b));
						_mm_store_ps(((float*)&resToZeroFX) + i, rtz);
					}
				}
				static Types::Mat99f CoarseTrackerUCalcGSSSE(float fx_lvl, float fy_lvl, double lastRef_aff_g2l_b, 
					double vecExposure, float* buf_warped_dx_P,float* buf_warped_dy_P, float* buf_warped_u_P, float* buf_warped_v_P,float* buf_warped_idepth_P, float* buf_warped_refColor_P,
				float* buf_warped_residual_P, float* buf_warped_weight_P,int n)
				{
					Accumulator9 acc;

					acc.initialize();

					__m128 fxl = _mm_set1_ps(fx_lvl);
					__m128 fyl = _mm_set1_ps(fy_lvl);
					__m128 b0 = _mm_set1_ps(lastRef_aff_g2l_b);
					__m128 a = _mm_set1_ps(vecExposure);
					
					__m128 one = _mm_set1_ps(1);
					__m128 minusOne = _mm_set1_ps(-1);
					__m128 zero = _mm_set1_ps(0);

					assert(n % 4 == 0);

					for (int i = 0; i < n; i += 4)
					{
						__m128 dx = _mm_mul_ps(_mm_load_ps(buf_warped_dx_P + i), fxl);
						__m128 dy = _mm_mul_ps(_mm_load_ps(buf_warped_dy_P + i), fyl);
						__m128 u = _mm_load_ps(buf_warped_u_P + i);
						__m128 v = _mm_load_ps(buf_warped_v_P + i);
						__m128 id = _mm_load_ps(buf_warped_idepth_P + i);
					
						acc.updateSSE_eighted(
							_mm_mul_ps(id, dx),
							_mm_mul_ps(id, dy),
							_mm_sub_ps(zero, _mm_mul_ps(id, _mm_add_ps(_mm_mul_ps(u, dx), _mm_mul_ps(v, dy)))),
							_mm_sub_ps(zero, _mm_add_ps(
								_mm_mul_ps(_mm_mul_ps(u, v), dx),
								_mm_mul_ps(dy, _mm_add_ps(one, _mm_mul_ps(v, v))))),
							_mm_add_ps(
								_mm_mul_ps(_mm_mul_ps(u, v), dy),
								_mm_mul_ps(dx, _mm_add_ps(one, _mm_mul_ps(u, u)))),
							_mm_sub_ps(_mm_mul_ps(u, dy), _mm_mul_ps(v, dx)),
							_mm_mul_ps(a, _mm_sub_ps(b0, _mm_load_ps(buf_warped_refColor_P + i))),
							minusOne,
							_mm_load_ps(buf_warped_residual_P + i),
							_mm_load_ps(buf_warped_weight_P + i));
					}
					acc.finish();
					return acc.H;
				}
			};
		}
	}
}