#include "EnergyFunctionalU.h"
#include "UnmanagedClass.cpp"


namespace SharpSLAM
{
	namespace Core
	{
		namespace Unmanaged
		{
			EnergyFunctionalU::EnergyFunctionalU()
			{
			}

			void EnergyFunctionalU::CalcLEnergyInit() {
				_E = new Accumulator11();
				_E->initialize();
			}
			void EnergyFunctionalU::CalcLEnergyIter(float Jp_delta_x_1, float Jp_delta_y_1, float dp6, float dp7, int patternNum,
				array<array<float>^>^ JIdx, array<array<float>^>^ JabF, array<float>^ resToZeroF) {

				auto JIdx0U = new float[patternNum / 4];
				auto JIdx1U = new float[patternNum / 4];
				auto JabF0U = new float[patternNum / 4];
				auto JabF1U = new float[patternNum / 4];
				auto ResToZeroFU = new float[patternNum / 4];
				for (int i = 0; i < patternNum / 4; i++)
				{
					JIdx0U[i] = JIdx[0][i];
					JIdx1U[i] = JIdx[1][i];
					JabF0U[i] = JabF[0][i];
					JabF1U[i] = JabF[1][i];
					ResToZeroFU[i] = resToZeroF[i];
				}

				UnmanagedClass::CalcLEnergyIterUnmanaged(patternNum, Jp_delta_x_1, Jp_delta_y_1, dp6, dp7, JIdx0U, JIdx1U, JabF0U, JabF1U, ResToZeroFU, _E);

				delete[] JIdx0U;
				delete[] JIdx1U;
				delete[] JabF0U;
				delete[] JabF1U;
				delete[] ResToZeroFU;
			}

			void EnergyFunctionalU::CalcLEnergyUpdateSingleNoShift(float value) {
				_E->updateSingleNoShift(value);
			}
			void EnergyFunctionalU::CalcLEnergyUpdateSingle(float value) {
				_E->updateSingle(value);
			}
			double EnergyFunctionalU::CalcLEnergyFinish() {
				_E->finish();

				float result = _E->A;
				delete _E;

				return result;
			}

			void EnergyFunctionalU::Orthogonalize(System::Collections::Generic::List<array<double>^>^ ns, MatXX^ H, array<double>^ b, double setting_solverModeDelta)
			{
				Types::VecX bX(b->Length);
				for (int i = 0; i < b->Length; i++)
				{
					bX[i] = b[i];
				}

				// make Nullspaces matrix
				Types::MatXX N(ns->default[0].Length, ns->Count);
				for (int i = 0; i < ns->Count; i++)
				{
					auto arr = ns->default[i];
					Types::VecX arrX(arr->Length);
					for (int ii = 0; ii < arr->Length; ii++)
					{
						arrX[ii] = arr[ii];
					}
					N.col(i) = arrX.normalized();
				}



				// compute Npi := N * (N' * N)^-1 = pseudo inverse of N.
				Eigen::JacobiSVD<Types::MatXX> svdNN(N, Eigen::ComputeThinU | Eigen::ComputeThinV);

				Types::VecX SNN = svdNN.singularValues();
				double minSv = 1e10, maxSv = 0;
				for (int i = 0; i < SNN.size(); i++)
				{
					if (SNN[i] < minSv) minSv = SNN[i];
					if (SNN[i] > maxSv) maxSv = SNN[i];
				}
				for (int i = 0; i < SNN.size(); i++)
				{
					if (SNN[i] > setting_solverModeDelta * maxSv) SNN[i] = 1.0 / SNN[i]; else SNN[i] = 0;
				}

				Types::MatXX Npi = svdNN.matrixU() * SNN.asDiagonal() * svdNN.matrixV().transpose();   // [dim] x 9.

				
				Types::MatXX NNpiT = N * Npi.transpose();  // [dim] x [dim].
				Types::MatXX NNpiTS = 0.5 * (NNpiT + NNpiT.transpose());   // = N * (N' * N)^-1 * N'.

				if (b != nullptr) bX -= NNpiTS * bX;
				if (H != nullptr) (*H->_mat) -= NNpiTS * (*H->_mat) * NNpiTS;

				for (int i = 0; i < b->Length; i++)
				{
					b[i] = bX[i];
				}
			}
			array<double>^ EnergyFunctionalU::SolveSystemFGetX(bool solverTypeSVD, bool solverSVDCut7, MatXX^ HFinal_top, array<double>^ bFinal_top, double setting_solverModeDelta)
			{
				Types::VecX bFinal_topX(bFinal_top->Length);
				for (int i = 0; i < bFinal_top->Length; i++)
				{
					bFinal_topX[i] = bFinal_top[i];
				}

				Types::VecX x;
				if (solverTypeSVD)
				{
					Types::VecX SVecI = (*HFinal_top->_mat).diagonal().cwiseSqrt().cwiseInverse();
					Types::MatXX HFinalScaled = SVecI.asDiagonal() * (*HFinal_top->_mat) * SVecI.asDiagonal();
					Types::VecX bFinalScaled = SVecI.asDiagonal() * bFinal_topX;
					Eigen::JacobiSVD<Types::MatXX> svd(HFinalScaled, Eigen::ComputeThinU | Eigen::ComputeThinV);

					Types::VecX S = svd.singularValues();
					double minSv = 1e10, maxSv = 0;
					for (int i = 0; i < S.size(); i++)
					{
						if (S[i] < minSv) minSv = S[i];
						if (S[i] > maxSv) maxSv = S[i];
					}

					Types::VecX Ub = svd.matrixU().transpose() * bFinalScaled;
					int setZero = 0;
					for (int i = 0; i < Ub.size(); i++)
					{
						if (S[i] < setting_solverModeDelta * maxSv)
						{
							Ub[i] = 0; setZero++;
						}

						if (solverSVDCut7 && (i >= Ub.size() - 7))
						{
							Ub[i] = 0; setZero++;
						}

						else Ub[i] /= S[i];
					}
					x = SVecI.asDiagonal() * svd.matrixV() * Ub;

				}
				else
				{
					Types::VecX SVecI = ((*HFinal_top->_mat).diagonal() + Types::VecX::Constant((*HFinal_top->_mat).cols(), 10)).cwiseSqrt().cwiseInverse();
					Types::MatXX HFinalScaled = SVecI.asDiagonal() * (*HFinal_top->_mat) * SVecI.asDiagonal();
					x = SVecI.asDiagonal() * HFinalScaled.ldlt().solve(SVecI.asDiagonal() * bFinal_topX);//  SVec.asDiagonal() * svd.matrixV() * Ub;
				}

				auto ret = gcnew array<double>(x.size());
				for (int i = 0; i < x.size(); i++)
				{
					ret[i] = x[i];
				}
				return ret;
			}

			void EnergyFunctionalU::MarginalizeFrame(int fhIdx, int ndim, int odim, int framesCount, int nFrames, array<double>^% bM, MatXX^ HM,
				array<double>^ fhPrior, array<double>^ fhDeltaPrior)
			{
				pin_ptr<double> bMPointer = &bM[0];
				pin_ptr<double> fhPriorPointer = &fhPrior[0];
				pin_ptr<double> fhDeltaPriorPointer = &fhDeltaPrior[0];
				Types::VecX bMX(bM->Length);
				for (int ii = 0; ii < bM->Length; ii++)
				{
					bMX[ii] = bM[ii];
				}

				Types::Vec8 fhPriorX(fhPriorPointer);
				Types::Vec8 fhDeltaPriorX(fhDeltaPriorPointer);

				////	VecX eigenvaluesPre = HM.eigenvalues().real();
				////	std::sort(eigenvaluesPre.data(), eigenvaluesPre.data()+eigenvaluesPre.size());
				////



				if (fhIdx != framesCount - 1)
				{
					int io = fhIdx * 8 + CPARS;   // index of frame to move to end
					int ntail = 8 * (nFrames - fhIdx - 1);
					assert((io + 8 + ntail) == nFrames * 8 + CPARS);

					Types::Vec8 bTmp = bMX.segment < 8 >(io);
					Types::VecX tailTMP = bMX.tail(ntail);
					bMX.segment(io, ntail) = tailTMP;
					bMX.tail < 8 >() = bTmp;

					Types::MatXX HtmpCol = (*HM->_mat).block(0, io, odim, 8);
					Types::MatXX rightColsTmp = (*HM->_mat).rightCols(ntail);
					(*HM->_mat).block(0, io, odim, ntail) = rightColsTmp;
					(*HM->_mat).rightCols(8) = HtmpCol;

					Types::MatXX HtmpRow = (*HM->_mat).block(io, 0, 8, odim);
					Types::MatXX botRowsTmp = (*HM->_mat).bottomRows(ntail);
					(*HM->_mat).block(io, 0, ntail, odim) = botRowsTmp;
					(*HM->_mat).bottomRows(8) = HtmpRow;
				}


				//	// marginalize. First add prior here, instead of to active.
				(*HM->_mat).bottomRightCorner <8, 8>().diagonal() += fhPriorX;
				bMX.tail < 8 >() += fhPriorX.cwiseProduct(fhDeltaPriorX);



				//	std::cout << std::setprecision(16) << "HMPre:\n" << HM << "\n\n";


				Types::VecX SVec = ((*HM->_mat).diagonal().cwiseAbs() + Types::VecX::Constant((*HM->_mat).cols(), 10)).cwiseSqrt();
				Types::VecX SVecI = SVec.cwiseInverse();


				//	std::cout << std::setprecision(16) << "SVec: " << SVec.transpose() << "\n\n";
				//	std::cout << std::setprecision(16) << "SVecI: " << SVecI.transpose() << "\n\n";

				// scale!
				Types::MatXX HMScaled = SVecI.asDiagonal() * (*HM->_mat) * SVecI.asDiagonal();
				Types::VecX bMScaled = SVecI.asDiagonal() * bMX;

				// invert bottom part!
				Types::Mat88 hpi = HMScaled.bottomRightCorner<8,8>();
				hpi = 0.5f * (hpi + hpi);
				hpi = hpi.inverse();
				hpi = 0.5f * (hpi + hpi);

				// schur-complement!
				Types::MatXX bli = HMScaled.bottomLeftCorner(8, ndim).transpose() * hpi;
				HMScaled.topLeftCorner(ndim, ndim).noalias() -= bli * HMScaled.bottomLeftCorner(8, ndim);
				bMScaled.head(ndim).noalias() -= bli * bMScaled.tail < 8 >();

				//unscale!
				HMScaled = SVec.asDiagonal() * HMScaled * SVec.asDiagonal();
				bMScaled = SVec.asDiagonal() * bMScaled;

				// set.
				(*HM->_mat) = 0.5 * (HMScaled.topLeftCorner(ndim, ndim) + HMScaled.topLeftCorner(ndim, ndim).transpose());
				bMX = bMScaled.head(ndim);

				////array<double>::Resize(bM, ndim);
				bM = gcnew array<double>(ndim);

				for (int ii = 0; ii < bM->Length; ii++)
				{
					bM[ii] = bMX[ii];
				}
			}

			void EnergyFunctionalU::InsertFrameUpdateHM(MatXX^ HM, int nFrames)
			{
				(*HM->_mat).conservativeResize(8 * nFrames + CPARS, 8 * nFrames + CPARS);
				(*HM->_mat).rightCols<8 >().setZero();
				(*HM->_mat).bottomRows<8>().setZero();
			}
		}
	}
}