#pragma once
#include "NumType.hpp"
//#include "Mat.cpp"

namespace SharpSLAM
{
	namespace Core
	{
		namespace Unmanaged
		{
			ref class Mat33;
			ref class Mat33f;

			public ref class SE3
			{
			internal:
				Types::SE3* _se3;

			public:
				SE3();
				SE3(Types::SE3* data);
				SE3(double w, double x, double y, double z, double tx, double ty, double tz);
				~SE3();
				!SE3();
				void TranslationSetZero();
				Mat33^ RotationMatrix();
				Mat33f^ RotationMatrixF();
				array<float>^ TranslationF();
				array<double>^ TranslationD();
				void TranslationMul(double fac);
				void TranslationDivide(double fac);
				SE3^ Inverse();
				SE3^ Clone();
				array<double>^ Log();
				array<double>^ Quaternion();
				System::String^ PrintLog();
				static SE3^ Exp(array<double>^ in);
				static SE3^ operator *(SE3^ a, SE3^ b);
				static array<double>^ operator *(SE3^ a, array<double>^ b);
			};
		}
	}
}

