#pragma once
#include "math.h"


namespace SharpSLAM
{
	namespace Core
	{
		namespace Unmanaged
		{
			public value class AffLight
			{
			public:
				// Affine Parameters:
				double a;
				double b;
				// I_frame = exp(a)*I_global + b. // I_global = exp(-a)*(I_frame - b).
				AffLight(double a, double b) {
					this->a = a; this->b = b;
				};
				/*AffLight() {
					a = 0; b = 0;
				};*/

				static array<double>^ FromToVecExposure(float exposureF, float exposureT, AffLight g2F, AffLight g2T)
				{
					if (exposureF == 0 || exposureT == 0)
					{
						exposureT = exposureF = 1;
						//printf("got exposure value of 0! please choose the correct model.\n");
						//assert(setting_brightnessTransferFunc < 2);
					}

					double a = exp(g2T.a - g2F.a) * exposureT / exposureF;
					double b = g2T.b - a * g2F.b;
					return gcnew array<double>(2) { a, b };
				}
			};
		}
	}
}