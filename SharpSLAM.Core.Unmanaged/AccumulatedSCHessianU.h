#pragma once

//#include "NumType.hpp"
#include "MatrixAccumulators.h"
#include "Mat.cpp"

namespace SharpSLAM
{
	namespace Core
	{
		namespace Unmanaged
		{
			public ref class AccumulatedSCHessianU
			{
			private:
			public:
				int NumThreads;
				AccumulatorXX<8, CPARS>** accE;
				AccumulatorX<8>** accEB;
				AccumulatorXX<8, 8>** accD;
				AccumulatorXX<CPARS, CPARS>* accHcc;
				AccumulatorX<CPARS>* accbc;

				AccumulatedSCHessianU(int numThreads)
				{
					this->NumThreads = numThreads;
					accE = new AccumulatorXX<8, CPARS>*[numThreads];
					accEB = new AccumulatorX<8>*[numThreads];
					accD = new AccumulatorXX<8, 8>*[numThreads];

					accHcc = new AccumulatorXX<CPARS, CPARS>[numThreads];
					accbc = new AccumulatorX<CPARS>[numThreads];

					for (int tid = 0; tid < numThreads; tid++)
					{
						accE[tid] = NULL;
						accEB[tid] = NULL;
						accD[tid] = NULL;
					}
				}
				~AccumulatedSCHessianU()
				{

					for (int tid = 0; tid < NumThreads; tid++)
					{
						if (accE[tid] != NULL) delete[] accE[tid];
						if (accEB[tid] != NULL) delete[] accEB[tid];
						if (accD[tid] != NULL) delete[] accD[tid];
					}

					delete[] accE;
					delete[] accEB;
					delete[] accD;

					delete[] accHcc;
					delete[] accbc;
				};

				void SetZero(int nFrames, int currentNFrame, int tid)
				{
					if (nFrames != currentNFrame)
					{
						if (accE[tid] != NULL) delete[] accE[tid];
						if (accEB[tid] != NULL) delete[] accEB[tid];
						if (accD[tid] != NULL) delete[] accD[tid];
						accE[tid] = new AccumulatorXX<8, CPARS>[nFrames*nFrames];
						accEB[tid] = new AccumulatorX<8>[nFrames*nFrames];
						accD[tid] = new AccumulatorXX<8, 8>[nFrames*nFrames*nFrames];
					}
					accbc[tid].initialize();
					accHcc[tid].initialize();

					for (int i = 0; i < nFrames*nFrames; i++)
					{
						accE[tid][i].initialize();
						accEB[tid][i].initialize();

						for (int j = 0; j < nFrames; j++)
							accD[tid][i*nFrames + j].initialize();
					}
				}

				void AddPoint1(array<float>^ hcd, float pHdiF, float pbdSumF, int tid);
				void AddPoint2(int sidx, array<float>^ r1JpJdF, array<float>^ r2JpJdF, float pHdiF, int tid);
				void AddPoint3(int r1ht, array<float>^ r1JpJdF, array<float>^ hcd, float pHdiF, float pbdSumF, int tid);

				void StitchDoubleMTIter(int hIdx, MatXX^ H);
				void StitchDoubleInternal(int nf, int min, int max, MatXX^ H, array<double>^ B, array<Mat88^>^ EfAdHost, array<Mat88^>^ EfAdTarget);
			};
		}
	}
}

