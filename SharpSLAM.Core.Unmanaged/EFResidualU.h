#pragma once

#include "NumType.hpp"
#include "UnmanagedClass.cpp"

namespace SharpSLAM
{
	namespace Core
	{
		namespace Unmanaged
		{
			public ref class EFResidualU
			{
			public:
				void FixLinearizationF(int patternNum, array<float>^ efAddHTdeltaFI,
					array<float>^ Jpdxi0, array<float>^ Jpdxi1,
					array<float>^ Jpdc0, array<float>^ Jpdc1,
					array<float>^ Jpdd, array<float>^ efCdeltaF,
					array<float>^ JResF, array<float>^ resToZeroF,
					array<float>^ JJIdx0, array<float>^ JJIdx1, array<float>^ JJabF0, array<float>^ JJabF1,
					float pointDeltaF);
			};
		}
	}
}

