#include "AccumulatedTopHessianU.h"

namespace SharpSLAM
{
	namespace Core
	{
		namespace Unmanaged
		{
			array<float>^ AccumulatedTopHessianU::AddPoint(int mode, int patternNumber, float fJp_delta_x, float fJp_delta_y, float dp6, float dp7,
				array<array<float>^>^ jIdx, array<array<float>^>^ jAbF, array<float>^ resToZeroF, array<float>^ resApprox,
				array<array<float>^>^ jpdc, array<array<float>^>^ jpdxi, Mat22f^ jIdx2, Mat22f^ jab2, Mat22f^ jabJIdx,
				int tid, int htIDX)
			{
				if (mode == 1)
				{

					for (int i = 0; i < patternNumber; i += 4)
					{
						pin_ptr<float> JIdx0I = &jIdx[0][i];
						pin_ptr<float> JIdx1I = &jIdx[1][i];
						pin_ptr<float> JabF0I = &jAbF[0][i];
						pin_ptr<float> JabF1I = &jAbF[1][i];
						pin_ptr<float> ResToZeroFI = &resToZeroF[i];
						pin_ptr<float> resApproxI = &resApprox[i];
						float * resApproxIFP = resApproxI;

						UnmanagedClass::AccTopHessAddPointIter(fJp_delta_x, fJp_delta_y, dp6, dp7, ResToZeroFI, JIdx0I, JIdx1I, JabF0I, JabF1I, resApproxIFP);
					}
				}

				// need to compute JI^T * r, and Jab^T * r. (both are 2-vectors).
				auto JI_r = gcnew array<float>(2);
				auto Jab_r = gcnew array<float>(2);
				float rr = 0;
				for (int i = 0; i < patternNumber; i++)
				{
					JI_r[0] += resApprox[i] * jIdx[0][i];
					JI_r[1] += resApprox[i] * jIdx[1][i];
					Jab_r[0] += resApprox[i] * jAbF[0][i];
					Jab_r[1] += resApprox[i] * jAbF[1][i];
					rr += resApprox[i] * resApprox[i];
				}


				pin_ptr<float> jpdc0 = &jpdc[0][0];
				pin_ptr<float> jpdc1 = &jpdc[1][0];

				pin_ptr<float> jpdxi0 = &jpdxi[0][0];
				pin_ptr<float> jpdxi1 = &jpdxi[1][0];


				acc[tid][htIDX].update(
					jpdc0, jpdxi0,
					jpdc1, jpdxi1,
					(*jIdx2->_mat)(0, 0), (*jIdx2->_mat)(0, 1), (*jIdx2->_mat)(1, 1));

				acc[tid][htIDX].updateBotRight(
					(*jab2->_mat)(0, 0), (*jab2->_mat)(0, 1), Jab_r[0],
					(*jab2->_mat)(1, 1), Jab_r[1], rr);

				acc[tid][htIDX].updateTopRight(
					jpdc0, jpdxi0,
					jpdc1, jpdxi1,
					(*jabJIdx->_mat)(0, 0), (*jabJIdx->_mat)(0, 1),
					(*jabJIdx->_mat)(1, 0), (*jabJIdx->_mat)(1, 1),
					JI_r[0], JI_r[1]);

				return JI_r;
			}

			void AccumulatedTopHessianU::StitchDoubleMTIter(int hIdx, MatXX^ H,int h, int nframes0)
			{
				(*H->_mat).block<CPARS, 8>(0, hIdx).noalias() = (*H->_mat).block < 8, CPARS >(hIdx, 0).transpose();

				for (int t = h + 1; t < nframes0; t++)
				{
					int tIdx = CPARS + t * 8;
					(*H->_mat).block < 8, 8 >(hIdx, tIdx).noalias() += (*H->_mat).block<8,8>(tIdx, hIdx).transpose();
					(*H->_mat).block < 8, 8 >(tIdx, hIdx).noalias() =  (*H->_mat).block<8,8>(hIdx, tIdx).transpose();
				}
			}

			void AccumulatedTopHessianU::StitchDoubleInternal(int min, int max, array<int>^ nframes, int toAggregate, MatXX^ H, array<double>^ B, array<Mat88^>^ EfAdHost, array<Mat88^>^ EfAdTarget, int tid, bool usePrior, array<double>^ cPrior, array<float>^ cDeltaf, array<array<double>^>^ framePriors, array<array<double>^>^ frameDeltaPriors)
			{
				//pin_ptr<double> BPointer = &B[0];
				pin_ptr<double> cPriorPointer = &cPrior[0];
				pin_ptr<float> cDeltafXPointer = &cDeltaf[0];

				Types::VecX bX(B->Length);
				for (int i = 0; i < B->Length; i++)
				{
					bX[i] = B[i];
				}

				Types::VecC cPriorX(cPriorPointer);
				Types::VecCf cDeltafX(cDeltafXPointer);
				
				for (int k = min; k < max; k++)
				{
					int h = k % nframes[0];
					int t = k / nframes[0];
					
					int hIdx = CPARS + h * 8;
					int tIdx = CPARS + t * 8;
					int aidx = h + nframes[0] * t;
					
					assert(aidx == k);
					
					
					Types::MatPCPC accH = Types::MatPCPC::Zero();
					
					for (int tid2 = 0; tid2 < toAggregate; tid2++)
					{
						acc[tid2][aidx].finish();
						if (acc[tid2][aidx].num == 0) continue;
						accH += acc[tid2][aidx].H.cast<double>();
					}
				
					(*H->_mat).block<8, 8>(hIdx, hIdx).noalias() += (*EfAdHost[aidx]->_mat) * accH.block<8, 8>(CPARS, CPARS) * (*EfAdHost[aidx]->_mat).transpose();
					
					(*H->_mat).block<8, 8>(tIdx, tIdx).noalias() += (*EfAdTarget[aidx]->_mat) * accH.block<8, 8>(CPARS, CPARS) * (*EfAdTarget[aidx]->_mat).transpose();
					
					(*H->_mat).block<8, 8>(hIdx, tIdx).noalias() += (*EfAdHost[aidx]->_mat) * accH.block<8, 8>(CPARS, CPARS) * (*EfAdTarget[aidx]->_mat).transpose();
					
					(*H->_mat).block<8, CPARS>(hIdx, 0).noalias() += (*EfAdHost[aidx]->_mat) * accH.block<8, CPARS>(CPARS, 0);
					
					(*H->_mat).block<8, CPARS>(tIdx, 0).noalias() += (*EfAdTarget[aidx]->_mat) * accH.block<8, CPARS>(CPARS, 0);
					
					(*H->_mat).topLeftCorner<CPARS, CPARS>().noalias() += accH.block<CPARS, CPARS>(0, 0);
					
					bX.segment<8>(hIdx).noalias() += (*EfAdHost[aidx]->_mat) * accH.block<8, 1>(CPARS, CPARS + 8);
					
					bX.segment<8>(tIdx).noalias() += (*EfAdTarget[aidx]->_mat) * accH.block<8, 1>(CPARS, CPARS + 8);
					
					bX.head<CPARS>().noalias() += accH.block<CPARS, 1 >(0, CPARS + 8);
				
				}
				
				
				// only do this on one thread.
				if (min == 0 && usePrior)
				{
				
					(*H->_mat).diagonal().head<CPARS>() += cPriorX;
					bX.head<CPARS>() += cPriorX.cwiseProduct(cDeltafX.cast<double>());
					for (int h = 0; h < nframes[tid]; h++)
					{
						pin_ptr<double> framePriorXPointer = &framePriors[h][0];
						pin_ptr<double> frameDeltaPriorXPointer = &frameDeltaPriors[h][0];
				
						Types::Vec8 framePriorX(framePriorXPointer);
						Types::Vec8 frameDeltaPriorX(frameDeltaPriorXPointer);
				
						(*H->_mat).diagonal().segment < 8 >(CPARS + h * 8) += framePriorX;
						bX.segment<8>(CPARS + h * 8) += framePriorX.cwiseProduct(frameDeltaPriorX);
					}
				}

				for (int i = 0; i < B->Length; i++)
				{
					B[i] = bX[i];
				}
			}
		}
	}
}