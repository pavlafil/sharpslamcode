#include "NumType.hpp"

namespace SharpSLAM
{
	namespace Core
	{
		namespace Unmanaged
		{
			static public ref class Extensions
			{
			public:
				static double CwiseProductAndDot(array<double>^ inVect, array<double>^ otherVect, array<double>^ dotWithVect)
				{
					if (inVect->Length != 8 || otherVect->Length != 8 || dotWithVect->Length != 8)
						throw gcnew System::Exception("Input vectors has to have 8 items!");

					Types::Vec8 in;
					Types::Vec8 other;
					Types::Vec8 dot;
					for (int i = 0; i < inVect->Length; i++)
					{
						in[i] = inVect[i];
						other[i] = otherVect[i];
						dot[i] = dotWithVect[i];
					}

					return in.cwiseProduct(other).dot(dot);
				}
				static double CwiseProductAndDot(array<double>^ inVect, array<float>^ otherVect, array<double>^ dotWithVect)
				{
					if (inVect->Length != 8 || otherVect->Length !=8 || dotWithVect->Length!=8)
						throw gcnew System::Exception("Input vectors has to have 8 items!");

					Types::Vec8 in;
					Types::Vec8 other;
					Types::Vec8 dot;
					for (int i = 0; i < inVect->Length; i++)
					{
						in[i] = inVect[i];
						other[i] = otherVect[i];
						dot[i] = dotWithVect[i];
					}

					return in.cwiseProduct(other).dot(dot);
				}
				static double CwiseProductAndDot(array<float>^ inVect, array<float>^ otherVect, array<float>^ dotWithVect)
				{
					if (inVect->Length != 8 || otherVect->Length != 8 || dotWithVect->Length != 8)
						throw gcnew System::Exception("Input vectors has to have 8 items!");

					Types::Vec8 in;
					Types::Vec8 other;
					Types::Vec8 dot;
					for (int i = 0; i < inVect->Length; i++)
					{
						in[i] = inVect[i];
						other[i] = otherVect[i];
						dot[i] = dotWithVect[i];
					}

					return in.cwiseProduct(other).dot(dot);
				}
			};
		}
	}
}
