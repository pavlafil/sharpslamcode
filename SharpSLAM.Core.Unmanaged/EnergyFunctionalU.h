#pragma once

#include "MatrixAccumulators.h"
#include "NumType.hpp"
#include "Mat.cpp"

namespace SharpSLAM
{
	namespace Core
	{
		namespace Unmanaged
		{
			public ref class EnergyFunctionalU
			{
			private:
				Accumulator11* _E;


			public:
				EnergyFunctionalU();
				void CalcLEnergyInit();
				void CalcLEnergyIter(float Jp_delta_x_1, float Jp_delta_y_1, float dp6, float dp7, int patternNum,array<array<float>^>^ JIdx, array<array<float>^>^ JabF, array<float>^ resToZeroF);
				void CalcLEnergyUpdateSingleNoShift(float value);
				void CalcLEnergyUpdateSingle(float value);
				double CalcLEnergyFinish();
				void Orthogonalize(System::Collections::Generic::List<array<double>^>^ ns, MatXX^ H, array<double>^ b, double setting_solverModeDelta);
				array<double>^ SolveSystemFGetX(bool solverTypeSVD, bool solverSVDCut7, MatXX^ HFinal_top, array<double>^ bFinal_top, double setting_solverModeDelta);
				void MarginalizeFrame(int fhIdx, int ndim, int odim, int framesCount, int nFrames, array<double>^% bM, MatXX^ HM,
					array<double>^ fhPrior, array<double>^ fhDeltaPrior);
				void InsertFrameUpdateHM(MatXX^ HM, int nFrames);
			};
		}
	}
}