using namespace System;

namespace SharpSLAM
{
	namespace Core
	{
		namespace Unmanaged
		{
			public ref class Point
			{
				// index in jacobian. never changes (actually, there is no reason why).
			public:
				array<float>^ uv;

				// idepth / isgood / energy during optimization.
				float idepth;
				bool isGood;
				array<float>^ energy;       //2 (UenergyPhotometric, energyRegularizer)
				bool isGood_new;
				float idepth_new;
				array<float>^ energy_new; //2

				float iR;
				float iRSumNum;

				float lastHessian;
				float lastHessian_new;

				// max stepsize for idepth (corresponding to max. movement in pixel-space).
				float maxstep;

				// idx (x+y*w) of closest point one pyramid level above.
				int parent;
				float parentDist;

				// idx (x+y*w) of up to 10 nearest points in pixel space.
				array<int>^ neighbours;
				array<float>^ neighboursDist;

				float Type;
				float outlierTH;

				Point()
				{
					energy = gcnew array<float>(2);
					energy_new = gcnew array<float>(2);
					//neighbours= new int[10];
					//neighboursDist = new float[10];
				}
			};
		}
	}
}