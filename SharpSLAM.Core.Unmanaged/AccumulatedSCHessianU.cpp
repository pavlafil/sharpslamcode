#include "AccumulatedSCHessianU.h"

namespace SharpSLAM
{
	namespace Core
	{
		namespace Unmanaged
		{
			void AccumulatedSCHessianU::AddPoint1(array<float>^ hcd, float pHdiF, float pbdSumF, int tid)
			{
				pin_ptr<float> hcdPointer = &hcd[0];
				Types::VecCf hcdX(hcdPointer);

				accHcc[tid].update(hcdX, hcdX, pHdiF);
				accbc[tid].update(hcdX, pbdSumF * pHdiF);
			}

			void AccumulatedSCHessianU::AddPoint2(int sidx, array<float>^ r1JpJdF, array<float>^ r2JpJdF, float pHdiF, int tid)
			{
				pin_ptr<float> r1JpJdFPointer = &r1JpJdF[0];
				pin_ptr<float> r2JpJdFPointer = &r2JpJdF[0];

				Types::Vec8f r1JpJdFX(r1JpJdFPointer);
				Types::Vec8f r2JpJdFX(r2JpJdFPointer);

				accD[tid][sidx].update(r1JpJdFX, r2JpJdFX, pHdiF);
			}

			void AccumulatedSCHessianU::AddPoint3(int r1ht, array<float>^ r1JpJdF, array<float>^ hcd, float pHdiF, float pbdSumF, int tid)
			{
				pin_ptr<float> r1JpJdFPointer = &r1JpJdF[0];
				pin_ptr<float> hcdPointer = &hcd[0];

				Types::Vec8f r1JpJdFX(r1JpJdFPointer);
				Types::VecCf hcdX(hcdPointer);

				accE[tid][r1ht].update(r1JpJdFX, hcdX, pHdiF);
				accEB[tid][r1ht].update(r1JpJdFX, pHdiF * pbdSumF);
			}

			void AccumulatedSCHessianU::StitchDoubleInternal(int nf, int min, int max, MatXX^ H, array<double>^ B, array<Mat88^>^ EfAdHost, array<Mat88^>^ EfAdTarget)
			{
				Types::VecX bX(B->Length);
				for (int i = 0; i < B->Length; i++)
				{
					bX[i] = B[i];
				}

				int nframes2 = nf * nf;
				int toAggregate = NumThreads;
				for (int k = min; k < max; k++)
				{
					int i = k % nf;
					int j = k / nf;

					int iIdx = CPARS + i * 8;
					int jIdx = CPARS + j * 8;
					int ijIdx = i + nf * j;

					Types::Mat8C Hpc = Types::Mat8C::Zero();
					Types::Vec8 bp = Types::Vec8::Zero();

					for (int tid2 = 0; tid2 < toAggregate; tid2++)
					{
						accE[tid2][ijIdx].finish();
						accEB[tid2][ijIdx].finish();
						Hpc += accE[tid2][ijIdx].A1m.cast<double>();
						bp += accEB[tid2][ijIdx].A1m.cast<double>();
					}

					(*H->_mat).block < 8, CPARS >(iIdx, 0) += (*EfAdHost[ijIdx]->_mat) * Hpc;
					(*H->_mat).block < 8, CPARS >(jIdx, 0) += (*EfAdTarget[ijIdx]->_mat) * Hpc;
					bX.segment<8>(iIdx) += (*EfAdHost[ijIdx]->_mat) * bp;
					bX.segment<8>(jIdx) += (*EfAdTarget[ijIdx]->_mat) * bp;



					for (int k = 0; k < nf; k++)
					{
						int kIdx = CPARS + k * 8;
						int ijkIdx = ijIdx + k * nframes2;
						int ikIdx = i + nf * k;

						Types::Mat88 accDM = Types::Mat88::Zero();

						for (int tid2 = 0; tid2 < toAggregate; tid2++)
						{
							accD[tid2][ijkIdx].finish();
							if (accD[tid2][ijkIdx].num == 0) continue;
							accDM += accD[tid2][ijkIdx].A1m.cast<double>();
						}

						(*H->_mat).block<8, 8>(iIdx, iIdx) += (*EfAdHost[ijIdx]->_mat)* accDM *    (*EfAdHost[ikIdx]->_mat).transpose();
						(*H->_mat).block<8, 8>(jIdx, kIdx) += (*EfAdTarget[ijIdx]->_mat) * accDM * (*EfAdTarget[ikIdx]->_mat).transpose();
						(*H->_mat).block<8, 8>(jIdx, iIdx) += (*EfAdTarget[ijIdx]->_mat) * accDM * (*EfAdHost[ikIdx]->_mat).transpose();
						(*H->_mat).block<8, 8>(iIdx, kIdx) += (*EfAdHost[ijIdx]->_mat) * accDM *   (*EfAdTarget[ikIdx]->_mat).transpose();
					}
				}

				if (min == 0)
				{
					for (int tid2 = 0; tid2 < toAggregate; tid2++)
					{
						accHcc[tid2].finish();
						accbc[tid2].finish();
						(*H->_mat).topLeftCorner<CPARS, CPARS>() += accHcc[tid2].A1m.cast<double>();
						bX.head<CPARS>() += accbc[tid2].A1m.cast<double>();
					}
				}

				for (int i = 0; i < B->Length; i++)
				{
					B[i] = bX[i];
				}
			}

			void AccumulatedSCHessianU::StitchDoubleMTIter(int hIdx, MatXX^ H)
			{
				(*H->_mat).block<CPARS,8>(0, hIdx).noalias() = (*H->_mat).block<8,CPARS >(hIdx, 0).transpose();
			}
		}
	}
}