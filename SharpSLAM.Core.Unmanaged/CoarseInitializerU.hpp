#pragma once
#include "NumType.hpp"
#include "Mat.cpp"
#include "SE3.hpp"
#include "AffLight.cpp"
#include "MatrixAccumulators.h"
#include "Point.cpp"

namespace SharpSLAM
{
	namespace Core
	{
		namespace Unmanaged
		{
			public ref class CoarseInitializerU
			{
			private:
				Accumulator9* acc9;
				Accumulator9* acc9SC;
				// temporary buffers for H and b.
				Types::Vec10f* JbBuffer;			// 0-7: sum(dd * dp). 8: sum(res*dd). 9: 1/(1+sum(dd*dd))=inverse hessian entry.
				Types::Vec10f* JbBuffer_new;


				EIGEN_ALWAYS_INLINE Eigen::Vector3f getInterpolatedElement33(array<float, 2>^ mat, const float x, const float y, const int width);
				EIGEN_ALWAYS_INLINE float getInterpolatedElement31(array<float, 2>^ mat, const float x, const float y, const int width);
				array<float>^ GetManagedVector(Types::Vec8f* vec);

			public:
				CoarseInitializerU(int ww, int hh);
				~CoarseInitializerU();
				!CoarseInitializerU();
				float GetB(int i, array<float>^ inc);
				float GetStep(float b, int i, float lambda);
				void FixAffineAndSE3Exp(bool fixAffine, DiagonalMatrix8f^ wM, Mat88f^ Hl, array<float>^ bl, SE3^ refToNew_current, array<float>^% out, SE3^% refToNew_new_out);
				void SwapJbBuffer();
				array<float>^ CalcEC(int lvl, bool snapped, float couplingWeight, array<int>^ numPoints, array<array<Point^>^>^ points);
				array<float>^ CalcResAndGS(
					int lvl, Mat88f^% H_out, array<float>^% b_out,
					Mat88f^% H_out_sc, array<float>^% b_out_sc,
					const SE3^ refToNew, AffLight refToNew_aff,
					bool plot, array<int>^ w, array<int>^ h,
					array<Mat33^>^ Ki,
					array<int>^ numPoints, array<array<Point^>^>^ points,
					int patternNum, int patternIndex, array<int, 3>^ patterns,
					float setting_huberTH, float alphaW, float alphaK, float couplingWeight,
					array<double>^ fx, array<double>^ fy, array<double>^ cx, array<double>^ cy,
					array<array<float, 2>^>^ firtFramDIP, array<array<float, 2>^>^ newFramDIP);
			};
		}
	}
}
