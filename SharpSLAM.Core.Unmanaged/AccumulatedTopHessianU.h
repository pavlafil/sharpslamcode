/**
* This file is part of DSO.
*
* Copyright 2016 Technical University of Munich and Intel.
* Developed by Jakob Engel <engelj at in dot tum dot de>,
* for more information see <http://vision.in.tum.de/dso>.
* If you use this code, please cite the respective publications as
* listed on the above website.
*
* DSO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DSO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DSO. If not, see <http://www.gnu.org/licenses/>.
*/


#pragma once


#include "NumType.hpp"
#include "MatrixAccumulators.h"
#include "Mat.cpp"

//#include "vector"
//#include <math.h>
//#include "util/IndexThreadReduce.h"


namespace SharpSLAM
{
	namespace Core
	{
		namespace Unmanaged
		{

			public ref class AccumulatedTopHessianU
			{
			private:

			public:
				int NumThreads;
				AccumulatorApprox** acc;


				//					EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
				AccumulatedTopHessianU(int numThreads)
				{
					this->NumThreads = numThreads;
					acc = new AccumulatorApprox*[numThreads];
					for (int tid = 0; tid < numThreads; tid++)
					{
						acc[tid] = NULL;
					}

				};
				~AccumulatedTopHessianU()
				{

					for (int tid = 0; tid < NumThreads; tid++)
					{
						if (acc[tid] != NULL) delete[] acc[tid];
					}
					delete[] acc;
				};

				void SetZero(int nFrames, int currentNFrame, int tid)
				{

					if (nFrames != currentNFrame)
					{
						if (acc[tid] != 0) delete[] acc[tid];
#if USE_XI_MODEL
						acc[tid] = new Accumulator14[nFrames*nFrames];
#else
						acc[tid] = new AccumulatorApprox[nFrames*nFrames];
#endif
					}

					for (int i = 0; i < nFrames*nFrames; i++)
					{
						acc[tid][i].initialize();
					}
				}

				array<float>^ AddPoint(int mode, int patternNumber, float fJp_delta_x, float fJp_delta_y, float dp6, float dp7,
					array<array<float>^>^ jIdx, array<array<float>^>^ jAbF, array<float>^ resToZeroF, array<float>^ resApprox,
					array<array<float>^>^ jpdc, array<array<float>^>^ jpdxi, Mat22f^ jIdx2, Mat22f^ jab2, Mat22f^ jabJIdx,
					int tid, int htIDX);
				void StitchDoubleMTIter(int hIdx, MatXX^ H, int h, int nframes0);
				void StitchDoubleInternal(int min, int max, array<int>^ nframes, int toAggregate, MatXX^ H, array<double>^ B, array<Mat88^>^ EfAdHost, array<Mat88^>^ EfAdTarrget, int tid, bool usePrior, array<double>^ cPrior, array<float>^ cDeltaf, array<array<double>^>^ framePriors, array<array<double>^>^ frameDeltaPriors);

			};
		}

	}
}
