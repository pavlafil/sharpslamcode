#pragma once
#include "NumType.hpp"
#include "Mat.cpp"
#include "MatrixAccumulators.h"
namespace SharpSLAM
{
	namespace Core
	{
		namespace Unmanaged
		{
			public ref class CoarseTrackerU
			{
			public:
				void CalcGSSSE(float fx_lvl, float fy_lvl, int buf_warped_n, double lastRef_aff_g2l_b, double vecExposure, Mat88^% H_out, array<double>^% b_out, const SE3^% refToNew, double aff_g2l_a, double aff_g2l_b,
					array<float>^ buf_warped_dx, array<float>^ buf_warped_dy, array<float>^ buf_warped_u, array<float>^ buf_warped_v, array<float>^ buf_warped_idepth,
					array<float>^ buf_warped_refColor, array<float>^ buf_warped_residual, array<float>^ buf_warped_weight,
					float SCALE_XI_ROT, float SCALE_XI_TRANS, float SCALE_A, float SCALE_B);
			};
		}
	}
}

