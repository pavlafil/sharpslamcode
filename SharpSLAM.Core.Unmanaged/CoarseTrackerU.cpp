#include "CoarseTrackerU.h"

namespace SharpSLAM
{
	namespace Core
	{
		namespace Unmanaged
		{

			void CoarseTrackerU::CalcGSSSE(float fx_lvl, float fy_lvl, int buf_warped_n, double lastRef_aff_g2l_b, double vecExposure, Mat88^% H_out, array<double>^% b_out, const SE3^% refToNew, double aff_g2l_a, double aff_g2l_b,
				array<float>^ buf_warped_dx, array<float>^ buf_warped_dy, array<float>^ buf_warped_u, array<float>^ buf_warped_v, array<float>^ buf_warped_idepth,
				array<float>^ buf_warped_refColor, array<float>^ buf_warped_residual, array<float>^ buf_warped_weight,
				float SCALE_XI_ROT, float SCALE_XI_TRANS, float SCALE_A, float SCALE_B)
			{
				pin_ptr<float> buf_warped_dx_P = &buf_warped_dx[0];
				pin_ptr<float> buf_warped_dy_P = &buf_warped_dy[0];
				pin_ptr<float> buf_warped_u_P = &buf_warped_u[0];
				pin_ptr<float> buf_warped_v_P = &buf_warped_v[0];
				pin_ptr<float> buf_warped_idepth_P = &buf_warped_idepth[0];
				pin_ptr<float> buf_warped_refColor_P = &buf_warped_refColor[0];
				pin_ptr<float> buf_warped_residual_P = &buf_warped_residual[0];
				pin_ptr<float> buf_warped_weight_P = &buf_warped_weight[0];

				float* buf_warped_dx_UP = new float[buf_warped_dx->Length];
				memcpy(buf_warped_dx_UP, buf_warped_dx_P, buf_warped_dx->Length * sizeof(float));
				float* buf_warped_dy_UP = new float[buf_warped_dy->Length];
				memcpy(buf_warped_dy_UP, buf_warped_dy_P, buf_warped_dy->Length * sizeof(float));
				float* buf_warped_u_UP = new float[buf_warped_u->Length];
				memcpy(buf_warped_u_UP, buf_warped_u_P, buf_warped_u->Length * sizeof(float));
				float* buf_warped_v_UP = new float[buf_warped_v->Length];
				memcpy(buf_warped_v_UP, buf_warped_v_P, buf_warped_v->Length * sizeof(float));
				float* buf_warped_idepth_UP = new float[buf_warped_idepth->Length];
				memcpy(buf_warped_idepth_UP, buf_warped_idepth_P, buf_warped_idepth->Length * sizeof(float));
				float* buf_warped_refColor_UP = new float[buf_warped_refColor->Length];
				memcpy(buf_warped_refColor_UP, buf_warped_refColor_P, buf_warped_refColor->Length * sizeof(float));
				float* buf_warped_residual_UP = new float[buf_warped_residual->Length];
				memcpy(buf_warped_residual_UP, buf_warped_residual_P, buf_warped_residual->Length * sizeof(float));
				float* buf_warped_weight_UP = new float[buf_warped_weight->Length];
				memcpy(buf_warped_weight_UP, buf_warped_weight_P, buf_warped_weight->Length * sizeof(float));

				int n = buf_warped_n;

				auto H = UnmanagedClass::CoarseTrackerUCalcGSSSE(fx_lvl, fy_lvl, lastRef_aff_g2l_b, vecExposure, buf_warped_dx_UP, buf_warped_dy_UP, buf_warped_u_UP, buf_warped_v_UP, buf_warped_idepth_UP, buf_warped_refColor_UP, buf_warped_residual_UP, buf_warped_weight_UP, n);


				memcpy(buf_warped_dx_P, buf_warped_dx_UP, buf_warped_dx->Length * sizeof(float));
				memcpy(buf_warped_dy_P, buf_warped_dy_UP, buf_warped_dy->Length * sizeof(float));
				memcpy(buf_warped_u_P, buf_warped_u_UP, buf_warped_u->Length * sizeof(float));
				memcpy(buf_warped_v_P, buf_warped_v_UP, buf_warped_v->Length * sizeof(float));
				memcpy(buf_warped_idepth_P, buf_warped_idepth_UP, buf_warped_idepth->Length * sizeof(float));
				memcpy(buf_warped_refColor_P, buf_warped_refColor_UP, buf_warped_refColor->Length * sizeof(float));
				memcpy(buf_warped_residual_P, buf_warped_residual_UP, buf_warped_residual->Length * sizeof(float));
				memcpy(buf_warped_weight_P, buf_warped_weight_UP, buf_warped_weight->Length * sizeof(float));


				delete[] buf_warped_dx_UP;
				delete[] buf_warped_dy_UP;
				delete[] buf_warped_u_UP;
				delete[] buf_warped_v_UP;
				delete[] buf_warped_idepth_UP;
				delete[] buf_warped_refColor_UP;
				delete[] buf_warped_residual_UP;
				delete[] buf_warped_weight_UP;

				Types::Mat88* H_out_u = new Types::Mat88(H.topLeftCorner<8, 8>().cast<double>() * (1.0f / n));

				Types::Vec8 b_out_u = H.topRightCorner<8, 1>().cast<double>() * (1.0f / n);

				H_out_u->block<8, 3>(0, 0) *= SCALE_XI_ROT;
				H_out_u->block<8, 3>(0, 3) *= SCALE_XI_TRANS;
				H_out_u->block<8, 1>(0, 6) *= SCALE_A;
				H_out_u->block<8, 1>(0, 7) *= SCALE_B;
				H_out_u->block<3, 8>(0, 0) *= SCALE_XI_ROT;
				H_out_u->block<3, 8>(3, 0) *= SCALE_XI_TRANS;
				H_out_u->block<1, 8>(6, 0) *= SCALE_A;
				H_out_u->block<1, 8>(7, 0) *= SCALE_B;
				b_out_u.segment<3>(0) *= SCALE_XI_ROT;
				b_out_u.segment<3>(3) *= SCALE_XI_TRANS;
				b_out_u.segment<1>(6) *= SCALE_A;
				b_out_u.segment<1>(7) *= SCALE_B;


				H_out = gcnew Mat88(H_out_u);
				b_out = gcnew array<double>(8);
				for (int i = 0; i < 8; i++)
				{
					b_out[i] = b_out_u[i];
				}
			}
		}
	}
}
