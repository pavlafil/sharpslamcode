#include "CoarseInitializerU.hpp"
#include "UnmanagedClass.cpp"

using namespace SharpSLAM::Core::Unmanaged;
using namespace System::Threading::Tasks;



EIGEN_ALWAYS_INLINE Eigen::Vector3f CoarseInitializerU::getInterpolatedElement33(array<float, 2>^ mat, const float x, const float y, const int width)
{
	int ix = (int)x;
	int iy = (int)y;
	float dx = x - ix;
	float dy = y - iy;
	float dxdy = dx * dy;

	int bp = ix + iy * width;

	return dxdy * Eigen::Vector3f((const float)mat[bp + 1 + width, 0], (const float)mat[bp + 1 + width, 1], (const float)mat[bp + 1 + width, 2])
		+ (dy - dxdy) * Eigen::Vector3f((const float)mat[bp + width, 0], (const float)mat[bp + width, 1], (const float)mat[bp + width, 2])
		+ (dx - dxdy) * Eigen::Vector3f((const float)mat[bp + 1, 0], (const float)mat[bp + 1, 1], (const float)mat[bp + 1, 2])
		+ (1 - dx - dy + dxdy) * Eigen::Vector3f((const float)mat[bp, 0], (const float)mat[bp, 1], (const float)mat[bp, 2]);
}
EIGEN_ALWAYS_INLINE float CoarseInitializerU::getInterpolatedElement31(array<float, 2>^ mat, const float x, const float y, const int width)
{
	int ix = (int)x;
	int iy = (int)y;
	float dx = x - ix;
	float dy = y - iy;
	float dxdy = dx * dy;

	int bp = ix + iy * width;

	return dxdy * mat[bp + 1 + width, 0]
		+ (dy - dxdy) * mat[bp + width, 0]
		+ (dx - dxdy) * mat[bp + 1, 0]
		+ (1 - dx - dy + dxdy) * mat[bp, 0];
}

array<float>^ CoarseInitializerU::GetManagedVector(Types::Vec8f* vec)
{
	auto r = gcnew array<float>(8);
	for (int i = 0; i < 8; i++)
	{
		r[i] = (*vec)[i];
	}
	return r;
}


CoarseInitializerU::CoarseInitializerU(int ww, int hh)
{
	acc9 = new Accumulator9();
	acc9SC = new Accumulator9();

	JbBuffer = new Types::Vec10f[ww*hh];
	JbBuffer_new = new Types::Vec10f[ww*hh];
}
CoarseInitializerU::~CoarseInitializerU()
{
	this->!CoarseInitializerU();
}
CoarseInitializerU::!CoarseInitializerU()
{
	delete acc9;
	delete acc9SC;
	delete JbBuffer;
	delete JbBuffer_new;
}

array<float>^ CoarseInitializerU::CalcResAndGS(
	int lvl, Mat88f^% H_out, array<float>^% b_out,
	Mat88f^% H_out_sc, array<float>^% b_out_sc,
	const SE3^ refToNew, AffLight refToNew_aff,
	bool plot, array<int>^ w, array<int>^ h,
	array<Mat33^>^ Ki,
	array<int>^ numPoints, array<array<Point^>^>^ points,
	int patternNum, int patternIndex, array<int, 3>^ patterns,
	float setting_huberTH, float alphaW, float alphaK, float couplingWeight,
	array<double>^ fx, array<double>^ fy, array<double>^ cx, array<double>^ cy,
	array<array<float, 2>^>^ firtFramDIP, array<array<float, 2>^>^ newFramDIP)
{
	int wl = w[lvl], hl = h[lvl];
	auto colorRef = firtFramDIP[lvl];
	auto colorNew = newFramDIP[lvl];

	Types::Mat33f RKi = (refToNew->_se3->rotationMatrix() * (*Ki[lvl]->_mat)).cast<float>();
	Types::Vec3f t = refToNew->_se3->translation().cast<float>();
	Eigen::Vector2f r2new_aff = Eigen::Vector2f((float)exp(refToNew_aff.a), (float)refToNew_aff.b);

	float fxl = fx[lvl];
	float fyl = fy[lvl];
	float cxl = cx[lvl];
	float cyl = cy[lvl];


	Accumulator11* E = new Accumulator11();
	acc9->initialize();
	E->initialize();


	int npts = numPoints[lvl];
	array<Point^>^ ptsl = points[lvl];


	Types::VecNRf* dp0 = new Types::VecNRf();
	Types::VecNRf* dp1 = new Types::VecNRf();
	Types::VecNRf* dp2 = new Types::VecNRf();
	Types::VecNRf* dp3 = new Types::VecNRf();
	Types::VecNRf* dp4 = new Types::VecNRf();
	Types::VecNRf* dp5 = new Types::VecNRf();
	Types::VecNRf* dp6 = new Types::VecNRf();
	Types::VecNRf* dp7 = new Types::VecNRf();
	Types::VecNRf* dd = new Types::VecNRf();
	Types::VecNRf* r = new Types::VecNRf();

	for (int i = 0; i < npts; i++)
	{

		Point^ point = ptsl[i];

		point->maxstep = 1e10;
		if (!point->isGood)
		{
			E->updateSingle((float)(point->energy[0]));
			point->energy_new = point->energy;
			point->isGood_new = false;
			continue;
		}
		
		//UnmanagedClass::SetZero(dp0);
		//UnmanagedClass::SetZero(dp1); 
		//UnmanagedClass::SetZero(dp2); 
		//UnmanagedClass::SetZero(dp3); 
		//UnmanagedClass::SetZero(dp4); 
		//UnmanagedClass::SetZero(dp5); 
		//UnmanagedClass::SetZero(dp6); 
		//UnmanagedClass::SetZero(dp7); 
		//UnmanagedClass::SetZero(dd);
		//UnmanagedClass::SetZero(r);

		JbBuffer_new[i].setZero();
		
			// sum over all residuals.
			bool isGood = true;
			float energy = 0;
			for (int idx = 0; idx < patternNum; idx++)
			{
				int dx = patterns[patternIndex,idx,0];
				int dy = patterns[patternIndex, idx, 1];
		
				//SLOW
				Types::Vec3f pt = RKi * Types::Vec3f(point->uv[0] + dx, point->uv[1] + dy, 1) + (t * (float)(point->idepth_new));
				float u = pt[0] / pt[2];
				float v = pt[1] / pt[2];
				float Ku = fxl * u + cxl;
				float Kv = fyl * v + cyl;
				float new_idepth = point->idepth_new / pt[2];
		
				if (!(Ku > 1 && Kv > 1 && Ku < wl - 2 && Kv < hl - 2 && new_idepth > 0))
				{
					isGood = false;
					break;
				}
		
				//SLOW
				Types::Vec3f hitColor = getInterpolatedElement33(colorNew, Ku, Kv, wl);
				
				
				////Vec3f hitColor = getInterpolatedElement33BiCub(colorNew, Ku, Kv, wl);
		
				////float rlR = colorRef[point->u+dx + (point->v+dy) * wl][0];
				float rlR = getInterpolatedElement31(colorRef, point->uv[0] + dx, point->uv[1] + dy, wl);
		
				if (!std::isfinite(rlR) || !std::isfinite((float)hitColor[0]))
				{
					isGood = false;
					break;
				}
		
		
				float residual = hitColor[0] - r2new_aff[0] * rlR - r2new_aff[1];
				float hw = fabs(residual) < setting_huberTH ? 1 : setting_huberTH / fabs(residual); //outliers easily stands out https://en.wikipedia.org/wiki/Huber_loss
				energy += hw * residual*residual*(2 - hw);
		
		
		
		
				float dxdd = (t[0] - t[2] * u) / pt[2];
				float dydd = (t[1] - t[2] * v) / pt[2];
		
				if (hw < 1) hw = sqrtf(hw);
				float dxInterp = hw * hitColor[1] * fxl;
				float dyInterp = hw * hitColor[2] * fyl;
				(*dp0)[idx] = new_idepth * dxInterp;
				(*dp1)[idx] = new_idepth * dyInterp;
				(*dp2)[idx] = -new_idepth * (u*dxInterp + v * dyInterp);
				(*dp3)[idx] = -u * v*dxInterp - (1 + v * v)*dyInterp;
				(*dp4)[idx] = (1 + u * u)*dxInterp + u * v*dyInterp;
				(*dp5)[idx] = -v * dxInterp + u * dyInterp;
				(*dp6)[idx] = -hw * r2new_aff[0] * rlR;
				(*dp7)[idx] = -hw * 1;
				(*dd)[idx] = dxInterp * dxdd + dyInterp * dydd;
				(*r)[idx] = hw * residual;
		
				float maxstep = 1.0f / Types::Vec2f(dxdd*fxl, dydd*fyl).norm();
				if (maxstep < point->maxstep) point->maxstep = maxstep;
		
				// immediately compute dp*dd' and dd*dd' in JbBuffer1.
				JbBuffer_new[i][0] += (*dp0)[idx] * (*dd)[idx];
				JbBuffer_new[i][1] += (*dp1)[idx] * (*dd)[idx];
				JbBuffer_new[i][2] += (*dp2)[idx] * (*dd)[idx];
				JbBuffer_new[i][3] += (*dp3)[idx] * (*dd)[idx];
				JbBuffer_new[i][4] += (*dp4)[idx] * (*dd)[idx];
				JbBuffer_new[i][5] += (*dp5)[idx] * (*dd)[idx];
				JbBuffer_new[i][6] += (*dp6)[idx] * (*dd)[idx];
				JbBuffer_new[i][7] += (*dp7)[idx] * (*dd)[idx];
				JbBuffer_new[i][8] += (*r)[idx] *   (*dd)[idx];
				JbBuffer_new[i][9] += (*dd)[idx] *  (*dd)[idx];
			}
		
			if (!isGood || energy > point->outlierTH * 20)
			{
				E->updateSingle((float)(point->energy[0]));
				point->isGood_new = false;
				point->energy_new = point->energy;
				continue;
			}
		
		
			// add into energy.
			E->updateSingle(energy);
			point->isGood_new = true;
			point->energy_new[0] = energy;
		
			// update Hessian matrix.
			for (int i = 0; i + 3 < patternNum; i += 4)
				acc9->updateSSE(
					(((float*)(dp0)) + i),
					(((float*)(dp1)) + i),
					(((float*)(dp2)) + i),
					(((float*)(dp3)) + i),
					(((float*)(dp4)) + i),
					(((float*)(dp5)) + i),
					(((float*)(dp6)) + i),
					(((float*)(dp7)) + i),
					(((float*)(r)) + i));
		
		
			for (int i = ((patternNum >> 2) << 2); i < patternNum; i++)
				acc9->updateSingle(
				(float)(*dp0)[i], (float)(*dp1)[i], (float)(*dp2)[i], (float)(*dp3)[i],
					(float)(*dp4)[i], (float)(*dp5)[i], (float)(*dp6)[i], (float)(*dp7)[i],
					(float)(*r)[i]);

			
	}

	delete dp0;
	delete dp1;
	delete dp2;
	delete dp3;
	delete dp4;
	delete dp5;
	delete dp6;
	delete dp7;
	delete dd;
	delete r;

	
	E->finish();
	acc9->finish();
	
	
	
	
	
	
	// calculate alpha energy, and decide if we cap it.
	Accumulator11* EAlpha=new Accumulator11();
	EAlpha->initialize();
	for (int i = 0; i < npts; i++)
	{
		Point^ point = ptsl[i];
		if (!point->isGood_new)
		{
			E->updateSingle((float)(point->energy[1]));
		}
		else
		{
			point->energy_new[1] = (point->idepth_new - 1)*(point->idepth_new - 1);
			E->updateSingle((float)(point->energy_new[1]));
		}
	}
	EAlpha->finish();
	float alphaEnergy = alphaW * (EAlpha->A + refToNew->_se3->translation().squaredNorm() * npts);

	//printf("AE %f = %f * %f + %f\n", alphaEnergy, alphaW, EAlpha->A, refToNew->_se3->translation().squaredNorm() * npts);
	delete EAlpha;
	
	
	// compute alpha opt.
	float alphaOpt;
	if (alphaEnergy > alphaK*npts)
	{
		alphaOpt = 0;
		alphaEnergy = alphaK * npts;
	}
	else
	{
		alphaOpt = alphaW;
	}
	
	
	acc9SC->initialize();
	for (int i = 0; i < npts; i++)
	{
		Point^ point = ptsl[i];
		if (!point->isGood_new)
			continue;
	
		point->lastHessian_new = JbBuffer_new[i][9];
	
		JbBuffer_new[i][8] += alphaOpt * (point->idepth_new - 1);
		JbBuffer_new[i][9] += alphaOpt;
	
		if (alphaOpt == 0)
		{
			JbBuffer_new[i][8] += couplingWeight * (point->idepth_new - point->iR);
			JbBuffer_new[i][9] += couplingWeight;
		}
	
		JbBuffer_new[i][9] = 1 / (1 + JbBuffer_new[i][9]);
		acc9SC->updateSingleWeighted(
			(float)JbBuffer_new[i][0], (float)JbBuffer_new[i][1], (float)JbBuffer_new[i][2], (float)JbBuffer_new[i][3],
			(float)JbBuffer_new[i][4], (float)JbBuffer_new[i][5], (float)JbBuffer_new[i][6], (float)JbBuffer_new[i][7],
			(float)JbBuffer_new[i][8], (float)JbBuffer_new[i][9]);
	}
	acc9SC->finish();
	
	
	//printf("nelements in H: %d, in E: %d, in Hsc: %d / 9!\n", (int)acc9.num, (int)E.num, (int)acc9SC.num*9);
	auto H_out_u = new Types::Mat88f(acc9->H.topLeftCorner<8, 8>());// / acc9.num;
	auto b_out_u = new Types::Vec8f(acc9->H.topRightCorner<8, 1>());// / acc9.num;
	H_out_sc = gcnew Mat88f( new Types::Mat88f(acc9SC->H.topLeftCorner<8, 8>()));// / acc9.num;
	auto b_out_sc_u = new Types::Vec8f(acc9SC->H.topRightCorner<8, 1>());// / acc9.num;
	b_out_sc = GetManagedVector(b_out_sc_u);
	delete b_out_sc_u;
	
	(*H_out_u)(0, 0) += alphaOpt * npts;
	(*H_out_u)(1, 1) += alphaOpt * npts;
	(*H_out_u)(2, 2) += alphaOpt * npts;
	

	Types::Vec3f* tlog = UnmanagedClass::Se3Log(refToNew->_se3);
	(*b_out_u)[0] += (*tlog)[0] * alphaOpt*npts;
	(*b_out_u)[1] += (*tlog)[1] * alphaOpt*npts;
	(*b_out_u)[2] += (*tlog)[2] * alphaOpt*npts;
	delete tlog;
	
	
	H_out = gcnew Mat88f(H_out_u);
	b_out = GetManagedVector(b_out_u);
	delete b_out_u;

	auto EA = E->A;
	auto ENum = E->num;

	delete E;

	return gcnew array<float>(3) { EA, alphaEnergy, ENum };
}

void CoarseInitializerU::SwapJbBuffer()
{
	auto tmp = JbBuffer;
	JbBuffer = JbBuffer_new;
	JbBuffer_new = tmp;
	//std::swap<Types::Vec10f*>(JbBuffer, JbBuffer_new);
}

void CoarseInitializerU::FixAffineAndSE3Exp(bool fixAffine,DiagonalMatrix8f^ wM,Mat88f^ Hl,array<float>^ bl, SE3^ refToNew_current, array<float>^% out, SE3^% refToNew_new_out)
{
	auto bl_m = new Types::Vec8f();
	(*bl_m) << (float)bl[0], (float)bl[1], (float)bl[2], (float)bl[3], (float)bl[4], (float)bl[5], (float)bl[6], (float)bl[7];

	Types::Vec8f* out_u;
	Types::SE3* refToNew_new_out_u;

	UnmanagedClass::FixAffineAndSE3Exp(fixAffine, wM->_mat, Hl->_mat, bl_m, refToNew_current->_se3, &out_u, &refToNew_new_out_u);
	delete bl_m;
	auto r = gcnew array<float>(8);
	for (int i = 0; i < 8; i++)
	{
		r[i] = out_u->data()[i];
	}
	delete out_u;
	refToNew_new_out = gcnew SE3(refToNew_new_out_u);
	out = r;
}

float CoarseInitializerU::GetB(int i, array<float>^ inc)
{
	auto inc_u = new Types::Vec8f();
	(*inc_u) << (float)inc[0], (float)inc[1], (float)inc[2], (float)inc[3], (float)inc[4], (float)inc[5], (float)inc[6], (float)inc[7];

	float b = JbBuffer[i][8] + JbBuffer[i].head<8>().dot(*inc_u);
	delete inc_u;
	return b;
}

float CoarseInitializerU::GetStep(float b, int i, float lambda)
{
	return -b * JbBuffer[i][9] / (1 + lambda);
}

array<float>^ CoarseInitializerU::CalcEC(int lvl, bool snapped, float couplingWeight,
	array<int>^ numPoints, array<array<Point^>^>^ points
)
{
	if (!snapped) return gcnew array<float>(3) { 0, 0, numPoints[lvl] };
	AccumulatorX<2> E;
	E.initialize();
	int npts = numPoints[lvl];
	for (int i = 0; i < npts; i++)
	{
		Point^ point = points[lvl][i];
		if (!point->isGood_new) continue;
		float rOld = (point->idepth - point->iR);
		float rNew = (point->idepth_new - point->iR);
		E.updateNoWeight(Types::Vec2f(rOld*rOld, rNew*rNew));

		//printf("%f %f %f!\n", point->idepth, point->idepth_new, point->iR);
	}
	E.finish();

	//printf("ER: %f %f %f!\n", couplingWeight*E.A1m[0], couplingWeight*E.A1m[1], (float)E.num.numIn1m);
	return gcnew array<float>(3) { couplingWeight*E.A1m[0], couplingWeight*E.A1m[1], E.num };
}
