//#include "Mat.hpp"
//
//using namespace SharpSLAM::Core::Unmanaged;
//
//Mat88f::Mat88f()
//{}
#include "NumType.hpp"
#include "SE3.hpp"
#include <iostream>
#include "UnmanagedClass.cpp"
#pragma once

namespace SharpSLAM
{
	namespace Core
	{
		namespace Unmanaged
		{
			public ref class Mat33f
			{
			private:
				Types::Mat33f* _mat;

			public:
				Mat33f() {
					_mat = new Types::Mat33f();
				}
				Mat33f(Types::Mat33f* data)
				{
					_mat = data;
				}
				Mat33f(array<float>^ data) {
					if (data->Length != 9)
					{
						throw gcnew System::Exception("Array must have 9 items");
					}
					_mat = new Types::Mat33f();

					auto _0 = data[0];
					auto _1 = data[1];
					auto _2 = data[2];

					auto _3 = data[3];
					auto _4 = data[4];
					auto _5 = data[5];

					auto _6 = data[6];
					auto _7 = data[7];
					auto _8 = data[8];

					(*_mat) << _0, _1, _2, _3, _4, _5, _6, _7, _8;
				}
				!Mat33f()
				{
					delete _mat;
				}
				~Mat33f()
				{
					this->!Mat33f();
				}
				static Mat33f^ Zero()
				{
					return gcnew Mat33f(new Types::Mat33f(Types::Mat33f::Zero()));
				}
				Mat33f^ Inverse()
				{
					return gcnew Mat33f(new Types::Mat33f(_mat->inverse()));
				}
				static Mat33f^ Identity()
				{
					return gcnew Mat33f(new Types::Mat33f(Types::Mat33f::Identity()));
				}
				static array<float>^ operator *(Mat33f^ a, array<float>^ b)
				{
					if (b->Length != 3)
						throw gcnew System::Exception("Length of vector must be 3 for multiplying with 3x3 matrix!");

					Types::Vec3f vec;
					for (int i = 0; i < 3; i++)
					{
						vec[i] = b[i];
					}
					Types::Vec3f m = (*a->_mat) * vec;
					auto o = gcnew array<float>(3);
					for (int i = 0; i < 3; i++)
					{
						o[i] = m[i];
					}
					return o;
				}
				static Mat33f^ operator *(Mat33f^ a, Mat33f^ b)
				{
					return gcnew Mat33f(new Types::Mat33f((*a->_mat)*(*b->_mat)));
				}
				property float default[int, int]
				{
					float get(int x,int y)
					{
						return (*_mat)(x,y);
					}
					void set(int x, int y, float val)
					{
						(*_mat)(x,y) = val;
					}
				}
			};

			public ref class Mat33
			{
			internal:
				Types::Mat33* _mat;

			public:
				Mat33() {
					_mat = new Types::Mat33();
				}
				Mat33(array<double>^ data) {
					if (data->Length != 9)
					{
						throw gcnew System::Exception("Array must have 9 items");
					}
					_mat = new Types::Mat33();

					auto _0 = data[0];
					auto _1 = data[1];
					auto _2 = data[2];

					auto _3 = data[3];
					auto _4 = data[4];
					auto _5 = data[5];

					auto _6 = data[6];
					auto _7 = data[7];
					auto _8 = data[8];

					(*_mat) << _0, _1, _2, _3, _4, _5, _6, _7, _8;
				}
				Mat33(Types::Mat33* data)
				{
					_mat = data;
				}

				!Mat33()
				{
					delete _mat;
				}
				~Mat33()
				{
					this->!Mat33();
				}

				Mat33^ Inverse()
				{
					return gcnew Mat33(new Types::Mat33(_mat->inverse()));
				}

				Mat33f^ CastFloat()
				{
					return gcnew Mat33f(new Types::Mat33f(_mat->cast<float>()));
				}

				static Mat33^ operator *(Mat33^ a, Mat33^ b)
				{
					return gcnew Mat33(new Types::Mat33((*a->_mat)*(*b->_mat)));
				}

				property double default[int, int]
				{
					double get(int x,int y)
					{
					return (*_mat)(x,y);
					}
				}
			};

			public ref class Mat88f
			{
			internal:
				Types::Mat88f* _mat;

			public:
				Mat88f() {
					_mat = new Types::Mat88f();
				}
				Mat88f(Types::Mat88f* data)
				{
					_mat = data;
				}
				!Mat88f()
				{
					delete _mat;
				}
				~Mat88f()
				{
					this->!Mat88f();
				}
				Mat88f^ Clone()
				{
					return gcnew Mat88f(new Types::Mat88f((*_mat)));
				}
				void Print()
				{
					std::cout << (*_mat) << std::endl;
				}

				static array<float>^ operator *(array<float>^ a, Mat88f^ b)
				{
					if (a->Length != 8)
						throw gcnew System::Exception("Length of vector must be 8 for multiplying with 8x8 matrix!");

					Types::Vec8f vec;
					for (int i = 0; i < 8; i++)
					{
						vec[i] = a[i];
					}
					Types::Vec8f m = vec.transpose() * (*b->_mat);
					auto o = gcnew array<float>(8);
					for (int i = 0; i < 8; i++)
					{
						o[i] = m[i];
					}
					return o;
				}

				static Mat88f^ operator *(Mat88f^ a, float b)
				{
					return gcnew Mat88f(new Types::Mat88f((*a->_mat)*b));
				}
				static Mat88f^ operator -(Mat88f^ a, Mat88f^ b)
				{
					return gcnew Mat88f(UnmanagedClass::SubMat88f(a->_mat, b->_mat));
				}
				property float default[int, int]
				{
					float get(int x,int y)
					{
						return (*_mat)(x,y);
					}
					void set(int x, int y, float val)
					{
						(*_mat)(x,y) = val;
					}
				}
			};

			public ref class Mat88
			{
			internal:
				Types::Mat88* _mat;

			public:
				Mat88() {
					_mat = new Types::Mat88();
				}
				Mat88(Types::Mat88* data)
				{
					_mat = data;
				}
				!Mat88()
				{
					delete _mat;
				}
				~Mat88()
				{
					this->!Mat88();
				}
				Mat88^ Clone()
				{
					return gcnew Mat88(new Types::Mat88((*_mat)));
				}

				void CpyCol(int fromCol, int toCol)
				{
					_mat->col(toCol) = _mat->col(fromCol);
				}

				void CpyRow(int fromRow, int toRow)
				{
					_mat->row(toRow) = _mat->row(fromRow);
				}

				void Print()
				{
					std::cout << (*_mat) << std::endl;
				}

				void MinusAdjTransponseToTopLeftCorner(SE3^ se3)
				{
					_mat->topLeftCorner<6,6>() = -(*se3->_se3).Adj().transpose();
				}

				array<double>^ LdltSolve(array<double>^ b)
				{
					return LdltSolve(b, -1);
				}

				array<double>^ LdltSolve(array<double>^ b, int cutOff)
				{
					pin_ptr<double> bP = &b[0];
					Types::Vec8 bX(bP);
					array<double>^ ret;
					if (cutOff < 0)
					{
						ret = gcnew array<double>(8);
						Types::Vec8 r = _mat->ldlt().solve(-bX);
						for (int i = 0; i < 8; i++)
						{
							ret[i] = r[i];
						}
					}
					else
					{
						ret = gcnew array<double>(cutOff);
						Types::VecX r = _mat->topLeftCorner(cutOff, cutOff).ldlt().solve(-bX.head(cutOff));
						for (int i = 0; i < cutOff; i++)
						{
							ret[i] = r[i];
						}
					}
					return ret;
				}

				Mat88f^ CastToFloat()
				{
					return gcnew Mat88f(new Types::Mat88f(_mat->cast<float>()));
				}

				static Mat88^ Identity()
				{
					return gcnew Mat88(new Types::Mat88(Types::Mat88::Identity()));
				}

				static array<double>^ operator *(array<double>^ a, Mat88^ b)
				{
					if (a->Length != 8)
						throw gcnew System::Exception("Length of vector must be 8 for multiplying with 8x8 matrix!");

					Types::Vec8 vec;
					for (int i = 0; i < 8; i++)
					{
						vec[i] = a[i];
					}
					Types::Vec8 m = vec.transpose() * (*b->_mat);
					auto o = gcnew array<double>(8);
					for (int i = 0; i < 8; i++)
					{
						o[i] = m[i];
					}
					return o;
				}

				static Mat88^ operator *(Mat88^ a, double b)
				{
					return gcnew Mat88(new Types::Mat88((*a->_mat)*b));
				}
				property double default[int, int]
				{
					double get(int x,int y)
					{
						return (*_mat)(x,y);
					}
					void set(int x, int y, double val)
					{
						(*_mat)(x,y) = val;
					}
				}
			};

			

			public ref class Mat22f
			{
			internal:
				Types::Mat22f* _mat;

			public:
				Mat22f() {
					_mat = new Types::Mat22f();
				}
				Mat22f(Types::Mat22f* data)
				{
					_mat = data;
				}
				!Mat22f()
				{
					delete _mat;
				}
				~Mat22f()
				{
					this->!Mat22f();
				}
				Mat22f^ Clone()
				{
					return gcnew Mat22f(new Types::Mat22f((*_mat)));
				}

				void SetZero()
				{
					_mat->setZero();
				}

				void Print()
				{
					std::cout << (*_mat) << std::endl;
				}

				static array<float>^ operator *(Mat22f^ a, array<float>^ b)
				{
					if (b->Length != 2)
						throw gcnew System::Exception("Length of vector must be 1 for multiplying with 2x2 matrix!");

					Types::Vec2f vec;
					for (int i = 0; i < 2; i++)
					{
						vec[i] = b[i];
					}
					Types::Vec2f m = (*a->_mat) * vec;
					auto o = gcnew array<float>(2);
					for (int i = 0; i < 2; i++)
					{
						o[i] = m[i];
					}
					return o;
				}

				static array<float>^ operator *(array<float>^ a, Mat22f^ b)
				{
					if (a->Length != 2)
						throw gcnew System::Exception("Length of vector must be 2 for multiplying with 2x2 matrix!");

					Types::Vec2f vec;
					for (int i = 0; i < 2; i++)
					{
						vec[i] = a[i];
					}
					Types::Vec2f m = vec.transpose() * (*b->_mat);
					auto o = gcnew array<float>(2);
					for (int i = 0; i < 2; i++)
					{
						o[i] = m[i];
					}
					return o;
				}

				static Mat22f^ operator *(Mat22f^ a, float b)
				{
					return gcnew Mat22f(new Types::Mat22f((*a->_mat)*b));
				}
				property float default[int, int]
				{
					float get(int x,int y)
					{
						return (*_mat)(x,y);
					}
					void set(int x, int y, float val)
					{
						(*_mat)(x,y) = val;
					}
				}
			};

			public ref class MatXX
			{
			internal:
				Types::MatXX* _mat;

			public:
				MatXX() {
					_mat = new Types::MatXX();
				}
				MatXX(int r, int c) {
					_mat = new Types::MatXX(r,c);
				}
				MatXX(Types::MatXX* data)
				{
					_mat = data;
				}
				!MatXX()
				{
					delete _mat;
				}
				~MatXX()
				{
					this->!MatXX();
				}
				MatXX^ Clone()
				{
					return gcnew MatXX(new Types::MatXX((*_mat)));
				}
				void Print()
				{
					std::cout << (*_mat) << std::endl;
				}

				int Rows()
				{
					return _mat->rows();
				}
				
				int Cols()
				{
					return _mat->cols();
				}

				static MatXX^ Zero(int a, int b)
				{
					return gcnew MatXX(new Types::MatXX(Types::MatXX::Zero(a,b)));
				}

				static array<double>^ operator *(MatXX^ a, array<double>^ b)
				{
					auto aDim = a->Cols();
					if (b->Length != aDim)
						throw gcnew System::Exception("Length of vector must be same as matrix dimensions!");

					Types::VecX vec(aDim);
					for (int i = 0; i < aDim; i++)
					{
						vec[i] = b[i];
					}
					Types::VecX m = (*a->_mat) * vec;
					auto o = gcnew array<double>(aDim);
					for (int i = 0; i < aDim; i++)
					{
						o[i] = m[i];
					}
					return o;
				}
				static MatXX^ operator *(MatXX^ a, float b)
				{
					return gcnew MatXX(new Types::MatXX((*a->_mat)*b));
				}
				static MatXX^ operator *(MatXX^ a, double b)
				{
					return gcnew MatXX(new Types::MatXX((*a->_mat)*b));
				}
				static MatXX^ operator *(double a, MatXX^ b)
				{
					return gcnew MatXX(new Types::MatXX(a*(*b->_mat)));
				}
				static MatXX^ operator +(MatXX^ a, MatXX^ b)
				{
					return gcnew MatXX(new Types::MatXX((*a->_mat) + (*b->_mat)));
				}
				static MatXX^ operator -(MatXX^ a, MatXX^ b)
				{
					return gcnew MatXX(new Types::MatXX((*a->_mat) - (*b->_mat)));
				}
				property double default[int, int]
				{
					double get(int x,int y)
					{
						return (*_mat)(x,y);
					}
					void set(int x, int y, double val)
					{
						(*_mat)(x,y) = val;
					}
				}
			};

			public ref class DiagonalMatrix8f
			{
			internal:
				Eigen::DiagonalMatrix<float, 8>* _mat;
			public:
				DiagonalMatrix8f() {
					_mat = new Eigen::DiagonalMatrix<float, 8>;
				}
				DiagonalMatrix8f(Eigen::DiagonalMatrix<float, 8>* data)
				{
					_mat = data;
				}
				DiagonalMatrix8f(array<float>^ data) {
					if (data->Length != 8)
					{
						throw gcnew System::Exception("Array must have 8 items");
					}
					_mat = new Eigen::DiagonalMatrix<float, 8>;

					_mat->diagonal()[0] = data[0];
					_mat->diagonal()[1] = data[1];
					_mat->diagonal()[2] = data[2];

					_mat->diagonal()[3] = data[3];
					_mat->diagonal()[4] = data[4];
					_mat->diagonal()[5] = data[5];

					_mat->diagonal()[6] = data[6];
					_mat->diagonal()[7] = data[7];

				}
				!DiagonalMatrix8f()
				{
					delete _mat;
				}
				~DiagonalMatrix8f()
				{
					this->!DiagonalMatrix8f();
				}
				float GetDiag(int idx)
				{
					return _mat->diagonal()[idx];
				}
				void Print()
				{
					std::cout << (Types::Mat88f)(*_mat) << std::endl;
				}
				static Mat88f^ operator *(DiagonalMatrix8f^ a, Mat88f^ b)
				{
					return gcnew Mat88f(new Types::Mat88f((*a->_mat) * (*b->_mat)));
				}
				static Mat88f^ operator *(Mat88f^ a,DiagonalMatrix8f^ b)
				{
					return gcnew Mat88f(new Types::Mat88f((*a->_mat) * (*b->_mat)));
				}
			};
		}
	}
}
