﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using SharpSLAM.Configuration;
using SharpSLAM.Shared;

namespace SharpSLAM.Output.Files
{
    public class OutputFiles : Abstractions.Output
    {
        private Configuration.Configuration _config;

        public OutputFiles()
        {
        }

        public override void Initialize()
        {
            foreach (var f in _config.Output.OutputFiles)
            { deleteFolder(f.OutputPath); }
        }

        private void deleteFolder(string path)
        {
            var di = new DirectoryInfo(path);
            deleteFolder(di);
        }

        private void deleteFolder(DirectoryInfo dir)
        {
            if (dir.Exists)
            {
                foreach (var d in dir.GetDirectories())
                {
                    deleteFolder(d);
                }

                foreach (var f in dir.GetFiles())
                {
                    f.Delete();
                }
                dir.Delete(true);
            }
        }

        public override void SetCurrentConfiguration(Configuration.Configuration configuration)
        {
            _config = configuration;
        }

        protected override void updateImage(string category, MinimalImageB3 image)
        {
            save<MinimalImageB3, byte>(category, image);
        }

        protected override void updateImage(string category, MinimalImageB image)
        {
            save<MinimalImageB, byte>(category, image, 1);
        }

        protected override void updateImage(string category, MinimalImageF3 image)
        {
            save<MinimalImageF3, float>(category, image);
        }

        protected override void updateImage(string category, MinimalImageF image)
        {
            save<MinimalImageF, float>(category, image, 1);
        }

        protected override void updateCamPose(string category, CamPose pose)
        {
            if (pose.RenderAll)//not take current update
            {
                foreach (var o in _config.Output.OutputFiles)
                {
                    //var newCategory = $"{(string.IsNullOrEmpty(CategoryPrefix) ? "" : $"{CategoryPrefix}.")}{category}";
                    var newCategory = category.Replace(".HistoryKey", "").Replace(".HistoryOthers", "");

                    var point = pose.Points[0];//origin point

                    var path = preparePath(o, newCategory, "", ".csv", true);

                    using (var fs = new FileStream(path, FileMode.Append))
                    {
                        using (var sw = new StreamWriter(fs))
                        {
                            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
                            customCulture.NumberFormat.NumberDecimalSeparator = ".";

                            var s = string.Format(customCulture, "{4};{0};{1};{2};{3}", point[0], point[1], point[2], pose.IsKeyFrame, pose.ID);
                            sw.WriteLine(s);
                        }
                    }
                }
            }
        }


        private Dictionary<string, Tuple<string, string, Dictionary<double, Dictionary<string, double>>>> _data;
        protected override void updateData(string category, string serie, double x, double y, string labelX = null, string labelY = null)
        {
            foreach (var o in _config.Output.OutputFiles)
            {
                #region Store
                if (_data == null)
                    _data = new Dictionary<string, Tuple<string, string, Dictionary<double, Dictionary<string, double>>>>();

                Tuple<string, string, Dictionary<double, Dictionary<string, double>>> cCategory;
                if (_data.ContainsKey(category))
                    cCategory = _data[category];
                else
                {
                    cCategory = new Tuple<string, string, Dictionary<double, Dictionary<string, double>>>(labelX, labelY, new Dictionary<double, Dictionary<string, double>>());
                    _data.Add(category, cCategory);
                }

                Dictionary<string, double> cSerie;
                if (cCategory.Item3.ContainsKey(x))
                    cSerie = cCategory.Item3[x];
                else
                {
                    cSerie = new Dictionary<string, double>();
                    cCategory.Item3.Add(x, cSerie);
                }

                if (cSerie.ContainsKey(serie))
                {
                    cSerie[serie] = y;
                }
                else
                {
                    cSerie.Add(serie, y);
                }
                #endregion

                if (Flush)
                {
                    #region Save

                    System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
                    customCulture.NumberFormat.NumberDecimalSeparator = ".";

                    //foreach (var cC in _data)
                    var cC = new KeyValuePair<string, Tuple<string, string, Dictionary<double, Dictionary<string, double>>>>(category, _data[category]); //update only file with current category
                    {
                        var path = preparePath(o, cC.Key, "", ".csv", true);

                        //var fi = new FileInfo(path);
                        //int i = 0;
                        //while (fi.Exists)
                        //{
                        //    Path.Combine(Path.GetDirectoryName(path), Path.GetFileNameWithoutExtension(path) + i + Path.GetExtension(path));
                        //    i++;
                        //}

                        using (var fs = new FileStream(path, FileMode.OpenOrCreate))
                        {
                            fs.SetLength(0);
                            using (var sw = new StreamWriter(fs))
                            {
                                string cLabelX = cC.Value.Item1;
                                string cLabelY = cC.Value.Item2;

                                sw.Write(cLabelX);

                                var seriesPos = new Dictionary<string, int>();
                                int counter = 0;
                                foreach (var cXkv in cC.Value.Item3)
                                {
                                    foreach (var cSkv in cXkv.Value)
                                    {
                                        if (!seriesPos.ContainsKey(cSkv.Key))
                                        {
                                            sw.Write(";");
                                            sw.Write(string.IsNullOrEmpty(cLabelY)?cSkv.Key:$"{cLabelY} - {cSkv.Key}");
                                            seriesPos.Add(cSkv.Key, counter);
                                            counter++;
                                        }
                                    }
                                }

                                sw.WriteLine();

                                foreach (var cXkv in cC.Value.Item3)
                                {
                                    var cX = cXkv.Key;
                                    sw.Write(string.Format(customCulture, "{0}", cX));

                                    counter = 0;


                                    var lx = cXkv.Value.ToList();
                                    lx.Sort((a, b) => { return (seriesPos[a.Key] - seriesPos[b.Key]); });
                                    foreach (var cSkv in lx)
                                    {
                                        var cS = cSkv.Key;
                                        var cY = cSkv.Value;

                                        while (seriesPos[cS] != counter)
                                        {
                                            sw.Write(";");
                                            counter++;
                                        }
                                        sw.Write(";");
                                        sw.Write(string.Format(customCulture, "{0}", cY));
                                        counter++;
                                    }
                                    while (counter != seriesPos.Count)
                                    {
                                        sw.Write(";");
                                        counter++;
                                    }
                                    sw.WriteLine();
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
        }

        protected override void updatePointsCloud(string category, PointsCloud pointsCloud)
        {

        }


        private void save<T, S>(string category, T image, int numberOfChannels = 3) where T : MinimalImage<S>
        {
            foreach (var o in _config.Output.OutputFiles)
            {
                Array data = image.Data;

                if (typeof(S) == typeof(float))
                {
                    byte[] output = new byte[image.Width * image.Height * image.ChannelsCount];
                    float[] maxColor = new float[image.ChannelsCount];
                    for (int i = 0; i < data.Length; i++)
                    {
                        if (i < image.ChannelsCount)
                        {
                            maxColor[i] = (float)data.GetValue(i);
                        }
                        else
                        {
                            var chI = i % image.ChannelsCount;
                            var val = (float)data.GetValue(i);
                            if (maxColor[chI] < val)
                            {
                                maxColor[chI] = val;
                            }
                        }
                    }
#if (PAR)
                Parallel.For(0, image.Width * image.Height* image.ChannelsCount, (i) =>
#else
                    for (int i = 0; i < image.Width * image.Height * image.ChannelsCount; i++)
#endif
                    {
                        var chI = i % image.ChannelsCount;
                        output[i] = (byte)Math.Round((((float)data.GetValue(i)) / maxColor[chI]) * 255, 0, MidpointRounding.AwayFromZero);
                    }
#if (PAR)
                                );
#endif
                    data = output;


                }

                PixelFormat pf;

                if (numberOfChannels == 3)
                {
                    pf = PixelFormat.Format24bppRgb;
                }
                else if (numberOfChannels == 1)
                {
                    pf = PixelFormat.Format8bppIndexed;
                }
                else
                {
                    throw new NotImplementedException("Only 3 and 1 channel images are supported");
                }

                var bitmap = DrawingHelper.MakeBitmap((byte[])data, image.Width, image.Height, pf);

                if (numberOfChannels == 1)
                {
                    ColorPalette colorPalette = bitmap.Palette;
                    for (int i = 0; i < colorPalette.Entries.Length; i++)
                    {
                        colorPalette.Entries[i] = Color.FromArgb(i, i, i);
                    }
                    bitmap.Palette = colorPalette;
                }

                var path = preparePath(o, category, image.ID, ".png");

                bitmap.Save(path, ImageFormat.Png);
            }
        }

        private string preparePath(Configuration.OutputFiles config, string cat, string mediaId, string ext, bool sameCatSamePath = false, bool usePrefix = true)
        {
            var fullCat = $"{(string.IsNullOrEmpty(CategoryPrefix) || !usePrefix ? "" : $"{CategoryPrefix}.")}{cat}";

            string fileName = "";
            var pathParts = new List<string>();


            pathParts.Add(config.OutputPath);
            if (config.Structure == OutputFilesStructureTypes.Flat)
            {
                if (sameCatSamePath)
                    fileName = fullCat;
                else
                    fileName = $"{fullCat}_{mediaId}";
            }
            else if (config.Structure == OutputFilesStructureTypes.ID_Flat)
            {
                pathParts.Add(mediaId);
                if (sameCatSamePath)
                    fileName = fullCat;
                else
                    fileName = $"{fullCat}_{mediaId}";
            }
            else if (config.Structure == OutputFilesStructureTypes.Category_ID || config.Structure == OutputFilesStructureTypes.ID_Category)
            {
                if (config.Structure == OutputFilesStructureTypes.ID_Category)
                {
                    pathParts.Add(mediaId);
                }

                pathParts.AddRange(fullCat.Split('.'));
                fileName = mediaId;

                if ((sameCatSamePath || config.Structure == OutputFilesStructureTypes.ID_Category) && pathParts.Count > 0)
                {
                    fileName = pathParts[pathParts.Count - 1];
                    if (!string.IsNullOrEmpty(mediaId))
                        fileName = $"{fileName}_{mediaId}";
                    pathParts.RemoveAt(pathParts.Count - 1);
                }
            }

            var filesCount = 0;
            var dirPath = Path.Combine(pathParts.ToArray());
            if (!Directory.Exists(dirPath))
            {
                Directory.CreateDirectory(dirPath);
            }
            else if (!sameCatSamePath)//needed only if you need unique name
            {
                filesCount = Directory.GetFiles(dirPath).Count();
            }
            if (sameCatSamePath)
                fileName = $"{fileName}{ext}";//make sure always same for same cat - not unique
            else
                fileName = $"{fileName}_{filesCount}{ext}";//make sure will be unique
            return Path.Combine(dirPath, fileName);
        }
    }
}
