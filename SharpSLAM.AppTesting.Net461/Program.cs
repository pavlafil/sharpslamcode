﻿using CNTK;
using SharpSLAM.Core;
using SharpSLAM.Input;
using SharpSLAM.Output.Abstractions;
using SharpSLAM.Output.Viewer.WPF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SharpSLAM.AppTesting.Net461
{
    class Program
    {
        static void Main(string[] args)
        {
            string configuration = null;
#if PUBLISH
            configuration=@".\configurationPublish.json";
#else
            configuration = @".\configurationOthers.json";
#endif

            //Load configuration
            var conff = Configuration.Configuration.FromFile(configuration);

            //Disable debug logging outputs if it is not debug
#if !DEBUG
            conff.Output.Log.Debug = false;
#endif

            //Create output
            var output = new OutputAggregator(new Output.Abstractions.Output[] { new OutputViewerWpf(), new Output.Files.OutputFiles() });
            output.SetCurrentConfiguration(conff);
            output.Initialize();

            //Create image reader
            var imr = new ImageReader(conff, output);

            //Create core context
            var coreContext = new CoreContext(conff, output);
            //Prepare core - solving system
            var core = new Core.Core(coreContext);

            var dt = DateTime.Now;






            #region Training
            var originalIn = conff.Input;
            foreach (var tr in conff.Core.Training.TrainingSets)
            {
                //temporary fake input
                conff.Input = tr;

                var trReader = new ImageReader(conff, output) { Plot = false };

                output.CategoryPrefix = "01 Train";

                //string originalPrefixBackup = null;

                core.Train(trReader.GetAllImagesAndExposuresFLazy(()=> {
                    //originalPrefixBackup = output.CategoryPrefix;
                    //output.CategoryPrefix = "01 Train.01 Preprocessing";
                },()=>{
                  //  output.CategoryPrefix = originalPrefixBackup;
                }));
                output.CategoryPrefix = null;
            }
            conff.Input = originalIn;
            #endregion









            int start = conff.Input.Start;
            int end = conff.Input.End.HasValue ? conff.Input.End.Value : imr.Count - 1;
            if (end < start || end >= imr.Count)
                end = imr.Count - 1;

            if (conff.Core.FramesPerSecondCompute <= 0)
            {
                for (int i = start; i <= end;)
                {
                    OneFrame(ref i, output, imr, core, ref dt);
                }
            }
            else
            {
                var time = 1000 / conff.Core.FramesPerSecondCompute;
                int i = start;
                Timer t = null;
                t = new Timer((o) => {
                    if (i <= end)
                    {
                        OneFrame(ref i, output, imr, core, ref dt);
                    }
                    else
                    {
                        t.Dispose();
                    }
                }, null, 0, time);
            }
        }

        private static void OneFrame(ref int i, Output.Abstractions.Output output, ImageReader imr, Core.Core core, ref DateTime dt)
        {
            output.CategoryPrefix = "02 Preprocessing";
            //Get one frame
            var image = imr.GetImageAndExposureF(i);

            output.CategoryPrefix = "03 Core";
            core.AddActiveFrame(image);
            output.CategoryPrefix = null;

            var dtn = DateTime.Now;
            Console.WriteLine($"\n=== Frame {i} {dtn - dt}\n");
            dt = dtn;

            //GC.Collect();
            //GC.WaitForPendingFinalizers();
            i++;
        }
    }
}
