﻿using System.Collections.Generic;

namespace SharpSLAM.Configuration
{
    public class Training : HasParent<Core>
    {
        public override void Build()
        {
            foreach (var i in TrainingSets)
            {
                i.SetSource(Parent.Parent.Input);
            }
            foreach (var i in EvaluateSets)
            {
                i.SetSource(Parent.Parent.Input);
            }
            base.Build();
        }

        public List<InputWithSource> TrainingSets { get; set; }
        public List<InputWithSource> EvaluateSets { get; set; }
    }
}