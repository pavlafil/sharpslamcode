﻿using System;

namespace SharpSLAM.Configuration
{
    public enum CameraModles
    {
        KannalaBrandt,
        RadTan,
        EquiDistant,
        FOV,
        Pinhole
    }

    public abstract class CameraModel
    {
        /// <summary>
        /// Focal length x
        /// </summary>
        public float fx { get; set; } = 1;

        /// <summary>
        /// Focal length y
        /// </summary>
        public float fy { get; set; } = 1;

        /// <summary>
        /// Principal point offset x
        /// </summary>
        public float cx { get; set; } = 0;

        /// <summary>
        /// Principal point offset y
        /// </summary>
        public float cy { get; set; } = 0;

        public void Distort(float[] x, float[] y, float[] oX, float[] oY, CameraModel outputCameraModel)
        {
            if (x.Length == oX.Length && y.Length == oY.Length && x.Length == y.Length)
            {
                for (int i = 0; i < x.Length; i++)
                {
                    Distort(x[i], y[i], out oX[i], out oY[i], outputCameraModel);
                }
            }
            else
            {
                throw new Exception("Dimensions of input and output array must be same!");
            }
        }

        /// <summary>
        /// Distort to this model
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="oX"></param>
        /// <param name="oY"></param>
        /// <param name="outputCameraModel"></param>
        public abstract void Distort(float x, float y, out float oX, out float oY, CameraModel outputCameraModel);
    }
    public class FOVCameraModel : CameraModel
    {
        public float dist { get; set; } = 0;

        public override void Distort(float x, float y, out float oX, out float oY, CameraModel outputCameraModel)
        {
            float ix = (x - outputCameraModel.cx) / outputCameraModel.fx;
            float iy = (y - outputCameraModel.cy) / outputCameraModel.fy;
            float r = (float)Math.Sqrt(ix * ix + iy * iy);

            float d2t = (float)(2.0f * Math.Tan(dist / 2.0f));
            float fac = (r == 0 || dist == 0) ? 1 : (float)(Math.Atan(r * d2t) / (dist * r));

            oX = fx * fac * ix + cx;
            oY = fy * fac * iy + cy;
        }
    }

    public class PinholeCameraModel : CameraModel
    {
        public override void Distort(float x, float y, out float oX, out float oY, CameraModel outputCameraModel)
        {
            float ix = (x - outputCameraModel.cx) / outputCameraModel.fx;
            float iy = (y - outputCameraModel.cy) / outputCameraModel.fy;

            oX = fx  * ix + cx;
            oY = fy  * iy + cy;
        }
    }
}
