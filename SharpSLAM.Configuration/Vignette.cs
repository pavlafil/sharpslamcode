﻿using SharpSLAM.Shared;
using System;
using System.Drawing;
using System.Linq;

namespace SharpSLAM.Configuration
{
    public class Vignette : HasParent<PhotometricCalibration>
    {
        public float[] Values { get; set; }
        public float[] InvertValues { get; set; }

        public override void Build()
        {
            base.Build();

            if (!string.IsNullOrEmpty(_fileSource))
            {
                #region LoadImage Input project can not be used because dependency cycle
                var image = new Bitmap(Parent.Parent.WorkingDirectory + _fileSource);
                {
                    var grayScale = DrawingHelper.MakeGrayscale3(image);
                    image.Dispose();
                    image = grayScale;
                }
                int height = image.Height, width = image.Width;
                byte[] bytes = DrawingHelper.MakeArray(image);
                image.Dispose();
                byte[] output = new byte[height * width];
#if (PAR)
                Parallel.For(0, height * width, (i) =>
                {
                   var color = data[i * 3];
                    output[i] = color;
                });
#else
                for (int i = 0; i < height * width; i++)
                {
                    var color = bytes[i * 3];
                    output[i] = color;
                }
#endif
                bytes = null;


                var img = new MinimalImageB(null, width, height, output);
                ////var imr = new ImageReader(value,null,null,true);
                ////var img = imr.GetImage(0);
                #endregion

                //Not validated!! not enought info in configuration file need to be done later by user
                //if (vm8->w != w || vm8->h != h)
                //{
                //    printf("PhotometricUndistorter: Invalid vignette image size! got %d x %d, expected %d x %d\n",
                //            vm8->w, vm8->h, w, h);
                //    if (vm16 != 0) delete vm16;
                //    if (vm8 != 0) delete vm8;
                //    return;
                //}

                float max = img.Data.Max();
                var size = img.Width * img.Height;

                Values = new float[size];
                InvertValues = new float[size];

                for (int i = 0; i < size; i++)
                {
                    Values[i] = img.Data[i] / max;
                    InvertValues[i] = 1f / Values[i];
                }
            }
        }

        private string _fileSource;
        public string FromFile
        {
            set { _fileSource = value; }
        }
    }
}