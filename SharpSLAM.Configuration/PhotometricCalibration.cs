﻿namespace SharpSLAM.Configuration
{
    public class PhotometricCalibration : HasParent<Input>
    {
        /// <summary>
        /// Gamma photometric correction
        /// </summary>
        public Gamma Gamma { get; set; }

        /// <summary>
        /// Vignette photometric correction
        /// </summary>
        public Vignette Vignette { get; set; }
    }
}