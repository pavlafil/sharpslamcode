﻿namespace SharpSLAM.Configuration
{
    public enum PausingMode
    {
        NoPause=0,
        OnKeyFrame=1,
        OnEachFrame = 3,
        Debugging=8
    }
}