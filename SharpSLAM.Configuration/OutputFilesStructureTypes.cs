﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharpSLAM.Configuration
{
    public enum OutputFilesStructureTypes
    {
        Category_ID,
        ID_Category,
        ID_Flat,
        Flat
    }
}
