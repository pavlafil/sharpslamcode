﻿using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace SharpSLAM.Configuration
{
    public class Timing : HasParent<Input>
    {
        public Dictionary<string, double> Times { get; set; }
        public Dictionary<string, float> Exposures { get; set; }


        public override void Build()
        {
            base.Build();

            if (!string.IsNullOrEmpty(_fileSource))
            {
                Times = new Dictionary<string, double>();
                Exposures = new Dictionary<string, float>();

                using (var fstream = File.OpenRead(Parent.WorkingDirectory + _fileSource))
                {
                    using (var freader = new StreamReader(fstream))
                    {
                        while (!freader.EndOfStream)
                        {
                            var line = freader.ReadLine();

                            var v = line.Replace("\t", " ").Replace('.', ',').Split(' ');
                            Times.Add(v[0], double.Parse(v[1]));
                            Exposures.Add(v[0], float.Parse(v[2]));
                        }
                    }
                }
            }
        }


        private string _fileSource;
        public string FromFile
        {
            set { _fileSource = value; }
        }
    }
}