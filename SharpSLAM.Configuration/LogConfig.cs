﻿using SharpSLAM.Shared;

namespace SharpSLAM.Configuration
{
    public class LogConfig
    {
        public bool Log
        {
            get
            {
                return Shared.LogConfig.LogOutput;
            }
            set
            {
                Shared.LogConfig.LogOutput = value;
            }
        }

        public bool Debug
        {
            get
            {
                return Shared.LogConfig.DebugOutput;
            }
            set
            {
                Shared.LogConfig.DebugOutput = value;
            }
        }
        public bool Info
        {
            get
            {
                return Shared.LogConfig.InfoOutput;
            }
            set
            {
                Shared.LogConfig.InfoOutput = value;
            }
        }
        public bool Error
        {
            get
            {
                return Shared.LogConfig.ErrorOutput;
            }
            set
            {
                Shared.LogConfig.ErrorOutput = value;
            }
        }
    }
}