﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharpSLAM.Configuration
{
    public class InputWithSource : Input
    {
        private Input _source;

        public void SetSource(Input source)
        {
            _source = source;
        }

        private T getter<T>(T local, T source) where T : class
        {
            return local != null ? local : source;
        }


        /// <summary>
        /// Path prefix
        /// </summary>
        private string _workingDirectory;
        public override string WorkingDirectory { get => getter(_workingDirectory, _source.WorkingDirectory); set => _workingDirectory = value; }

        /// <summary>
        /// Index of frame where program will end (null is default - no end)
        /// </summary>
        private int? _end;
        public override int? End { get => _end.HasValue ? _end.Value : _source.End; set => _end = value; }

        /// <summary>
        /// Path to folder with images
        /// </summary>
        private string _imagesPath;
        public override string ImagesPath { get => getter(_imagesPath, _source.ImagesPath); set => _imagesPath = value; }

        /// <summary>
        /// Geometric camera calibration info
        /// </summary>
        private GeometricCalibration _geometricCalibration;
        public override GeometricCalibration GeometricCalibration { get =>getter(_geometricCalibration, _source.GeometricCalibration); set => _geometricCalibration = value; }

        /// <summary>
        /// Photometric camera calibration info
        /// </summary>
        private PhotometricCalibration _photometricCalibration;
        public override PhotometricCalibration PhotometricCalibration { get => getter(_photometricCalibration, _source.PhotometricCalibration); set => _photometricCalibration = value; }

        /// <summary>
        /// Times & exposures
        /// From Image id to time and exposure
        /// </summary>
        private Timing _timing;
        public override Timing Timing { get => getter(_timing, _source.Timing); set => _timing = value; }

        /// <summary>
        /// Ground truth of position
        /// From Time to position
        /// </summary>
        private GroundTruth _groundTruth;
        public override GroundTruth GroundTruth { get => getter(_groundTruth, _source.GroundTruth); set => _groundTruth = value; }
    }
}
