﻿namespace SharpSLAM.Configuration
{
    public class GeneticAlg
    {
        public bool Enable { get; set; }
        public bool EnableStateBrute { get; set; }
        public string InterestFrameID { get; set; }
        public bool ContinueAfterStarterFrame { get; set; } = false;
    }
}