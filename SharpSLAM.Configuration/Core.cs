﻿using Newtonsoft.Json;

namespace SharpSLAM.Configuration
{
    public class Core : HasParent<Configuration>
    {
        public Training Training { get; set; }

        public GeneticAlg GeneticAlg { get; set; }

        [JsonProperty(TypeNameHandling = TypeNameHandling.Auto)]
        public PixelSelector PixelSelector { get; set; }

        #region Top level (main etc..)
        /// <summary>
        /// 0=Without waiting, Other=number of frames per second to try to achieve (if algorithm is not fast enough try as fast as possible)
        /// </summary>
        public int FramesPerSecondCompute { get; set; } = 0;

        private PausingMode _pausingMode=PausingMode.NoPause;
        /// <summary>
        /// Define when should program automatically wait for user
        /// </summary>
        public PausingMode PausingMode { get => _pausingMode; set {
                _pausingMode = value;
#if !DEBUG
                if ((_pausingMode& PausingMode.Debugging) != PausingMode.NoPause)
                {
                    _pausingMode &= ~PausingMode.Debugging;
                }
#endif
            }
        }
        
        #endregion

        #region BeforePublishToOutput
        public int PointCloudMode { get; set; } =0; //fromConfigFile
        public double ScaledVarTH { get; set; } = 0.001;//1e-10,1e10
        public double AbsVarTH { get; set; } = 0.001;//1e-10,1e10
        public double MinBS { get; set; } = 0.1;//0,1
        public int Sparsity { get; set; } = 1;//1,20
        #endregion

        #region Core
        public enum SOLVERTYPE
        {
            SVD = 1,
            ORTHOGONALIZE_SYSTEM = 2,
            ORTHOGONALIZE_POINTMARG = 4,
            ORTHOGONALIZE_FULL = 8,
            SVD_CUT7 = 16,
            REMOVE_POSEPRIOR = 32,
            USE_GN = 64,
            FIX_LAMBDA = 128,
            ORTHOGONALIZE_X = 256,
            MOMENTUM = 512,
            STEPMOMENTUM = 1024,
            ORTHOGONALIZE_X_LATER = 2048,
        }

        /// <summary>
        /// Pyramid levels (default/maximum)
        /// </summary>
        public int MaxPyrLevels { get; set; } = 6;

        public float KeyframesPerSecond { get; set; } = 0;   // if !=0, takes a fixed number of KF per second.

        public bool RealTimeMaxKF { get; set; } = false; // if true, takes as many KF's as possible (will break the system if the camera stays stationary)

        public float MaxShiftWeightT { get; set; } = 0.04f * (640 + 480);
        public float MaxShiftWeightR { get; set; } = 0.0f * (640 + 480);
        public float MaxShiftWeightRT { get; set; } = 0.02f * (640 + 480);
        public int KfGlobalWeight { get; set; } = 1;  // general weight on threshold, the larger the more KF's are taken (e.g., 2 = double the amount of KF's).
        public float MaxAffineWeight { get; set; } = 2;



        public float FreeDebugParam1 { get; set; } = 1;
        public float FreeDebugParam5 { get; set; } = 1;



        public float MinGradHistCut { get; set; } = 0.5f;
        public float MinGradHistAdd { get; set; } = 7;
        public float GradDownWeightPerLevel { get; set; } = 0.75f;
        public float HuberTH { get; set; } = 9; // Huber Threshold
        public bool SelectDirectionDistribution { get; set; } = true;


        public int PatternNumber { get { return StaticPatternNum[PatternIndex]; } }
        public int PatternPadding { get { return StaticPatternPadding[PatternIndex]; } }
        public int PatternIndex { get; set; } = 8;
        public float MargWeightFac { get; set; } = 0.5f * 0.5f;// factor on hessian when marginalizing, to account for inaccurate linearization points.
        public int OutlierTH { get; set; } = 12 * 12;

        public float ScaleXIROT { get; set; } = 1f;
        public float ScaleXITRANS { get; set; } = 0.5f;
        public float ScaleA { get; set; } = 10f;
        public float ScaleB { get; set; } = 1000f;
        public float ScaleF { get; set; } = 50.0f;
        public float ScaleC { get; set; } = 50.0f;
        public float ScaleIdepth { get; set; } = 1.0f;

        public float ScaleXITRANSInverse { get { return 1f / ScaleXITRANS; } }
        public float ScaleXIROTInverse { get { return 1f / ScaleXIROT; } }
        public float ScaleAInverse { get { return 1f / ScaleA; } }
        public float ScaleBInverse { get { return 1f / ScaleB; } }
        public float ScaleFInverse { get { return 1f / ScaleF; } }
        public float ScaleCInverse { get { return 1f / ScaleC; } }
        public float ScaleIdepthInverse { get { return 1f / ScaleIdepth; } }


        public int SolverMode { get; set; } = (int)(SOLVERTYPE.FIX_LAMBDA | SOLVERTYPE.ORTHOGONALIZE_X_LATER);
        public double SolverModeDelta { get; set; } = 0.00001;
        public bool ForceAceptStep { get; set; } = true;

        public float InitialTransPrior { get; set; } = 1e10f;
        public float InitialRotPrior { get; set; } = 1e11f;
        public float InitialAffAPrior { get; set; } = 1e14f;
        public float InitialAffBPrior { get; set; } = 1e14f;
        public float InitialCalibHessian { get; set; } = 5e9f;
        public float AffineOptModeA { get; set; } = 1e12f; //-1: fix. >=0: optimize (with prior, if > 0).
        public float AffineOptModeB { get; set; } = 1e8f; //-1: fix. >=0: optimize (with prior, if > 0).
        public int Cpars { get; set; } = 4;

        /* initial hessian values to fix unobservable dimensions / priors on affine lighting parameters.*/
        public float IdepthFixPrior { get; set; } = 50 * 50;
        public float IdepthFixPriorMargFac { get; set; } = 600 * 600;

        public float OutlierTHSumComponent { get; set; } = 50 * 50;// higher -> less strong gradient-based reweighting .

        public int OverallEnergyTHWeight { get; set; } = 1;

        public float DesiredImmatureDensity { get; set; } = 1500; // immature points per frame
        public float DesiredPointDensity { get; set; } = 2000; // aimed total points in the active window.

        public bool GoStepByStep { get; set; } = true;//false;

        public bool RenderDisplayCoarseTrackingFull { get; set; } = true;// false;

        /* when to re-track a frame */
        public float ReTrackThreshold { get; set; } = 1.5f; // (larger = re-track more often)

        /* settings controling initial immature point tracking */
        public float MaxPixSearch { get; set; } = 0.027f;        //max length of the ep. line segment searched during immature point tracking. relative to image resolution.
        public float MinTraceQuality { get; set; } = 3;
        public float TraceSlackInterval { get; set; } = 1.5f;    //if pixel-interval is smaller than this, leave it be.
        public float TraceMinImprovementFactor { get; set; } = 2f;// if pixel-interval is smaller than this, leave it be.
        public float TraceStepsize { get; set; } = 1.0f;         //stepsize for initial discrete search.
        public int MaxResPerPoint { get; set; } = 8;
        public int GNItsOnPointActivation { get; set; } = 3;
        public int MinTraceTestRadius { get; set; } = 2;
        public int TraceGNIterations { get; set; } = 3;             // max # GN iterations
        public float TraceGNThreshold { get; set; } = 0.1f;         // GN stop after this stepsize.
        public float TraceExtraSlackOnTH { get; set; } = 1.2f;      // for energy-based outlier check, be slightly more relaxed by this factor.




        public int MinFrameAge { get; set; } = 1; //frame has to be at leas this old to be marginalized otherwise stays in wndows unles it has no points
        public int MinFrames { get; set; } = 5; // min frames in window.
        public int MaxFrames { get; set; } = 7; // max frames in window.
        public int MaxOptIterations { get; set; } = 6; // max GN iterations.
        public int MinOptIterations { get; set; } = 1; // min GN iterations.
        public float MinPointsRemaining { get; set; } = 0.05f;  // marg a frame if less than X% points remain.
        public float MaxLogAffFacInWindow { get; set; } = 0.7f; // marg a frame if factor between intensities to current frame is larger than 1/X or X.
        public float ThOptIterations { get; set; } = 1.2f; // factor on break threshold for GN iteration (larger = break earlier)



        /* some thresholds on when to activate / marginalize points */
        public float MinIdepthHAct { get; set; } = 100;
        public float MinIdepthHMarg { get; set; } = 50;


        // for benchmarking different undistortion settings
        //float benchmarkSetting_fxfyfac = 0;
        //int benchmarkSetting_width = 0;
        //int benchmarkSetting_height = 0;
        //float benchmark_varNoise = 0;
        //float benchmark_varBlurNoise = 0;
        public float BenchmarkInitializerSlackFactor { get; set; } = 1;

        //int benchmark_noiseGridsize = 3;




        // parameters controlling adaptive energy threshold computation.
        //float setting_frameEnergyTHConstWeight = 0.5;
        public float FrameEnergyTHN { get; set; } = 0.7f;
        public float FrameEnergyTHFacMedian { get; set; } = 1.5f;
        public float FrameEnergyTHConstWeight { get; set; } = 1;
        public float CoarseCutoffTH { get; set; } = 20;


        /* require some minimum number of residuals for a point to become valid */
        public int MinGoodActiveResForMarg { get; set; } = 3;
        public int MinGoodResForMarg { get; set; } = 4;



        //Constants
        public static readonly int[,,] StaticPatterns = new int[10, 40, 2]
        {
        {{0,0},       {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100},	// .
		 {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100},
         {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100},
         {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}},

        {{0,-1},      {-1,0},      {0,0},       {1,0},       {0,1},       {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100},	// +
		 {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100},
         {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100},
         {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}},

        {{-1,-1},     {1,1},       {0,0},       {-1,1},      {1,-1},      {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100},	// x
		 {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100},
         {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100},
         {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}},

        {{-1,-1},     {-1,0},      {-1,1},      {-1,0},      {0,0},       {0,1},       {1,-1},      {1,0},       {1,1},       {-100,-100},	// full-tight
		 {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100},
         {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100},
         {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}},

        {{0,-2},      {-1,-1},     {1,-1},      {-2,0},      {0,0},       {2,0},       {-1,1},      {1,1},       {0,2},       {-100,-100},	// full-spread-9
		 {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100},
         {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100},
         {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}},

        {{0,-2},      {-1,-1},     {1,-1},      {-2,0},      {0,0},       {2,0},       {-1,1},      {1,1},       {0,2},       {-2,-2},   // full-spread-13
		 {-2,2},      {2,-2},      {2,2},       {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100},
         {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100},
         {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}},

        {{-2,-2},     {-2,-1}, {-2,-0}, {-2,1}, {-2,2}, {-1,-2}, {-1,-1}, {-1,-0}, {-1,1}, {-1,2}, 										// full-25
		 {-0,-2},     {-0,-1}, {-0,-0}, {-0,1}, {-0,2}, {+1,-2}, {+1,-1}, {+1,-0}, {+1,1}, {+1,2},
         {+2,-2},     {+2,-1}, {+2,-0}, {+2,1}, {+2,2}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100},
         {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}},

        {{0,-2},      {-1,-1},     {1,-1},      {-2,0},      {0,0},       {2,0},       {-1,1},      {1,1},       {0,2},       {-2,-2},   // full-spread-21
		 {-2,2},      {2,-2},      {2,2},       {-3,-1},     {-3,1},      {3,-1},      {3,1},       {1,-3},      {-1,-3},     {1,3},
         {-1,3},      {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100},
         {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}},

        {{0,-2},      {-1,-1},     {1,-1},      {-2,0},      {0,0},       {2,0},       {-1,1},      {0,2},       {-100,-100}, {-100,-100},	// 8 for SSE efficiency
		 {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100},
         {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100},
         {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}, {-100,-100}},

        {{-4,-4},     {-4,-2}, {-4,-0}, {-4,2}, {-4,4}, {-2,-4}, {-2,-2}, {-2,-0}, {-2,2}, {-2,4}, 										// full-45-SPREAD
		 {-0,-4},     {-0,-2}, {-0,-0}, {-0,2}, {-0,4}, {+2,-4}, {+2,-2}, {+2,-0}, {+2,2}, {+2,4},
         {+4,-4},     {+4,-2}, {+4,-0}, {+4,2}, {+4,4}, {-200,-200}, {-200,-200}, {-200,-200}, {-200,-200}, {-200,-200},
         {-200,-200}, {-200,-200}, {-200,-200}, {-200,-200}, {-200,-200}, {-200,-200}, {-200,-200}, {-200,-200}, {-200,-200}, {-200,-200}},
};
        public static readonly int[] StaticPatternNum = new int[10] { 1, 5, 5, 9, 9, 13, 25, 21, 8, 25 };
        public static readonly int[] StaticPatternPadding = new int[10] { 1, 1, 1, 1, 2, 2, 2, 3, 2, 4 };
    }
    #endregion
}