﻿using Newtonsoft.Json;
using System.IO;
using System.Linq;
using System.Text;

namespace SharpSLAM.Configuration
{
    public class Configuration : HasParent<Configuration>
    {
        public static Configuration FromFile(string file)
        {
            var conf = JsonConvert.DeserializeObject<Configuration>(File.ReadAllText(file), new JsonSerializerSettings()
            {
                SerializationBinder = new KnownSuffixBinder()
            });
            conf.Build();
            return conf;
        }

        /// <summary>
        /// Input configuration (files, camera settings, etc...)
        /// </summary>
        public Input Input { get; set; } = new Input();

        /// <summary>
        /// Core configuration (modify computation)
        /// </summary>
        public Core Core { get; set; } = new Core();

        /// <summary>
        /// Output configuration (how data will be presented)
        /// </summary>
        public Output Output { get; set; } = new Output();
    }
}
