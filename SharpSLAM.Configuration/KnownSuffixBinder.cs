﻿using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SharpSLAM.Configuration
{
    public class KnownSuffixBinder : ISerializationBinder
    {
        private IList<string> _knownSuffixes { get; set; } = new List<string>(){ "CameraModel","PixelSelector" };

        public Type BindToType(string assemblyName, string typeName)
        {
            var lAssemblyName = assemblyName;
            if (string.IsNullOrEmpty(lAssemblyName))
                lAssemblyName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
            var suffix = _knownSuffixes.FirstOrDefault(x => Type.GetType($"{lAssemblyName}.{typeName}{x}")!=null);

            if (suffix == null)
                suffix = "";

            return Type.GetType($"{lAssemblyName}.{typeName}{suffix}");
        }

        public void BindToName(Type serializedType, out string assemblyName, out string typeName)
        {
            assemblyName = null;

            var suffix = _knownSuffixes.FirstOrDefault(x => serializedType.Name.EndsWith(x));

            if (suffix == null)
            {
                typeName = serializedType.Name;
            }
            else
            {
                typeName = serializedType.Name.Remove(serializedType.Name.IndexOf(suffix));
            }
        }
    }
}
