﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SharpSLAM.Configuration
{
    public class GroundTruth : HasParent<Input>
    {
        public Dictionary<double, double[]> Truth { get; set; }


        public override void Build()
        {
            base.Build();

            if (!string.IsNullOrEmpty(_fileSource))
            {
                Truth = new Dictionary<double, double[]>();

                using (var fstream = File.OpenRead(Parent.WorkingDirectory + _fileSource))
                {
                    using (var freader = new StreamReader(fstream))
                    {
                        while (!freader.EndOfStream)
                        {
                            var line = freader.ReadLine();
                            var v = line.Replace("\t", " ").Replace('.', ',').Split(' ').Select(x => double.Parse(x)).ToList();
                            if (!v.Contains(double.NaN))
                            {
                                Truth.Add(v[0], v.Skip(1).ToArray());
                            }
                        }
                    }
                }
            }
        }


        private string _fileSource;
        public string FromFile
        {
            set { _fileSource = value; }
        }
    }

}