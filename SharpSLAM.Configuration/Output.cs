﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharpSLAM.Configuration
{
    public class Output : HasParent<Configuration>
    {
        /// <summary>
        /// Logging settings (adapter to static classes)
        /// </summary>
        public LogConfig Log { get; set; } = new LogConfig();

        ///// <summary>
        ///// 0=Without waiting, -1 Wait after each keyframe, -2 Wait after each frame, Other=number of frame per second
        ///// </summary>
        //public int FramesPerSecond { get; set; }


        public List<OutputFiles> OutputFiles { get; set; }
        
    }

}
