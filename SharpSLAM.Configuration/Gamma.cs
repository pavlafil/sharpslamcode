﻿using SharpSLAM.Shared;
using System.Collections.Generic;
using System.IO;

namespace SharpSLAM.Configuration
{
    public class Gamma : HasParent<PhotometricCalibration>
    {
        private float[] _values;
        private float[] _invertValues;

        public bool WeightsPixelSelect { get; set; } = true;

        /// <summary>
        /// Value as restricted to 0 .. 255
        /// </summary>
        public float[] Values
        {
            get
            {
                //if (_values==null)
                //{
                //    for (int i = 0; i < 256; i++) _values[i] = i;
                //}
                return _values;
            }

            set
            {
                if (value != null)
                {
                    for (int i = 0; i < value.Length - 1; i++)
                    {
                        if (value[i + 1] <= value[i])
                        {
                            Log.LogError("PhotometricUndistorter: Gamm is invalid! it has to be strictly increasing, but it isnt!");
                            return;
                        }
                    }

                    float min = value[0];
                    float max = value[value.Length - 1];
                    for (int i = 0; i < value.Length; i++) value[i] = 255.0f * (value[i] - min) / (max - min);         // make it to 0..255 => 0..255.
                }
                _values = value;

                //Invert

                _invertValues = new float[_values.Length];
                for (int i = 1; i < _values.Length - 1; i++)
                {
                    // find val, such that G[val] = i.
                    // I dont care about speed for this, so do it the stupid way.

                    for (int s = 1; s < _values.Length - 1; s++)
                    {
                        if (_values[s] <= i && _values[s + 1] >= i)
                        {
                            _invertValues[i] = s + (i - _values[s]) / (_values[s + 1] - _values[s]);
                            break;
                        }
                    }
                }
                _invertValues[0] = 0;
                _invertValues[_invertValues.Length - 1] = 255;

            }
        }

        public float[] InvertedValues { get => _invertValues; }

        public override void Build()
        {
            base.Build();

            if (!string.IsNullOrEmpty(_fileSource))
            {
                using (var fstream = File.OpenRead(Parent.Parent.WorkingDirectory + _fileSource))
                {
                    using (var freader = new StreamReader(fstream))
                    {
                        var v = freader.ReadToEnd().Replace("\t", " ").Replace('.', ',').Split(' ');
                        var l = new List<float>();
                        foreach (var i in v)
                        {
                            float num;
                            if (float.TryParse(i, out num))
                            {
                                l.Add(num);
                            }
                        }

                        if (l.Count < 256)
                        {
                            Log.LogError($"PhotometricUndistorter: invalid format! got {l.Count} expected at least 256!");
                        }
                        else
                        {
                            Values = l.ToArray();
                        }
                    }
                }
            }
        }

        private string _fileSource;
        public string FromFile
        {
            set { _fileSource = value; }
        }
    }
}