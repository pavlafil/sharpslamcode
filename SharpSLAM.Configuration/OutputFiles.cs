﻿namespace SharpSLAM.Configuration
{
    public class OutputFiles
    {
        public string OutputPath { get; set; }
        public OutputFilesStructureTypes Structure { get; set; } = OutputFilesStructureTypes.Category_ID;
    }
}