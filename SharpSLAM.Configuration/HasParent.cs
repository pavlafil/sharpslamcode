﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace SharpSLAM.Configuration
{
    public class HasParent<P>
    {
        public virtual void Build()
        {
            var cType = this.GetType();
            var properties = cType.GetProperties();

            foreach (var p in properties)
            {
                if (p.Name != "Parent")//do not want go up in tree
                {
                    var pTypeE = typeof(IEnumerable<>).MakeGenericType(new Type[] { typeof(HasParent<object>) });
                    if (pTypeE.IsAssignableFrom(p.PropertyType))
                    {
                        var instance = p.GetValue(this) as IEnumerable<HasParent<object>>;
                        foreach (var i in instance)
                        {
                            i.Parent = this;
                            i.Build();
                        }
                    }

                    var pType = typeof(HasParent<>).MakeGenericType(new Type[] { typeof(object) });
                    process(pType, p);
                    pType = typeof(HasParent<>).MakeGenericType(new Type[] { cType });
                    process(pType, p);
                }
            }
        }

        private void process(Type pType, PropertyInfo propertyInfo)
        {
            if (pType.IsAssignableFrom(propertyInfo.PropertyType))
            {
                object propInst = propertyInfo.GetValue(this);
                if (propInst != null)
                {
                    propInst.GetType().GetProperty("Parent").SetValue(propInst, this);
                    propInst.GetType().GetMethod("Build").Invoke(propInst, null);
                }
            }
        }

        public P Parent { get; protected set; }

    }
}
