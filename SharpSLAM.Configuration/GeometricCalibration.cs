﻿using System;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using SharpSLAM.Shared;

namespace SharpSLAM.Configuration
{
    //public enum OutputCalibrationType
    //{
    //    NoRectification,
    //    Full,
    //    Crop,
    //    Custom
    //}

    public class GeometricCalibration : HasParent<Input>
    {
        [JsonProperty(TypeNameHandling = TypeNameHandling.Auto)]
        public CameraModel CameraModel { get; set; }
        public int OriginalHeight { get; set; }
        public int OriginalWidth { get; set; }

        [JsonProperty(TypeNameHandling = TypeNameHandling.Auto)]
        public CameraModel OutputCameraModel { get; set; }
        //public OutputCalibrationType OutputCalibrationType { get; set; }
        //public float[] OutputCalibration { get; set; }

        public int OutputHeight { get; set; }
        public int OutputWidth { get; set; }


        /// <summary>
        /// Camera models
        ///  Kalibr supports the following projection models:
        ///   pinhole camera model(pinhole)
        ///   (intrinsics vector: [fu fv pu pv])
        ///   omnidirectional camera model(omni)
        ///   (intrinsics vector: [xi fu fv pu pv])
        ///   double sphere camera model(ds)
        ///   (intrinsics vector: [xi alpha fu fv pu pv])
        ///   extended unified camera model(eucm)
        ///   (intrinsics vector: [alpha beta fu fv pu pv])

        ///  The intrinsics vector contains all parameters for the model:
        ///   fu, fv: focal-length
        ///   pu, pv: principal point
        ///   xi: mirror parameter(only omni)
        ///   xi, alpha: double sphere model parameters(only ds)
        ///   alpha, beta: extended unified model parameters(only eucm)

        /// Distortion models
        ///  Kalibr supports the following distortion models:
        ///   radial-tangential(radtan)
        ///   (distortion_coeffs: [k1 k2 r1 r2])
        ///   equidistant(equi)
        ///   (distortion_coeffs: [k1 k2 k3 k4])
        ///   fov(fov)
        ///   (distortion_coeffs: [w])
        ///   none(none)
        ///   (distortion_coeffs: [])
        ///   
        /// https://github.com/ethz-asl/kalibr/wiki/supported-models
        /// </summary>
        public override void Build()
        {
            base.Build();

            if (!string.IsNullOrEmpty(_fileSource))
            {
                //Read first line of calibration

                //Backward compatibility
                //- 8x float = RadTan (OpenCV) camera model
                //- 5x float = ATAN camera model, special case if last float is 0 then PINHOLE camera model

                //- KannalaBrandt %f %f %f %f %f %f %f %f
                //- RadTan %f %f %f %f %f %f %f %f
                //- EquiDistant %f %f %f %f %f %f %f %f
                //- FOV %f %f %f %f %f
                //- Pinhole %f %f %f %f %f

                using (var fstream = File.OpenRead(Parent.WorkingDirectory + _fileSource))
                {
                    using (var freader = new StreamReader(fstream))
                    {
                        //l1
                        var line = freader.ReadLine().Replace("\t", " ").Replace(".", ",");

                        CameraModles? cameraModel = null;
                        float[] firstLineArgs;

                        {
                            var firstLine = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                            if (Enum.GetNames(typeof(CameraModles)).Contains(firstLine.First()))
                            {
                                cameraModel = (CameraModles)Enum.Parse(typeof(CameraModles), firstLine.First());
                                firstLine.RemoveAt(0);
                            }
                            firstLineArgs = firstLine.Select(x => float.Parse(x)).ToArray();
                        }

                        if (!cameraModel.HasValue)
                        {
                            if (firstLineArgs.Count() == 8)
                                cameraModel = CameraModles.RadTan;
                            else if (firstLineArgs.Count() == 5)
                            {
                                if (firstLineArgs.Last() == 0f)
                                    cameraModel = CameraModles.Pinhole;
                                else
                                    cameraModel = CameraModles.FOV;
                            }
                        }
                        if (!cameraModel.HasValue)
                        {
                            throw new Exception("Camera model was not recognized!");
                        }

                        switch (cameraModel.Value)
                        {
                            case (CameraModles.FOV):
                                CameraModel = new FOVCameraModel() { dist = firstLineArgs[4] };
                                break;

                            default: throw new NotImplementedException("Camera model not implemented!");
                        }

                        //l2 original weight & height
                        line = freader.ReadLine();
                        var originalDimensions = line.Split(' ').Select(x => int.Parse(x)).ToArray();
                        OriginalWidth = originalDimensions[0];
                        OriginalHeight = originalDimensions[1];
                        //----------------------------

                        if (firstLineArgs[2] < 1 && firstLineArgs[3] < 1)
                        {
                            var logString = $"Found fx={firstLineArgs[0]}, fy={firstLineArgs[1]}, cx={firstLineArgs[2]}, cy={firstLineArgs[3]}.\nI'm assuming this is the \"relative\" calibration file format, and will rescale this by image width / height to ";

                            // rescale and substract 0.5 offset.
                            // the 0.5 is because I'm assuming the calibration is given such that the pixel at (0,0)
                            // contains the integral over intensity over [0,0]-[1,1], whereas I assume the pixel (0,0)
                            // to contain a sample of the intensity at [0,0], which is best approximated by the integral over
                            // [-0.5,-0.5]-[0.5,0.5]. Thus, the shift by -0.5.
                            firstLineArgs[0] = firstLineArgs[0] * OriginalWidth;
                            firstLineArgs[1] = firstLineArgs[1] * OriginalHeight;
                            firstLineArgs[2] = firstLineArgs[2] * OriginalWidth - 0.5f;
                            firstLineArgs[3] = firstLineArgs[3] * OriginalHeight - 0.5f;

                            logString += $"fx={firstLineArgs[0]}, fy={firstLineArgs[1]}, cx={firstLineArgs[2]}, cy={firstLineArgs[3]}.";
                            Log.LogInfo(logString);
                        }

                        CameraModel.fx = firstLineArgs[0];
                        CameraModel.fy = firstLineArgs[1];
                        CameraModel.cx = firstLineArgs[2];
                        CameraModel.cy = firstLineArgs[3];

                        // l3
                        line = freader.ReadLine().Trim().ToLowerInvariant().Replace(".", ",");

                        // l4
                        var line4 = freader.ReadLine().Trim();
                        int[] outDimensions = null;
                        try
                        {
                            outDimensions = line4.Split(' ').Select(x => int.Parse(x)).ToArray();
                        }
                        catch (Exception)
                        {
                        }
                        if (outDimensions != null && outDimensions.Count() == 2)
                        {
                            OutputWidth = outDimensions[0];
                            OutputHeight = outDimensions[1];
                            //if (benchmarkSetting_width != 0)
                            //{
                            //    w = benchmarkSetting_width;
                            //    if (outputCalibration[0] == -3)
                            //        outputCalibration[0] = -1;  // crop instead of none, since probably resolution changed.
                            //}
                            //if (benchmarkSetting_height != 0)
                            //{
                            //    h = benchmarkSetting_height;
                            //    if (outputCalibration[0] == -3)
                            //        outputCalibration[0] = -1;  // crop instead of none, since probably resolution changed.
                            //}
                            Log.LogInfo($"Output resolution: {OutputWidth} {OutputHeight}");
                        }
                        else
                        {
                            //OutputCalibrationType = OutputCalibrationType.NoRectification;
                            Log.LogInfo("Out: Failed to Read Output resolution ... not rectifying");
                        }

                        // l3
                        if (line == "crop")
                        {
                            //OutputCalibrationType = OutputCalibrationType.Crop;
                            OutputCameraModel = makeOptimalKCrop();
                            Log.LogInfo("Out: Rectify Crop");
                        }
                        else if (line == "full")
                        {
                            //OutputCalibrationType = OutputCalibrationType.Full;
                            //TODO
                            Log.LogInfo("Out: Rectify Full");
                        }
                        else if (line == "none")
                        {
                            //OutputCalibrationType = OutputCalibrationType.NoRectification;
                            //TODO
                            Log.LogInfo("Out: No Rectification");
                        }
                        else
                        {
                            float[] args = null;
                            try
                            {
                                args = line.Split(' ').Select(x => float.Parse(x)).ToArray();
                            }
                            catch (Exception)
                            { }

                            if (args != null && args.Count() == 5)
                            {
                                //OutputCalibrationType = OutputCalibrationType.Custom;
                                //OutputCalibration = args;

                                if (args[2] < 1 && args[3] < 1)
                                {
                                    var logString = $"Found fx={args[0]}, fy={args[1]}, cx={args[2]}, cy={args[3]}.\nI'm assuming this is the \"relative\" calibration file format, and will rescale this by image width / height to ";

                                    // rescale and substract 0.5 offset.
                                    // the 0.5 is because I'm assuming the calibration is given such that the pixel at (0,0)
                                    // contains the integral over intensity over [0,0]-[1,1], whereas I assume the pixel (0,0)
                                    // to contain a sample of the intensity at [0,0], which is best approximated by the integral over
                                    // [-0.5,-0.5]-[0.5,0.5]. Thus, the shift by -0.5.
                                    args[0] = args[0] * OutputWidth;
                                    args[1] = args[1] * OutputHeight;
                                    args[2] = args[2] * OutputWidth - 0.5f;
                                    args[3] = args[3] * OutputHeight - 0.5f;

                                    logString += $"fx={args[0]}, fy={args[1]}, cx={args[2]}, cy={args[3]}.";
                                    Log.LogInfo(logString);
                                }

                                OutputCameraModel = new FOVCameraModel() { fx = args[0], fy = args[1], cx = args[2], cy = args[3], dist = args[4] };
                                Log.LogInfo(string.Format("Out: {0} {1} {2} {3} {4}", args[0], args[1], args[2], args[3], args[4]));
                            }
                            else
                            {
                                //OutputCalibrationType = OutputCalibrationType.NoRectification;
                                Log.LogInfo("Out: Failed to Read Output pars ... not rectifying.");
                            }
                        }
                    }
                }
            }
        }

        private CameraModel makeOptimalKCrop()
        {
            var cm = new FOVCameraModel();

            Log.LogInfo("Finding CROP optimal new model!");
            //K.setIdentity();

            // 1. stretch the center lines as far as possible, to get initial coarse quess.
            float[] tgX = new float[100000];
            float[] tgY = new float[100000];
            float minX = 0;
            float maxX = 0;
            float minY = 0;
            float maxY = 0;

            for (int x = 0; x < 100000; x++)
            {
                tgX[x] = (x - 50000.0f) / 10000.0f; tgY[x] = 0;
                CameraModel.Distort(tgX[x], tgY[x], out tgX[x], out tgY[x], cm);
            }

            for (int x = 0; x < 100000; x++)
            {
                if (tgX[x] > 0 && tgX[x] < OriginalWidth - 1)
                {
                    if (minX == 0) minX = (x - 50000.0f) / 10000.0f;
                    maxX = (x - 50000.0f) / 10000.0f;
                }
            }
            for (int y = 0; y < 100000; y++)
            {
                tgY[y] = (y - 50000.0f) / 10000.0f; tgX[y] = 0;
                CameraModel.Distort(tgX[y], tgY[y], out tgX[y], out tgY[y], cm);
            }

            for (int y = 0; y < 100000; y++)
            {
                if (tgY[y] > 0 && tgY[y] < OriginalHeight - 1)
                {
                    if (minY == 0) minY = (y - 50000.0f) / 10000.0f;
                    maxY = (y - 50000.0f) / 10000.0f;
                }
            }
            tgX = null;
            tgY = null;

            minX *= 1.01f;
            maxX *= 1.01f;
            minY *= 1.01f;
            maxY *= 1.01f;



            Log.LogInfo(string.Format("Initial range: x: {0} - {1}; y: {2} - {3}!", minX, maxX, minY, maxY));


            var remapX = new float[OutputHeight * OutputWidth];
            var remapY = new float[OutputHeight * OutputWidth];

            // 2. while there are invalid pixels at the border: shrink square at the side that has invalid pixels,
            // if several to choose from, shrink the wider dimension.
            bool oobLeft = true, oobRight = true, oobTop = true, oobBottom = true;
            int iteration = 0;
            while (oobLeft || oobRight || oobTop || oobBottom)
            {
                oobLeft = oobRight = oobTop = oobBottom = false;
                for (int y = 0; y < OutputHeight; y++)
                {
                    remapX[y * 2] = minX;
                    remapX[y * 2 + 1] = maxX;
                    remapY[y * 2] = remapY[y * 2 + 1] = minY + (maxY - minY) * (float)y / ((float)OutputHeight - 1.0f);

                }
                for (int y = 0; y < 2 * OutputHeight; y++)
                {
                    CameraModel.Distort(remapX[y], remapY[y], out remapX[y], out remapY[y], cm);
                }
                for (int y = 0; y < OutputHeight; y++)
                {
                    if (!(remapX[2 * y] > 0 && remapX[2 * y] < OriginalWidth - 1))
                        oobLeft = true;
                    if (!(remapX[2 * y + 1] > 0 && remapX[2 * y + 1] < OriginalWidth - 1))
                        oobRight = true;
                }



                for (int x = 0; x < OutputWidth; x++)
                {
                    remapY[x * 2] = minY;
                    remapY[x * 2 + 1] = maxY;
                    remapX[x * 2] = remapX[x * 2 + 1] = minX + (maxX - minX) * (float)x / ((float)OutputWidth - 1.0f);
                }
                for (int x = 0; x < 2 * OutputWidth; x++)
                {
                    CameraModel.Distort(remapX[x], remapY[x], out remapX[x], out remapY[x], cm);
                }

                for (int x = 0; x < OutputWidth; x++)
                {
                    if (!(remapY[2 * x] > 0 && remapY[2 * x] < OriginalHeight - 1))
                        oobTop = true;
                    if (!(remapY[2 * x + 1] > 0 && remapY[2 * x + 1] < OriginalHeight - 1))
                        oobBottom = true;
                }


                if ((oobLeft || oobRight) && (oobTop || oobBottom))
                {
                    if ((maxX - minX) > (maxY - minY))
                        oobBottom = oobTop = false; // only shrink left/right
                    else
                        oobLeft = oobRight = false; // only shrink top/bottom
                }

                if (oobLeft) minX *= 0.995f;
                if (oobRight) maxX *= 0.995f;
                if (oobTop) minY *= 0.995f;
                if (oobBottom) maxY *= 0.995f;

                iteration++;


                Log.LogInfo(string.Format("Iteration {0}: range: x: {1} - {2}; y: {3} - {4}", iteration, minX, maxX, minY, maxY));
                if (iteration > 500)
                {
                    throw new Exception("FAILED TO COMPUTE GOOD CAMERA MATRIX - SOMETHING IS SERIOUSLY WRONG. ABORTING");
                }
            }

            cm.fx = ((float)OutputWidth - 1.0f) / (maxX - minX);
            cm.fy = ((float)OutputHeight - 1.0f) / (maxY - minY);
            cm.cx = -minX * cm.fx;
            cm.cy = -minY * cm.fy;
            return cm;
        }

        private string _fileSource;
        public string FromFile
        {
            set { _fileSource = value; }
        }
    }
}
