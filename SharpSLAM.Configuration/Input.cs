﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharpSLAM.Configuration
{
    public class Input : HasParent<object>
    {
        /// <summary>
        /// Path prefix
        /// </summary>
        public virtual string WorkingDirectory { get; set; } = "";

        /// <summary>
        /// Index of frame where program will start (0 is default)
        /// </summary>
        public virtual int Start { get; set; } = 0;

        /// <summary>
        /// Index of frame where program will end (null is default - no end)
        /// </summary>
        public virtual int? End { get; set; }

        /// <summary>
        /// Path to folder with images
        /// </summary>
        public virtual string ImagesPath { get; set; }

        /// <summary>
        /// Geometric camera calibration info
        /// </summary>
        public virtual GeometricCalibration GeometricCalibration { get; set; }

        /// <summary>
        /// Photometric camera calibration info
        /// </summary>
        public virtual PhotometricCalibration PhotometricCalibration { get; set; }

        /// <summary>
        /// Times & exposures
        /// From Image id to time and exposure
        /// </summary>
        public virtual Timing Timing { get; set; }

        /// <summary>
        /// Ground truth of position
        /// From Time to position
        /// </summary>
        public virtual GroundTruth GroundTruth { get; set; }
    }
}
