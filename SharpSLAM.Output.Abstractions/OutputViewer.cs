﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using SharpSLAM.Configuration;
using SharpSLAM.Shared;

namespace SharpSLAM.Output.Abstractions
{
    public class OutputViewer : Output
    {
        public Configuration.Configuration Configuration { get; private set; }

        private Dictionary<string, OutputViewerContextImgeHolder> _images { get; set; }
        private ICollection<string> _activeCategories;
        private Action<string, OutputContextMedium, Action<string, OutputContextMedium>> _asyncHandle;
        private Action<string, OutputViewerContextImgeHolder> _draw;
        private Action<string, OutputViewerContextImgeHolder> _updated;
        private Action<IDictionary<string, OutputViewerContextImgeHolder>> _updateCategories;
        private bool _initialized = false;

        public OutputViewer()
        {
            _images = new Dictionary<string, OutputViewerContextImgeHolder>();
        }

        public void InitializeViewer(Action<string, OutputContextMedium, Action<string, OutputContextMedium>> asyncHandle, Action<string, OutputViewerContextImgeHolder> draw, Action<string, OutputViewerContextImgeHolder> updated, Action<IDictionary<string, OutputViewerContextImgeHolder>> updateCategories)
        {
            _asyncHandle = asyncHandle;
            _draw = draw;
            _updated = updated;
            _updateCategories = updateCategories;
            _initialized = true;
            _updateCategories(_images);
        }

        public void DeInitializeViewer()
        {
            _asyncHandle = null;
            _draw = null;
            _updateCategories = null;
            _initialized = false;
        }

        #region Core
        //Region called by core thread must be as fast as possible - ideally only trigger async operation
        private void Update(string category, OutputContextMedium media)
        {
            if (_initialized)
            {
                _asyncHandle(CategoryPrefix == null ? category : $"{CategoryPrefix}.{category}", media, AddImage);
            }
        }

        protected override void updateImage(string category, MinimalImageB3 image)
        {
            Update(category, image);
        }

        protected override void updateImage(string category, MinimalImageB image)
        {
            Update(category, image);
        }

        protected override void updateImage(string category, MinimalImageF image)
        {
            Update(category, image);
        }

        protected override void updateImage(string category, MinimalImageF3 image)
        {
            Update(category, image);
        }

        protected override void updateCamPose(string category, CamPose pose)
        {
            Update(category, pose);
        }

        protected override void updatePointsCloud(string category, PointsCloud pointsCloud)
        {
            Update(category, pointsCloud);
        }

        protected override void updateData(string category, string serie, double x, double y, string labelX = null, string labelY = null)
        {
            //TODO
        }

        public override void SetCurrentConfiguration(Configuration.Configuration configuration)
        {
            Configuration = configuration;
        }

        //for core but only for viewers
        public bool IsCategoryActive(string category)
        {
            return _activeCategories == null ? false : _activeCategories.Contains($"{CategoryPrefix}.{category}");
        }

        public event Func<bool> ViewerClosedEvent;
        public bool ViewerClosed()
        {
            var h = ViewerClosedEvent;
            if (h != null)
                return h();
            else return false;
        }
        #endregion

        #region Viewer
        private void AddImage(string category, OutputContextMedium medium)
        {
            if (medium == null)
            {
                _images.Remove(category);
                _updateCategories(_images);
                _updated(category, null);
            }
            else
            {
                //always in render thread - can be slow
                if (_images.ContainsKey(category))
                {
                    var wrapper = _images[category];
                    wrapper.Clear();
                    wrapper.Fill(medium);
                    _updated(category, wrapper);
                }
                else
                {
                    var wrapper = new OutputViewerContextImgeHolder();
                    wrapper.Fill(medium);
                    _images.Add(category, wrapper);
                    _updateCategories(_images);
                    _updated(category, wrapper);
                }
            }
        }

        public void DrawIfDirty(string category)
        {
            if (_images.ContainsKey(category) && _images[category].DirtyFlag && _draw!=null)
            {
                _images[category].DirtyFlag = false;
                _draw(category, _images[category]);
            }
        }

        public void SetDirty(string category)
        {
            if (_images.ContainsKey(category))
            {
                _images[category].DirtyFlag = true;
            }
        }

        public void SetActiveCategires()
        {
            if (_activeCategories != null)
            {
                foreach (var i in _activeCategories)
                {
                    DrawIfDirty(i);
                }
            }
        }

        public void SetActiveCategires(ICollection<string> categories)
        {
            _activeCategories = categories;
            SetActiveCategires();
        }
        #endregion
    }
}
