﻿using SharpSLAM.Shared;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace SharpSLAM.Output.Abstractions
{
    public class Output
    {
        #region Core
        public bool DisableUpdates { get; set; } = false;
        public virtual bool Flush { get; set; } = true;
        public virtual string CategoryPrefix { get; set; }

        private void checkAndUpdate(Action action)
        {
            if (!DisableUpdates)
                action();
        }

        public virtual void Initialize(){ }

        #region Updates
        public void UpdateImage(string category, MinimalImageB3 image) { checkAndUpdate(()=>updateImage(category,image)); }

        public void UpdateImage(string category, MinimalImageB image) { checkAndUpdate(() => updateImage(category, image)); }

        public void UpdateImage(string category, MinimalImageF image) { checkAndUpdate(() => updateImage(category, image)); }

        public void UpdateImage(string category, MinimalImageF3 image) { checkAndUpdate(() => updateImage(category, image)); }

        public void UpdateCamPose(string category, CamPose pose) { checkAndUpdate(() => updateCamPose(category, pose)); }

        public void UpdateData(string category, string serie, double x, double y, string labelX = null, string labelY = null) { checkAndUpdate(() => updateData(category, serie,x,y,labelX,labelY)); }

        public void UpdatePointsCloud(string category, PointsCloud pointsCloud) { checkAndUpdate(() => updatePointsCloud(category, pointsCloud)); }


        protected virtual void updateImage(string category, MinimalImageB3 image) { throw new NotImplementedException(); }
        protected virtual void updateImage(string category, MinimalImageB image) { throw new NotImplementedException(); }
        protected virtual void updateImage(string category, MinimalImageF image) { throw new NotImplementedException(); }
        protected virtual void updateImage(string category, MinimalImageF3 image) { throw new NotImplementedException(); }
        protected virtual void updateCamPose(string category, CamPose pose) { throw new NotImplementedException(); }
        protected virtual void updateData(string category, string serie, double x, double y, string labelX=null, string labelY=null) { throw new NotImplementedException(); }
        protected virtual void updatePointsCloud(string category, PointsCloud pointsCloud) { throw new NotImplementedException(); }
        #endregion


        public virtual void SetCurrentConfiguration(Configuration.Configuration configuration) { throw new NotImplementedException(); }
        #endregion
    }
}
