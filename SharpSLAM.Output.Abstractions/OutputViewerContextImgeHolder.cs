﻿using SharpSLAM.Shared;
using System.Drawing;

namespace SharpSLAM.Output.Abstractions
{
    public class OutputViewerContextImgeHolder
    {
        public OutputContextMedium Medium { get; private set; }
        public bool DirtyFlag { get; set; } = true;

        public void Clear()
        {
            Medium = null;
            DirtyFlag = true;
        }
        public void Fill(OutputContextMedium medium)
        {
            Medium = medium;
            DirtyFlag = true;
        }
    }
}
