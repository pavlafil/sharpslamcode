﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using SharpSLAM.Configuration;
using SharpSLAM.Shared;

namespace SharpSLAM.Output.Abstractions
{
    public class OutputAggregator : Output
    {
        private IEnumerable<Output> _outputs;

        public override string CategoryPrefix
        {
            get {
                if (_outputs != null)
                {
                    foreach (var i in _outputs)
                    {
                        return i.CategoryPrefix;
                    }
                }
                return null;
            }
            set {
                apply(x => x.CategoryPrefix = value);
            }
        }

        public override bool Flush
        {
            get
            {
                if (_outputs != null)
                {
                    foreach (var i in _outputs)
                    {
                        return i.Flush;
                    }
                }
                return false;
            }
            set
            {
                apply(x => x.Flush = value);
            }
        }

        public override void Initialize()
        {
            apply(x => x.Initialize());
        }

        public OutputAggregator(IEnumerable<Output> outputs)
        {
            _outputs = outputs;
        }
        public override void SetCurrentConfiguration(Configuration.Configuration configuration)
        {
            apply(x => { x.SetCurrentConfiguration(configuration); });
        }

        protected override void updateCamPose(string category, CamPose pose)
        {
            apply(x => { x.UpdateCamPose(category, pose); });
        }

        protected override void updateData(string category, string serie, double x, double y, string labelX = null, string labelY = null)
        {
            apply(z => { z.UpdateData(category, serie, x, y, labelX, labelY); });
        }

        protected override void updatePointsCloud(string category, PointsCloud pointsCloud)
        {
            apply(x => { x.UpdatePointsCloud(category, pointsCloud); });
        }

        protected override void updateImage(string category, MinimalImageB image)
        {
            apply(x => { x.UpdateImage(category, image); });
        }

        protected override void updateImage(string category, MinimalImageB3 image)
        {
            apply(x => { x.UpdateImage(category, image); });
        }

        protected override void updateImage(string category, MinimalImageF image)
        {
            apply(x => { x.UpdateImage(category, image); });
        }

        protected override void updateImage(string category, MinimalImageF3 image)
        {
            apply(x => { x.UpdateImage(category, image); });
        }

        private void apply(Action<Output> action)
        {
            foreach (var i in _outputs)
            {
                action(i);
            }
        }
    }
}
