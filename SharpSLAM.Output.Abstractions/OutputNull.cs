﻿using System;
using System.Collections.Generic;
using System.Text;
using SharpSLAM.Configuration;
using SharpSLAM.Shared;

namespace SharpSLAM.Output.Abstractions
{
    /// <summary>
    /// To have possibility define output that can be used in core but do nothing
    /// </summary>
    public class OutputNull : Output
    {
        public override void SetCurrentConfiguration(Configuration.Configuration configuration)
        {
        }

        protected override void updateCamPose(string category, CamPose pose)
        {
        }

        protected override void updateImage(string category, MinimalImageB3 image)
        {
        }

        protected override void updateImage(string category, MinimalImageB image)
        {
        }

        protected override void updateImage(string category, MinimalImageF image)
        {
        }

        protected override void updateImage(string category, MinimalImageF3 image)
        {
        }

        protected override void updatePointsCloud(string category, PointsCloud pointsCloud)
        {
        }
    }
}
