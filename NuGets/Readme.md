# Packages
You are not supposed use this `NuGet` packages. Preferred method is download packages from some online source. This folder can be used in emergency if package or version is not available.

You can somehow set this path to be used for finding packages or copy packages into package cache (by default: `C:\Users\{User}\.nuget\packages`).