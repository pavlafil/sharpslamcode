﻿using System.Collections.Generic;
using static SharpSLAM.Configuration.Core;

namespace SharpSLAM.Core
{
    public class EFPoint : Contextual
    {
        public EFPoint(CoreContext context, PointHessian d, EFFrame host) : base(context)
        {
            Host = host;
            Data = d;

            PriorF = Data.HasDepthPrior ? Context.Config.Core.IdepthFixPrior * Context.Config.Core.ScaleIdepth * Context.Config.Core.ScaleIdepth : 0;
            if (((Context.Config.Core.SolverMode & (int)SOLVERTYPE.REMOVE_POSEPRIOR)) != 0)
                PriorF = 0;

            DeltaF = Data.Idepth - Data.IdepthZero;

            ResidualsAll = new List<EFResidual>();

            StateFlag = EFPointStatus.PS_GOOD;
        }

        public PointHessian Data { get; set; }
        public EFFrame Host { get; set; }

        public float PriorF { get; set; }
        public float DeltaF { get; set; }

        public EFPointStatus StateFlag { get; set; }

        public List<EFResidual> ResidualsAll { get; set; }
        public int IdxInPoints { get; set; }

        public float HddAccAF { get; set; }


        public float BdSumF { get; set; }
        public float HdiF { get; set; }
        public float BdAccAF { get; set; }
        public float HddAccLF { get; set; }
        public float BdAccLF { get; set; }
        public float[] HcdAccLF { get; set; }
        public float[] HcdAccAF { get; set; }
    }

    public enum EFPointStatus { PS_GOOD = 0, PS_MARGINALIZE, PS_DROP };
}