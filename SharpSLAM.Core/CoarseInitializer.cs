﻿using SharpSLAM.Core.Unmanaged;
using SharpSLAM.Configuration;
using SharpSLAM.Output.Abstractions;
using SharpSLAM.Shared;
using Supercluster.KDTree;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace SharpSLAM.Core
{
    public class CoarseInitializer
    {
        private CoarseInitializerU _u;

        public FrameHessian FirstFrame { get; private set; }
        public FrameHessian NewFrame { get; private set; }
        public AffLight ThisToNext_aff { get; set; }


        public int FrameID { get; private set; } = -1;

        public float[][][] PointsDataDuplicated { get; private set; }
        public Point[][] Points { get; private set; }
        public int[] NumPoints { get; private set; }

        public SE3 ThisToNext { get; private set; }

        public bool FixAffine { get; set; }


        private DiagonalMatrix8f _wM;
        private readonly CoreContext _context;
        private bool _snapped { get; set; }
        private int _snappedAt { get; set; }


        private float _alphaK;
        private float _alphaW;
        private float _regWeight;
        private float _couplingWeight;

        private Mat33[] _K;
        private Mat33[] _Ki;
        private double[] _fxi;
        private double[] _fyi;
        private double[] _cxi;
        private double[] _cyi;


        public CoarseInitializer(CoreContext context)
        {
            _context = context;

            Points = new Point[_context.PyrLevelsUsed][];
            PointsDataDuplicated = new float[_context.PyrLevelsUsed][][];
            NumPoints = new int[_context.PyrLevelsUsed];

            FixAffine = true;

            _wM = new DiagonalMatrix8f(new float[8] {_context.Config.Core.ScaleXIROT, _context.Config.Core.ScaleXIROT, _context.Config.Core.ScaleXIROT, _context.Config.Core.ScaleXITRANS, _context.Config.Core.ScaleXITRANS, _context.Config.Core.ScaleXITRANS, _context.Config.Core.ScaleA, _context.Config.Core.ScaleB });
            _u = new CoarseInitializerU(_context.w[0], _context.h[0]);
            _K = new Mat33[_context.PyrLevelsUsed];
            _Ki = new Mat33[_context.PyrLevelsUsed];
            _fxi = new double[_context.PyrLevelsUsed];
            _fyi = new double[_context.PyrLevelsUsed];
            _cxi = new double[_context.PyrLevelsUsed];
            _cyi = new double[_context.PyrLevelsUsed];
        }

        public void SetFirst(FrameHessian frameHessian)
        {
            makeK();
            FirstFrame = frameHessian;

            var sel = new PixelSelector(_context);

            var size = _context.w[0] * _context.h[0];
            float[] statusMap = new float[size];
            bool[] statusMapB = new bool[size];

            float[] densities = new float[5] { 0.03f, 0.05f, 0.15f, 0.5f, 1f };
            for (int lvl = 0; lvl < _context.PyrLevelsUsed; lvl++)
            {
                sel.CurrentPotential = 3;
                int npts;
                if (lvl == 0)
                {
                    npts = sel.MakeMaps(FirstFrame, statusMap, densities[lvl] * _context.w[0] * _context.h[0], 1, true, 2);
                    _context.Output?.UpdateImage($"01 CoarseInit.03 Points map.Lvl {lvl}", new Shared.MinimalImageF(frameHessian.Shell.IncomingId, _context.w[0], _context.h[0], statusMap));
                }
                else
                {
                    npts = MakePixelStatus(FirstFrame.dIp[lvl], statusMapB, _context.w[lvl], _context.h[lvl], densities[lvl] * _context.w[0] * _context.h[0]);
                    _context.Output?.UpdateImage($"01 CoarseInit.03 Points map.Lvl {lvl}", new Shared.MinimalImageF(frameHessian.Shell.IncomingId, _context.w[lvl], _context.h[lvl], statusMapB.Select(x => x ? 1f : 0f).ToArray()));
                }


                Points[lvl] = new Point[npts];
                PointsDataDuplicated[lvl] = new float[npts][];

                // set idepth map to initially 1 everywhere.
                int wl = _context.w[lvl], hl = _context.h[lvl];
                Point[] pl = Points[lvl];
                var pld = PointsDataDuplicated[lvl];
                int nl = 0;


                for (int y = _context.Config.Core.PatternPadding + 1; y < hl - _context.Config.Core.PatternPadding - 2; y++)
                    for (int x = _context.Config.Core.PatternPadding + 1; x < wl - _context.Config.Core.PatternPadding - 2; x++)
                    {
                        //if(x==2) printf("y=%d!\n",y);
                        if ((lvl != 0 && statusMapB[x + y * wl]) || (lvl == 0 && statusMap[x + y * wl] != 0))
                        {
                            pl[nl] = new Point();
                            //assert(patternNum==9);
                            pl[nl].uv = new float[2] { x + 0.1f, y + 0.1f };
                            pl[nl].idepth = 1;
                            pl[nl].iR = 1;
                            pl[nl].isGood = true;
                            pl[nl].energy = new float[2] { 0, 0 };
                            pl[nl].lastHessian = 0;
                            pl[nl].lastHessian_new = 0;
                            pl[nl].Type = (lvl != 0) ? 1 : statusMap[x + y * wl];

                            var dIpIdx = x + y * _context.w[lvl];
                            float sumGrad2 = 0;
                            for (int idx = 0; idx < _context.Config.Core.PatternNumber; idx++)
                            {
                                int dx = Configuration.Core.StaticPatterns[_context.Config.Core.PatternIndex, idx, 0];
                                int dy = Configuration.Core.StaticPatterns[_context.Config.Core.PatternIndex, idx, 1];

                                //squaredNorm
                                var cpt1 = FirstFrame.dIp[lvl][dIpIdx + dx + dy * _context.w[lvl], 1];
                                var cpt2 = FirstFrame.dIp[lvl][dIpIdx + dx + dy * _context.w[lvl], 2];

                                float absgrad = cpt1 * cpt1 + cpt2 * cpt2;
                                sumGrad2 += absgrad;
                            }

                            ////				float gth = setting_outlierTH * (sqrtf(sumGrad2)+setting_outlierTHSumComponent);
                            ////				pl[nl].outlierTH = patternNum*gth*gth;

                            pl[nl].outlierTH = _context.Config.Core.PatternNumber * _context.Config.Core.OutlierTH;
                            pld[nl] = pl[nl].uv;

                            nl++;
                            System.Diagnostics.Debug.Assert(nl < npts);
                        }
                    }
                NumPoints[lvl] = nl;
            }
            statusMap = null; //let GC handle this //delete[] statusMap;
            statusMapB = null;//let GC handle this //delete[] statusMapB;

            makeNN();

            ThisToNext = new SE3();
            _snapped = false;
            FrameID = _snappedAt = 0;

            //really needed?
            //for (int i = 0; i < _context.PyrLevelsUsed; i++)
            //    dGrads[i].setZero();

        }

        private void makeK()
        {
            for (int level = 0; level < _context.PyrLevelsUsed; ++level)
            {
                _K[level] = new Mat33(new double[9] { _context.fx[level], 0.0, _context.cx[level], 0.0, _context.fy[level], _context.cy[level], 0.0, 0.0, 1.0 });
                _Ki[level] = _K[level].Inverse();
                _fxi[level] = _Ki[level][0, 0];
                _fyi[level] = _Ki[level][1, 1];
                _cxi[level] = _Ki[level][0, 2];
                _cyi[level] = _Ki[level][1, 2];
            }
        }

        /// <summary>
        /// Set ir of points in bigger image in correspodence with smaller already computed and blur with neighbors
        /// </summary>
        /// <param name="srcLvl"></param>
        private void PropagateDown(int srcLvl)
        {
            Debug.Assert(srcLvl > 0);
            // set idepth of target

            int nptst = NumPoints[srcLvl - 1];
            var ptss = Points[srcLvl];
            var ptst = Points[srcLvl - 1];

            for (int i = 0; i < nptst; i++)
            {
                Point point = ptst[i];
                Point parent = ptss[point.parent];

                if (!parent.isGood || parent.lastHessian < 0.1) continue;
                if (!point.isGood)
                {
                    point.iR = point.idepth = point.idepth_new = parent.iR;
                    point.isGood = true;
                    point.lastHessian = 0;
                }
                else
                {
                    float newiR = (point.iR * point.lastHessian * 2 + parent.iR * parent.lastHessian) / (point.lastHessian * 2 + parent.lastHessian);
                    point.iR = point.idepth = point.idepth_new = newiR;
                }
            }
            optReg(srcLvl - 1);
        }

        /// <summary>
        /// If not snapped reset ir otherwise blur ir with neighbors
        /// </summary>
        /// <param name="lvl"></param>
        private void optReg(int lvl)
        {
            int npts = NumPoints[lvl];
            var ptsl = Points[lvl];
            if (!_snapped)
            {
                for (int i = 0; i < npts; i++)
                    ptsl[i].iR = 1;
                return;
            }

            for (int i = 0; i < npts; i++)
            {
                var point = ptsl[i];
                if (!point.isGood) continue;

                var idnn = new float[10];
                int nnn = 0;
                for (int j = 0; j < 10; j++)//10should be some config variable
                {
                    if (point.neighbours[j] == -1) continue;
                    var other = ptsl[point.neighbours[j]];
                    if (!other.isGood) continue;
                    idnn[nnn] = other.iR;
                    nnn++;
                }

                if (nnn > 2)
                {
                    Array.Sort(idnn,0,nnn);//can be more efectiv e does not need complete sort  std::nth_element(idnn, idnn + nnn / 2, idnn + nnn);
                    point.iR = (1 - _regWeight) * point.idepth + _regWeight * idnn[nnn / 2];
                }
            }
        }

        public bool TrackFrame(FrameHessian frameHessian)
        {
            NewFrame = frameHessian;

            //Show current frame not necessary now maybe later
            //for (IOWrap::Output3DWrapper* ow : wraps)
            //    ow->pushLiveFrame(newFrameHessian);

            var maxIterations = new int[5] { 5, 5, 10, 30, 50 };

            _alphaK = 2.5f * 2.5f;//*freeDebugParam1*freeDebugParam1;
            _alphaW = 150 * 150;//*freeDebugParam2*freeDebugParam2;
            _regWeight = 0.8f;//*freeDebugParam4;
            _couplingWeight = 1;//*freeDebugParam5;

            if (!_snapped)
            {
                ThisToNext.TranslationSetZero();
                for (int lvl = 0; lvl < _context.PyrLevelsUsed; lvl++)
                {
                    int npts = NumPoints[lvl];
                    var ptsl = Points[lvl];
                    for (int i = 0; i < npts; i++)
                    {
                        ptsl[i].iR = 1;
                        ptsl[i].idepth_new = 1;
                        ptsl[i].lastHessian = 0;
                    }
                }
            }

            SE3 refToNew_current = ThisToNext.Clone();
            AffLight refToNew_aff_current = ThisToNext_aff;

            if (FirstFrame.Shell.Image.ExposureTime > 0 && NewFrame.Shell.Image.ExposureTime > 0)
                refToNew_aff_current = new AffLight(Math.Log(NewFrame.Shell.Image.ExposureTime / FirstFrame.Shell.Image.ExposureTime), 0); // coarse approximation.

            float[] latestRes = new float[3] { 0, 0, 0 };
            for (int lvl = _context.PyrLevelsUsed - 1; lvl >= 0; lvl--)
            {
                if (lvl < _context.PyrLevelsUsed - 1)
                    PropagateDown(lvl + 1);

                Mat88f H = new Mat88f(); Mat88f Hsc = new Mat88f();
                float[] b = null;// new float[8]
                float[] bsc = null;// new float[8]; //Vec8f b, bsc;

                ResetPoints(lvl);
                
                //compute new point in current lvl? should find residual in other frames
                float[] resOld = CalcResAndGS(lvl, ref H, ref b, ref Hsc, ref bsc, refToNew_current, refToNew_aff_current, false); //Vec3f resOld = calcResAndGS(lvl, H, b, Hsc, bsc, refToNew_current, refToNew_aff_current, false);

                applyStep(lvl);

                float lambda = 0.1f;
                float eps = 1e-4f;
                int fails = 0;

                if (_context.Config.Output.Log.Debug)
                {
                    var start = string.Format($"lvl {lvl}, it 0 (l={lambda}) {"INITIA"}: {{0}} + {{1}} -> {{2}} + {{3}} ({{4}} -> {{5}}) (|inc| = {0})! \n",
                            Math.Sqrt((float)(resOld[0] / resOld[2])),
                            Math.Sqrt((float)(resOld[1] / resOld[2])),
                            Math.Sqrt((float)(resOld[0] / resOld[2])),
                            Math.Sqrt((float)(resOld[1] / resOld[2])),
                            (resOld[0] + resOld[1]) / resOld[2],
                            (resOld[0] + resOld[1]) / resOld[2]);


                    start += $"{refToNew_current.PrintLog()} AFF {refToNew_aff_current.a} {refToNew_aff_current.b}";

                    Shared.Log.LogDebug(start);
                }

                int iteration = 0;
                while (true)
                {
                    Mat88f Hl = H.Clone();
                    for (int i = 0; i < 8; i++)
                        Hl[i, i] = Hl[i, i] * (1 + lambda);

                    Hl -= Hsc * (1 / (1 + lambda));

                    var bl = new float[8];

                    Hl = _wM * Hl * _wM * (0.01f / (_context.w[lvl] * _context.h[lvl]));
                    for (int vi = 0; vi < 8; vi++)
                    {
                        bl[vi] = b[vi] - bsc[vi] * (1 / (1 + lambda));//Vec8f bl = b - bsc * (1 / (1 + lambda));
                        bl[vi] = _wM.GetDiag(vi) * bl[vi] * (0.01f / (_context.w[lvl] * _context.h[lvl]));
                    }

                    float[] inc = null;
                    SE3 refToNew_new = null;
                    _u.FixAffineAndSE3Exp(FixAffine, _wM, Hl, bl, refToNew_current, ref inc, ref refToNew_new);

                    AffLight refToNew_aff_new = refToNew_aff_current;
                    refToNew_aff_new.a += inc[6];
                    refToNew_aff_new.b += inc[7];
                    doStep(lvl, lambda, inc);


                    Mat88f H_new = new Mat88f();
                    Mat88f Hsc_new = new Mat88f();
                    float[] b_new = new float[8];
                    float[] bsc_new = new float[8];
                    float[] resNew = CalcResAndGS(lvl, ref H_new, ref b_new, ref Hsc_new, ref bsc_new, refToNew_new, refToNew_aff_new, false);
                    float[] regEnergy = calcEC(lvl);

                    float eTotalNew = (resNew[0] + resNew[1] + regEnergy[1]);
                    float eTotalOld = (resOld[0] + resOld[1] + regEnergy[0]);


                    bool accept = eTotalOld > eTotalNew;

                    if (_context.Config.Output.Log.Debug)
                    {
                        var start = string.Format($"lvl {lvl}, it {iteration} (l={lambda}) {(accept ? "ACCEPT" : "REJECT")}: {{0}} + {{1}} + {{2}} -> {{3}} + {{4}} + {{5}} ({{6}}->{{7}}) (|inc| = { inc.Norm() }! \n",
                                    Math.Sqrt((float)(resOld[0] / resOld[2])),
                                    Math.Sqrt((float)(regEnergy[0] / regEnergy[2])),
                                    Math.Sqrt((float)(resOld[1] / resOld[2])),
                                    Math.Sqrt((float)(resNew[0] / resNew[2])),
                                    Math.Sqrt((float)(regEnergy[1] / regEnergy[2])),
                                    Math.Sqrt((float)(resNew[1] / resNew[2])),
                                    eTotalOld / resNew[2],
                                    eTotalNew / resNew[2]);


                        start += $"{refToNew_new.PrintLog()} AFF {refToNew_aff_new.a} {refToNew_aff_new.b}";

                        Shared.Log.LogDebug(start);
                    }
                    
                     if (accept)
                     {
                    
                         if (resNew[1] == _alphaK * NumPoints[lvl])
                             _snapped = true;
                         H = H_new;
                         b = b_new;
                         Hsc = Hsc_new;
                         bsc = bsc_new;
                         resOld = resNew;
                         refToNew_aff_current = refToNew_aff_new;
                         refToNew_current = refToNew_new;
                         applyStep(lvl);
                         optReg(lvl);
                         lambda *= 0.5f;
                         fails = 0;
                         if (lambda < 0.0001) lambda = 0.0001f;
                     }
                     else
                     {
                         fails++;
                         lambda *= 4;
                         if (lambda > 10000) lambda = 10000;
                     }
                    
                     bool quitOpt = false;
                    
                    //norm>eps
                     if (!(inc.Norm() > eps) || iteration >= maxIterations[lvl] || fails >= 2)
                     {
                         //allocated in fuction not for instance reset is not required?? //Mat88f H, Hsc; Vec8f b, bsc;
                         quitOpt = true;
                     }

                    if (quitOpt) break;
                     iteration++;
                }
                latestRes = resOld;

            }



            ThisToNext = refToNew_current;
            ThisToNext_aff = refToNew_aff_current;

            for (int i = 0; i < _context.PyrLevelsUsed - 1; i++)
                propagateUp(i);




            FrameID++;
            if (!_snapped) _snappedAt = 0;

            if (_snapped && _snappedAt == 0)
                _snappedAt = FrameID;


            debugPlot(0);

            return _snapped && FrameID > _snappedAt + 5;
        }

        private void debugPlot(int lvl)
        {
            //bool needCall = false;
            //for (IOWrap::Output3DWrapper* ow : wraps)
            //    needCall = needCall || ow->needPushDepthImage();
            //if (!needCall) return;


            var wl = _context.w[lvl];
            var hl = _context.h[lvl];
            var colorRef = FirstFrame.dIp[lvl];

            var iRImg = new MinimalImageF3(NewFrame.Shell.IncomingId, wl, hl);


            for (int i = 0; i < wl * hl; i++)
            {
                iRImg.At(i, 0) = colorRef[i, 0];
                iRImg.At(i, 1) = colorRef[i,0];
                iRImg.At(i, 2) = colorRef[i,0];
            }

            int npts = NumPoints[lvl];

            float nid = 0, sid = 0;
            for (int i = 0; i < npts; i++)
            {
                Point point = Points[lvl][i];
                if (point.isGood)
                {
                    nid++;
                    sid += point.iR;
                }
            }
            float fac = nid / sid;



            for (int i = 0; i < npts; i++)
            {
                Point point = Points[lvl][i];

                if (!point.isGood)
                { iRImg.SetPixel9((int)(point.uv[0] + 0.5f), (int)(point.uv[1] + 0.5), 0); }

                else
                {
                    var c = iRImg.GetRainbowColor(point.iR * fac, 3);
                    iRImg.SetPixel9((int)(point.uv[0] + 0.5f), (int)(point.uv[1] + 0.5f),c);
                }
            }

            _context.Output.UpdateImage("01 CoarseInit.05 Point colored by IR", iRImg);
            //Log.LogDebug("====PAUSE\n");
            //Console.ReadKey();
        }

        /// <summary>
        /// Propagate correct depth from bigger image to smaller one  and blur with neighbors
        /// </summary>
        /// <param name="srcLvl"></param>
        private void propagateUp(int srcLvl)
        {
            Debug.Assert(srcLvl + 1 < _context.PyrLevelsUsed);
            // set idepth of target

            int nptss = NumPoints[srcLvl];
            int nptst = NumPoints[srcLvl + 1];
            Point[] ptss = Points[srcLvl];
            Point[] ptst = Points[srcLvl + 1];

            // set to zero.
            for (int i = 0; i < nptst; i++)
            {
                Point parent = ptst[i];
                parent.iR = 0;
                parent.iRSumNum = 0;
            }

            for (int i = 0; i < nptss; i++)
            {
                Point point = ptss[i];
                if (!point.isGood) continue;

                Point parent = ptst[point.parent];
                parent.iR += point.iR * point.lastHessian;
                parent.iRSumNum += point.lastHessian;
            }

            for (int i = 0; i < nptst; i++)
            {
                Point parent = ptst[i];
                if (parent.iRSumNum > 0)
                {
                    parent.idepth = parent.iR = (parent.iR / parent.iRSumNum);
                    parent.isGood = true;
                }
            }

            optReg(srcLvl + 1);
        }

        private float[] calcEC(int lvl)
        {
            return _u.CalcEC(lvl, _snapped, _couplingWeight, NumPoints, Points);
        }

        private void doStep(int lvl, float lambda, float[] inc)
        {
            const float maxPixelStep = 0.25f;
            const float idMaxStep = 1e10f;
            Point[] pts = Points[lvl];
            int npts = NumPoints[lvl];
            for (int i = 0; i < npts; i++)
            {
                if (!pts[i].isGood) continue;


                float b = _u.GetB(i, inc);// JbBuffer[i][8] + JbBuffer[i].head < 8 > ().dot(inc);
                float step = _u.GetStep(b, i, lambda);// -b * JbBuffer[i][9] / (1 + lambda);


                float maxstep = maxPixelStep * pts[i].maxstep;
                if (maxstep > idMaxStep) maxstep = idMaxStep;

                if (step > maxstep) step = maxstep;
                if (step < -maxstep) step = -maxstep;

                float newIdepth = pts[i].idepth + step;
                if (newIdepth < 1e-3) newIdepth = 1e-3f;
                if (newIdepth > 50) newIdepth = 50;
                pts[i].idepth_new = newIdepth;
            }
        }

        private void applyStep(int lvl)
        {
            Point[] pts = Points[lvl];
            int npts = NumPoints[lvl];
            for (int i = 0; i < npts; i++)
            {
                if (!pts[i].isGood)
                {
                    pts[i].idepth = pts[i].idepth_new = pts[i].iR;
                    continue;
                }
                pts[i].energy = pts[i].energy_new;
                pts[i].isGood = pts[i].isGood_new;
                pts[i].idepth = pts[i].idepth_new;
                pts[i].lastHessian = pts[i].lastHessian_new;
            }
            ////std::swap<Vec10f*>(JbBuffer, JbBuffer_new);
            _u.SwapJbBuffer();//std::swap(JbBuffer, JbBuffer_new);
        }


        /// <summary>
        /// calculates residual, Hessian and Hessian-block needed for re-substituting depth.
        /// </summary>
        private float[] CalcResAndGS(int lvl, ref Mat88f h_out, ref float[] b_out, ref Mat88f h_out_sc, ref float[] b_out_sc, SE3 refToNew, AffLight refToNew_aff, bool plot)
        {

            return _u.CalcResAndGS(
                lvl, ref h_out, ref b_out, ref h_out_sc, ref b_out_sc,
                refToNew, refToNew_aff, plot,
                _context.w, _context.h, _Ki, NumPoints, Points,
                _context.Config.Core.PatternNumber, _context.Config.Core.PatternIndex, Configuration.Core.StaticPatterns,
                _context.Config.Core.HuberTH, _alphaW, _alphaK, _couplingWeight,
                _context.fx, _context.fy, _context.cx, _context.cy,
                FirstFrame.dIp, NewFrame.dIp
                );

        }

        private void ResetPoints(int lvl)
        {
            var pts = Points[lvl];
            int npts = NumPoints[lvl];
            for (int i = 0; i < npts; i++)
            {
                pts[i].energy[0] = 0; pts[i].energy[1] = 0;
                pts[i].idepth_new = pts[i].idepth;


                if (lvl == _context.PyrLevelsUsed - 1 && !pts[i].isGood)
                {
                    float snd = 0, sn = 0;
                    for (int n = 0; n < 10; n++)
                    {
                        if (pts[i].neighbours[n] == -1 || !pts[pts[i].neighbours[n]].isGood) continue;
                        snd += pts[pts[i].neighbours[n]].iR;
                        sn += 1;
                    }

                    if (sn > 0)
                    {
                        pts[i].isGood = true;
                        pts[i].iR = pts[i].idepth = pts[i].idepth_new = snd / sn;
                    }
                }
            }
        }


        //Used KDTree is very slow!!!
        private void makeNN()
        {
            const float NNDistFactor = 0.05f;

            Func<float[], float[], double> getDist = (x, y) =>
            {
                var dx = x[0] - y[0];
                var dy = x[1] - y[1];
                return Math.Sqrt(dx * dx + dy * dy);
            };

            // build indices
            KDTree<float, int>[] indexes = new KDTree<float, int>[_context.PyrLevelsUsed];


            for (int i = 0; i < _context.PyrLevelsUsed; i++)
            {
                var dataIndexes = Enumerable.Range(0, NumPoints[i]).ToArray();
                indexes[i] = new KDTree<float, int>(2, PointsDataDuplicated[i], dataIndexes, getDist);
            }

            const int nn = 10;

            // find NN & parents
            for (int lvl = 0; lvl < _context.PyrLevelsUsed; lvl++)
            {
                var pts = Points[lvl];


                for (int i = 0; i < NumPoints[lvl]; i++)
                {
                    pts[i].neighbours = new int[nn];
                    pts[i].neighboursDist = new float[nn];

                    //resultSet.init(pts[i].neighbours, pts[i].neighboursDist );
                    var pt = new float[2] { pts[i].uv[0], pts[i].uv[1] };

                    var r = indexes[lvl].NearestNeighbors(pt, nn);
                    float sumDF = 0;
                    for (int k = 0; k < nn; k++)
                    {
                        pts[i].neighbours[k] = r[k].Item2;
                        float df = (float)Math.Exp(-getDist(r[k].Item1, pt) * NNDistFactor);
                        sumDF += df;
                        pts[i].neighboursDist[k] = df;
                        Debug.Assert(r[k].Item2 >= 0 && r[k].Item2 < NumPoints[lvl]);
                    }
                    for (int k = 0; k < nn; k++)
                        pts[i].neighboursDist[k] *= 10 / sumDF; //is thi "10" related to nn=10?


                    if (lvl < _context.PyrLevelsUsed - 1)
                    {
                        pt[0] = 0.5f * pt[0] - 0.25f;
                        pt[1] = 0.5f * pt[1] - 0.25f;

                        r = indexes[lvl + 1].NearestNeighbors(pt, 1);

                        pts[i].parent = r[0].Item2;
                        pts[i].parentDist = (float)Math.Exp(-getDist(r[0].Item1, pt) * NNDistFactor);

                        Debug.Assert(r[0].Item2 >= 0 && r[0].Item2 < NumPoints[lvl + 1]);
                    }
                    else
                    {
                        pts[i].parent = -1;
                        pts[i].parentDist = -1;
                    }
                }

            }
        }



        private int gridMaxSelection(int pot, float[,] grads, bool[] mapOut, int w, int h, float THFac)
        {
            Array.Clear(mapOut, 0, mapOut.Length);
            const float minUseGrad_pixsel = 10;
            int numGood = 0;
            for (int y = 1; y < h - pot; y += pot)
            {
                for (int x = 1; x < w - pot; x += pot)
                {
                    int bestXXID = -1;
                    int bestYYID = -1;
                    int bestXYID = -1;
                    int bestYXID = -1;

                    float bestXX = 0, bestYY = 0, bestXY = 0, bestYX = 0;

                    int grads0 = x + y * w;
                    for (int dx = 0; dx < pot; dx++)
                        for (int dy = 0; dy < pot; dy++)
                        {
                            int idx = dx + dy * w;
                            var g0 = grads[idx + grads0, 0];
                            var g1 = grads[idx + grads0, 1];
                            var g2 = grads[idx + grads0, 2];

                            //squaredNorm
                            float sqgd = g1 * g1 + g2 * g2;
                            float TH = THFac * minUseGrad_pixsel * (0.75f);

                            if (sqgd > TH * TH)
                            {
                                float agx = Math.Abs(g1);
                                if (agx > bestXX) { bestXX = agx; bestXXID = idx; }

                                float agy = Math.Abs(g2);
                                if (agy > bestYY) { bestYY = agy; bestYYID = idx; }

                                float gxpy = Math.Abs(g1 - g2);
                                if (gxpy > bestXY) { bestXY = gxpy; bestXYID = idx; }

                                float gxmy = Math.Abs(g1 + g2);
                                if (gxmy > bestYX) { bestYX = gxmy; bestYXID = idx; }
                            }
                        }

                    int map0 = x + y * w;

                    if (bestXXID >= 0)
                    {
                        if (!mapOut[bestXXID + map0])
                            numGood++;
                        mapOut[bestXXID + map0] = true;

                    }
                    if (bestYYID >= 0)
                    {
                        if (!mapOut[bestYYID + map0])
                            numGood++;
                        mapOut[bestYYID + map0] = true;

                    }
                    if (bestXYID >= 0)
                    {
                        if (!mapOut[bestXYID + map0])
                            numGood++;
                        mapOut[bestXYID + map0] = true;

                    }
                    if (bestYXID >= 0)
                    {
                        if (!mapOut[bestYXID + map0])
                            numGood++;
                        mapOut[bestYXID + map0] = true;

                    }
                }
            }
            return numGood;
        }

        private int MakePixelStatus(float[,] grads, bool[] map, int w, int h, float desiredDensity, int recsLeft = 5, float THFac = 1, int sparsityFactor = 5)  //sparsityFactor not actually a setting, only some legacy stuff for coarse initializer.
        {
            int numGoodPoints;


            if (sparsityFactor == 1) numGoodPoints = gridMaxSelection(1, grads, map, w, h, THFac);
            else if (sparsityFactor == 2) numGoodPoints = gridMaxSelection(2, grads, map, w, h, THFac);
            else if (sparsityFactor == 3) numGoodPoints = gridMaxSelection(3, grads, map, w, h, THFac);
            else if (sparsityFactor == 4) numGoodPoints = gridMaxSelection(4, grads, map, w, h, THFac);
            else if (sparsityFactor == 5) numGoodPoints = gridMaxSelection(5, grads, map, w, h, THFac);
            else if (sparsityFactor == 6) numGoodPoints = gridMaxSelection(6, grads, map, w, h, THFac);
            else if (sparsityFactor == 7) numGoodPoints = gridMaxSelection(7, grads, map, w, h, THFac);
            else if (sparsityFactor == 8) numGoodPoints = gridMaxSelection(8, grads, map, w, h, THFac);
            else if (sparsityFactor == 9) numGoodPoints = gridMaxSelection(9, grads, map, w, h, THFac);
            else if (sparsityFactor == 10) numGoodPoints = gridMaxSelection(10, grads, map, w, h, THFac);
            else if (sparsityFactor == 11) numGoodPoints = gridMaxSelection(11, grads, map, w, h, THFac);
            else numGoodPoints = gridMaxSelection(sparsityFactor, grads, map, w, h, THFac);


            /*
             * #points is approximately proportional to sparsityFactor^2.
             */

            float quotia = numGoodPoints / desiredDensity;

            int newSparsity = (int)((sparsityFactor * (float)Math.Sqrt(quotia)) + 0.7f);


            if (newSparsity < 1) newSparsity = 1;


            float oldTHFac = THFac;
            if (newSparsity == 1 && sparsityFactor == 1) THFac = 0.5f;


            if ((Math.Abs(newSparsity - sparsityFactor) < 1 && THFac == oldTHFac) ||
                    (quotia > 0.8 && 1.0f / quotia > 0.8) ||
                    recsLeft == 0)
            {

                //		printf(" \n");
                //all good
                //sparsityFactor = newSparsity; no needed used only here
                return numGoodPoints;
            }
            else
            {
                //		printf(" -> re-evaluate! \n");
                // re-evaluate.
                //sparsityFactor = newSparsity; no needed used only here
                return MakePixelStatus(grads, map, w, h, desiredDensity, recsLeft - 1, THFac, newSparsity);
            }
        }
    }
}
