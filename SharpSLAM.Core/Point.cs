﻿//using System;

//namespace SharpSLAM.Core.Shared
//{
//    public class Point
//    {
// 	    // index in jacobian. never changes (actually, there is no reason why).
//    	public float[] uv;

//        // idepth / isgood / energy during optimization.
//        public float idepth;
//        public bool isGood;
//        public ValueTuple<float,float> energy;       //2 (UenergyPhotometric, energyRegularizer)
//        public bool isGood_new;
//        public float idepth_new;
//        public ValueTuple<float, float> energy_new; //2

//        public float iR;
//        public float iRSumNum;

//        public float lastHessian;
//        public float lastHessian_new;

//        // max stepsize for idepth (corresponding to max. movement in pixel-space).
//        public float maxstep;

//        // idx (x+y*w) of closest point one pyramid level above.
//        public int parent;
//        public float parentDist;

//        // idx (x+y*w) of up to 10 nearest points in pixel space.
//        public int[] neighbours;
//        public float[] neighboursDist;

//        public float my_type;
//        public float outlierTH;

//        //public Point()
//        //{
//            //neighbours= new int[10];
//            //neighboursDist = new float[10];
//        //}
//    };
//}
