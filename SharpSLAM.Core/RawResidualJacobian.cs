﻿using SharpSLAM.Core.Unmanaged;

namespace SharpSLAM.Core
{
    public class RawResidualJacobian : Contextual
    {
        public RawResidualJacobian(CoreContext context):base(context)
        {
            ResF = new float[Context.Config.Core.MaxResPerPoint];
            Jpdd = new float[2];

            Jpdxi = new float[2][];
            Jpdc = new float[2][];
            JIdx = new float[2][];
            JabF = new float[2][];

            for (int i = 0; i < 2; i++)
            {
                Jpdxi[i] = new float[6];
                Jpdc [i]=new float[Context.Config.Core.Cpars];
                JIdx [i]=new float[Context.Config.Core.MaxResPerPoint];
                JabF [i]=new float[Context.Config.Core.MaxResPerPoint];
            }

            JIdx2 = new Mat22f();
            JabJIdx = new Mat22f();
            Jab2 = new Mat22f();
        }

        // ================== new structure: save independently =============.
        //VecNRf resF;
        public float[] ResF { get; set; }

        // the two rows of d[x,y]/d[xi].
        //Vec6f Jpdxi[2];         // 2x6
        public float[][] Jpdxi { get; set; }

        // the two rows of d[x,y]/d[C].
        //VecCf Jpdc[2];          // 2x4
        public float[][] Jpdc { get; set; }

        // the two rows of d[x,y]/d[idepth].
        //Vec2f Jpdd;             // 2x1
        public float[] Jpdd { get; set; }

        // the two columns of d[r]/d[x,y].
        //VecNRf JIdx[2];         // 2x8
        public float[][] JIdx { get; set; }

        // = the two columns of d[r] / d[ab]
        //VecNRf JabF[2];         // 2x8
        public float[][] JabF { get; set; }

        // = JIdx^T * JIdx (inner product). Only as a shorthand.
        //Mat22f JIdx2;               // 2x2
        public Mat22f JIdx2 { get; set; }  // = Jab^T * JIdx (inner product). Only as a shorthand.

        //Mat22f JabJIdx;         // 2x2
        public Mat22f JabJIdx { get; set; }   // = Jab^T * Jab (inner product). Only as a shorthand.

        //Mat22f Jab2;
        public Mat22f Jab2 { get; set; }
    }
}