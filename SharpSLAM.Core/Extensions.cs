﻿using SharpSLAM.Core.Unmanaged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharpSLAM.Core
{
    static public class Extensions
    {
        public static double[] Plus(this double[] a, double[] b)
        {
            return a.Zip(b, (aa, bb) => { return aa + bb; }).ToArray();
        }
        public static float[] Plus(this float[] a, float[] b)
        {
            return a.Zip(b, (aa, bb) => { return aa + bb; }).ToArray();
        }


        public static float[] Minus(this float[] a, float[] b)
        {
            return a.Zip(b, (aa, bb) => { return aa - bb; }).ToArray();
        }
        public static double[] Minus(this double[] a, double[] b)
        {
            return a.Zip(b, (aa, bb) => { return aa - bb; }).ToArray();
        }


        public static float[] Mul(this float[] a, float b)
        {
            return a.Select(x => x * b).ToArray();
        }
        public static double[] Mul(this double[] a, double b)
        {
            return a.Select(x => x * b).ToArray();
        }
        public static float Mul(this IEnumerable<float> a, IEnumerable<float> b)
        {
            return a.Zip(b, (x, y) => x * y).Sum();
        }
        public static double Mul(this IEnumerable<double> a, IEnumerable<double> b)
        {
            return a.Zip(b, (x, y) => x * y).Sum();
        }
        public static IEnumerable<double> CwiseProduct(this IEnumerable<double> a, IEnumerable<double> b)
        {
            return a.Zip(b, (x, y) => x * y);
        }

        public static float[] Divide(this float[] a, float b)
        {
            return a.Select(x => x / b).ToArray();
        }
        public static float SquaredNorm(this IEnumerable<float> items)
        {
            return items.Aggregate(0f, (a, i) => { return a + (i * i); });
        }
        public static double SquaredNorm(this IEnumerable<double> items)
        {
            return items.Aggregate(0d, (a, i) => { return a + (i * i); });
        }
        public static float Norm(this IEnumerable<float> items)
        {
            return (float)Math.Sqrt(items.SquaredNorm());
        }
        public static double Norm(this IEnumerable<double> items)
        {
            return (double)Math.Sqrt(items.SquaredNorm());
        }
        public static IEnumerable<T> Cast<O, T>(this IEnumerable<O> input)
        {
            return input.Select((x) => { return (T)Convert.ChangeType(x, typeof(T)); });
        }



        public static float[] GetInterpolatedElement33(this float[,] mat, float x, float y, int width)
        {
            int ix = (int)x;
            int iy = (int)y;
            float dx = x - ix;
            float dy = y - iy;
            float dxdy = dx * dy;
            var bpIndex = ix + iy * width;

            return new float[3] {
                dxdy * mat[bpIndex + 1 + width,0] + (dy-dxdy) * mat[bpIndex+width,0] + (dx-dxdy) * mat[bpIndex+1,0] + (1-dx-dy+dxdy) * mat[bpIndex,0],
                dxdy * mat[bpIndex + 1 + width,1] + (dy-dxdy) * mat[bpIndex+width,1] + (dx-dxdy) * mat[bpIndex+1,1] + (1-dx-dy+dxdy) * mat[bpIndex,1],
                dxdy * mat[bpIndex + 1 + width,2] + (dy-dxdy) * mat[bpIndex+width,2] + (dx-dxdy) * mat[bpIndex+1,2] + (1-dx-dy+dxdy) * mat[bpIndex,2]
            };
        }

        public static float[] GetInterpolatedElement33BiLin(this float[,] mat, float x, float y, int width)
        {
            int ix = (int)x;
            int iy = (int)y;
            var bpIndex = ix + iy * width;

            float tl = mat[bpIndex, 0];
            float tr = mat[bpIndex + 1, 0];
            float bl = mat[bpIndex + width, 0];
            float br = mat[bpIndex + width + 1, 0];

            float dx = x - ix;
            float dy = y - iy;

            float topInt = dx * tr + (1 - dx) * tl;
            float botInt = dx * br + (1 - dx) * bl;
            float leftInt = dy * bl + (1 - dy) * tl;
            float rightInt = dy * br + (1 - dy) * tr;

            return new float[3] {
                    dx * rightInt + (1 - dx) * leftInt,
                    rightInt - leftInt,
                    botInt - topInt };
        }

        public static float GetInterpolatedElement31(this float[,] mat, float x, float y, int width)
        {
            int ix = (int)x;
            int iy = (int)y;
            float dx = x - ix;
            float dy = y - iy;
            float dxdy = dx * dy;
            var bpIndex = ix + iy * width;


            return dxdy * mat[bpIndex + 1 + width, 0]
            + (dy - dxdy) * mat[bpIndex + width, 0]
            + (dx - dxdy) * mat[bpIndex + 1, 0]
            + (1 - dx - dy + dxdy) * mat[bpIndex, 0];
        }

        public static void Resize<T>(this List<T> list, int sz, Func<T> c)
        {
            int cur = list.Count;
            if (sz < cur)
                list.RemoveRange(sz, cur - sz);
            else if (sz > cur)
            {
                if (sz > list.Capacity)//this bit is purely an optimisation, to avoid multiple automatic capacity changes.
                    list.Capacity = sz;
                for (int i = 0; i < sz - cur; i++)
                {
                    list.Add(c());
                }
            }
        }


        public static bool ProjectPoint(CoreContext context, float u_pt, float v_pt, float idepth, Mat33f KRKi, float[] Kt, out float Ku, out float Kv)
        {
            float[] ptp = (KRKi * new float[3] { u_pt, v_pt, 1 }).Plus(Kt.Mul(idepth));
            Ku = ptp[0] / ptp[2];
            Kv = ptp[1] / ptp[2];
            return Ku > 1.1f && Kv > 1.1f && Ku < (context.w[0] - 3) && Kv < (context.h[0] - 3);
        }

        public static bool ProjectPoint(CoreContext context, float u_pt, float v_pt, float idepth, int dx, int dy, CalibHessian hCalib, Mat33f R, float[] t,
            out float drescale, out float u, out float v, out float Ku, out float Kv, out float[] KliP, out float new_idepth)
        {
            KliP = new float[3]{
        (u_pt + dx - hCalib.cxl) * hCalib.fxli,
        (v_pt + dy - hCalib.cyl) * hCalib.fyli,
        1 };

            float[] ptp = (R * KliP).Plus(t.Mul(idepth));
            drescale = 1.0f / ptp[2];
            new_idepth = idepth * drescale;

            //To be sure all out parameters are filled
            u = -1;
            v = -1;
            Ku = -1;
            Kv = -1;
            //--

            if (!(drescale > 0)) return false;

            u = ptp[0] * drescale;
            v = ptp[1] * drescale;
            Ku = u * hCalib.fxl + hCalib.cxl;
            Kv = v * hCalib.fyl + hCalib.cyl;

            return Ku > 1.1f && Kv > 1.1f && Ku < (context.w[0] - 3) && Kv < (context.h[0] - 3);
        }
    }

}


