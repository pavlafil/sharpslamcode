﻿using SharpSLAM.Core.Unmanaged;
using SharpSLAM.Shared;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SharpSLAM.Core
{
    public partial class Core
    {
        public bool Initialized { get; set; }

        public bool LinearizeOperation { get; set; }
        public bool InitFailed { get; set; }
        public bool IsLost { get; set; }

        private CoarseInitializer _coarseInitializer;
        private double[] _lastCoarseRMSE;
        private CoreContext _context;
        private bool _isLost;

        private List<FrameShell> _allFramesHistory;
        private EnergyFunctional _ef;
        private CalibHessian _hCalib;

        private int _lastRefStopID;

        //Should be thread safe ba lock
        private List<FrameShell> _allKeyFramesHistory;
        private List<FrameHessian> _frameHessians;  // ONLY changed in marginalizeFrame and addFrame.

        private List<PointFrameResidual> _activeResiduals;
        private List<float> _allResVec;

        private CoarseTracker _coarseTracker;                   // always used to track new frames. protected by [trackMutex].
        private CoarseTracker _coarseTrackerForNewKF;			// set as as reference. protected by [coarseTrackerSwapMutex].
        private CoarseDistanceMap _coarseDistanceMap;
        private double _currentMinActDist;

        private bool _multiThreading;

        // statistics
        private long _statisticsLastNumOptIts;
        private long _statisticsNumDroppedPoints;
        private long _statisticsNumActivatedPoints;
        private long _statisticsNumCreatedPoints;
        private long _statisticsNumForceDroppedResBwd;
        private long _statisticsNumForceDroppedResFwd;
        private long _statisticsNumMargResFwd;
        private long _statisticsNumMargResBwd;
        private float _statisticsLastFineTrackRMSE;

        private float _minIdJetVisTracker = -1;
        private float _maxIdJetVisTracker = -1;
        private float _minIdJetVisDebug = -1;
        private float _maxIdJetVisDebug = -1;

        private PixelSelector _pixelSelector;
        private float[] _selectionMap;

        // tracking / mapping synchronization. All protected by [trackMapSyncMutex].
        private LinkedList<FrameHessian> _unmappedTrackedFrames;
        private int _needNewKFAfter = -1;


        private Task _mappingTask;
        private bool _runMapping;
        private bool _needToKetchupMapping;

        private SE3 _groundTruthReferenceCorection;

        public Core(CoreContext context)
        {
            _context = context;
            _coarseInitializer = new CoarseInitializer(_context);
            _lastCoarseRMSE = new double[5];

            if (_context.Config.Core.PixelSelector is Configuration.NNPixelSelector)
            {
                _pixelSelector = new PixelSelectorNN(_context);
            }
            else
            {
                _pixelSelector = new PixelSelector(_context);
            }
            _selectionMap = new float[_context.w[0] * _context.h[0]];
            _allFramesHistory = new List<FrameShell>();
            _hCalib = new CalibHessian(_context);

            _coarseTracker = new CoarseTracker(_context);
            _coarseTrackerForNewKF = new CoarseTracker(_context);

            _coarseDistanceMap = new CoarseDistanceMap(_context);

            _ef = new EnergyFunctional(_context);

            _allKeyFramesHistory = new List<FrameShell>();
            _frameHessians = new List<FrameHessian>();

            _activeResiduals = new List<PointFrameResidual>();
            _allResVec = new List<float>();

            LinearizeOperation = true;

            _currentMinActDist = 2;

#if PAR
            _multiThreading = true;
#else
            _multiThreading = false;
#endif


            _statisticsNumActivatedPoints = 0;

            InitFailed = false;
            IsLost = false;

            _unmappedTrackedFrames = new LinkedList<FrameHessian>();

            _mappingTask = new Task(mappingLoop);
            _mappingTask.Start();
            _runMapping = true;
        }

        public void AddActiveFrame(ImageAndExposure<MinimalImageF, float> image)
        {
            if (_isLost) return;

            // =========================== add into allFrameHistory =========================
            var frameHessian = new FrameHessian(_context);

            FrameShell shell = new FrameShell();
            //shell.CamToWorld = SE3();      // no lock required, as fh is not used anywhere yet.
            //shell->aff_g2l = AffLight(0, 0);
            shell.MarginalizedAt = shell.Id = _allFramesHistory.Count;
            shell.Timestamp = image.TimeStamp;
            shell.IncomingId = image.Image.ID;
            if (image.GroundTruth != null)
                shell.GroundTruth = new SE3(image.GroundTruth[3], image.GroundTruth[4], image.GroundTruth[5], image.GroundTruth[6], image.GroundTruth[0], image.GroundTruth[1], image.GroundTruth[2]);
            shell.Image = image;
            frameHessian.Shell = shell;
            _allFramesHistory.Add(shell);


            // =========================== make Images / derivatives etc. =========================
            frameHessian.AbExposure = image.ExposureTime; //no needed values hold by context
            var start = DateTime.Now;
            frameHessian.MakeImages(image.Image);
            var end = DateTime.Now;
            _context.Output.UpdateData("MakeMapsTiming", "OnlyMakeImages", frameHessian.Shell.Id, (end - start).TotalMilliseconds, "Frame", "Time [ms]");
            frameHessian.Shell.Image.Image = null;//GC can collect original bitmap now


            if (!Initialized)
            {
                // use initializer!
                if (_coarseInitializer.FrameID < 0) // first frame set. fh is kept by coarseInitializer.
                {
                    _coarseInitializer.SetFirst(frameHessian);
                }
                else if (_coarseInitializer.TrackFrame(frameHessian))  // if SNAPPED
                {

                    InitializeFromInitializer(frameHessian);
                    //lock.unlock();
                    deliverTrackedFrame(frameHessian, true);
                }
                else
                {
                    // if still initializing
                    frameHessian.Shell.PoseValid = false;
                    frameHessian = null; //GC can collect // delete fh;
                }
                publishCamPose(shell, true);//force make init frames as keyFrames -> cam null pose included (so i dont wait for keyframe pose category to appear in GUI)
                _groundTruthReferenceCorection = null;//force resync ground truth after init
            }
            else    // do front-end operation.
            {
                // =========================== SWAP tracking reference?. =========================
                if (_coarseTrackerForNewKF.RefFrameId > _coarseTracker.RefFrameId)
                {
                    //boost::unique_lock<boost::mutex> crlock(coarseTrackerSwapMutex);
                    CoarseTracker tmp = _coarseTracker; _coarseTracker = _coarseTrackerForNewKF; _coarseTrackerForNewKF = tmp;
                }


                double[] tres = trackNewCoarse(frameHessian);
                if (double.IsInfinity(tres[0]) || double.IsInfinity(tres[1]) || double.IsInfinity(tres[2]) || double.IsInfinity(tres[3]))
                {
                    Log.LogError("Initial Tracking failed: LOST!");
                    IsLost = true;
                    return;
                }

                bool needToMakeKF = false;
                if (_context.Config.Core.KeyframesPerSecond > 0)
                {
                    needToMakeKF = _allFramesHistory.Count == 1 ||
                            (frameHessian.Shell.Timestamp - _allKeyFramesHistory[_allKeyFramesHistory.Count - 1].Timestamp) > 0.95f / _context.Config.Core.KeyframesPerSecond;
                }
                else
                {
                    // BRIGHTNESS CHECK
                    var refToFh = AffLight.FromToVecExposure(_coarseTracker.LastRef.AbExposure, frameHessian.AbExposure,
                            _coarseTracker.LastRefAffG2l, frameHessian.Shell.Aff_g2l);

                    needToMakeKF = _allFramesHistory.Count == 1 ||
                            _context.Config.Core.KfGlobalWeight * _context.Config.Core.MaxShiftWeightT * Math.Sqrt(tres[1]) / (_context.w[0] + _context.h[0]) +
                            _context.Config.Core.KfGlobalWeight * _context.Config.Core.MaxShiftWeightR * Math.Sqrt(tres[2]) / (_context.w[0] + _context.h[0]) +
                            _context.Config.Core.KfGlobalWeight * _context.Config.Core.MaxShiftWeightRT * Math.Sqrt(tres[3]) / (_context.w[0] + _context.h[0]) +
                            _context.Config.Core.KfGlobalWeight * _context.Config.Core.MaxAffineWeight * Math.Abs(Math.Log(refToFh[0])) > 1 ||
                            2 * _coarseTracker.FirstCoarseRMSE < tres[0];

                }


                //lock.unlock();
                deliverTrackedFrame(frameHessian, needToMakeKF);



                publishCamPose(shell, needToMakeKF);
                //for (IOWrap::Output3DWrapper* ow : outputWrapper)
                //    ow->publishCamPose(fh->shell, &Hcalib);
            }

            //if (frameHessian != null && frameHessian.Shell.PoseValid && frameHessian.Shell.Id > 0)
            //{
            //    var truth = frameHessian.Shell.GroundTruth;
            //    var last = _allFramesHistory[frameHessian.Shell.Id - 1].CamToWorld;
            //    var curr = frameHessian.Shell.CamToWorld;
            //    var diff = last.Inverse() * curr;
            //}
        }

        private void publishGraph()
        {
            //TODO
        }

        private PointsCloud _pointsCloudForPublish = new PointsCloud();
        private void publishKeyFrames(List<FrameHessian> frames)
        {
            foreach (var fh in frames)
            {
                if (!_pointsCloudForPublish.Frames.ContainsKey(fh.FrameID))
                {
                    _pointsCloudForPublish.Frames.Add(fh.FrameID, new PointsCloudFrame(fh.Shell.IncomingId));
                }
                var pcf = _pointsCloudForPublish.Frames[fh.FrameID];

                var inputSparse = new List<InputPointSparse>();

                //setFromKF
                ////SetFrame
                ////id = frame->id;
                var fx = _hCalib.fxl;
                var fy = _hCalib.fyl;
                var cx = _hCalib.cxl;
                var cy = _hCalib.cyl;
                ////width = wG[0];
                ////height = hG[0];
                var fxi = 1 / fx;
                var fyi = 1 / fy;
                var cxi = -cx / fx;
                var cyi = -cy / fy;
                ////needRefresh = true;

                // add all traces, inlier and outlier points.
                foreach (ImmaturePoint p in fh.ImmaturePoints)
                {
                    var ip = new InputPointSparse(_context);
                    for (int i = 0; i < _context.Config.Core.PatternNumber; i++)
                        ip.Color[i] = p.Color[i];
                    ip.U = p.U;
                    ip.V = p.V;
                    ip.Idpeth = (p.IdepthMax + p.IdepthMin) * 0.5f;
                    ip.IdepthHessian = 1000;
                    ip.RelObsBaseline = 0;
                    ip.NumGoodRes = 1;
                    ip.Status = 0;
                    inputSparse.Add(ip);
                }

                foreach (PointHessian p in fh.PointHessians)
                {
                    var ip = new InputPointSparse(_context);

                    for (int i = 0; i < _context.Config.Core.PatternNumber; i++)
                        ip.Color[i] = p.Color[i];
                    ip.U = p.U;
                    ip.V = p.V;
                    ip.Idpeth = p.IdepthScaled;
                    ip.RelObsBaseline = p.MaxRelBaseline;
                    ip.IdepthHessian = p.IdepthHessian;
                    ip.NumGoodRes = 0;
                    ip.Status = 1;
                    inputSparse.Add(ip);
                }

                foreach (PointHessian p in fh.PointHessiansMarginalized)
                {
                    var ip = new InputPointSparse(_context);
                    for (int i = 0; i < _context.Config.Core.PatternNumber; i++)
                        ip.Color[i] = p.Color[i];
                    ip.U = p.U;
                    ip.V = p.V;
                    ip.Idpeth = p.IdepthScaled;
                    ip.RelObsBaseline = p.MaxRelBaseline;
                    ip.IdepthHessian = p.IdepthHessian;
                    ip.NumGoodRes = 0;
                    ip.Status = 2;
                    inputSparse.Add(ip);
                }

                foreach (PointHessian p in fh.PointHessiansOut)
                {
                    var ip = new InputPointSparse(_context);
                    for (int i = 0; i < _context.Config.Core.PatternNumber; i++)
                        ip.Color[i] = p.Color[i];
                    ip.U = p.U;
                    ip.V = p.V;
                    ip.Idpeth = p.IdepthScaled;
                    ip.RelObsBaseline = p.MaxRelBaseline;
                    ip.IdepthHessian = p.IdepthHessian;
                    ip.NumGoodRes = 0;
                    ip.Status = 3;
                    inputSparse.Add(ip);
                }

                var camToWorld = fh.PRECamToWorld;
                //needRefresh = true;









                //refreshPC
                //if (canRefresh)
                //{
                //    needRefresh = needRefresh ||
                //            my_scaledTH != scaledTH ||
                //            my_absTH != absTH ||
                //            my_displayMode != mode ||
                //            my_minRelBS != minBS ||
                //            my_sparsifyFactor != sparsity;
                //}

                //if (!needRefresh) return false;
                //needRefresh = false;

                var scaledTH = _context.Config.Core.ScaledVarTH;
                var absTH = _context.Config.Core.AbsVarTH;
                var displayMode = _context.Config.Core.PointCloudMode;
                var minRelBS = _context.Config.Core.MinBS;
                var sparsifyFactor = _context.Config.Core.Sparsity;


                // make data
                pcf.Points = new PointsCloudPoint[inputSparse.Count * _context.Config.Core.PatternNumber];

                for (int i = 0; i < inputSparse.Count; i++)
                {
                    var p = inputSparse[i];
                    /* display modes:
                     * my_displayMode==0 - all pts, color-coded
                     * my_displayMode==1 - normal points
                     * my_displayMode==2 - active only
                     * my_displayMode==3 - nothing
                     */

                    if (displayMode == 1 && p.Status != 1 && p.Status != 2) continue;
                    if (displayMode == 2 && p.Status != 1) continue;
                    if (displayMode > 2) continue;

                    if (p.Idpeth < 0) continue;


                    float depth = 1.0f / p.Idpeth;
                    float depth4 = depth * depth; depth4 *= depth4;
                    float var = (1.0f / (p.IdepthHessian + 0.01f));

                    if (var * depth4 > scaledTH)
                        continue;

                    if (var > absTH)
                        continue;

                    if (p.RelObsBaseline < minRelBS)
                        continue;

                    for (int pnt = 0; pnt < _context.Config.Core.PatternNumber; pnt++)
                    {
                        if (sparsifyFactor > 1 && _context.Rand.Next() % sparsifyFactor != 0) continue;

                        pcf.Points[i + pnt] = new PointsCloudPoint();

                        int dx = Configuration.Core.StaticPatterns[_context.Config.Core.PatternIndex, pnt, 0];
                        int dy = Configuration.Core.StaticPatterns[_context.Config.Core.PatternIndex, pnt, 1];

                        pcf.Points[i + pnt].Coords = new double[3];
                        pcf.Points[i + pnt].Coords[0] = ((p.U + dx) * fxi + cxi) * depth;
                        pcf.Points[i + pnt].Coords[1] = ((p.V + dy) * fyi + cyi) * depth;
                        pcf.Points[i + pnt].Coords[2] = depth * (1 + 2 * fxi * (_context.Rand.NextDouble() - 0.5f));

                        pcf.Points[i + pnt].Coords = camToWorld * pcf.Points[i + pnt].Coords; //move and rotate point relative to current cam position


                        //mirror z & y axes for my output
                        pcf.Points[i + pnt].Coords[1] = -pcf.Points[i + pnt].Coords[1];
                        pcf.Points[i + pnt].Coords[2] = -pcf.Points[i + pnt].Coords[2];


                        pcf.Points[i + pnt].Color = new double[3];
                        if (displayMode == 0)
                        {
                            if (p.Status == 0)
                            {
                                pcf.Points[i + pnt].Color[0] = 0;
                                pcf.Points[i + pnt].Color[1] = 255;
                                pcf.Points[i + pnt].Color[2] = 255;
                            }
                            else if (p.Status == 1)
                            {
                                pcf.Points[i + pnt].Color[0] = 0;
                                pcf.Points[i + pnt].Color[1] = 255;
                                pcf.Points[i + pnt].Color[2] = 0;
                            }
                            else if (p.Status == 2)
                            {
                                pcf.Points[i + pnt].Color[0] = 0;
                                pcf.Points[i + pnt].Color[1] = 0;
                                pcf.Points[i + pnt].Color[2] = 255;
                            }
                            else if (p.Status == 3)
                            {
                                pcf.Points[i + pnt].Color[0] = 255;
                                pcf.Points[i + pnt].Color[1] = 0;
                                pcf.Points[i + pnt].Color[2] = 0;
                            }
                            else
                            {
                                pcf.Points[i + pnt].Color[0] = 255;
                                pcf.Points[i + pnt].Color[1] = 255;
                                pcf.Points[i + pnt].Color[2] = 255;
                            }

                        }
                        else
                        {
                            pcf.Points[i + pnt].Color[0] = p.Color[pnt];
                            pcf.Points[i + pnt].Color[1] = p.Color[pnt];
                            pcf.Points[i + pnt].Color[2] = p.Color[pnt];
                        }
                    }
                }
            }
            _context.Output.UpdatePointsCloud("ZZ Camera.PointsCloud", _pointsCloudForPublish);
        }

        private void publishCamPose(FrameShell shell, bool keyFrame)
        {
            //SetFrame
            //id = frame->id;
            var fx = _hCalib.fxl;
            var fy = _hCalib.fyl;
            var cx = _hCalib.cxl;
            var cy = _hCalib.cyl;
            //width = wG[0];
            //height = hG[0];
            var fxi = 1 / fx;
            var fyi = 1 / fy;
            var cxi = -cx / fx;
            var cyi = -cy / fy;
            //camToWorld = frame->camToWorld;
            //needRefresh = true;

            //DrawCam
            var height = _context.h[0];
            var width = _context.w[0];

            for (int i = 0; i < 2; i++)
            {
                var cp = new CamPose(shell.IncomingId);
                var cpH = new CamPose(shell.IncomingId);

                SE3 m;
                if (i == 1)
                {
                    if (shell.GroundTruth == null)
                    {
                        _groundTruthReferenceCorection = null;
                        continue;
                    }

                    if (_groundTruthReferenceCorection == null)
                    {
                        _groundTruthReferenceCorection = shell.GroundTruth.Inverse() * shell.CamToWorld;
                    }

                    m = shell.GroundTruth * _groundTruthReferenceCorection;// * new SE3(-0.83853208151455971, -0.383429817864945, -0.35807145614879288, -0.14707261925456475, -0.30228914801647205, -0.61760030873543093, 0.16710999547863095);
                }
                else
                {
                    m = shell.CamToWorld;
                }

                var vector = new double[3];

                Action<double[][], double> createCamPoints = (points, lsz) =>
                 {
                     vector[0] = 0; vector[1] = 0; vector[2] = 0;
                     points[0] = m * vector; points[0][2] = -points[0][2]; points[0][1] = -points[0][1];

                     //lb
                     vector[0] = lsz * (0 - cx) / fx; vector[1] = lsz * (0 - cy) / fy; vector[2] = lsz;
                     points[1] = m * vector; points[1][2] = -points[1][2]; points[1][1] = -points[1][1];

                     //lt
                     vector[0] = lsz * (0 - cx) / fx; vector[1] = lsz * (height - 1 - cy) / fy; vector[2] = lsz;
                     points[2] = m * vector; points[2][2] = -points[2][2]; points[2][1] = -points[2][1];

                     //rt
                     vector[0] = lsz * (width - 1 - cx) / fx; vector[1] = lsz * (height - 1 - cy) / fy; vector[2] = lsz;
                     points[3] = m * vector; points[3][2] = -points[3][2]; points[3][1] = -points[3][1];

                     //rb
                     vector[0] = lsz * (width - 1 - cx) / fx; vector[1] = lsz * (0 - cy) / fy; vector[2] = lsz;
                     points[4] = m * vector; points[4][2] = -points[4][2]; points[4][1] = -points[4][1];
                 };

                createCamPoints(cp.Points, 0.2);
                createCamPoints(cpH.Points, 0.1);

                cp.RenderAll = false;
                cpH.RenderAll = true;

                cp.IsKeyFrame = keyFrame;
                cpH.IsKeyFrame = keyFrame;

                if (i == 0)
                {
                    cp.Color = new byte[] { 255, 0, 0 };//Red
                    if (keyFrame)
                        cpH.Color = new byte[] { 255, 165, 0 };//Orange
                    else
                        cpH.Color = new byte[] { 0, 50, 255 };//Blue

                    _context.Output.UpdateCamPose("ZZ Camera.Cam.Current", cp);
                    _context.Output.UpdateCamPose($"ZZ Camera.Cam.History{(keyFrame ? "Key" : "Others")}", cpH);
                }
                else
                {
                    cp.Color = new byte[] { 255, 128, 128 };//Pink sort of
                    if (keyFrame)
                        cpH.Color = new byte[] { 255, 215, 0 };//Gold
                    else
                        cpH.Color = new byte[] { 0, 165, 255 };//Light blue

                    _context.Output.UpdateCamPose("ZZ Camera.TruthCam.Current", cp);
                    _context.Output.UpdateCamPose($"ZZ Camera.TruthCam.History{(keyFrame ? "Key" : "Others")}", cpH);
                }
            }

        }

        private double[] trackNewCoarse(FrameHessian fh)
        {
            Debug.Assert(_allFramesHistory.Count > 0);
            // set pose initialization.

            //TODO push live frame
            //for (IOWrap::Output3DWrapper* ow : outputWrapper)
            //    ow->pushLiveFrame(fh);



            FrameHessian lastF = _coarseTracker.LastRef;

            AffLight aff_last_2_l = new AffLight(0, 0);

            var lastF_2_fh_tries = new List<SE3>();
            if (_allFramesHistory.Count == 2)
                for (int i = 0; i < lastF_2_fh_tries.Count(); i++) lastF_2_fh_tries.Add(new SE3());//?wtf never do anything
            else
            {
                FrameShell slast = _allFramesHistory[_allFramesHistory.Count - 2];
                FrameShell sprelast = _allFramesHistory[_allFramesHistory.Count - 3];
                SE3 slast_2_sprelast = null;
                SE3 lastF_2_slast = null;
                {   // lock on global pose consistency!
                    //boost::unique_lock<boost::mutex> crlock(shellPoseMutex);
                    slast_2_sprelast = sprelast.CamToWorld.Inverse() * slast.CamToWorld;
                    lastF_2_slast = slast.CamToWorld.Inverse() * lastF.Shell.CamToWorld;
                    aff_last_2_l = slast.Aff_g2l;
                }
                SE3 fh_2_slast = slast_2_sprelast.Clone();// assumed to be the same as fh_2_slast.;i think this has to be deep copy otherwise if fh_2_slast will be changed slast_2_sprelast will be too but maybe is not changed anywhere
                SE3 fh_2_slastInv = fh_2_slast.Inverse();

                // get last delta-movement.
                lastF_2_fh_tries.Add(fh_2_slastInv * lastF_2_slast);   // assume constant motion.
                lastF_2_fh_tries.Add(fh_2_slastInv * fh_2_slastInv * lastF_2_slast);    // assume double motion (frame skipped)
                lastF_2_fh_tries.Add(SE3.Exp(fh_2_slast.Log().Mul(0.5)).Inverse() * lastF_2_slast); // assume half motion.
                lastF_2_fh_tries.Add(lastF_2_slast); // assume zero motion.
                lastF_2_fh_tries.Add(new SE3()); // assume zero motion FROM KF.


                SE3 fh_2_slastInv_lastf_2_slast = fh_2_slastInv * lastF_2_slast;
                // just try a TON of different initializations (all rotations). In the end,
                // if they don't work they will only be tried on the coarsest level, which is super fast anyway.
                // also, if tracking rails here we loose, so we really, really want to avoid that.
                for (float rotDelta = 0.02f; rotDelta < 0.05f; rotDelta++)
                {
                    lastF_2_fh_tries.Add(fh_2_slastInv_lastf_2_slast * new SE3(1, rotDelta, 0, 0, 0, 0, 0));                  // assume constant motion.
                    lastF_2_fh_tries.Add(fh_2_slastInv_lastf_2_slast * new SE3(1, 0, rotDelta, 0, 0, 0, 0));                  // assume constant motion.
                    lastF_2_fh_tries.Add(fh_2_slastInv_lastf_2_slast * new SE3(1, 0, 0, rotDelta, 0, 0, 0));                  // assume constant motion.
                    lastF_2_fh_tries.Add(fh_2_slastInv_lastf_2_slast * new SE3(1, -rotDelta, 0, 0, 0, 0, 0));                 // assume constant motion.
                    lastF_2_fh_tries.Add(fh_2_slastInv_lastf_2_slast * new SE3(1, 0, -rotDelta, 0, 0, 0, 0));                 // assume constant motion.
                    lastF_2_fh_tries.Add(fh_2_slastInv_lastf_2_slast * new SE3(1, 0, 0, -rotDelta, 0, 0, 0));                 // assume constant motion.
                    lastF_2_fh_tries.Add(fh_2_slastInv_lastf_2_slast * new SE3(1, rotDelta, rotDelta, 0, 0, 0, 0));           // assume constant motion.
                    lastF_2_fh_tries.Add(fh_2_slastInv_lastf_2_slast * new SE3(1, 0, rotDelta, rotDelta, 0, 0, 0));           // assume constant motion.
                    lastF_2_fh_tries.Add(fh_2_slastInv_lastf_2_slast * new SE3(1, rotDelta, 0, rotDelta, 0, 0, 0));           // assume constant motion.
                    lastF_2_fh_tries.Add(fh_2_slastInv_lastf_2_slast * new SE3(1, -rotDelta, rotDelta, 0, 0, 0, 0));          // assume constant motion.
                    lastF_2_fh_tries.Add(fh_2_slastInv_lastf_2_slast * new SE3(1, 0, -rotDelta, rotDelta, 0, 0, 0));          // assume constant motion.
                    lastF_2_fh_tries.Add(fh_2_slastInv_lastf_2_slast * new SE3(1, -rotDelta, 0, rotDelta, 0, 0, 0));          // assume constant motion.
                    lastF_2_fh_tries.Add(fh_2_slastInv_lastf_2_slast * new SE3(1, rotDelta, -rotDelta, 0, 0, 0, 0));          // assume constant motion.
                    lastF_2_fh_tries.Add(fh_2_slastInv_lastf_2_slast * new SE3(1, 0, rotDelta, -rotDelta, 0, 0, 0));          // assume constant motion.
                    lastF_2_fh_tries.Add(fh_2_slastInv_lastf_2_slast * new SE3(1, rotDelta, 0, -rotDelta, 0, 0, 0));          // assume constant motion.
                    lastF_2_fh_tries.Add(fh_2_slastInv_lastf_2_slast * new SE3(1, -rotDelta, -rotDelta, 0, 0, 0, 0));         // assume constant motion.
                    lastF_2_fh_tries.Add(fh_2_slastInv_lastf_2_slast * new SE3(1, 0, -rotDelta, -rotDelta, 0, 0, 0));         // assume constant motion.
                    lastF_2_fh_tries.Add(fh_2_slastInv_lastf_2_slast * new SE3(1, -rotDelta, 0, -rotDelta, 0, 0, 0));         // assume constant motion.
                    lastF_2_fh_tries.Add(fh_2_slastInv_lastf_2_slast * new SE3(1, -rotDelta, -rotDelta, -rotDelta, 0, 0, 0)); // assume constant motion.
                    lastF_2_fh_tries.Add(fh_2_slastInv_lastf_2_slast * new SE3(1, -rotDelta, -rotDelta, rotDelta, 0, 0, 0));  // assume constant motion.
                    lastF_2_fh_tries.Add(fh_2_slastInv_lastf_2_slast * new SE3(1, -rotDelta, rotDelta, -rotDelta, 0, 0, 0));  // assume constant motion.
                    lastF_2_fh_tries.Add(fh_2_slastInv_lastf_2_slast * new SE3(1, -rotDelta, rotDelta, rotDelta, 0, 0, 0));   // assume constant motion.
                    lastF_2_fh_tries.Add(fh_2_slastInv_lastf_2_slast * new SE3(1, rotDelta, -rotDelta, -rotDelta, 0, 0, 0));  // assume constant motion.
                    lastF_2_fh_tries.Add(fh_2_slastInv_lastf_2_slast * new SE3(1, rotDelta, -rotDelta, rotDelta, 0, 0, 0));   // assume constant motion.
                    lastF_2_fh_tries.Add(fh_2_slastInv_lastf_2_slast * new SE3(1, rotDelta, rotDelta, -rotDelta, 0, 0, 0));   // assume constant motion.
                    lastF_2_fh_tries.Add(fh_2_slastInv_lastf_2_slast * new SE3(1, rotDelta, rotDelta, rotDelta, 0, 0, 0));    // assume constant motion.
                }

                if (!slast.PoseValid || !sprelast.PoseValid || !lastF.Shell.PoseValid)
                {
                    lastF_2_fh_tries.Clear();
                    lastF_2_fh_tries.Add(new SE3());
                }
            }


            var flowVecs = new double[3] { 100, 100, 100 };
            SE3 lastF_2_fh = new SE3();
            AffLight aff_g2l = new AffLight(0, 0);


            // as long as maxResForImmediateAccept is not reached, I'll continue through the options.
            // I'll keep track of the so-far best achieved residual for each level in achievedRes.
            // If on a coarse level, tracking is WORSE than achievedRes, we will not continue to save time.


            var achievedRes = Enumerable.Repeat(double.NaN, 5).ToArray();
            bool haveOneGood = false;
            int tryIterations = 0;
            for (int i = 0; i < lastF_2_fh_tries.Count; i++)
            {
                AffLight aff_g2l_this = aff_last_2_l;
                SE3 lastF_2_fh_this = lastF_2_fh_tries[i].Clone();
                bool trackingIsGood = _coarseTracker.TrackNewestCoarse(
                        fh, ref lastF_2_fh_this, ref aff_g2l_this,
                        _context.PyrLevelsUsed - 1,
                        achievedRes);   // in each level has to be at least as good as the last try.
                tryIterations++;

                if (i != 0)
                {
                    Log.LogDebug(string.Format("RE-TRACK ATTEMPT {0} with initOption {1} and start-lvl {2} (ab {3} {4}): {5} {6} {7} {8} {9} -> {10} {11} {12} {13} {14}",
                            i,
                            i, _context.PyrLevelsUsed - 1,
                            aff_g2l_this.a, aff_g2l_this.b,
                            achievedRes[0],
                            achievedRes[1],
                            achievedRes[2],
                            achievedRes[3],
                            achievedRes[4],
                            _coarseTracker.LastResiduals[0],
                            _coarseTracker.LastResiduals[1],
                            _coarseTracker.LastResiduals[2],
                            _coarseTracker.LastResiduals[3],
                            _coarseTracker.LastResiduals[4]));
                }

                //for (int ii = 0; ii < 5; ii++)
                //{
                //    //var bF = _context.Output.Flush;
                //    //if (i == 30)
                //    //    _context.Output.Flush = true;
                //    //else
                //    //    _context.Output.Flush = false;
                //    //_context.Output.UpdateData($"TrackNewCoarse.Frame{fh.Shell.IncomingId}", $"Lvl{ii}",i, _coarseTracker.LastResiduals[0],"Iteration","Relative energy");

                //    _context.Output.UpdateData($"TrackNewCoarse", $"Lvl{ii}", (int.Parse(fh.Shell.IncomingId) * 100) + i, _coarseTracker.LastResiduals[ii], "Iteration", "Relative energy");

                //    //_context.Output.Flush = bF;
                //}

                // do we have a new winner?
                if (trackingIsGood && !double.IsInfinity(_coarseTracker.LastResiduals[0]) && !double.IsNaN(_coarseTracker.LastResiduals[0]) && !(_coarseTracker.LastResiduals[0] >= achievedRes[0]))
                {
                    Log.LogDebug(string.Format("Take over. minRes {0} -> {1}!", achievedRes[0], _coarseTracker.LastResiduals[0]));
                    flowVecs = _coarseTracker.LastFlowIndicators;
                    aff_g2l = aff_g2l_this;
                    lastF_2_fh = lastF_2_fh_this;
                    haveOneGood = true;
                }

                // take over achieved res (always).
                if (haveOneGood)
                {
                    for (int ii = 0; ii < 5; ii++)
                    {
                        if (double.IsNaN(achievedRes[ii]) || double.IsInfinity(achievedRes[ii]) || achievedRes[ii] > _coarseTracker.LastResiduals[ii])  // take over if achievedRes is either bigger or NAN.
                            achievedRes[ii] = _coarseTracker.LastResiduals[ii];
                    }
                }

                if (haveOneGood && achievedRes[0] < _lastCoarseRMSE[0] * _context.Config.Core.ReTrackThreshold)
                    break;

            }


            //for (int ii = 0; ii < 5; ii++)
            //{
            //    //var bF = _context.Output.Flush;
            //    //if (i == 30)
            //    //    _context.Output.Flush = true;
            //    //else
            //    //    _context.Output.Flush = false;
            //    //_context.Output.UpdateData($"TrackNewCoarse.Frame{fh.Shell.IncomingId}", $"Lvl{ii}",i, _coarseTracker.LastResiduals[0],"Iteration","Relative energy");

            //    _context.Output.UpdateData($"TrackNewCoarse", $"Lvl{ii}", (int.Parse(fh.Shell.IncomingId) * 100) + 0, achievedRes[ii], "Iteration", "Relative energy");

            //    //_context.Output.Flush = bF;
            //}


            if (!haveOneGood)
            {
                Log.LogDebug("BIG ERROR! tracking failed entirely. Take predicted pose and hope we may somehow recover.");
                flowVecs = new double[3];
                aff_g2l = aff_last_2_l;
                lastF_2_fh = lastF_2_fh_tries[0];
            }

            _lastCoarseRMSE = achievedRes;

            // no lock required, as fh is not used anywhere yet.
            fh.Shell.CamToTrackingRef = lastF_2_fh.Inverse();
            fh.Shell.TrackingRef = lastF.Shell;
            fh.Shell.Aff_g2l = aff_g2l;
            fh.Shell.CamToWorld = fh.Shell.TrackingRef.CamToWorld * fh.Shell.CamToTrackingRef;


            if (_coarseTracker.FirstCoarseRMSE < 0)
                _coarseTracker.FirstCoarseRMSE = achievedRes[0];

            Log.LogDebug(string.Format("Coarse Tracker tracked ab = {0} {1} (exp {2}). Res {3}!", aff_g2l.a, aff_g2l.b, fh.AbExposure, achievedRes[0]));

            //TODO logging stuff if neccesary
            //if (setting_logStuff)
            //{
            //    (*coarseTrackingLog) << std::setprecision(16)
            //                    << fh->shell->id << " "
            //                    << fh->shell->timestamp << " "
            //                    << fh->ab_exposure << " "
            //                    << fh->shell->camToWorld.log().transpose() << " "
            //                    << aff_g2l.a << " "
            //                    << aff_g2l.b << " "
            //                    << achievedRes[0] << " "
            //                    << tryIterations << "\n";
            //}


            return new double[4] { achievedRes[0], flowVecs[0], flowVecs[1], flowVecs[2] };
        }


        private void mappingLoop()
        {
            //boost::unique_lock < boost::mutex > lock (trackMapSyncMutex) ;

            while (_runMapping)
            {
                lock (_unmappedTrackedFrames)
                {
                    while (_unmappedTrackedFrames.Count == 0)
                    {
                        Monitor.Wait(_unmappedTrackedFrames);
                        //trackedFrameSignal.wait(lock) ;
                        if (!_runMapping) return;
                    }

                    FrameHessian fh = _unmappedTrackedFrames.First.Value;
                    _unmappedTrackedFrames.RemoveFirst();


                    // guaranteed to make a KF for the very first two tracked frames.
                    if (_allKeyFramesHistory.Count <= 2)
                    {
                        //lock.unlock();
                        makeKeyFrame(fh);
                        //lock.lock () ;

                        lock (_coarseTracker)
                        {
                            Monitor.PulseAll(_coarseTracker);
                        }
                        //mappedFrameSignal.notify_all();
                        continue;
                    }

                    if (_unmappedTrackedFrames.Count > 3)
                        _needToKetchupMapping = true;


                    if (_unmappedTrackedFrames.Count > 0) // if there are other frames to tracke, do that first.
                    {
                        //lock.unlock();
                        makeNonKeyFrame(fh);
                        //lock.lock () ;

                        if (_needToKetchupMapping && _unmappedTrackedFrames.Count > 0)
                        {
                            FrameHessian fhi = _unmappedTrackedFrames.First.Value;
                            _unmappedTrackedFrames.RemoveFirst();
                            {
                                //boost::unique_lock<boost::mutex> crlock(shellPoseMutex);
                                Debug.Assert(fhi.Shell.TrackingRef != null);
                                fhi.Shell.CamToWorld = fhi.Shell.TrackingRef.CamToWorld * fhi.Shell.CamToTrackingRef;
                                fhi.SetEvalPTScaled(fhi.Shell.CamToWorld.Inverse(), fhi.Shell.Aff_g2l);
                            }
                        }

                    }
                    else
                    {
                        if (_context.Config.Core.RealTimeMaxKF || _needNewKFAfter >= _frameHessians[_frameHessians.Count - 1].Shell.Id)
                        {
                            //lock.unlock();
                            makeKeyFrame(fh);
                            _needToKetchupMapping = false;
                            //lock.lock () ;
                        }
                        else
                        {
                            //lock.unlock();
                            makeNonKeyFrame(fh);
                            //lock.lock () ;
                        }
                    }
                }
                lock (_coarseTracker)
                {
                    Monitor.PulseAll(_coarseTracker);
                }
                //mappedFrameSignal.notify_all();
            }
            Log.LogInfo("MAPPING FINISHED!");
        }

        private void deliverTrackedFrame(FrameHessian fh, bool needKF)
        {
            if (LinearizeOperation)
            {
                //this if maybe can be removed and _lastRefStopID too now have other mechanism to stop program
                if (_context.Config.Core.GoStepByStep && _lastRefStopID != _coarseTracker.RefFrameId)
                {
                    float[] tmp = new float[_context.w[0] * _context.h[0] * 3];
                    Buffer.BlockCopy(fh.dI, 0, tmp, 0, tmp.Length * sizeof(float));

                    var img = new MinimalImageF3(fh.Shell.IncomingId, _context.w[0], _context.h[0], tmp);
                    _context.Output.UpdateImage("FrameToTrack", img);// IOWrap::displayImage("frameToTrack", &img);

                    //Debuging stuff maybe can be uselful TODO
                    //Console.ReadKey();
                    ////while (true)
                    ////{
                    ////    char k = IOWrap::waitKey(0);
                    ////    if (k == ' ') break;
                    ////    handleKey(k);
                    ////}

                    _lastRefStopID = _coarseTracker.RefFrameId;
                }
                else
                {
                    //Console.ReadKey();
                }


                if (needKF)
                {
                    var start = DateTime.Now;
                    makeKeyFrame(fh);
                    var end = DateTime.Now;
                    _context.Output.UpdateData("MakeMapsTiming", "WholeKeyFrame", fh.Shell.Id, (end - start).TotalMilliseconds, "Frame", "Time [ms]");

                    if ((_context.Config.Core.PausingMode & Configuration.PausingMode.OnKeyFrame) != Configuration.PausingMode.NoPause)
                    {
                        Log.LogInfo($"Keyframe {fh.Shell.Id} finished, press key to continue...");
                        Console.ReadKey();
                    }
                }
                else
                {
                    var start = DateTime.Now;
                    makeNonKeyFrame(fh);
                    var end = DateTime.Now;
                    _context.Output.UpdateData("MakeMapsTiming", "WholeNonKeyFrame", fh.Shell.Id, (end - start).TotalMilliseconds, "Frame", "Time [ms]");

                    if ((_context.Config.Core.PausingMode & Configuration.PausingMode.OnEachFrame) != Configuration.PausingMode.NoPause)
                    {
                        Log.LogInfo($"Frame {fh.Shell.Id} finished, press key to continue...");
                        Console.ReadKey();
                    }
                }
            }
            else
            {
                //boost::unique_lock < boost::mutex > lock (trackMapSyncMutex) ;
                lock (_unmappedTrackedFrames)
                {
                    _unmappedTrackedFrames.AddLast(fh);
                    if (needKF) _needNewKFAfter = fh.Shell.TrackingRef.Id;
                    //trackedFrameSignal.notify_all();
                    Monitor.PulseAll(_unmappedTrackedFrames);
                }

                lock (_coarseTracker)
                {
                    while (_coarseTrackerForNewKF.RefFrameId == -1 && _coarseTracker.RefFrameId == -1)
                    {
                        Monitor.Wait(_coarseTracker);
                        //mappedFrameSignal.wait(lock) ;
                    }
                }

                //lock.unlock();
            }
        }
        private void traceNewCoarse(FrameHessian fh)
        {
            //boost::unique_lock < boost::mutex > lock (mapMutex) ;

            int trace_total = 0, trace_good = 0, trace_oob = 0, trace_out = 0, trace_skip = 0, trace_badcondition = 0, trace_uninitialized = 0;

            Mat33f K = Mat33f.Identity();
            K[0, 0] = _hCalib.fxl;
            K[1, 1] = _hCalib.fyl;
            K[0, 2] = _hCalib.cxl;
            K[1, 2] = _hCalib.cyl;

            ///*Mat33f K = Mat33f::Identity();
            //K(0, 0) = Hcalib.fxl();
            //K(1, 1) = Hcalib.fyl();
            //K(0, 2) = Hcalib.cxl() - 0.5;
            //K(1, 2) = Hcalib.cyl() - 0.5;*/

            foreach (var host in _frameHessians)        // go through all active frames
            {

                SE3 hostToNew = fh.PREWorldToCam * host.PRECamToWorld;
                Mat33f KRKi = K * hostToNew.RotationMatrixF() * K.Inverse();
                float[] Kt = K * hostToNew.TranslationF();

                float[] aff = AffLight.FromToVecExposure(host.AbExposure, fh.AbExposure, host.Aff_g2l(), fh.Aff_g2l()).Cast<double, float>().ToArray();

                foreach (ImmaturePoint ph in host.ImmaturePoints)
                {
                    ph.TraceOn(fh, KRKi, Kt, aff, _hCalib, _context.Config.Output.Log.Debug);

                    if (ph.LastTraceStatus == ImmaturePointStatus.IPS_GOOD) trace_good++;
                    if (ph.LastTraceStatus == ImmaturePointStatus.IPS_BADCONDITION) trace_badcondition++;
                    if (ph.LastTraceStatus == ImmaturePointStatus.IPS_OOB) trace_oob++;
                    if (ph.LastTraceStatus == ImmaturePointStatus.IPS_OUTLIER) trace_out++;
                    if (ph.LastTraceStatus == ImmaturePointStatus.IPS_SKIPPED) trace_skip++;
                    if (ph.LastTraceStatus == ImmaturePointStatus.IPS_UNINITIALIZED) trace_uninitialized++;
                    trace_total++;
                }
            }
            Log.LogDebug(string.Format("ADD: TRACE: {0} points. {1} ({2}%) good. {3} ({4}%) skip. {5} ({6}%) badcond. {7} ({8}%) oob. {9} ({10}%) out. {11} ({12}%) uninit.",
                    trace_total,
                    trace_good, 100 * trace_good / (float)trace_total,
                    trace_skip, 100 * trace_skip / (float)trace_total,
                    trace_badcondition, 100 * trace_badcondition / (float)trace_total,
                    trace_oob, 100 * trace_oob / (float)trace_total,
                    trace_out, 100 * trace_out / (float)trace_total,
                    trace_uninitialized, 100 * trace_uninitialized / (float)trace_total));
        }

        private void makeNonKeyFrame(FrameHessian frameHessian)
        {

            // needs to be set by mapping thread. no lock required since we are in mapping thread.
            {
                //boost::unique_lock<boost::mutex> crlock(shellPoseMutex);
                Debug.Assert(frameHessian.Shell.TrackingRef != null);
                frameHessian.Shell.CamToWorld = frameHessian.Shell.TrackingRef.CamToWorld * frameHessian.Shell.CamToTrackingRef;
                frameHessian.SetEvalPTScaled(frameHessian.Shell.CamToWorld.Inverse(), frameHessian.Shell.Aff_g2l);
            }

            traceNewCoarse(frameHessian);
            ////delete fh;
        }


        private void makeKeyFrame(FrameHessian fh)
        {
            // needs to be set by mapping thread
            {
                //boost::unique_lock<boost::mutex> crlock(shellPoseMutex);
                Debug.Assert(fh.Shell.TrackingRef != null);
                fh.Shell.CamToWorld = fh.Shell.TrackingRef.CamToWorld * fh.Shell.CamToTrackingRef;
                fh.SetEvalPTScaled(fh.Shell.CamToWorld.Inverse(), fh.Shell.Aff_g2l);
            }

            traceNewCoarse(fh);

            //boost::unique_lock < boost::mutex > lock (mapMutex) ;

            // =========================== Flag Frames to be Marginalized. =========================
            flagFramesForMarginalization(fh);


            // =========================== add New Frame to Hessian Struct. =========================
            fh.Idx = _frameHessians.Count;
            _frameHessians.Add(fh);
            fh.FrameID = _allKeyFramesHistory.Count;
            _allKeyFramesHistory.Add(fh.Shell);
            _ef.InsertFrame(fh);

            setPrecalcValues();



            // =========================== add new residuals for old points =========================
            int numFwdResAdde = 0;
            foreach (var fh1 in _frameHessians)     // go through all active frames
            {
                if (fh1 == fh) continue;
                foreach (var ph in fh1.PointHessians)
                {
                    var r = new PointFrameResidual(_context, ph, fh1, fh);
                    r.StateState = ResState.IN;
                    ph.Residuals.Add(r);
                    Log.CountAdd("ResidualAdded for old points");
                    _ef.InsertResidual(r);
                    ph.LastResiduals[1] = ph.LastResiduals[0];
                    ph.LastResiduals[0] = new ValueTuple<PointFrameResidual, ResState>(r, ResState.IN);
                    numFwdResAdde += 1;
                }
            }
            Log.CountLogDebug("ResidualAdded for old points");

            // =========================== Activate Points (& flag for marginalization). =========================
            activatePointsMT();
            _ef.MakeIDX();

            // =========================== OPTIMIZE ALL =========================
            //wtf it is probably for nothing last item in framehessians is always fh i think
            fh.FrameEnergyTh = _frameHessians.Last().FrameEnergyTh;
            float rmse = optimize(_context.Config.Core.MaxOptIterations);



            // =========================== Figure Out if INITIALIZATION FAILED =========================
            if (_allKeyFramesHistory.Count <= 4)
            {
                if (_allKeyFramesHistory.Count == 2 && rmse > 20 * _context.Config.Core.BenchmarkInitializerSlackFactor)
                {
                    Log.LogInfo("I THINK INITIALIZATINO FAILED! Resetting.");
                    InitFailed = true;
                }
                if (_allKeyFramesHistory.Count == 3 && rmse > 13 * _context.Config.Core.BenchmarkInitializerSlackFactor)
                {
                    Log.LogInfo("I THINK INITIALIZATINO FAILED! Resetting.");
                    InitFailed = true;
                }
                if (_allKeyFramesHistory.Count == 4 && rmse > 9 * _context.Config.Core.BenchmarkInitializerSlackFactor)
                {
                    Log.LogInfo("I THINK INITIALIZATINO FAILED! Resetting.");
                    InitFailed = true;
                }
            }



            if (IsLost) return;




            // =========================== REMOVE OUTLIER =========================
            removeOutliers();




            {
                //boost::unique_lock<boost::mutex> crlock(coarseTrackerSwapMutex);
                _coarseTrackerForNewKF.MakeK(_hCalib);
                _coarseTrackerForNewKF.SetCoarseTrackingRef(_frameHessians);



                _coarseTrackerForNewKF.debugPlotIDepthMap(ref _minIdJetVisTracker, ref _maxIdJetVisTracker);
                _coarseTrackerForNewKF.debugPlotIDepthMapFloat();
            }


            debugPlot("Post Optimize");






            // =========================== (Activate-)Marginalize Points =========================
            flagPointsForRemoval();
            _ef.DropPointsF();
            getNullspaces(
                    _ef.LastNullspaces_pose,
                    _ef.LastNullspaces_scale,
                    _ef.LastNullspaces_affA,
                    _ef.LastNullspaces_affB);
            _ef.MarginalizePointsF();



            // =========================== add new Immature points & new residuals =========================
            makeNewTraces(fh);




            publishGraph();
            publishKeyFrames(_frameHessians);
            //for (IOWrap::Output3DWrapper* ow : outputWrapper)
            //{
            //    ow->publishGraph(ef->connectivityMap);
            //    ow->publishKeyframes(frameHessians, false, &Hcalib);
            //}



            // =========================== Marginalize Frames =========================

            for (int i = 0; i < _frameHessians.Count; i++)
                if (_frameHessians[i].FlaggedForMarginalization)
                { marginalizeFrame(_frameHessians[i]); i = 0; }



            printLogLine();
            ////printEigenValLine();
        }

        private void printLogLine()
        {
            if (_frameHessians.Count == 0) return;

            Log.LogDebug(string.Format("LOG {0}: {1} fine. Res: {2} A, {3} L, {4} M; ({5} / {6}) forceDrop. a={7}, b={8}. Window {9} ({10})",
                    _allKeyFramesHistory[_allKeyFramesHistory.Count - 1].Id,
                    _statisticsLastFineTrackRMSE,
                    _ef.ResInA,
                    _ef.ResInL,
                    _ef.ResInM,
                    (int)_statisticsNumForceDroppedResFwd,
                    (int)_statisticsNumForceDroppedResBwd,
                    _allKeyFramesHistory[_allKeyFramesHistory.Count - 1].Aff_g2l.a,
                    _allKeyFramesHistory[_allKeyFramesHistory.Count - 1].Aff_g2l.b,
                    _frameHessians[_frameHessians.Count - 1].Shell.Id - _frameHessians[0].Shell.Id,
                    _frameHessians.Count));


            //TODO log stuff if neccesarry
            //if (!setting_logStuff) return;
            //
            //if (numsLog != 0)
            //{
            //    (*numsLog) << allKeyFramesHistory.back()->id << " " <<
            //            statistics_lastFineTrackRMSE << " " <<
            //            (int)statistics_numCreatedPoints << " " <<
            //            (int)statistics_numActivatedPoints << " " <<
            //            (int)statistics_numDroppedPoints << " " <<
            //            (int)statistics_lastNumOptIts << " " <<
            //            ef->resInA << " " <<
            //            ef->resInL << " " <<
            //            ef->resInM << " " <<
            //            statistics_numMargResFwd << " " <<
            //            statistics_numMargResBwd << " " <<
            //            statistics_numForceDroppedResFwd << " " <<
            //            statistics_numForceDroppedResBwd << " " <<
            //            frameHessians.back()->aff_g2l().a << " " <<
            //            frameHessians.back()->aff_g2l().b << " " <<
            //            frameHessians.back()->shell->id - frameHessians.front()->shell->id << " " <<
            //            (int)frameHessians.size() << " " << "\n";
            //    numsLog->flush();
            //}
        }

        private void marginalizeFrame(FrameHessian frame)
        {
            // marginalize or remove all this frames points.
            Debug.Assert(frame.PointHessians.Count == 0);


            _ef.MarginalizeFrame(frame.EfFrame);

            // drop all observations of existing points in that frame.

            foreach (FrameHessian fh in _frameHessians)
            {
                if (fh == frame) continue;

                foreach (PointHessian ph in fh.PointHessians)
                {
                    for (int i = 0; i < ph.Residuals.Count; i++)
                    {
                        PointFrameResidual r = ph.Residuals[i];
                        if (r.Target == frame)
                        {
                            if (ph.LastResiduals[0].Item1 == r)
                                ph.LastResiduals[0].Item1 = null;
                            else if (ph.LastResiduals[1].Item1 == r)
                                ph.LastResiduals[1].Item1 = null;


                            if (r.Host.FrameID < r.Target.FrameID)
                                _statisticsNumForceDroppedResFwd++;
                            else
                                _statisticsNumForceDroppedResBwd++;

                            _ef.DropResidual(r.EfResidual);
                            ph.Residuals[i] = ph.Residuals[ph.Residuals.Count - 1];
                            ph.Residuals.RemoveAt(ph.Residuals.Count - 1);
                            break;
                        }
                    }
                }
            }


            {
                var v = new List<FrameHessian>();
                v.Add(frame);
                publishKeyFrames(v);
                //for (IOWrap::Output3DWrapper* ow : outputWrapper)
                //    ow->publishKeyframes(v, true, &Hcalib);
            }


            frame.Shell.MarginalizedAt = _frameHessians[_frameHessians.Count - 1].Shell.Id;
            frame.Shell.MovedByOpt = frame.W2cLeftEps().Norm();

            _frameHessians.Remove(frame);

            for (int i = 0; i < _frameHessians.Count; i++)
                _frameHessians[i].Idx = i;


            setPrecalcValues();
            _ef.SetAdjointsF();
        }

        private void makeNewTraces(FrameHessian newFrame)
        {
            //can be used when fast is uncommented in selector:makemaps
            //_pixelSelector.AllowFast = true;

            ////int numPointsTotal = makePixelStatus(newFrame->dI, selectionMap, wG[0], hG[0], setting_desiredDensity);
            int numPointsTotal = _pixelSelector.MakeMaps(newFrame, _selectionMap, _context.Config.Core.DesiredImmatureDensity);

            newFrame.PointHessians.Capacity = (int)(numPointsTotal * 1.2f);
            ////fh->pointHessiansInactive.reserve(numPointsTotal*1.2f);
            newFrame.PointHessiansMarginalized.Capacity = (int)(numPointsTotal * 1.2f);
            newFrame.PointHessiansOut.Capacity = (int)(numPointsTotal * 1.2f);

            for (int y = _context.Config.Core.PatternPadding + 1; y < _context.h[0] - _context.Config.Core.PatternPadding - 2; y++)
                for (int x = _context.Config.Core.PatternPadding + 1; x < _context.w[0] - _context.Config.Core.PatternPadding - 2; x++)
                {
                    int i = x + y * _context.w[0];
                    if (_selectionMap[i] == 0) continue;

                    ImmaturePoint impt = new ImmaturePoint(_context, x, y, newFrame, _selectionMap[i]);
                    if (!float.IsInfinity(impt.EnergyTh) && !float.IsNaN(impt.EnergyTh))
                        newFrame.ImmaturePoints.Add(impt);

                }
            Log.LogDebug(string.Format("MADE {0} IMMATURE POINTS!", (int)newFrame.ImmaturePoints.Count));
        }

        private List<double[]> getNullspaces(List<double[]> nullspaces_pose, List<double[]> nullspaces_scale, List<double[]> nullspaces_affA, List<double[]> nullspaces_affB)
        {
            nullspaces_pose.Clear();
            nullspaces_scale.Clear();
            nullspaces_affA.Clear();
            nullspaces_affB.Clear();


            int n = _context.Config.Core.Cpars + _frameHessians.Count * 8;
            List<double[]> nullspaces_x0_pre = new List<double[]>();
            for (int i = 0; i < 6; i++)
            {
                var nullspacex0 = new double[n];
                // nullspace_x0.SetZero();
                foreach (FrameHessian fh in _frameHessians)
                {
                    var si = _context.Config.Core.Cpars + fh.Idx * 8;
                    for (int ii = 0; ii < 6; ii++)//nullspace_x0.segment < 6 > (CPARS + fh->idx * 8) = fh.NullspacesPose[i];
                    {
                        nullspacex0[si + ii] = fh.NullspacesPose[i][ii];
                    }
                    for (int ii = 0; ii < 3; ii++)//nullspace_x0.segment < 3 > (CPARS + fh->idx * 8) *= SCALE_XI_TRANS_INVERSE;
                    {
                        nullspacex0[si + ii] *= _context.Config.Core.ScaleXITRANSInverse;
                    }
                    si = _context.Config.Core.Cpars + fh.Idx * 8 + 3;
                    for (int ii = 0; ii < 3; ii++)//nullspace_x0.segment < 3 > (CPARS + fh->idx * 8 + 3) *= SCALE_XI_ROT_INVERSE;
                    {
                        nullspacex0[si + ii] *= _context.Config.Core.ScaleXIROTInverse;
                    }


                }
                nullspaces_x0_pre.Add(nullspacex0);
                nullspaces_pose.Add(nullspacex0);
            }
            for (int i = 0; i < 2; i++)
            {
                var nullspacex0 = new double[n];
                //nullspacex0.setZero();
                foreach (FrameHessian fh in _frameHessians)
                {
                    var si = _context.Config.Core.Cpars + fh.Idx * 8 + 6;
                    for (int ii = 0; ii < 2; ii++) //nullspacex0.segment < 2 > (CPARS + fh->idx * 8 + 6) = fh->nullspaces_affine.col(i).head < 2 > ();
                    {
                        nullspacex0[si + ii] = fh.NullspacesAffine[i][ii];
                    }

                    nullspacex0[si] *= _context.Config.Core.ScaleAInverse;
                    si = _context.Config.Core.Cpars + fh.Idx * 8 + 7;
                    nullspacex0[si] *= _context.Config.Core.ScaleBInverse;
                }
                nullspaces_x0_pre.Add(nullspacex0);
                if (i == 0) nullspaces_affA.Add(nullspacex0);
                if (i == 1) nullspaces_affB.Add(nullspacex0);
            }

            var nullspace_x0 = new double[n];
            //nullspacex0.setZero();
            foreach (FrameHessian fh in _frameHessians)
            {
                var si = _context.Config.Core.Cpars + fh.Idx * 8;
                for (int ii = 0; ii < 6; ii++)//nullspace_x0.segment < 6 > (CPARS + fh->idx * 8) = fh->nullspaces_scale;
                {
                    nullspace_x0[si + ii] = fh.NullspacesScale[ii];
                }
                for (int ii = 0; ii < 3; ii++)//nullspace_x0.segment < 3 > (CPARS + fh->idx * 8) *= SCALE_XI_TRANS_INVERSE;
                {
                    nullspace_x0[si + ii] *= _context.Config.Core.ScaleXITRANSInverse;
                }
                si = _context.Config.Core.Cpars + fh.Idx * 8 + 3;
                for (int ii = 0; ii < 3; ii++)//nullspace_x0.segment < 3 > (CPARS + fh->idx * 8 + 3) *= SCALE_XI_ROT_INVERSE;
                {
                    nullspace_x0[si + ii] *= _context.Config.Core.ScaleXIROTInverse;

                }
            }
            nullspaces_x0_pre.Add(nullspace_x0);
            nullspaces_scale.Add(nullspace_x0);

            return nullspaces_x0_pre;
        }

        private void flagPointsForRemoval()
        {
            Debug.Assert(_ef.EFIndicesValid);

            List<FrameHessian> fhsToKeepPoints = new List<FrameHessian>();
            List<FrameHessian> fhsToMargPoints = new List<FrameHessian>();

            ////if(setting_margPointVisWindow>0)
            {
                for (int i = (_frameHessians.Count) - 1; i >= 0 && i >= (_frameHessians.Count); i--)//TODO what?? seems to be never true
                    if (!_frameHessians[i].FlaggedForMarginalization) fhsToKeepPoints.Add(_frameHessians[i]);

                for (int i = 0; i < _frameHessians.Count; i++)
                    if (_frameHessians[i].FlaggedForMarginalization) fhsToMargPoints.Add(_frameHessians[i]);
            }



            ////ef->setAdjointsF();
            ////ef->setDeltaF(&Hcalib);
            int flag_oob = 0, flag_in = 0, flag_inin = 0, flag_nores = 0;

            foreach (FrameHessian host in _frameHessians)        // go through all active frames
            {

                for (int i = 0; i < host.PointHessians.Count; i++)
                {
                    PointHessian ph = host.PointHessians[i];
                    if (ph == null) continue;

                    if (ph.IdepthScaled < 0 || ph.Residuals.Count == 0)
                    {
                        host.PointHessiansOut.Add(ph);
                        ph.EfPoint.StateFlag = EFPointStatus.PS_DROP;
                        host.PointHessians[i] = null;
                        flag_nores++;
                        Log.CountAdd("ph.IdepthScaled < 0 || ph.Residuals.Count == 0");
                    }
                    else if (ph.IsOOB(fhsToKeepPoints, fhsToMargPoints) || host.FlaggedForMarginalization)
                    {
                        flag_oob++;
                        if (ph.IsInlierNew())
                        {
                            flag_in++;
                            int ngoodRes = 0;
                            foreach (PointFrameResidual r in ph.Residuals)
                            {
                                r.ResetOOB();
                                r.Linearize(_hCalib);
                                r.EfResidual.IsLinearized = false;
                                r.ApplyRes(true);
                                if (r.EfResidual.IsActiveAndIsGoodNEW)
                                {
                                    r.EfResidual.FixLinearizationF(_ef);
                                    ngoodRes++;
                                }
                            }
                            if (ph.IdepthHessian > _context.Config.Core.MinIdepthHMarg)
                            {
                                flag_inin++;
                                ph.EfPoint.StateFlag = EFPointStatus.PS_MARGINALIZE;
                                host.PointHessiansMarginalized.Add(ph);
                            }
                            else
                            {
                                ph.EfPoint.StateFlag = EFPointStatus.PS_DROP;
                                host.PointHessiansOut.Add(ph);
                                Log.CountAdd("not ph.IdepthHessian > _context.Config.Core.MinIdepthHMarg");
                            }
                        }
                        else
                        {
                            host.PointHessiansOut.Add(ph);
                            Log.CountAdd("not ph.IsInlierNew()");
                            ph.EfPoint.StateFlag = EFPointStatus.PS_DROP;
                            ////printf("drop point in frame %d (%d goodRes, %d activeRes)\n", ph->host->idx, ph->numGoodResiduals, (int)ph->residuals.size());
                        }

                        host.PointHessians[i] = null;
                    }
                }


                for (int i = 0; i < host.PointHessians.Count; i++)
                {
                    if (host.PointHessians[i] == null)
                    {
                        host.PointHessians[i] = host.PointHessians[host.PointHessians.Count - 1];
                        host.PointHessians.RemoveAt(host.PointHessians.Count - 1);
                        i--;
                    }
                }



                Log.CountLogDebug("ph.IdepthScaled < 0 || ph.Residuals.Count == 0");
                Log.CountLogDebug("not ph.IdepthHessian > _context.Config.Core.MinIdepthHMarg");
                Log.CountLogDebug("not ph.IsInlierNew()");
            }
        }

        private void debugPlot(string name)
        {
            var images = new List<MinimalImageB3>();


            float minID = 0, maxID = 0;
            if ((int)(_context.Config.Core.FreeDebugParam5 + 0.5f) == 7)
            {
                List<float> allID = new List<float>();
                for (int f = 0; f < _frameHessians.Count; f++)
                {
                    foreach (PointHessian ph in _frameHessians[f].PointHessians)
                        if (ph != null) allID.Add(ph.IdepthScaled);

                    foreach (PointHessian ph in _frameHessians[f].PointHessiansMarginalized)
                        if (ph != null) allID.Add(ph.IdepthScaled);

                    foreach (PointHessian ph in _frameHessians[f].PointHessiansOut)
                        if (ph != null) allID.Add(ph.IdepthScaled);
                }
                allID.Sort();
                int n = allID.Count - 1;
                minID = allID[(int)(n * 0.05)];
                maxID = allID[(int)(n * 0.95)];


                // slowly adapt: change by maximum 10% of old span.
                float maxChange = 0.1f * (_maxIdJetVisDebug - _minIdJetVisDebug);
                if (_maxIdJetVisDebug < 0 || _minIdJetVisDebug < 0) maxChange = 1e5f;


                if (minID < _minIdJetVisDebug - maxChange)
                    minID = _minIdJetVisDebug - maxChange;
                if (minID > _minIdJetVisDebug + maxChange)
                    minID = _minIdJetVisDebug + maxChange;


                if (maxID < _maxIdJetVisDebug - maxChange)
                    maxID = _maxIdJetVisDebug - maxChange;
                if (maxID > _maxIdJetVisDebug + maxChange)
                    maxID = _maxIdJetVisDebug + maxChange;

                _maxIdJetVisDebug = maxID;
                _minIdJetVisDebug = minID;

            }












            int wh = _context.h[0] * _context.w[0];
            for (int f = 0; f < _frameHessians.Count; f++)
            {
                MinimalImageB3 img = new MinimalImageB3(_frameHessians[f].Shell.IncomingId, _context.w[0], _context.h[0]);
                images.Add(img);
                //float* fd = frameHessians[f]->I;
                var fd = _frameHessians[f].dI;


                for (int i = 0; i < wh; i++)
                {
                    int c = (int)(fd[i, 0] * 0.9f);
                    if (c > 255) c = 255;
                    img.At(i, 0) = (byte)c;
                    img.At(i, 1) = (byte)c;
                    img.At(i, 2) = (byte)c;
                }

                if ((int)(_context.Config.Core.FreeDebugParam5 + 0.5f) == 0)
                {
                    foreach (PointHessian ph in _frameHessians[f].PointHessians)
                    {
                        if (ph == null) continue;

                        img.SetPixelCirc((int)(ph.U + 0.5f), (int)(ph.V + 0.5f), img.GetRainbowColor(ph.IdepthScaled));
                    }
                    foreach (PointHessian ph in _frameHessians[f].PointHessiansMarginalized)
                    {
                        if (ph == null) continue;
                        img.SetPixelCirc((int)(ph.U + 0.5f), (int)(ph.V + 0.5f), img.GetRainbowColor(ph.IdepthScaled));
                    }
                    foreach (PointHessian ph in _frameHessians[f].PointHessiansOut)
                        img.SetPixelCirc((int)(ph.U + 0.5f), (int)(ph.V + 0.5f), new byte[] { 255, 255, 255 });
                }
                else if ((int)(_context.Config.Core.FreeDebugParam5 + 0.5f) == 1)
                {
                    foreach (PointHessian ph in _frameHessians[f].PointHessians)
                    {
                        if (ph == null) continue;
                        img.SetPixelCirc((int)(ph.U + 0.5f), (int)(ph.V + 0.5f), img.GetRainbowColor(ph.IdepthScaled));
                    }

                    foreach (PointHessian ph in _frameHessians[f].PointHessiansMarginalized)
                        img.SetPixelCirc((int)(ph.U + 0.5f), (int)(ph.V + 0.5f), new byte[] { 0, 0, 0 });

                    foreach (PointHessian ph in _frameHessians[f].PointHessiansOut)
                        img.SetPixelCirc((int)(ph.U + 0.5f), (int)(ph.V + 0.5f), new byte[] { 255, 255, 255 });
                }
                else if ((int)(_context.Config.Core.FreeDebugParam5 + 0.5f) == 2)
                {
                    //nothing
                }
                else if ((int)(_context.Config.Core.FreeDebugParam5 + 0.5f) == 3)
                {
                    foreach (ImmaturePoint ph in _frameHessians[f].ImmaturePoints)
                    {
                        if (ph == null) continue;
                        if (ph.LastTraceStatus == ImmaturePointStatus.IPS_GOOD ||
                                ph.LastTraceStatus == ImmaturePointStatus.IPS_SKIPPED ||
                                ph.LastTraceStatus == ImmaturePointStatus.IPS_BADCONDITION)
                        {
                            if (float.IsInfinity(ph.IdepthMax) || float.IsNaN(ph.IdepthMax))
                                img.SetPixelCirc((int)(ph.U + 0.5f), (int)(ph.V + 0.5f), new byte[] { 0, 0, 0 });
                            else
                            {
                                img.SetPixelCirc((int)(ph.U + 0.5f), (int)(ph.V + 0.5f), img.GetRainbowColor((ph.IdepthMin + ph.IdepthMax) * 0.5f));
                            }
                        }
                    }
                }
                else if ((int)(_context.Config.Core.FreeDebugParam5 + 0.5f) == 4)
                {
                    foreach (ImmaturePoint ph in _frameHessians[f].ImmaturePoints)
                    {
                        if (ph == null) continue;

                        if (ph.LastTraceStatus == ImmaturePointStatus.IPS_GOOD)
                            img.SetPixelCirc((int)(ph.U + 0.5f), (int)(ph.V + 0.5f), new byte[] { 0, 255, 0 });
                        if (ph.LastTraceStatus == ImmaturePointStatus.IPS_OOB)
                            img.SetPixelCirc((int)(ph.U + 0.5f), (int)(ph.V + 0.5f), new byte[] { 255, 0, 0 });
                        if (ph.LastTraceStatus == ImmaturePointStatus.IPS_OUTLIER)
                            img.SetPixelCirc((int)(ph.U + 0.5f), (int)(ph.V + 0.5f), new byte[] { 0, 0, 255 });
                        if (ph.LastTraceStatus == ImmaturePointStatus.IPS_SKIPPED)
                            img.SetPixelCirc((int)(ph.U + 0.5f), (int)(ph.V + 0.5f), new byte[] { 255, 255, 0 });
                        if (ph.LastTraceStatus == ImmaturePointStatus.IPS_BADCONDITION)
                            img.SetPixelCirc((int)(ph.U + 0.5f), (int)(ph.V + 0.5f), new byte[] { 255, 255, 255 });
                        if (ph.LastTraceStatus == ImmaturePointStatus.IPS_UNINITIALIZED)
                            img.SetPixelCirc((int)(ph.U + 0.5f), (int)(ph.V + 0.5f), new byte[] { 0, 0, 0 });
                    }
                }
                else if ((int)(_context.Config.Core.FreeDebugParam5 + 0.5f) == 5)
                {
                    foreach (ImmaturePoint ph in _frameHessians[f].ImmaturePoints)
                    {
                        if (ph == null) continue;

                        if (ph.LastTraceStatus == ImmaturePointStatus.IPS_UNINITIALIZED) continue;
                        float d = _context.Config.Core.FreeDebugParam1 * ((float)Math.Sqrt(ph.Quality) - 1);
                        if (d < 0) d = 0;
                        if (d > 1) d = 1;
                        img.SetPixelCirc((int)(ph.U + 0.5f), (int)(ph.V + 0.5f), new byte[] { 0, (byte)(d * 255), (byte)((1 - d) * 255) });
                    }

                }
                else if ((int)(_context.Config.Core.FreeDebugParam5 + 0.5f) == 6)
                {
                    foreach (PointHessian ph in _frameHessians[f].PointHessians)
                    {
                        if (ph == null) continue;
                        if (ph.Type == 0)
                            img.SetPixelCirc((int)(ph.U + 0.5f), (int)(ph.V + 0.5f), new byte[] { 255, 0, 255 });
                        if (ph.Type == 1)
                            img.SetPixelCirc((int)(ph.U + 0.5f), (int)(ph.V + 0.5f), new byte[] { 255, 0, 0 });
                        if (ph.Type == 2)
                            img.SetPixelCirc((int)(ph.U + 0.5f), (int)(ph.V + 0.5f), new byte[] { 0, 0, 255 });
                        if (ph.Type == 3)
                            img.SetPixelCirc((int)(ph.U + 0.5f), (int)(ph.V + 0.5f), new byte[] { 0, 255, 255 });
                    }
                    foreach (PointHessian ph in _frameHessians[f].PointHessiansMarginalized)
                    {
                        if (ph == null) continue;
                        if (ph.Type == 0)
                            img.SetPixelCirc((int)(ph.U + 0.5f), (int)(ph.V + 0.5f), new byte[] { 255, 0, 255 });
                        if (ph.Type == 1)
                            img.SetPixelCirc((int)(ph.U + 0.5f), (int)(ph.V + 0.5f), new byte[] { 255, 0, 0 });
                        if (ph.Type == 2)
                            img.SetPixelCirc((int)(ph.U + 0.5f), (int)(ph.V + 0.5f), new byte[] { 0, 0, 255 });
                        if (ph.Type == 3)
                            img.SetPixelCirc((int)(ph.U + 0.5f), (int)(ph.V + 0.5f), new byte[] { 0, 255, 255 });
                    }

                }
                if ((int)(_context.Config.Core.FreeDebugParam5 + 0.5f) == 7)
                {
                    foreach (PointHessian ph in _frameHessians[f].PointHessians)
                    {
                        img.SetPixelCirc((int)(ph.U + 0.5f), (int)(ph.V + 0.5f), img.GetJetColor((ph.IdepthScaled - minID) / ((maxID - minID))));
                    }
                    foreach (PointHessian ph in _frameHessians[f].PointHessiansMarginalized)
                    {
                        if (ph == null) continue;
                        img.SetPixelCirc((int)(ph.U + 0.5f), (int)(ph.V + 0.5f), new byte[] { 0, 0, 0 });
                    }
                }
            }


            for (int i = 0; i < images.Count; i++)
            {
                _context.Output.UpdateImage($"{name}.{i}", images[i]);
            }

            images = null; //GC can collect if it can



            //TODO if want output to file
            //if ((_context.Configuration.Core.DebugSaveImages && false))
            //{
            //    for (int f = 0; f < _frameHessians.Count; f++)
            //    {
            //        MinimalImageB3 img = new MinimalImageB3(_context.w[0], _context.h[0]);
            //        var fd = _frameHessians[f].dI;
            //
            //        for (int i = 0; i < wh; i++)
            //        {
            //            int c = (int)(fd[i,0] * 0.9f);
            //            if (c > 255) c = 255;
            //            img.At(i,0) = (byte)c;
            //            img.At(i,1) = (byte)c;
            //            img.At(i,2) = (byte)c;
            //        }
            //
            //        foreach (PointHessian ph in _frameHessians[f].PointHessians)
            //        {
            //            img.SetPixelCirc((int)(ph.U + 0.5f), (int)(ph.V + 0.5f), img.GetJetColor((ph.IdepthScaled - minID) / ((maxID - minID))));
            //        }
            //        foreach (PointHessian ph in _frameHessians[f].PointHessiansMarginalized)
            //        {
            //            if (ph == null) continue;
            //            img.SetPixelCirc((int)(ph.U + 0.5f), (int)(ph.V + 0.5f), new byte[] { 0, 0, 0 });
            //        }
            //
            //        char buf[1000];
            //        snprintf(buf, 1000, "images_out/kf_%05d_%05d_%02d.png",
            //                frameHessians.back()->shell->id, frameHessians.back()->frameID, f);
            //        IOWrap::writeImage(buf, img);
            //
            //        delete img;
            //    }
            //}
        }

        private void removeOutliers()
        {
            int numPointsDropped = 0;
            foreach (FrameHessian fh in _frameHessians)
            {
                for (int i = 0; i < fh.PointHessians.Count; i++)
                {
                    PointHessian ph = fh.PointHessians[i];
                    if (ph == null) continue;

                    if (ph.Residuals.Count == 0)
                    {
                        Log.CountAdd("ph.Residuals.Count == 0");
                        fh.PointHessiansOut.Add(ph);
                        ph.EfPoint.StateFlag = EFPointStatus.PS_DROP;
                        fh.PointHessians[i] = fh.PointHessians[fh.PointHessians.Count - 1];
                        fh.PointHessians.RemoveAt(fh.PointHessians.Count - 1);
                        i--;
                        numPointsDropped++;
                    }
                }
                Log.CountLogDebug("ph.Residuals.Count == 0");
            }
            _ef.DropPointsF();
        }

        private float optimize(int mnumOptIts)
        {
            if (_frameHessians.Count < 2) return 0;
            if (_frameHessians.Count < 3) mnumOptIts = 20;
            if (_frameHessians.Count < 4) mnumOptIts = 15;

            // get statistics and active residuals.

            _activeResiduals.Clear();
            int numPoints = 0;
            int numLRes = 0;
            foreach (FrameHessian fh in _frameHessians)
                foreach (PointHessian ph in fh.PointHessians)
                {
                    foreach (PointFrameResidual r in ph.Residuals)
                    {
                        if (!r.EfResidual.IsLinearized)
                        {
                            _activeResiduals.Add(r);
                            r.ResetOOB();
                        }
                        else
                            numLRes++;
                    }
                    numPoints++;
                }

            Log.LogDebug(string.Format("OPTIMIZE {0} pts, {1} active res, {2} lin res!", _ef.nPoints, _activeResiduals.Count, numLRes));

            double[] lastEnergy = linearizeAll(false);
            double lastEnergyL = calcLEnergy();
            double lastEnergyM = calcMEnergy();





            if (_multiThreading)
                throw new NotImplementedException();
            //treadReduce.reduce(boost::bind(&FullSystem::applyRes_Reductor, this, true, _1, _2, _3, _4), 0, activeResiduals.size(), 50);
            else
                applyResReductor(true, 0, _activeResiduals.Count);


            Log.LogDebug("Initial Error");
            printOptRes(lastEnergy, lastEnergyL, lastEnergyM, 0, 0, _frameHessians.Last().Aff_g2l().a, _frameHessians.Last().Aff_g2l().b);

            debugPlotTracking();



            double lambda = 1e-1;
            float stepsize = 1f;
            double[] previousX = Enumerable.Repeat<double>(double.NaN, _context.Config.Core.Cpars + 8 * _frameHessians.Count).ToArray();
            for (int iteration = 0; iteration < mnumOptIts; iteration++)
            {
                // solve!
                backupState(iteration != 0);
                ////solveSystemNew(0);
                solveSystem(iteration, lambda);
                double incDirChange = (1e-20 + previousX.Mul(_ef.LastX)) / (1e-20 + previousX.Norm() * _ef.LastX.Norm());
                previousX = _ef.LastX;


                if (!double.IsInfinity(incDirChange) && !double.IsNaN(incDirChange) && (_context.Config.Core.SolverMode & (int)Configuration.Core.SOLVERTYPE.STEPMOMENTUM) != 0)
                {
                    float newStepsize = (float)Math.Exp(incDirChange * 1.4);
                    if (incDirChange < 0 && stepsize > 1) stepsize = 1;

                    stepsize = (float)Math.Sqrt(Math.Sqrt(newStepsize * stepsize * stepsize * stepsize));
                    if (stepsize > 2) stepsize = 2;
                    if (stepsize < 0.25) stepsize = 0.25f;
                }

                bool canbreak = doStepFromBackup(stepsize, stepsize, stepsize, stepsize, stepsize);







                // eval new energy!
                double[] newEnergy = linearizeAll(false);
                double newEnergyL = calcLEnergy();
                double newEnergyM = calcMEnergy();


                Log.LogDebug(string.Format("{0} {1} (L {2}, dir {3}, ss {4}):",
                     (newEnergy[0] + newEnergy[1] + newEnergyL + newEnergyM <
                             lastEnergy[0] + lastEnergy[1] + lastEnergyL + lastEnergyM) ? "ACCEPT" : "REJECT",
                     iteration,
                     Math.Log10(lambda),
                     incDirChange,
                     stepsize));
                printOptRes(newEnergy, newEnergyL, newEnergyM, 0, 0, _frameHessians.Last().Aff_g2l().a, _frameHessians.Last().Aff_g2l().b);

                if (_context.Config.Core.ForceAceptStep || (newEnergy[0] + newEnergy[1] + newEnergyL + newEnergyM <
                        lastEnergy[0] + lastEnergy[1] + lastEnergyL + lastEnergyM))
                {

#if PAR
                   throw new NotImplementedException();
                   treadReduce.reduce(boost::bind(&FullSystem::applyRes_Reductor, this, true, _1, _2, _3, _4), 0, activeResiduals.size(), 50);
#else
                    applyResReductor(true, 0, _activeResiduals.Count);
#endif

                    lastEnergy = newEnergy;
                    lastEnergyL = newEnergyL;
                    lastEnergyM = newEnergyM;

                    lambda *= 0.25;
                }
                else
                {
                    loadSateBackup();
                    lastEnergy = linearizeAll(false);
                    lastEnergyL = calcLEnergy();
                    lastEnergyM = calcMEnergy();
                    lambda *= 1e2;
                }


                if (canbreak && iteration >= _context.Config.Core.MinOptIterations) break;
            }



            double[] newStateZero = new double[10];
            newStateZero[6] = _frameHessians.Last().State[6];
            newStateZero[7] = _frameHessians.Last().State[7];

            _frameHessians.Last().SetEvalPT(_frameHessians.Last().PREWorldToCam,
                    newStateZero);
            _ef.EFDeltaValid = false;
            _ef.EFAdjointsValid = false;
            _ef.SetAdjointsF();
            setPrecalcValues();




            lastEnergy = linearizeAll(true);




            if (double.IsNaN(lastEnergy[0]) || double.IsInfinity(lastEnergy[0]) || double.IsNaN(lastEnergy[1]) || double.IsInfinity(lastEnergy[1]) || double.IsNaN(lastEnergy[2]) || double.IsInfinity(lastEnergy[2]))
            {
                Log.LogInfo("KF Tracking failed: LOST!");
                IsLost = true;
            }


            _statisticsLastFineTrackRMSE = (float)Math.Sqrt(lastEnergy[0] / (_context.Config.Core.PatternNumber * _ef.ResInA));

            //TODO if Logging stream will be created
            //if (calibLog != 0)
            //{
            //    (*calibLog) << Hcalib.value_scaled.transpose() <<
            //            " " << frameHessians.back()->get_state_scaled().transpose() <<
            //            " " << sqrtf((float)(lastEnergy[0] / (patternNum * ef->resInA))) <<
            //            " " << ef->resInM << "\n";
            //    calibLog->flush();
            //}

            {
                //boost::unique_lock<boost::mutex> crlock(shellPoseMutex);
                foreach (FrameHessian fh in _frameHessians)
                {
                    fh.Shell.CamToWorld = fh.PRECamToWorld;
                    fh.Shell.Aff_g2l = fh.Aff_g2l();
                }
            }




            debugPlotTracking();

            return (float)Math.Sqrt(lastEnergy[0] / (_context.Config.Core.PatternNumber * _ef.ResInA));
        }

        private void loadSateBackup()
        {
            _hCalib.SetValue(_hCalib.ValueBackup);
            foreach (FrameHessian fh in _frameHessians)
            {
                fh.SetState(fh.StateBackup);
                foreach (PointHessian ph in fh.PointHessians)
                {
                    ph.SetIdepth(ph.IdepthBackup);

                    ph.SetIdepthZero(ph.IdepthBackup);
                }

            }

            _ef.EFDeltaValid = false;
            setPrecalcValues();
        }

        private bool doStepFromBackup(float stepfacC, float stepfacT, float stepfacR, float stepfacA, float stepfacD)
        {
            ////	float meanStepC=0,meanStepP=0,meanStepD=0;
            ////	meanStepC += Hcalib.step.norm();

            double[] pstepfac = new double[10];

            for (int i = 0; i < 3; i++)
            {
                pstepfac[i + 0] = stepfacT;
            }
            for (int i = 3; i < 6; i++)
            {
                pstepfac[i] = stepfacR;
            }
            for (int i = 6; i < 10; i++)
            {
                pstepfac[i] = stepfacA;
            }

            float sumA = 0, sumB = 0, sumT = 0, sumR = 0, sumID = 0, numID = 0;

            float sumNID = 0;

            if ((_context.Config.Core.SolverMode & (int)Configuration.Core.SOLVERTYPE.MOMENTUM) != 0)
            {
                _hCalib.SetValue(_hCalib.ValueBackup.Plus(_hCalib.Step));
                foreach (FrameHessian fh in _frameHessians)
                {
                    var step = fh.Step;
                    for (int i = 0; i < 6; i++)
                    {
                        step[i] += 0.5f * fh.StepBackup[i];
                    }

                    fh.SetState(fh.StateBackup.Plus(step));
                    sumA += (float)(step[6] * step[6]);
                    sumB += (float)(step[7] * step[7]);
                    sumT += (float)step.Take(3).SquaredNorm();
                    sumR += (float)step.Skip(3).Take(3).SquaredNorm();

                    foreach (PointHessian ph in fh.PointHessians)
                    {
                        float stepl = ph.Step + 0.5f * (ph.StepBackup);
                        ph.SetIdepth(ph.IdepthBackup + stepl);
                        sumID += stepl * stepl;
                        sumNID += Math.Abs(ph.IdepthBackup);
                        numID++;

                        ph.SetIdepthZero(ph.IdepthBackup + stepl);
                    }
                }
            }
            else
            {
                _hCalib.SetValue(_hCalib.ValueBackup.Plus(_hCalib.Step.Mul(stepfacC)));
                foreach (FrameHessian fh in _frameHessians)
                {
                    var prod = pstepfac.CwiseProduct(fh.Step).ToArray();
                    fh.SetState(fh.StateBackup.Plus(prod));
                    sumA += (float)(fh.Step[6] * fh.Step[6]);
                    sumB += (float)(fh.Step[7] * fh.Step[7]);
                    sumT += (float)fh.Step.Take(3).SquaredNorm();
                    sumR += (float)fh.Step.Skip(3).Take(3).SquaredNorm();

                    foreach (PointHessian ph in fh.PointHessians)
                    {
                        ph.SetIdepth(ph.IdepthBackup + stepfacD * ph.Step);
                        sumID += ph.Step * ph.Step;
                        sumNID += Math.Abs(ph.IdepthBackup);
                        numID++;

                        ph.SetIdepthZero(ph.IdepthBackup + stepfacD * ph.Step);
                    }
                }
            }

            sumA /= _frameHessians.Count;
            sumB /= _frameHessians.Count;
            sumR /= _frameHessians.Count;
            sumT /= _frameHessians.Count;
            sumID /= numID;
            sumNID /= numID;


            Log.LogDebug(string.Format("STEPS: A {0}; B {1}; R {2}; T {3}.",
                        Math.Sqrt(sumA) / (0.0005 * _context.Config.Core.ThOptIterations),
                        Math.Sqrt(sumB) / (0.00005 * _context.Config.Core.ThOptIterations),
                        Math.Sqrt(sumR) / (0.00005 * _context.Config.Core.ThOptIterations),
                        Math.Sqrt(sumT) * sumNID / (0.00005 * _context.Config.Core.ThOptIterations)));

            _ef.EFDeltaValid = false;
            setPrecalcValues();



            return Math.Sqrt(sumA) < 0.0005 * _context.Config.Core.ThOptIterations &&
                    Math.Sqrt(sumB) < 0.00005 * _context.Config.Core.ThOptIterations &&
                    Math.Sqrt(sumR) < 0.00005 * _context.Config.Core.ThOptIterations &&
                    Math.Sqrt(sumT) * sumNID < 0.00005 * _context.Config.Core.ThOptIterations;
            ////
            ////	printf("mean steps: %f %f %f!\n",
            ////			meanStepC, meanStepP, meanStepD);
        }

        private void solveSystem(int iteration, double lambda)
        {
            //TODO maybe - for loging in fact it is not used anywhere normally
            //_ef.LastNullspacesForLogging = getNullspaces......

            getNullspaces(
            _ef.LastNullspaces_pose,
            _ef.LastNullspaces_scale,
            _ef.LastNullspaces_affA,
            _ef.LastNullspaces_affB);

            _ef.SolveSystemF(iteration, lambda, _hCalib);
        }

        private void backupState(bool backupLastStep)
        {
            if ((_context.Config.Core.SolverMode & (int)Configuration.Core.SOLVERTYPE.MOMENTUM) != 0)
            {
                if (backupLastStep)
                {
                    _hCalib.StepBackup = _hCalib.Step;
                    _hCalib.ValueBackup = _hCalib.Value;
                    foreach (FrameHessian fh in _frameHessians)
                    {
                        fh.StepBackup = fh.Step;
                        fh.StateBackup = fh.State;
                        foreach (PointHessian ph in fh.PointHessians)
                        {
                            ph.IdepthBackup = ph.Idepth;
                            ph.StepBackup = ph.Step;
                        }
                    }
                }
                else
                {
                    Array.Clear(_hCalib.StepBackup, 0, _hCalib.StepBackup.Length);
                    _hCalib.ValueBackup = _hCalib.Value;
                    foreach (FrameHessian fh in _frameHessians)
                    {
                        Array.Clear(fh.StepBackup, 0, fh.StepBackup.Length);
                        fh.StateBackup = fh.State;
                        foreach (PointHessian ph in fh.PointHessians)
                        {
                            ph.IdepthBackup = ph.Idepth;
                            ph.StepBackup = 0;
                        }
                    }
                }
            }
            else
            {
                _hCalib.ValueBackup = _hCalib.Value;
                foreach (FrameHessian fh in _frameHessians)
                {
                    fh.StateBackup = fh.State;
                    foreach (PointHessian ph in fh.PointHessians)
                        ph.IdepthBackup = ph.Idepth;
                }
            }
        }

        private void debugPlotTracking()
        {
            //if (disableAllDisplay) return;
            //if (!setting_render_plotTrackingFull) return;
            int wh = _context.h[0] * _context.w[0];



            int idx = 0;
            foreach (FrameHessian f in _frameHessians)
            {

                // make images for all frames. will be deleted by the FrameHessian's destructor.
                //for (FrameHessian* f2 : frameHessians)
                //    if (f2->debugImage == 0) f2->debugImage = new MinimalImageB3(wG[0], hG[0]);
                var images = new Dictionary<FrameHessian, MinimalImageB3>();

                foreach (FrameHessian f2 in _frameHessians)
                {
                    MinimalImageB3 debugImage = new MinimalImageB3(f2.Shell.IncomingId, _context.w[0], _context.h[0]); // f2->debugImage;
                    images.Add(f2, debugImage);

                    var fd = f2.dI;

                    //if (f2 == _frameHessians.Last())
                    //    fd = _frameHessians[_frameHessians.Count - 2].dI;

                    var affL = AffLight.FromToVecExposure(f2.AbExposure, f.AbExposure, f2.Aff_g2l(), f.Aff_g2l());

                    for (int i = 0; i < wh; i++)
                    {
                        // BRIGHTNESS TRANSFER
                        float colL = (float)affL[0] * fd[i, 0] + (float)affL[1];
                        if (colL < 0) colL = 0; if (colL > 255) colL = 255;
                        debugImage.At(i, 0) = (byte)colL;
                        debugImage.At(i, 1) = (byte)colL;
                        debugImage.At(i, 2) = (byte)colL;
                    }
                }

                foreach (PointHessian ph in f.PointHessians)
                {
                    Debug.Assert(ph.Status == PointHessianStatus.ACTIVE);
                    if (ph.Status == PointHessianStatus.ACTIVE || ph.Status == PointHessianStatus.MARGINALIZED)
                    {
                        foreach (PointFrameResidual r in ph.Residuals)
                        {
                            if (r.StateState == ResState.OOB) continue;
                            byte[] cT = new byte[3];

                            if (_context.Config.Core.FreeDebugParam5 == 0)
                            {
                                float rT = 20 * (float)Math.Sqrt(r.StateEnergy / 9);
                                if (rT < 0) rT = 0; if (rT > 255) rT = 255;
                                cT[0] = 0;
                                cT[1] = (byte)(255 - rT);
                                cT[2] = (byte)rT;
                            }
                            else
                            {
                                if (r.StateState == ResState.IN)
                                {
                                    cT[0] = 255;
                                    cT[1] = 0;
                                    cT[2] = 0;
                                }
                                else if (r.StateState == ResState.OOB)
                                {
                                    cT[0] = 255;
                                    cT[1] = 255;
                                    cT[2] = 0;
                                }
                                else if (r.StateState == ResState.OUTLIER)
                                {
                                    cT[0] = 0;
                                    cT[1] = 0;
                                    cT[2] = 255;
                                }
                                else
                                {
                                    cT[0] = 255;
                                    cT[1] = 255;
                                    cT[2] = 255;
                                }
                            }

                            for (int i = 0; i < _context.Config.Core.PatternNumber; i++)
                            {
                                if ((r.ProjectedTo[i][0] > 2 && r.ProjectedTo[i][1] > 2 && r.ProjectedTo[i][0] < _context.w[0] - 3 && r.ProjectedTo[i][1] < _context.h[0] - 3))
                                    images[r.Target].SetPixel((int)(r.ProjectedTo[i][0] + 0.5f), (int)(r.ProjectedTo[i][1] + 0.5f), cT);
                            }
                        }

                        images[f].SetPixel9((int)(ph.U + 0.5), (int)(ph.V + 0.5), images[f].GetRainbowColor(ph.IdepthScaled));
                    }
                }

                int iidx = 0;
                foreach (var img in images.Values)
                {
                    _context.Output.UpdateImage($"05_FullTrace.F{idx}.{iidx}", img);
                    iidx++;
                }
                idx++;
            }
        }

        private void printOptRes(double[] res, double resL, double resM, int resPrior, int LExact, double a, double b)
        {
            Log.LogDebug(string.Format("A({0})=(AV {1}). Num: A({2}) + M({3}); ab {4} {5}!",
            res[0],
            Math.Sqrt(res[0] / (_context.Config.Core.PatternNumber * _ef.ResInA)),
            _ef.ResInA,
            _ef.ResInM,
            a,
            b));
        }

        private void applyResReductor(bool copyJacobians, int min, int max)
        {
            for (int k = min; k < max; k++)
                _activeResiduals[k].ApplyRes(copyJacobians);
        }

        private double calcMEnergy()
        {
            if (_context.Config.Core.ForceAceptStep) return 0;
            //// calculate (x-x0)^T * [2b + H * (x-x0)] for everything saved in L.
            ////ef->makeIDX();
            ////ef->setDeltaF(&Hcalib);
            return _ef.CalcMEnergyF();
        }

        private double calcLEnergy()
        {
            if (_context.Config.Core.ForceAceptStep) return 0;

            double Ef = _ef.CalcLEnergyFMT();
            return Ef;
        }

        private double[] linearizeAll(bool fixLinearization)
        {
            double lastEnergyP = 0;
            double lastEnergyR = 0;
            double num = 0;

            var numThreads = 6;//rewrite with paralel optimization reduce will not be same as original
            var toRemove = new List<PointFrameResidual>[numThreads];
            for (int i = 0; i < numThreads; i++) toRemove[i] = new List<PointFrameResidual>();

            if (_multiThreading)
            {
                throw new NotImplementedException();
                //treadReduce.reduce(boost::bind(&FullSystem::linearizeAll_Reductor, this, fixLinearization, toRemove, _1, _2, _3, _4), 0, activeResiduals.size(), 0);
                //lastEnergyP = treadReduce.stats[0];
            }
            else
            {
                var stats = new double[10];
                linearizeAllReductor(fixLinearization, toRemove, 0, _activeResiduals.Count, stats, 0);
                lastEnergyP = stats[0];
            }

            setNewFrameEnergyTH();

            if (fixLinearization)
            {

                foreach (PointFrameResidual r in _activeResiduals)
                {
                    PointHessian ph = r.Point;
                    if (ph.LastResiduals[0].Item1 == r)
                        ph.LastResiduals[0].Item2 = r.StateState;
                    else if (ph.LastResiduals[1].Item1 == r)
                        ph.LastResiduals[1].Item2 = r.StateState;
                }

                int nResRemoved = 0;

                for (int i = 0; i < numThreads; i++)
                {
                    foreach (PointFrameResidual r in toRemove[i])
                    {
                        PointHessian ph = r.Point;

                        if (ph.LastResiduals[0].Item1 == r)
                            ph.LastResiduals[0].Item1 = null;
                        else if (ph.LastResiduals[1].Item1 == r)
                            ph.LastResiduals[1].Item1 = null;

                        for (int k = 0; k < ph.Residuals.Count; k++)
                            if (ph.Residuals[k] == r)
                            {
                                _ef.DropResidual(r.EfResidual);
                                ph.Residuals.RemoveAt(k);
                                nResRemoved++;
                                break;
                            }
                    }
                }
                Log.LogDebug(string.Format("FINAL LINEARIZATION: removed {0} / {1} residuals!", nResRemoved, _activeResiduals.Count));

            }

            return new double[3] { lastEnergyP, lastEnergyR, num };
        }

        private void setNewFrameEnergyTH()
        {
            // collect all residuals and make decision on TH.
            _allResVec.Clear();
            _allResVec.Capacity = _activeResiduals.Count * 2;
            FrameHessian newFrame = _frameHessians.Last();

            foreach (PointFrameResidual r in _activeResiduals)
                if (r.StateNewEnergyWithOutlier >= 0 && r.Target == newFrame)
                {
                    _allResVec.Add(r.StateNewEnergyWithOutlier);
                }

            if (_allResVec.Count == 0)
            {
                newFrame.FrameEnergyTh = 12 * 12 * _context.Config.Core.PatternNumber;
                return;     // should never happen, but lets make sure.
            }


            int nthIdx = (int)(_context.Config.Core.FrameEnergyTHN * _allResVec.Count);

            Debug.Assert(nthIdx < _allResVec.Count);
            Debug.Assert(_context.Config.Core.FrameEnergyTHN < 1);

            _allResVec.Sort(); //std::nth_element(allResVec.begin(), allResVec.begin() + nthIdx, allResVec.end());
            float nthElement = (float)Math.Sqrt(_allResVec[nthIdx]);



            newFrame.FrameEnergyTh = nthElement * _context.Config.Core.FrameEnergyTHFacMedian;
            newFrame.FrameEnergyTh = 26.0f * _context.Config.Core.FrameEnergyTHConstWeight + newFrame.FrameEnergyTh * (1 - _context.Config.Core.FrameEnergyTHConstWeight);
            newFrame.FrameEnergyTh = newFrame.FrameEnergyTh * newFrame.FrameEnergyTh;
            newFrame.FrameEnergyTh *= _context.Config.Core.OverallEnergyTHWeight * _context.Config.Core.OverallEnergyTHWeight;

            ////
            ////	int good=0,bad=0;
            ////	for(float f : allResVec) if(f<newFrame->frameEnergyTH) good++; else bad++;
            ////	printf("EnergyTH: mean %f, median %f, result %f (in %d, out %d)! \n",
            ////			meanElement, nthElement, sqrtf(newFrame->frameEnergyTH),
            ////			good, bad);
        }

        private void linearizeAllReductor(bool fixLinearization, List<PointFrameResidual>[] toRemove, int min, int max, double[] stats, int tid)
        {
            //var debugImg = new[] { new Shared.MinimalImageF3(_context.w[0], _context.h[0]), new Shared.MinimalImageF3(_context.w[0], _context.h[0]), new Shared.MinimalImageF3(_context.w[0], _context.h[0]) };

            //for (int i = 0; i < debugImg[0].Height * debugImg[0].Width; i++)
            //{
            //    var Target = _activeResiduals.Last().Target;
            //    var Host = _activeResiduals.Last().Host;

            //    debugImg[0].At(i, 1) = Target.dI[i, 0];
            //    debugImg[0].At(i, 2) = Target.dI[i, 0];
            //    debugImg[0].At(i, 0) = Target.dI[i, 0];

            //    debugImg[1].At(i, 0) = Host.dI[i, 0];
            //    debugImg[1].At(i, 1) = Host.dI[i, 0];
            //    debugImg[1].At(i, 2) = Host.dI[i, 0];


            //    debugImg[2].At(i, 0) = Math.Abs(Target.dI[i, 0] - Host.dI[i, 0]);
            //    debugImg[2].At(i, 1) = Math.Abs(Target.dI[i, 0] - Host.dI[i, 0]);
            //    debugImg[2].At(i, 2) = Math.Abs(Target.dI[i, 0] - Host.dI[i, 0]);
            //}

            for (int k = min; k < max; k++)
            {
                Log.CountAdd("Residuals count");
                PointFrameResidual r = _activeResiduals[k];
                stats[0] += r.Linearize(_hCalib);//, debugImg);

                if (fixLinearization)
                {
                    r.ApplyRes(true);

                    if (r.EfResidual.IsActiveAndIsGoodNEW)
                    {
                        if (r.IsNew)
                        {
                            PointHessian p = r.Point;
                            var ptp_inf = r.Host.TargetPrecalc[r.Target.Idx].PRE_KRKiTll * new float[3] { p.U, p.V, 1 };  // projected point assuming infinite depth.
                            var ptp = ptp_inf.Plus(r.Host.TargetPrecalc[r.Target.Idx].PRE_KtTll.Mul(p.IdepthScaled));  // projected point with real depth.
                            float relBS = 0.01f * ((new float[2] { ptp_inf[0], ptp_inf[1] }.Divide(ptp_inf[2])).Minus(new float[2] { ptp[0], ptp[1] }.Divide(ptp[2]))).Norm();  // 0.01 = one pixel.

                            if (relBS > p.MaxRelBaseline)
                                p.MaxRelBaseline = relBS;

                            p.NumGoodResiduals++;
                        }
                    }
                    else
                    {
                        toRemove[tid].Add(_activeResiduals[k]);
                    }
                }
            }

            Log.CountLogDebug("Energy too hight");
            Log.CountLogDebug("wJI2_sum too low");
            Log.CountLogDebug("OOB State OOB");
            Log.CountLogDebug("OOB Project point fail");
            Log.CountLogDebug("OOB Project point fail in pattern");
            Log.CountLogDebug("OOB Hit color out of range");
            Log.CountLogDebug("OK - in");
            Log.CountLogDebug("Residuals count");

            //_context.Output.UpdateImage("02 Residuals coresspodence.Target", debugImg[0]);
            //_context.Output.UpdateImage("02 Residuals coresspodence.Host", debugImg[1]);
            //_context.Output.UpdateImage("02 Residuals coresspodence.Diff", debugImg[2]);
            //Console.WriteLine("\n====PAUSE!");
            //Console.ReadKey();
#if DEBUG
            if (_activeResiduals.Count(x => x.StateState == ResState.OUTLIER) >= 2000)
            {
                Console.WriteLine("\n====PAUSE!");
                debugPlotTracking();
                Console.ReadKey();
            }
#endif
        }

        private void activatePointsMT()
        {
            if (_ef.nPoints < _context.Config.Core.DesiredPointDensity * 0.66)
                _currentMinActDist -= 0.8;
            if (_ef.nPoints < _context.Config.Core.DesiredPointDensity * 0.8)
                _currentMinActDist -= 0.5;
            else if (_ef.nPoints < _context.Config.Core.DesiredPointDensity * 0.9)
                _currentMinActDist -= 0.2;
            else if (_ef.nPoints < _context.Config.Core.DesiredPointDensity)
                _currentMinActDist -= 0.1;

            if (_ef.nPoints > _context.Config.Core.DesiredPointDensity * 1.5)
                _currentMinActDist += 0.8;
            if (_ef.nPoints > _context.Config.Core.DesiredPointDensity * 1.3)
                _currentMinActDist += 0.5;
            if (_ef.nPoints > _context.Config.Core.DesiredPointDensity * 1.15)
                _currentMinActDist += 0.2;
            if (_ef.nPoints > _context.Config.Core.DesiredPointDensity)
                _currentMinActDist += 0.1;

            if (_currentMinActDist < 0) _currentMinActDist = 0;
            if (_currentMinActDist > 4) _currentMinActDist = 4;

            Log.LogDebug(string.Format("SPARSITY:  MinActDist {0} (need {1} points, have {2} points)!",
                    _currentMinActDist, (int)(_context.Config.Core.DesiredPointDensity), _ef.nPoints));

            FrameHessian newestHs = _frameHessians.Last();

            // make dist map.
            //duplicit information computed here alread stored in context//_coarseDistanceMap.MakeK(&Hcalib);
            _coarseDistanceMap.MakeDistanceMap(_frameHessians, newestHs);

            ////coarseTracker->debugPlotDistMap("distMap");

            var toOptimize = new List<ImmaturePoint>(20000);

            foreach (FrameHessian host in _frameHessians)        // go through all active frames
            {
                if (host == newestHs) continue;

                SE3 fhToNew = newestHs.PREWorldToCam * host.PRECamToWorld;
                Mat33f KRKi = (_context.K[1] * fhToNew.RotationMatrixF() * _context.Ki[0]);
                float[] Kt = (_context.K[1] * fhToNew.TranslationF());


                for (int i = 0; i < host.ImmaturePoints.Count; i += 1)
                {
                    ImmaturePoint ph = host.ImmaturePoints[i];
                    ph.IdxInImmaturePoints = i;

                    // delete points that have never been traced successfully, or that are outlier on the last trace.
                    if (float.IsInfinity(ph.IdepthMax) || float.IsNaN(ph.IdepthMax) || ph.LastTraceStatus == ImmaturePointStatus.IPS_OUTLIER)
                    {
                        ////				immature_invalid_deleted++;
                        //// remove point.
                        //delete ph;
                        host.ImmaturePoints[i] = null;
                        continue;
                    }

                    // can activate only if this is true.
                    bool canActivate = (ph.LastTraceStatus == ImmaturePointStatus.IPS_GOOD
                            || ph.LastTraceStatus == ImmaturePointStatus.IPS_SKIPPED
                            || ph.LastTraceStatus == ImmaturePointStatus.IPS_BADCONDITION
                            || ph.LastTraceStatus == ImmaturePointStatus.IPS_OOB)
                                    && ph.LastTracePixelInterval < 8
                                    && ph.Quality > _context.Config.Core.MinTraceQuality
                                    && (ph.IdepthMax + ph.IdepthMin) > 0;


                    // if I cannot activate the point, skip it. Maybe also delete it.
                    if (!canActivate)
                    {
                        // if point will be out afterwards, delete it instead.
                        if (ph.Host.FlaggedForMarginalization || ph.LastTraceStatus == ImmaturePointStatus.IPS_OOB)
                        {
                            //					immature_notReady_deleted++;
                            //delete ph;
                            host.ImmaturePoints[i] = null;
                        }
                        //				immature_notReady_skipped++;
                        continue;
                    }


                    // see if we need to activate point due to distance map.
                    float[] ptp = (KRKi * new float[3] { ph.U, ph.V, 1 }).Plus(Kt.Mul((0.5f * (ph.IdepthMax + ph.IdepthMin))));
                    int u = (int)(ptp[0] / ptp[2] + 0.5f);
                    int v = (int)(ptp[1] / ptp[2] + 0.5f);

                    if ((u > 0 && v > 0 && u < _context.w[1] && v < _context.h[1]))
                    {

                        float dist = _coarseDistanceMap.FwdWarpedIDDistFinal[u + _context.w[1] * v] + (ptp[0] - (float)Math.Floor(ptp[0]));

                        if (dist >= _currentMinActDist * ph.Type)
                        {
                            _coarseDistanceMap.AddIntoDistFinal(u, v);
                            toOptimize.Add(ph);
                        }
                    }
                    else
                    {
                        //delete ph;
                        host.ImmaturePoints[i] = null;
                    }
                }
            }


            ////Log.LogDebug(string.Format("ACTIVATE: {0}. (del {1}, notReady {2}, marg {3}, good {4}, marg-skip {5})",
            ////		toOptimize.Count, _immatureDeleted, immature_notReady, immature_needMarg, immature_want, immature_margskip);

            var optimized = new List<ValueTuple<PointHessian, bool>>();
            optimized.Resize(toOptimize.Count, () => default(ValueTuple<PointHessian, bool>));


            if (_multiThreading)
                throw new NotImplementedException();
            //treadReduce.reduce(boost::bind(&FullSystem::activatePointsMT_Reductor, this, &optimized, &toOptimize, _1, _2, _3, _4), 0, toOptimize.size(), 50);
            else
                activatePointsMTReductor(optimized, toOptimize, 0, toOptimize.Count);

            for (int k = 0; k < toOptimize.Count; k++)
            {
                var newpoint = optimized[k];
                var ph = toOptimize[k];

                if (newpoint.Item1 != null)
                {
                    newpoint.Item1.Host.ImmaturePoints[ph.IdxInImmaturePoints] = null;
                    newpoint.Item1.Host.PointHessians.Add(newpoint.Item1);
                    _ef.InsertPoint(newpoint.Item1);
                    foreach (PointFrameResidual r in newpoint.Item1.Residuals)
                        _ef.InsertResidual(r);
                    Debug.Assert(newpoint.Item1.EfPoint != null);
                }
                else if (newpoint.Item2 || ph.LastTraceStatus == ImmaturePointStatus.IPS_OOB)
                {
                    ph.Host.ImmaturePoints[ph.IdxInImmaturePoints] = null;
                }
                else
                {
                    Debug.Assert(newpoint.Item1 == null);
                }
            }

            foreach (FrameHessian host in _frameHessians)
            {
                for (int i = 0; i < host.ImmaturePoints.Count; i++)
                {
                    if (host.ImmaturePoints[i] == null)
                    {
                        host.ImmaturePoints[i] = host.ImmaturePoints[host.ImmaturePoints.Count - 1];
                        host.ImmaturePoints.RemoveAt(host.ImmaturePoints.Count - 1);
                        i--;
                    }
                }
            }
        }

        private void activatePointsMTReductor(List<ValueTuple<PointHessian, bool>> optimized, List<ImmaturePoint> toOptimize, int min, int max)
        {
            ImmaturePointTemporaryResidual[] tr = new ImmaturePointTemporaryResidual[_frameHessians.Count];
            for (int i = 0; i < _frameHessians.Count; i++)
            {
                tr[i] = new ImmaturePointTemporaryResidual();
            }

            for (int k = min; k < max; k++)
            {
                optimized[k] = optimizeImmaturePoint(toOptimize[k], 1, tr);
            }
            Log.CountLogDebug("Added resiudal by optimize");
        }

        private ValueTuple<PointHessian, bool> optimizeImmaturePoint(ImmaturePoint point, int minObs, ImmaturePointTemporaryResidual[] residuals)
        {
            int nres = 0;
            foreach (FrameHessian fh in _frameHessians)
            {
                if (fh != point.Host)
                {
                    residuals[nres].StateNewEnergy = residuals[nres].StateEnergy = 0;
                    residuals[nres].StateNewState = ResState.OUTLIER;
                    residuals[nres].StateState = ResState.IN;
                    residuals[nres].Target = fh;
                    nres++;
                }
            }
            Debug.Assert(nres == _frameHessians.Count - 1);

            bool print = false;//rand()%50==0;

            float lastEnergy = 0;
            float lastHdd = 0;
            float lastbd = 0;
            float currentIdepth = (point.IdepthMax + point.IdepthMin) * 0.5f;



            for (int i = 0; i < nres; i++)
            {
                lastEnergy += (float)point.LinearizeResidual(_hCalib, 1000, residuals[i], ref lastHdd, ref lastbd, currentIdepth);
                residuals[i].StateState = residuals[i].StateNewState;
                residuals[i].StateEnergy = residuals[i].StateNewEnergy;
            }

            if (float.IsInfinity(lastEnergy) || float.IsNaN(lastEnergy) || lastHdd < _context.Config.Core.MinIdepthHAct)
            {
                if (print)
                    Log.LogDebug(string.Format("OptPoint: Not well-constrained ({0} res, H={1}). E={2}. SKIP!",
                        nres, lastHdd, lastEnergy));
                return new ValueTuple<PointHessian, bool>(null, false);
            }

            if (print)
                Log.LogDebug(string.Format("Activate point. {0} residuals. H={1}. Initial Energy: {2}. Initial Id={3}",
                     nres, lastHdd, lastEnergy, currentIdepth));

            float lambda = 0.1f;
            for (int iteration = 0; iteration < _context.Config.Core.GNItsOnPointActivation; iteration++)
            {
                float H = lastHdd;
                H *= 1 + lambda;
                float step = (1.0f / H) * lastbd;
                float newIdepth = currentIdepth - step;

                float newHdd = 0; float newbd = 0; float newEnergy = 0;
                for (int i = 0; i < nres; i++)
                    newEnergy += (float)point.LinearizeResidual(_hCalib, 1, residuals[i], ref newHdd, ref newbd, newIdepth);

                if (float.IsInfinity(lastEnergy) || float.IsNaN(lastEnergy) || newHdd < _context.Config.Core.MinIdepthHAct)
                {
                    if (print)
                        Log.LogDebug(string.Format("OptPoint: Not well-constrained ({0} res, H={1}). E={2}. SKIP!",
                             nres,
                             newHdd,
                             lastEnergy));
                    return new ValueTuple<PointHessian, bool>(null, false); ;
                }

                if (print)
                    Log.LogDebug(string.Format("{0} {1} (L {2}) {3}: {4} -> {5} (idepth {6})!",
                         (newEnergy < lastEnergy) ? "ACCEPT" : "REJECT", //(true || newEnergy < lastEnergy) ? "ACCEPT" : "REJECT",
                         iteration,
                         Math.Log10(lambda),
                         "",
                         lastEnergy, newEnergy, newIdepth));

                if (newEnergy < lastEnergy)
                {
                    currentIdepth = newIdepth;
                    lastHdd = newHdd;
                    lastbd = newbd;
                    lastEnergy = newEnergy;
                    for (int i = 0; i < nres; i++)
                    {
                        residuals[i].StateState = residuals[i].StateNewState;
                        residuals[i].StateEnergy = residuals[i].StateNewEnergy;
                    }

                    lambda *= 0.5f;
                }
                else
                {
                    lambda *= 5f;
                }

                if (Math.Abs(step) < 0.0001 * currentIdepth)
                    break;
            }

            if (float.IsInfinity(currentIdepth) || float.IsNaN(currentIdepth))
            {
                Log.LogDebug(string.Format("MAJOR ERROR! point idepth is nan after initialization ({0}).", currentIdepth));
                return new ValueTuple<PointHessian, bool>(null, true); // yeah I'm like 99% sure this is OK on 32bit systems.
            }


            int numGoodRes = 0;
            for (int i = 0; i < nres; i++)
                if (residuals[i].StateState == ResState.IN) numGoodRes++;

            if (numGoodRes < minObs)
            {
                if (print)
                    Log.LogDebug("OptPoint: OUTLIER!");
                return new ValueTuple<PointHessian, bool>(null, true);   // yeah I'm like 99% sure this is OK on 32bit systems.
            }


            PointHessian p = new PointHessian(_context, point);
            if (float.IsInfinity(p.EnergyTh) || float.IsNaN(p.EnergyTh)) { return new ValueTuple<PointHessian, bool>(null, true); }

            p.LastResiduals[0].Item1 = null;
            p.LastResiduals[0].Item2 = ResState.OOB;
            p.LastResiduals[1].Item1 = null;
            p.LastResiduals[1].Item2 = ResState.OOB;
            p.SetIdepthZero(currentIdepth);
            p.Idepth = currentIdepth;
            p.Status = PointHessianStatus.ACTIVE;

            for (int i = 0; i < nres; i++)
                if (residuals[i].StateState == ResState.IN)
                {
                    PointFrameResidual r = new PointFrameResidual(_context, p, p.Host, residuals[i].Target);
                    r.StateNewEnergy = r.StateEnergy = 0;
                    r.StateNewState = ResState.OUTLIER;
                    r.StateState = ResState.IN;
                    p.Residuals.Add(r);
                    Log.CountAdd("Added resiudal by optimize");

                    if (r.Target == _frameHessians.Last())
                    {
                        p.LastResiduals[0].Item1 = r;
                        p.LastResiduals[0].Item2 = ResState.IN;
                    }
                    else if (r.Target == (_frameHessians.Count < 2 ? null : _frameHessians[_frameHessians.Count - 2]))
                    {
                        p.LastResiduals[1].Item1 = r;
                        p.LastResiduals[1].Item2 = ResState.IN;
                    }
                }

            if (print) Log.LogDebug("point activated!");

            _statisticsNumActivatedPoints++;
            return new ValueTuple<PointHessian, bool>(p, false);
        }

        private void setPrecalcValues()
        {
            foreach (FrameHessian fh in _frameHessians)
            {
                fh.TargetPrecalc.Resize(_frameHessians.Count, () => new FramePrecalc(_context, _hCalib));
                for (int i = 0; i < _frameHessians.Count; i++)
                    fh.TargetPrecalc[i].Set(fh, _frameHessians[i]);
            }

            _ef.SetDeltaF(_hCalib);
        }

        private void flagFramesForMarginalization(FrameHessian newFh)
        {
            if (_context.Config.Core.MinFrameAge > _context.Config.Core.MaxFrames)
            {
                for (int i = _context.Config.Core.MaxFrames; i < _frameHessians.Count; i++)
                {
                    FrameHessian fh = _frameHessians[i - _context.Config.Core.MaxFrames];
                    fh.FlaggedForMarginalization = true;
                }
                return;
            }


            int flagged = 0;
            // marginalize all frames that have not enough points.
            for (int i = 0; i < _frameHessians.Count; i++)
            {
                FrameHessian fh = _frameHessians[i];
                int @in = fh.PointHessians.Count + fh.ImmaturePoints.Count;
                int @out = fh.PointHessiansMarginalized.Count + fh.PointHessiansOut.Count;


                double[] refToFh = AffLight.FromToVecExposure(_frameHessians.Last().AbExposure, fh.AbExposure,
                        _frameHessians.Last().Aff_g2l(), fh.Aff_g2l());


                if ((@in < _context.Config.Core.MinPointsRemaining * (@in + @out) || Math.Abs(Math.Log(refToFh[0])) > _context.Config.Core.MaxLogAffFacInWindow)

                && (_frameHessians.Count) - flagged > _context.Config.Core.MinFrames)
                {
                    Log.LogDebug(string.Format("MARGINALIZE frame {0}, as only {1}/{2} points remaining ({3} {4} {5} {6})!",
                            fh.FrameID, @in, @in + @out,
                            fh.PointHessians.Count, fh.ImmaturePoints.Count,
                            fh.PointHessiansMarginalized.Count, fh.PointHessiansOut.Count));
                    fh.FlaggedForMarginalization = true;
                    flagged++;
                }
                else
                {
                    Log.LogDebug(string.Format("May Keep frame {0}, as {1}/{2} points remaining ({3} {4} {5} {6})!",
                                       fh.FrameID, @in, @in + @out,
                                       fh.PointHessians.Count, fh.ImmaturePoints.Count,
                                       fh.PointHessiansMarginalized.Count, fh.PointHessiansOut.Count));
                }
            }

            // marginalize one.
            if (_frameHessians.Count - flagged >= _context.Config.Core.MaxFrames)
            {
                double smallestScore = 1;
                FrameHessian toMarginalize = null;
                FrameHessian latest = _frameHessians.Last();


                foreach (FrameHessian fh in _frameHessians)
                {
                    if (fh.FrameID > latest.FrameID - _context.Config.Core.MinFrameAge || fh.FrameID == 0) continue;
                    ////if(fh==frameHessians.front() == 0) continue;

                    double distScore = 0;
                    foreach (var ffh in fh.TargetPrecalc)
                    {
                        if (ffh.Target.FrameID > latest.FrameID - _context.Config.Core.MinFrameAge + 1 || ffh.Target == ffh.Host) continue;
                        distScore += 1 / (1e-5 + ffh.DistanceLL);
                        Log.LogDebug($"From {fh.FrameID} To {ffh.Target.FrameID} distance: { ffh.DistanceLL} ({(1 / (1e-5 + ffh.DistanceLL))})");

                    }
                    distScore *= -Math.Sqrt(fh.TargetPrecalc.Last().DistanceLL);
                    Log.LogDebug($"Frame {fh.FrameID} lastDist: {fh.TargetPrecalc.Last().DistanceLL} result distance: { distScore}");


                    if (distScore < smallestScore)
                    {
                        smallestScore = distScore;
                        toMarginalize = fh;
                    }
                }

                Log.LogDebug(string.Format("MARGINALIZE frame {0}, as it is the closest (score {1})!",
                        toMarginalize.FrameID, smallestScore));

                toMarginalize.FlaggedForMarginalization = true;
                flagged++;
            }

            Log.LogDebug(string.Format("FRAMES LEFT: {0}", string.Join(", ", _frameHessians.Select(x => x.FrameID))));

            //Log.LogDebug("PAUSE");
            //Console.ReadKey();
        }

        void InitializeFromInitializer(FrameHessian newFrame)
        {
            //boost::unique_lock < boost::mutex > lock (mapMutex) ;

            // add firstframe.
            FrameHessian firstFrame = _coarseInitializer.FirstFrame;
            firstFrame.Idx = _frameHessians.Count;
            _frameHessians.Add(firstFrame);
            firstFrame.FrameID = _allKeyFramesHistory.Count;

            _allKeyFramesHistory.Add(firstFrame.Shell);
            _ef.InsertFrame(firstFrame);

            setPrecalcValues();

            ////int numPointsTotal = makePixelStatus(firstFrame->dI, selectionMap, wG[0], hG[0], setting_desiredDensity);
            ////int numPointsTotal = pixelSelector->makeMaps(firstFrame->dIp, selectionMap,setting_desiredDensity);

            //probably not needed preserver space before needed with luck list will do for me (just effectivity)
            //firstFrame->pointHessians.reserve(wG[0] * hG[0] * 0.2f);
            //firstFrame->pointHessiansMarginalized.reserve(wG[0] * hG[0] * 0.2f);
            //firstFrame->pointHessiansOut.reserve(wG[0] * hG[0] * 0.2f);


            float sumID = 1e-5f, numID = 1e-5f;
            for (int i = 0; i < _coarseInitializer.NumPoints[0]; i++)
            {
                sumID += _coarseInitializer.Points[0][i].iR;
                numID++;
            }
            float rescaleFactor = 1 / (sumID / numID);

            // randomly sub-select the points I need.
            float keepPercentage = _context.Config.Core.DesiredPointDensity / _coarseInitializer.NumPoints[0];

            //no needed same condition inside log if (!_context.Configuration.Log.DebugOutput)
            Log.LogDebug(string.Format("Initialization: keep {0}% (need {1}, have {2})!", 100 * keepPercentage,
                    (int)(_context.Config.Core.DesiredPointDensity), _coarseInitializer.NumPoints[0]));


            for (int i = 0; i < _coarseInitializer.NumPoints[0]; i++)
            {

                if (_context.Rand.NextDouble() > keepPercentage) continue;

                Point point = _coarseInitializer.Points[0][i];
                ImmaturePoint pt = new ImmaturePoint(_context, (int)(point.uv[0] + 0.5f), (int)(point.uv[1] + 0.5f), firstFrame, point.Type);

                if (float.IsInfinity(pt.EnergyTh) || float.IsNaN(pt.EnergyTh)) { continue; }

                pt.IdepthMax = pt.IdepthMin = 1;
                PointHessian ph = new PointHessian(_context, pt);
                pt = null;//GC can collect

                if (float.IsInfinity(ph.EnergyTh) || float.IsNaN(ph.EnergyTh)) { continue; }

                ph.SetIdepthScaled(point.iR * rescaleFactor);
                ph.SetIdepthZero(ph.Idepth);
                ph.HasDepthPrior = true;
                ph.Status = PointHessianStatus.ACTIVE;

                firstFrame.PointHessians.Add(ph);
                _ef.InsertPoint(ph);
            }



            SE3 firstToNew = _coarseInitializer.ThisToNext.Clone();
            firstToNew.TranslationDivide(rescaleFactor);


            // really no lock required, as we are initializing.
            {
                //boost::unique_lock<boost::mutex> crlock(shellPoseMutex);
                firstFrame.Shell.CamToWorld = new SE3();
                firstFrame.Shell.Aff_g2l = new AffLight(0, 0);
                firstFrame.SetEvalPTScaled(firstFrame.Shell.CamToWorld.Inverse(), firstFrame.Shell.Aff_g2l);
                firstFrame.Shell.TrackingRef = null;
                firstFrame.Shell.CamToTrackingRef = new SE3();

                newFrame.Shell.CamToWorld = firstToNew.Inverse();
                newFrame.Shell.Aff_g2l = new AffLight(0, 0);
                newFrame.SetEvalPTScaled(newFrame.Shell.CamToWorld.Inverse(), newFrame.Shell.Aff_g2l);
                newFrame.Shell.TrackingRef = firstFrame.Shell;
                newFrame.Shell.CamToTrackingRef = firstToNew.Inverse();
            }

            Initialized = true;
            Log.LogInfo(string.Format("INITIALIZE FROM INITIALIZER ({0} pts)!", (int)firstFrame.PointHessians.Count));
        }
    }
}
