﻿using SharpSLAM.Core.Unmanaged;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using SharpSLAM.Core;

namespace SharpSLAM.Core
{
    public class EnergyFunctional : Contextual
    {

        private EnergyFunctionalU _energyFunctionalU { get; set; }


        MatXX HM { get; set; }
        double[] bM { get; set; }

        public List<EFFrame> Frames { get; set; }

        public int nFrames { get; set; }
        public int nResiduals { get; set; }
        public int nPoints { get; set; }

        public int ResInA { get; set; }
        public int ResInL { get; set; }
        public int ResInM { get; set; }

        public bool EFIndicesValid { get; set; }
        public bool EFAdjointsValid { get; set; }
        public bool EFDeltaValid { get; set; }

        public Dictionary<ulong, int[]> ConnectivityMap { get; set; }


        //List<double[]> LastNullspaces_forLogging { get; set; }
        public List<double[]> LastNullspaces_pose { get; set; }
        public List<double[]> LastNullspaces_scale { get; set; }
        public List<double[]> LastNullspaces_affA { get; set; }
        public List<double[]> LastNullspaces_affB { get; set; }

        public MatXX LastHS { get; set; }
        public double[] LastbS { get; set; }


        private List<EFPoint> _allPoints { get; set; }
        private List<EFPoint> _allPointsToMarg { get; set; }

        internal Mat88[] _adHost { get; set; }
        internal Mat88[] _adTarget { get; set; }
        private Mat88f[] _adHostF { get; set; }
        private Mat88f[] _adTargetF { get; set; }

        internal float[] _cDeltaF { get; set; }
        internal float[] _cPriorF { get; set; }

        internal float[][] _adHTdeltaF { get; set; }
        internal double[] _cPrior { get; set; }

        private float _currentLambda { get; set; }
        public double[] LastX { get; set; }

        private AccumulatedTopHessian _accSSE_top_A { get; set; }
        private AccumulatedTopHessian _accSSE_top_L { get; set; }

        private AccumulatedSCHessian _accSSE_bot { get; set; }


        /// <summary>
        /// TODO maybe
        /// </summary>
        /// <param name="context"></param>
        public EnergyFunctional(CoreContext context) : base(context)
        {
            _energyFunctionalU = new EnergyFunctionalU();

            Frames = new List<EFFrame>();
            ConnectivityMap = new Dictionary<ulong, int[]>();
            _allPoints = new List<EFPoint>();
            _allPointsToMarg = new List<EFPoint>();


            nFrames = nResiduals = nPoints = 0;

            HM = MatXX.Zero(Context.Config.Core.Cpars, Context.Config.Core.Cpars);
            bM = new double[Context.Config.Core.Cpars];

            int numThread = 1;
#if PAR
            throw new NotImplementedException();
#endif
            _accSSE_top_L = new AccumulatedTopHessian(Context, numThread);
            _accSSE_top_A = new AccumulatedTopHessian(Context, numThread);
            _accSSE_bot = new AccumulatedSCHessian(Context, numThread);

            LastNullspaces_pose = new List<double[]>();
            LastNullspaces_scale = new List<double[]>();
            LastNullspaces_affA = new List<double[]>();
            LastNullspaces_affB = new List<double[]>();

            ResInA = ResInL = ResInM = 0;
            _currentLambda = 0;
        }


        /// <summary>
        /// TODO maybe
        /// </summary>
        /// <param name="fh"></param>
        public EFFrame InsertFrame(FrameHessian fh)
        {
            EFFrame eff = new EFFrame(fh);
            eff.Idx = Frames.Count;
            Frames.Add(eff);

            nFrames++;
            fh.EfFrame = eff;

            Debug.Assert(HM.Cols() == 8 * nFrames + Context.Config.Core.Cpars - 8);

            var tmpbM = bM;
            Array.Resize(ref tmpbM, 8 * nFrames + Context.Config.Core.Cpars);
            bM = tmpbM;

            //TODO this should not be necessary because c# already prefilled all with 0 bud should be sure
            //for (int i = bM.Length-8; i < bM.Length; i++)
            //{
            //    bM[i] = 0;
            //}

            //resizeHM
            _energyFunctionalU.InsertFrameUpdateHM(HM, nFrames);

            EFIndicesValid = false;
            EFAdjointsValid = false;
            EFDeltaValid = false;

            SetAdjointsF();
            MakeIDX();


            foreach (var fh2 in Frames)
            {
                ConnectivityMap[(((ulong)eff.FrameID) << 32) + ((ulong)fh2.FrameID)] = new int[2] { 0, 0 };
                if (fh2 != eff)
                    ConnectivityMap[(((ulong)fh2.FrameID) << 32) + ((ulong)eff.FrameID)] = new int[2] { 0, 0 };
            }

            return eff;
        }

        public void MakeIDX()
        {
            for (int idx = 0; idx < Frames.Count; idx++)
                Frames[idx].Idx = idx;

            _allPoints.Clear();

            foreach (var f in Frames)
                foreach (var p in f.Points)
                {
                    _allPoints.Add(p);

                    //solved by properties why duplicate value into new structure?
                    ////foreach (var r in p.ResidualsAll)
                    ////{
                    ////    r.hostIDX = r->host->idx;
                    ////    r.targetIDX = r->target->idx;
                    ////}
                }

            EFIndicesValid = true;
        }

        public void SetAdjointsF()
        {
            _adHost = new Mat88[nFrames * nFrames];
            _adTarget = new Mat88[nFrames * nFrames];

            for (int h = 0; h < nFrames; h++)
                for (int t = 0; t < nFrames; t++)
                {
                    FrameHessian host = Frames[h].Data;
                    FrameHessian target = Frames[t].Data;

                    SE3 hostToTarget = target.WorldToCamEvalPT * host.WorldToCamEvalPT.Inverse();

                    Mat88 AH = Mat88.Identity();
                    Mat88 AT = Mat88.Identity();

                    AH.MinusAdjTransponseToTopLeftCorner(hostToTarget);
                    AT = Mat88.Identity(); //only 6x6 needed but 2 last we will owerride later


                    float[] affLL = AffLight.FromToVecExposure(host.AbExposure, target.AbExposure, host.Aff_g2l_0(), target.Aff_g2l_0()).Cast<double, float>().ToArray();
                    AT[6, 6] = -affLL[0];
                    AH[6, 6] = affLL[0];
                    AT[7, 7] = -1;
                    AH[7, 7] = affLL[0];

                    ////AH.block<3,8>(0, 0) *= SCALE_XI_TRANS;
                    ////AT.block<3,8>(0, 0) *= SCALE_XI_TRANS;
                    for (int i = 0; i < 3; i++)
                    {
                        for (int ii = 0; ii < 8; ii++)
                        {
                            AH[i, ii] *= Context.Config.Core.ScaleXITRANS;
                            AT[i, ii] *= Context.Config.Core.ScaleXITRANS;
                        }
                    }
                    ////AH.block<3,8>(3, 0) *= SCALE_XI_ROT;
                    ////AT.block<3,8>(3, 0) *= SCALE_XI_ROT;
                    for (int i = 3; i < 6; i++)
                    {
                        for (int ii = 0; ii < 8; ii++)
                        {
                            AH[i, ii] *= Context.Config.Core.ScaleXIROT;
                            AT[i, ii] *= Context.Config.Core.ScaleXIROT;
                        }
                    }
                    ////AH.block<1,8>(6, 0) *= SCALE_A;
                    ////AT.block<1,8>(6, 0) *= SCALE_A;
                    for (int i = 0; i < 8; i++)
                    {
                        AH[6, i] *= Context.Config.Core.ScaleA;
                        AT[6, i] *= Context.Config.Core.ScaleA;
                    }
                    ////AH.block<1,8>(7, 0) *= SCALE_B;
                    ////AT.block<1,8>(7, 0) *= SCALE_B;
                    for (int i = 0; i < 8; i++)
                    {
                        AH[7, i] *= Context.Config.Core.ScaleB;
                        AT[7, i] *= Context.Config.Core.ScaleB;
                    }

                    _adHost[h + t * nFrames] = AH;
                    _adTarget[h + t * nFrames] = AT;
                }
            _cPrior = Enumerable.Repeat((double)Context.Config.Core.InitialCalibHessian, 4).ToArray();


            _adHostF = new Mat88f[nFrames * nFrames];
            _adTargetF = new Mat88f[nFrames * nFrames];

            for (int h = 0; h < nFrames; h++)
                for (int t = 0; t < nFrames; t++)
                {
                    _adHostF[h + t * nFrames] = _adHost[h + t * nFrames].CastToFloat();
                    _adTargetF[h + t * nFrames] = _adTarget[h + t * nFrames].CastToFloat();
                }

            _cPriorF = _cPrior.Cast<double, float>().ToArray();

            EFAdjointsValid = true;
        }

        public void SetDeltaF(CalibHessian hCalib)
        {
            _adHTdeltaF = new float[nFrames * nFrames][];
            for (int h = 0; h < nFrames; h++)
                for (int t = 0; t < nFrames; t++)
                {
                    int idx = h + t * nFrames;

                    var fPart = Frames[h].Data.StateMinusStateZero.Take(8).Cast<double, float>().ToArray() * _adHostF[idx];
                    var sPart = Frames[t].Data.StateMinusStateZero.Take(8).Cast<double, float>().ToArray() * _adTargetF[idx];

                    _adHTdeltaF[idx] = fPart.Zip(sPart, (a, b) => { return a + b; }).ToArray();
                }

            _cDeltaF = hCalib.ValueMinusValueZero.Cast<double, float>().ToArray();
            foreach (var f in Frames)
            {
                f.Delta = f.Data.StateMinusStateZero.Take(8).ToArray();
                f.DeltaPrior = f.Data.State.Take(8).ToArray();

                foreach (var p in f.Points)
                    p.DeltaF = p.Data.Idepth - p.Data.IdepthZero;
            }

            EFDeltaValid = true;
        }

        public EFPoint InsertPoint(PointHessian ph)
        {
            EFPoint efp = new EFPoint(Context, ph, ph.Host.EfFrame);
            efp.IdxInPoints = ph.Host.EfFrame.Points.Count;
            ph.Host.EfFrame.Points.Add(efp);

            nPoints++;
            ph.EfPoint = efp;

            EFIndicesValid = false;

            return efp;
        }

        public EFResidual InsertResidual(PointFrameResidual r)
        {
            EFResidual efr = new EFResidual(Context, r, r.Point.EfPoint, r.Host.EfFrame, r.Target.EfFrame);
            efr.IdxInAll = r.Point.EfPoint.ResidualsAll.Count;
            r.Point.EfPoint.ResidualsAll.Add(efr);

            ConnectivityMap[(((ulong)efr.Host.FrameID) << 32) + ((ulong)efr.Target.FrameID)][0]++;

            nResiduals++;
            r.EfResidual = efr;
            return efr;
        }

        public void DropResidual(EFResidual residual)
        {
            EFPoint p = residual.Point;
            Debug.Assert(residual == p.ResidualsAll[residual.IdxInAll]);

            p.ResidualsAll[residual.IdxInAll] = p.ResidualsAll.Last();
            p.ResidualsAll[residual.IdxInAll].IdxInAll = residual.IdxInAll;
            p.ResidualsAll.RemoveAt(p.ResidualsAll.Count - 1);

            if (residual.IsActiveAndIsGoodNEW)
                residual.Host.Data.Shell.StatisticsGoodResOnThis++;
            else
                residual.Host.Data.Shell.StatisticsOutlierResOnThis++;


            ConnectivityMap[(((ulong)residual.Host.FrameID) << 32) + ((ulong)residual.Target.FrameID)][0]--;
            nResiduals--;
            residual.Data.EfResidual = null;
            //delete residual;
        }

        public double CalcLEnergyFMT()
        {
            Debug.Assert(EFDeltaValid);
            Debug.Assert(EFAdjointsValid);
            Debug.Assert(EFIndicesValid);

            double E = 0;
            foreach (EFFrame f in Frames)
                E += Unmanaged.Extensions.CwiseProductAndDot(f.DeltaPrior, f.Prior, f.DeltaPrior);

            E += Unmanaged.Extensions.CwiseProductAndDot(_cDeltaF, _cPriorF, _cDeltaF);

            double res = 0;
#if PAR
            throw new NotImplementedException();
            red->reduce(boost::bind(&EnergyFunctional::calcLEnergyPt,this, _1, _2, _3, _4), 0, allPoints.size(), 50);
#else
            CalcLEnergyPt(0, _allPoints.Count, ref res);
#endif

            return E + res;
        }

        public void CalcLEnergyPt(int min, int max, ref double stats)
        {
            _energyFunctionalU.CalcLEnergyInit();
            var dc = _cDeltaF;

            for (int i = min; i < max; i++)
            {
                EFPoint p = _allPoints[i];
                float dd = p.DeltaF;

                foreach (EFResidual r in p.ResidualsAll)
                {
                    if (!r.IsLinearized || !r.IsActiveAndIsGoodNEW) continue;

                    var dp = _adHTdeltaF[r.HostIdx + nFrames * r.TargetIdx];
                    RawResidualJacobian rJ = r.J;



                    // compute Jp*delta
                    float Jp_delta_x_1 = rJ.Jpdxi[0].Mul(dp.Take(6))
                                   + rJ.Jpdc[0].Mul(dc)
                                   + rJ.Jpdd[0] * dd;

                    float Jp_delta_y_1 = rJ.Jpdxi[1].Mul(dp.Take(6))
                                   + rJ.Jpdc[1].Mul(dc)
                                   + rJ.Jpdd[1] * dd;


                    _energyFunctionalU.CalcLEnergyIter(Jp_delta_x_1, Jp_delta_y_1, dp[6], dp[7], Context.Config.Core.PatternNumber, rJ.JIdx, rJ.JabF, r.ResToZeroF);

                    for (int ii = ((Context.Config.Core.PatternNumber >> 2) << 2); ii < Context.Config.Core.PatternNumber; ii++)
                    {
                        float Jdelta = rJ.JIdx[0][ii] * Jp_delta_x_1 + rJ.JIdx[1][ii] * Jp_delta_y_1 +
                                        rJ.JabF[0][ii] * dp[6] + rJ.JabF[1][ii] * dp[7];

                        var toUpdateI = (Jdelta * (Jdelta + 2 * r.ResToZeroF[ii]));
                        _energyFunctionalU.CalcLEnergyUpdateSingleNoShift(toUpdateI);
                    }
                }

                var toUpdate = p.DeltaF * p.DeltaF * p.PriorF;
                _energyFunctionalU.CalcLEnergyUpdateSingle(toUpdate);


            }
            var EA = _energyFunctionalU.CalcLEnergyFinish();
            stats += EA;
        }

        internal double CalcMEnergyF()
        {
            Debug.Assert(EFDeltaValid);
            Debug.Assert(EFAdjointsValid);
            Debug.Assert(EFIndicesValid);

            double[] delta = getStitchedDeltaF();
            return delta.Mul(bM.Mul(2).Plus((HM * delta)));
        }

        private double[] getStitchedDeltaF()
        {
            var cpars = Context.Config.Core.Cpars;
            double[] d = new double[cpars + nFrames * 8];
            for (int i = 0; i < cpars; i++)
            {
                d[i] = _cDeltaF[i];
            }
            for (int h = 0; h < nFrames; h++)
            {

                var startIndex = cpars + 8 * h;
                for (int i = 0; i < 8; i++)
                {
                    d[startIndex + i] = Frames[h].Delta[i];
                }
            }
            return d;
        }

        public void SolveSystemF(int iteration, double lambda, CalibHessian hCalib)
        {
            if ((Context.Config.Core.SolverMode & (int)Configuration.Core.SOLVERTYPE.USE_GN) != 0) lambda = 0;
            if ((Context.Config.Core.SolverMode & (int)Configuration.Core.SOLVERTYPE.FIX_LAMBDA) != 0) lambda = 1e-5;

            Debug.Assert(EFDeltaValid);
            Debug.Assert(EFAdjointsValid);
            Debug.Assert(EFIndicesValid);

            MatXX HL_top, HA_top, H_sc;
            double[] bL_top, bA_top, bM_top, b_sc;

            accumulateAF_MT(out HA_top, out bA_top);


            accumulateLF_MT(out HL_top, out bL_top);


            accumulateSCF_MT(out H_sc, out b_sc);

            bM_top = bM.Plus(HM * getStitchedDeltaF());




            MatXX HFinal_top;
            double[] bFinal_top;

            if ((Context.Config.Core.SolverMode & (int)Configuration.Core.SOLVERTYPE.ORTHOGONALIZE_SYSTEM) != 0)
            {
                // have a look if prior is there.
                bool haveFirstFrame = Frames.FirstOrDefault(xx => xx.FrameID == 0) != null;


                MatXX HT_act = HL_top + HA_top - H_sc;
                var bT_act = bL_top.Plus(bA_top).Minus(b_sc);


                if (!haveFirstFrame)
                    orthogonalize(bT_act, HT_act);

                HFinal_top = HT_act + HM;
                bFinal_top = bT_act.Plus(bM_top);


                LastHS = HFinal_top;
                LastbS = bFinal_top;

                for (int i = 0; i < 8 * nFrames + Context.Config.Core.Cpars; i++) HFinal_top[i, i] *= (1 + lambda);

            }
            else
            {


                HFinal_top = HL_top + HM + HA_top;
                bFinal_top = bL_top.Plus(bM_top).Plus(bA_top).Minus(b_sc);

                LastHS = HFinal_top - H_sc;
                LastbS = bFinal_top;

                for (int i = 0; i < 8 * nFrames + Context.Config.Core.Cpars; i++) HFinal_top[i, i] *= (1 + lambda);
                HFinal_top -= H_sc * (1.0f / (1 + lambda));
            }






            double[] x = _energyFunctionalU.SolveSystemFGetX((Context.Config.Core.SolverMode & (int)Configuration.Core.SOLVERTYPE.SVD) != 0,
                (Context.Config.Core.SolverMode & (int)Configuration.Core.SOLVERTYPE.SVD_CUT7) != 0,
                HFinal_top, bFinal_top, Context.Config.Core.SolverModeDelta);



            if ((Context.Config.Core.SolverMode & (int)Configuration.Core.SOLVERTYPE.ORTHOGONALIZE_X) != 0 || (iteration >= 2 && (Context.Config.Core.SolverMode & (int)Configuration.Core.SOLVERTYPE.ORTHOGONALIZE_X_LATER) != 0))
            {
                orthogonalize(x, null);
            }


            LastX = x;


            ////resubstituteF(x, HCalib);
            _currentLambda = (float)lambda;
            resubstituteF_MT(x, hCalib);
            _currentLambda = 0;
        }

        public void MarginalizePointsF()
        {
            Debug.Assert(EFDeltaValid);
            Debug.Assert(EFAdjointsValid);
            Debug.Assert(EFIndicesValid);


            _allPointsToMarg.Clear();
            foreach (EFFrame f in Frames)
            {
                for (int i = 0; i < f.Points.Count; i++)
                {
                    EFPoint p = f.Points[i];
                    if (p.StateFlag == EFPointStatus.PS_MARGINALIZE)
                    {
                        p.PriorF *= Context.Config.Core.IdepthFixPriorMargFac;
                        foreach (EFResidual r in p.ResidualsAll)
                            if (r.IsActiveAndIsGoodNEW)
                                ConnectivityMap[(((ulong)r.Host.FrameID) << 32) + ((ulong)r.Target.FrameID)][1]++;
                        _allPointsToMarg.Add(p);
                    }
                }
            }

            _accSSE_bot.SetZero(nFrames);
            _accSSE_top_A.SetZero(nFrames);
            foreach (EFPoint p in _allPointsToMarg)
            {
                _accSSE_top_A.AddPoint(2, p, this, 0);
                _accSSE_bot.AddPoint(p, false, 0);
                removePoint(p);
            }
            MatXX M, Msc;
            double[] Mb, Mbsc;



            //TODO check if MT make same thing as without MT should bud potentionally source of errors
            _accSSE_top_A.StitchDoubleMT(out M, out Mb, this, false);
            _accSSE_bot.StitchDoubleMT(out Msc, out Mbsc, this);
            //_accSSE_top_A.StitchDouble(M, Mb, this, false, false);
            //_accSSE_bot.StitchDouble(Msc, Mbsc, this);

            ResInM += _accSSE_top_A.Nres[0];

            MatXX H = M - Msc;
            double[] b = Mb.Minus(Mbsc);

            if ((Context.Config.Core.SolverMode & (int)Configuration.Core.SOLVERTYPE.ORTHOGONALIZE_POINTMARG) != 0)
            {
                // have a look if prior is there.
                bool haveFirstFrame = Frames.FirstOrDefault(y => y.FrameID == 0) != null;

                if (!haveFirstFrame)
                    orthogonalize(b, H);

            }

            HM += Context.Config.Core.MargWeightFac * H;
            bM = bM.Plus(b.Mul(Context.Config.Core.MargWeightFac));

            if ((Context.Config.Core.SolverMode & (int)Configuration.Core.SOLVERTYPE.ORTHOGONALIZE_FULL) != 0)
                orthogonalize(bM, HM);

            EFIndicesValid = false;
            MakeIDX();
        }

        public void MarginalizeFrame(EFFrame fh)
        {

            Debug.Assert(EFDeltaValid);
            Debug.Assert(EFAdjointsValid);
            Debug.Assert(EFIndicesValid);

            Debug.Assert(fh.Points.Count == 0);
            int ndim = nFrames * 8 + Context.Config.Core.Cpars - 8;// new dimension
            int odim = nFrames * 8 + Context.Config.Core.Cpars;// old dimension


            var lbM = bM;
            //unmanaged
            _energyFunctionalU.MarginalizeFrame(fh.Idx, ndim, odim, Frames.Count, nFrames,ref lbM, HM, fh.Prior, fh.DeltaPrior);
            bM = lbM;

            // remove from vector, without changing the order!
            for (int i = fh.Idx; i + 1 < Frames.Count; i++)
            {
                Frames[i] = Frames[i + 1];
                Frames[i].Idx = i;
            }
            Frames.RemoveAt(Frames.Count - 1);
            nFrames--;
            fh.Data.EfFrame = null;

            Debug.Assert(Frames.Count * 8 + Context.Config.Core.Cpars == HM.Rows());
            Debug.Assert(Frames.Count * 8 + Context.Config.Core.Cpars == HM.Cols());
            Debug.Assert(Frames.Count * 8 + Context.Config.Core.Cpars == bM.Length);
            Debug.Assert(Frames.Count == (int)nFrames);




            ////	VecX eigenvaluesPost = HM.eigenvalues().real();
            ////	std::sort(eigenvaluesPost.data(), eigenvaluesPost.data()+eigenvaluesPost.size());
            //
            ////	std::cout << std::setprecision(16) << "HMPost:\n" << HM << "\n\n";
            //
            ////	std::cout << "EigPre:: " << eigenvaluesPre.transpose() << "\n";
            ////	std::cout << "EigPost: " << eigenvaluesPost.transpose() << "\n";

            EFIndicesValid = false;
            EFAdjointsValid = false;
            EFDeltaValid = false;

            MakeIDX();
        }

        private void resubstituteF_MT(double[] x, CalibHessian hCalib)
        {
            Debug.Assert(x.Length == Context.Config.Core.Cpars + nFrames * 8);

            float[] xF = x.Cast<double, float>().ToArray();
            hCalib.Step = x.Take(Context.Config.Core.Cpars).Select(y => -y).ToArray();

            float[][] xAd = new float[nFrames * nFrames][];
            float[] cstep = xF.Take(Context.Config.Core.Cpars).ToArray();
            foreach (EFFrame h in Frames)
            {
                ////h->data->step.head < 8 > () = -x.segment < 8 > (CPARS + 8 * h->idx);
                var start = Context.Config.Core.Cpars + 8 * h.Idx;
                for (int i = start; i < start + 8; i++)
                {
                    h.Data.Step[i - start] = -x[i];
                }
                ////h->data->step.tail < 2 > ().setZero();
                h.Data.Step[h.Data.Step.Length - 2] = 0;
                h.Data.Step[h.Data.Step.Length - 1] = 0;

                foreach (EFFrame t in Frames)
                {
                    var xadIdx = nFrames * h.Idx + t.Idx;
                    var xfS1 = Context.Config.Core.Cpars + 8 * h.Idx;
                    var xfS2 = Context.Config.Core.Cpars + 8 * t.Idx;
                    var adIdx = h.Idx + nFrames * t.Idx;

                    xAd[xadIdx] = (xF.Skip(xfS1).Take(8).ToArray() * _adHostF[adIdx]).Plus(
                                 xF.Skip(xfS2).Take(8).ToArray() * _adTargetF[adIdx]);
                }
            }

#if PAR
            throw new NotImplementedException();
                ////red->reduce(boost::bind(&EnergyFunctional::resubstituteFPt, this, cstep, xAd,  _1, _2, _3, _4), 0, allPoints.size(), 50);
                red->reduce(boost::bind(&EnergyFunctional::resubstituteFPt, this, boost::cref(cstep), xAd, _1, _2, _3, _4), 0, allPoints.size(), 50);
#else
            resubstituteFPt(cstep, xAd, 0, _allPoints.Count, 0);
#endif
        }

        internal void DropPointsF()
        {
            foreach (EFFrame f in Frames)
            {
                for (int i = 0; i < f.Points.Count; i++)
                {
                    EFPoint p = f.Points[i];
                    if (p.StateFlag == EFPointStatus.PS_DROP)
                    {
                        removePoint(p);
                        i--;
                    }
                }
            }

            EFIndicesValid = false;
            MakeIDX();
        }

        private void removePoint(EFPoint pp)
        {
            while(pp.ResidualsAll.Count>0)
            {
                dropResidual(pp.ResidualsAll[pp.ResidualsAll.Count-1]);
            }

            EFFrame h = pp.Host;
            h.Points[pp.IdxInPoints] = h.Points[h.Points.Count-1];
            h.Points[pp.IdxInPoints].IdxInPoints = pp.IdxInPoints;
            h.Points.RemoveAt(h.Points.Count - 1);

            nPoints--;
            pp.Data.EfPoint = null;

            EFIndicesValid = false;
        }

        private void dropResidual(EFResidual rr)
        {
            EFPoint p = rr.Point;
            Debug.Assert(rr == p.ResidualsAll[rr.IdxInAll]);

            p.ResidualsAll[rr.IdxInAll] = p.ResidualsAll[p.ResidualsAll.Count-1];
            p.ResidualsAll[rr.IdxInAll].IdxInAll = rr.IdxInAll;
            p.ResidualsAll.RemoveAt(p.ResidualsAll.Count - 1);


            if (rr.IsActiveAndIsGoodNEW)
                rr.Host.Data.Shell.StatisticsGoodResOnThis++;
            else
                rr.Host.Data.Shell.StatisticsOutlierResOnThis++;


            ConnectivityMap[(((ulong)rr.Host.FrameID) << 32) + ((ulong)rr.Target.FrameID)][0]--;
            nResiduals--;
            rr.Data.EfResidual = null;
        }

        private void resubstituteFPt(float[] xc, float[][] xAd, int min, int max, int tid)
        {
            for (int k = min; k < max; k++)
            {
                EFPoint p = _allPoints[k];

                int ngoodres = 0;
                foreach (EFResidual r in p.ResidualsAll) if (r.IsActiveAndIsGoodNEW) ngoodres++;
                if (ngoodres == 0)
                {
                    p.Data.Step = 0;
                    continue;
                }
                float b = p.BdSumF;
                b -= xc.Mul(p.HcdAccAF.Plus(p.HcdAccLF));

                foreach (EFResidual r in p.ResidualsAll)
                {
                    if (!r.IsActiveAndIsGoodNEW) continue;
                    b -= xAd[r.HostIdx * nFrames + r.TargetIdx].Mul(r.JpJdF);
                }

                p.Data.Step = -b * p.HdiF;
                Debug.Assert(!float.IsInfinity(p.Data.Step) && !float.IsNaN(p.Data.Step));
            }
        }

        private void orthogonalize(double[] b, MatXX H)
        {
            ////	VecX eigenvaluesPre = H.eigenvalues().real();
            ////	std::sort(eigenvaluesPre.data(), eigenvaluesPre.data()+eigenvaluesPre.size());
            ////	std::cout << "EigPre:: " << eigenvaluesPre.transpose() << "\n";


            // decide to which nullspaces to orthogonalize.
            var ns = new List<double[]>();
            ns.InsertRange(ns.Count, LastNullspaces_pose);
            ns.InsertRange(ns.Count, LastNullspaces_scale);
            ////	if(setting_affineOptModeA <= 0)
            ////		ns.insert(ns.end(), lastNullspaces_affA.begin(), lastNullspaces_affA.end());
            ////	if(setting_affineOptModeB <= 0)
            ////		ns.insert(ns.end(), lastNullspaces_affB.begin(), lastNullspaces_affB.end());



            //unmanaged
            _energyFunctionalU.Orthogonalize(ns, H, b, Context.Config.Core.SolverModeDelta);

            ////	std::cout << std::setprecision(16) << "Orth SV: " << SNN.reverse().transpose() << "\n";

            ////	VecX eigenvaluesPost = H.eigenvalues().real();
            ////	std::sort(eigenvaluesPost.data(), eigenvaluesPost.data()+eigenvaluesPost.size());
            ////	std::cout << "EigPost:: " << eigenvaluesPost.transpose() << "\n";
        }

        private void accumulateSCF_MT(out MatXX H, out double[] b)
        {
#if PAR
             throw new NotImplementedException();
              red->reduce(boost::bind(&AccumulatedSCHessianSSE::setZero, accSSE_bot, nFrames,  _1, _2, _3, _4), 0, 0, 0);
		red->reduce(boost::bind(&AccumulatedSCHessianSSE::addPointsInternal,
				accSSE_bot, &allPoints, true,  _1, _2, _3, _4), 0, allPoints.size(), 50);
		accSSE_bot->stitchDoubleMT(red,H,b,this,true);
           
#else
            _accSSE_bot.SetZero(nFrames);
            foreach (EFFrame f in Frames)
                foreach (EFPoint p in f.Points)
                    _accSSE_bot.AddPoint(p, true, 0);
            _accSSE_bot.StitchDoubleMT(out H, out b, this);
#endif
        }

        private void accumulateLF_MT(out MatXX H, out double[] b)
        {
#if PAR
             throw new NotImplementedException();
               red->reduce(boost::bind(&AccumulatedTopHessianSSE::setZero, accSSE_top_L, nFrames,  _1, _2, _3, _4), 0, 0, 0);
		red->reduce(boost::bind(&AccumulatedTopHessianSSE::addPointsInternal<1>,
				accSSE_top_L, &allPoints, this,  _1, _2, _3, _4), 0, allPoints.size(), 50);
		accSSE_top_L->stitchDoubleMT(red,H,b,this,true,true);
		resInL = accSSE_top_L->nres[0];
           
#else
            _accSSE_top_L.SetZero(nFrames);
            foreach (EFFrame f in Frames)
                foreach (EFPoint p in f.Points)
                    _accSSE_top_L.AddPoint(1, p, this, 0);
            _accSSE_top_L.StitchDoubleMT(out H, out b, this, true);
            ResInL = _accSSE_top_L.Nres[0];
#endif
        }

        private void accumulateAF_MT(out MatXX H, out double[] b)
        {
#if PAR
             throw new NotImplementedException();
                red->reduce(boost::bind(&AccumulatedTopHessianSSE::setZero, accSSE_top_A, nFrames, _1, _2, _3, _4), 0, 0, 0);
                red->reduce(boost::bind(&AccumulatedTopHessianSSE::addPointsInternal < 0 >,
                        accSSE_top_A, &allPoints, this, _1, _2, _3, _4), 0, allPoints.size(), 50);
                accSSE_top_A->stitchDoubleMT(red, H, b, this, false, true);
                resInA = accSSE_top_A->nres[0];
           
#else
            _accSSE_top_A.SetZero(nFrames);
            foreach (EFFrame f in Frames)
                foreach (EFPoint p in f.Points)
                    _accSSE_top_A.AddPoint(0, p, this, 0);
            _accSSE_top_A.StitchDoubleMT(out H, out b, this, false);
            ResInA = _accSSE_top_A.Nres[0];
#endif
        }
    }
}
