﻿using System;
using System.Diagnostics;
using SharpSLAM.Core.Unmanaged;
using System.Linq;

namespace SharpSLAM.Core
{
    public class AccumulatedTopHessian : Contextual
    {
        private AccumulatedTopHessianU _accumulatedTopHessianU { get; set; }

        public int[] Nres { get; set; }
        public int[] Nframes { get; set; }

        public AccumulatedTopHessian(CoreContext context, int numThreads) : base(context)
        {
            _accumulatedTopHessianU = new AccumulatedTopHessianU(numThreads);

            Nres = new int[numThreads];
            Nframes = new int[numThreads];

            for (int tid = 0; tid < numThreads; tid++)
            {
                Nres[tid] = 0;
                Nframes[tid] = 0;
            }
        }

        public void SetZero(int nFrames, int min = 0, int max = 1, int tid = 0)
        {
            _accumulatedTopHessianU.SetZero(nFrames, Nframes[tid], tid);

            Nframes[tid] = nFrames;
            Nres[tid] = 0;
        }

        public void AddPoint(int mode, EFPoint p, EnergyFunctional ef, int tid)
        {
            // 0 = active, 1 = linearized, 2=marginalize
            Debug.Assert(mode == 0 || mode == 1 || mode == 2);

            var dc = ef._cDeltaF;
            float dd = p.DeltaF;

            float bd_acc = 0;
            float Hdd_acc = 0;
            var Hcd_acc = new float[Context.Config.Core.Cpars];

            foreach (EFResidual r in p.ResidualsAll)
            {
                if (mode == 0)
                {
                    if (r.IsLinearized || !r.IsActiveAndIsGoodNEW) continue;
                }
                if (mode == 1)
                {
                    if (!r.IsLinearized || !r.IsActiveAndIsGoodNEW) continue;
                }
                if (mode == 2)
                {
                    if (!r.IsActiveAndIsGoodNEW) continue;
                    Debug.Assert(r.IsLinearized);
                }


                RawResidualJacobian rJ = r.J;
                int htIDX = r.HostIdx + r.TargetIdx * Nframes[tid];
                var dp = ef._adHTdeltaF[htIDX];



                float[] resApprox=new float[Context.Config.Core.MaxResPerPoint];
                if (mode == 0)
                    resApprox = rJ.ResF;
                if (mode == 2)
                    resApprox = r.ResToZeroF;

                var FJp_delta_x = rJ.Jpdxi[0].Mul(dp.Take(6)) + rJ.Jpdc[0].Mul(dc) + rJ.Jpdd[0] * dd;
                var FJp_delta_y = rJ.Jpdxi[1].Mul(dp.Take(6)) + rJ.Jpdc[1].Mul(dc) + rJ.Jpdd[1] * dd;

                //Unmanaged
                var JI_r = _accumulatedTopHessianU.AddPoint(mode, Context.Config.Core.PatternNumber, FJp_delta_x, FJp_delta_y, dp[6], dp[7], rJ.JIdx, rJ.JabF, r.ResToZeroF, resApprox, rJ.Jpdc, rJ.Jpdxi, rJ.JIdx2, rJ.Jab2, rJ.JabJIdx, tid, htIDX);

                var Ji2_Jpdd = rJ.JIdx2 * rJ.Jpdd;
                bd_acc += JI_r[0] * rJ.Jpdd[0] + JI_r[1] * rJ.Jpdd[1];
                Hdd_acc += Ji2_Jpdd.Mul(rJ.Jpdd);
                Hcd_acc = Hcd_acc.Plus(rJ.Jpdc[0].Mul(Ji2_Jpdd[0]).Plus(rJ.Jpdc[1].Mul(Ji2_Jpdd[1])));

                Nres[tid]++;
            }


            if (mode == 0)
            {
                p.HddAccAF = Hdd_acc;
                p.BdAccAF = bd_acc;
                p.HcdAccAF = Hcd_acc;
            }
            if (mode == 1 || mode == 2)
            {
                p.HddAccLF = Hdd_acc;
                p.BdAccLF = bd_acc;
                p.HcdAccLF = Hcd_acc;
            }
            if (mode == 2)
            {
                Array.Clear(p.HcdAccAF, 0, p.HcdAccAF.Length);
                p.HddAccAF = 0;
                p.BdAccAF = 0;
            }
        }

        public void StitchDoubleMT(out MatXX H, out double[] b, EnergyFunctional ef, bool usePrior)
        {
            // sum up, splitting by bock in square.
#if PAR
            throw new NotImplementedException();
                MatXX Hs[NUM_THREADS];
                VecX bs[NUM_THREADS];
                for (int i = 0; i < NUM_THREADS; i++)
                {
                    assert(nframes[0] == nframes[i]);
                    Hs[i] = MatXX::Zero(nframes[0] * 8 + CPARS, nframes[0] * 8 + CPARS);
                    bs[i] = VecX::Zero(nframes[0] * 8 + CPARS);
                }

                red->reduce(boost::bind(&AccumulatedTopHessianSSE::stitchDoubleInternal,
                    this, Hs, bs, EF, usePrior, _1, _2, _3, _4), 0, nframes[0] * nframes[0], 0);

                // sum up results
                H = Hs[0];
                b = bs[0];

                for (int i = 1; i < NUM_THREADS; i++)
                {
                    H.noalias() += Hs[i];
                    b.noalias() += bs[i];
                    nres[0] += nres[i];
                }
#else
            var size = Nframes[0] * 8 + Context.Config.Core.Cpars;
            H = MatXX.Zero(size,size);
            b = new double[size];
            stitchDoubleInternal(H,b, ef, usePrior, 0, Nframes[0] * Nframes[0], -1);
#endif

            // make diagonal by copying over parts.
            for (int h = 0; h < Nframes[0]; h++)
            {
                int hIdx = Context.Config.Core.Cpars + h * 8;
                //unamanged
                _accumulatedTopHessianU.StitchDoubleMTIter(hIdx,H,h,Nframes[0]);
            }
        }

        private void stitchDoubleInternal(MatXX H, double[] b, EnergyFunctional EF, bool usePrior, int min, int max, int tid)
        {
            int toAggregate=_accumulatedTopHessianU.NumThreads; //NUM_THREADS;
            if (tid == -1) { toAggregate = 1; tid = 0; }    // special case: if we dont do multithreading, dont aggregate.
            if (min == max) return;

            //unamanged
            _accumulatedTopHessianU.StitchDoubleInternal(min, max, Nframes, toAggregate, H, b, EF._adHost, EF._adTarget, tid, usePrior, EF._cPrior, EF._cDeltaF, EF.Frames.Select(x => x.Prior).ToArray(), EF.Frames.Select(x => x.DeltaPrior).ToArray());
        }
    }
}