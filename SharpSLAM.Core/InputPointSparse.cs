﻿namespace SharpSLAM.Core
{
    public class InputPointSparse:Contextual
    {
        public float U;
        public float V;
        public float Idpeth;
        public float IdepthHessian;
        public float RelObsBaseline;
        public int NumGoodRes;
        public float[] Color;
        public byte Status;

        public InputPointSparse(CoreContext context) : base(context)
        {
            Color = new float[Context.Config.Core.PatternNumber];
        }
    };
}
