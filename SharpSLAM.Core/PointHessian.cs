﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace SharpSLAM.Core
{
    public enum PointHessianStatus { ACTIVE = 0, INACTIVE, OUTLIER, OOB, MARGINALIZED };

    public class PointHessian : Contextual
    {
        public bool HasDepthPrior { get; set; }

        public float IdepthHessian { get; set; }
        public float MaxRelBaseline { get; set; }
        public int NumGoodResiduals { get; set; }

        public float U { get; set; }
        public float V { get; set; }

        public EFPoint EfPoint { get; set; }
        public FrameHessian Host { get; set; }
        public float Type { get; set; }
        public PointHessianStatus Status { get; set; }

        public float Idepth { get; set; }
        public float Step { get;  set; }

        public float IdepthBackup { get; set; }
        public float StepBackup { get; set; }

        public float IdepthScaled { get; set; }
        public float IdepthZero { get; set; }
        public float IdepthZeroScaled { get; set; }
        public float NullspacesScale { get; set; }
        public float[] Color { get; set; }
        public float[] Weights { get; set; }
        public float EnergyTh { get; set; }

        public List<PointFrameResidual> Residuals { get; set; }                         // only contains good residuals (not OOB and not OUTLIER). Arbitrary order.
        public ValueTuple<PointFrameResidual, ResState>[] LastResiduals { get; set; }   // contains information about residuals to the last two (!) frames. ([0] = latest, [1] = the one before).

        public PointHessian(CoreContext context, ImmaturePoint rawPoint) : base(context)
        {
            Host = rawPoint.Host;
            HasDepthPrior = false;

            IdepthHessian = 0;
            MaxRelBaseline = 0;
            NumGoodResiduals = 0;

            // set static values & initialization.
            U = rawPoint.U;
            V = rawPoint.V;
            Debug.Assert(!float.IsInfinity(rawPoint.IdepthMax) && !float.IsNaN(rawPoint.IdepthMax));
            ////idepth_init = rawPoint->idepth_GT;

            Type = rawPoint.Type;

            SetIdepthScaled((rawPoint.IdepthMax + rawPoint.IdepthMin) * 0.5f);
            Status = PointHessianStatus.INACTIVE;

            int n = Context.Config.Core.PatternNumber;
            Color = (float[])rawPoint.Color.Clone();
            Weights = (float[])rawPoint.Weights.Clone();
            EnergyTh = rawPoint.EnergyTh;

            Residuals = new List<PointFrameResidual>();
            LastResiduals = new ValueTuple<PointFrameResidual, ResState>[2];
        }


        public void SetIdepth(float idepth)
        {
            Idepth = idepth;
            IdepthScaled = Context.Config.Core.ScaleIdepth * idepth;
        }
        public void SetIdepthScaled(float idepthScaled)
        {
            Idepth = Context.Config.Core.ScaleIdepthInverse * idepthScaled;
            IdepthScaled = idepthScaled;
        }

        public void SetIdepthZero(float idepth)
        {
            IdepthZero = idepth;
            IdepthZeroScaled = Context.Config.Core.ScaleIdepth * idepth;
            NullspacesScale = -(Idepth * 1.001f - Idepth / 1.001f) * 500f;
        }

        public bool IsOOB(List<FrameHessian> toKeep, List<FrameHessian> toMarg)
        {
            int visInToMarg = 0;
            foreach (PointFrameResidual r in Residuals)
            {
                if (r.StateState != ResState.IN) continue;
                foreach (FrameHessian k in toMarg)
                    if (r.Target == k) visInToMarg++;
            }
            if (Residuals.Count >= Context.Config.Core.MinGoodActiveResForMarg &&
                    NumGoodResiduals > Context.Config.Core.MinGoodResForMarg + 10 &&
                    Residuals.Count - visInToMarg < Context.Config.Core.MinGoodActiveResForMarg)
                return true;


            if (LastResiduals[0].Item2 == ResState.OOB) return true;
            if (Residuals.Count < 2) return false;
            if (LastResiduals[0].Item2 == ResState.OUTLIER && LastResiduals[1].Item2 == ResState.OUTLIER) return true;
            return false;
        }

        public bool IsInlierNew()
        {
            return Residuals.Count >= Context.Config.Core.MinGoodActiveResForMarg
                    && NumGoodResiduals >= Context.Config.Core.MinGoodResForMarg;
        }
    }
}