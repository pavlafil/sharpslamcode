﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using CC = CNTK.CNTKLib;
using C = CNTK;
using SharpSLAM.Shared;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace SharpSLAM.Core
{
    public class PixelSelectorNN : PixelSelector
    {
        private PixelSelector _origSelector;
        private ProducerConsumerQueue<ValueTuple<ImageAndExposure<MinimalImageF, float>, float[], float[]>> _imagesQueue = new ProducerConsumerQueue<(ImageAndExposure<MinimalImageF, float>, float[], float[])>();

        public PixelSelectorNN(CoreContext context) : base(context)
        {
            _origSelector = new PixelSelector(context);

            buildModel();
        }

        private void buildModel()
        {
        }

        public void Train(IList<ImageAndExposure<MinimalImageF, float>> images)
        {
            //makeAll(images);

            //var ohO = Context.Config.Input.GeometricCalibration.OutputHeight;
            //var owO = Context.Config.Input.GeometricCalibration.OutputWidth;
            //var pyrLevelsO = Context.PyrLevelsUsed;
            //Context.w[0]=Context.Config.Input.GeometricCalibration.OutputWidth = 64;
            //Context.h[0] =Context.Config.Input.GeometricCalibration.OutputHeight =(int)( Context.Config.Input.GeometricCalibration.OutputWidth * ((double)ohO / owO));
            //Context.PyrLevelsUsed = 1;

            Log.LogInfo("Training of CNN started");

            var layers = new List<C.Function>();

            var config = Context?.Config?.Core?.Training;
            var configNN = Context?.Config?.Core?.PixelSelector as Configuration.NNPixelSelector;

            int h = Context.h[0];
            int w = Context.w[0];
            int ioDim = w * h;

            //Params
            var filterShape = new int[] { 3, 3 };
            int featuresLayers = 2;
            int featureOutputChannels = 4;//6;
            int pointOutputChannels = 2;
            var convolShape = new int[] { 2, 2 };

            bool useBias = true;
            double learningRate = 0.03;
            double momentum = 0.2;
            double normScale = 1;

            int epochs = 1;
            int batchSize = 10;
            int numberOfPrinted = 1;
            int logFrequency = 1;
            Func<C.Variable, string, C.Function> activation = CC.ReLU;
            //------

            var device = Util.get_compute_device();
            Log.LogInfo("Compute Device: " + device.AsString());

            var stride1 = new int[] { 1, 1 };

            var inputShape = CNTK.NDShape.CreateNDShape(new int[] { Context.w[0], Context.h[0], 1 });
            var pointPresent = CNTK.NDShape.CreateNDShape(new int[] { Context.w[0], Context.h[0], 1 });
            var outputShape = pointPresent;
            var labelsShape = CNTK.NDShape.CreateNDShape(new int[] { Context.w[0], Context.h[0] });

            #region Model
            #region Input
            var imageInput = CNTK.Variable.InputVariable(inputShape, CNTK.DataType.Float, "Input");
            layers.Add(imageInput);
            #endregion

            #region Features
            C.Function features = imageInput;
            for (int i = 0; i < featuresLayers; i++)
            {
                features = Util.Convolution2D(features, featureOutputChannels, filterShape, device, true, useBias, stride1, null, $"Features{i}");
                features = bn_with_relu(features, device, normScale, activation, $"Features{i}Normalized");
                layers.Add(features);
            }
            #endregion

            #region PointDetection
            var pointsDetection = Util.Convolution2D(features, pointOutputChannels, filterShape, device, true, useBias, stride1, null, "PointsDetection");

            pointsDetection = bn_with_relu(pointsDetection, device, normScale, activation);
            pointsDetection = bn_with_relu(pointsDetection, device, normScale, CC.Tanh, "PointsDetectionNormalized");
            //pointsDetection = CC.Tanh(pointsDetection, "PointsDetectionNormalized");

            //pointsDetection = CC.Greater(pointsDetection, C.Constant.Scalar(C.DataType.Float, 0.5));

            layers.Add(pointsDetection);
            #endregion


            //#region ConvolutionDeconvolution
            //C.Function convolution = features;
            //var strides = new int[3][];
            //var shapes = new int[3][];

            //for (int i = 0; i < 3; i++)
            //{
            //    var width = convolution.Output.Shape[0];
            //    var height = convolution.Output.Shape[1];
            //    var timesX = width / convolShape[0];
            //    var timesY = height / convolShape[1];
            //    var reduceStepX = timesX - 1;
            //    var reduceStepY = timesY - 1;
            //    var residueX = width % convolShape[0];
            //    var residueY = width % convolShape[0];
            //    var needToReduxKoefX = (int)Math.Ceiling((reduceStepX - residueX) / (double)convolShape[0]);
            //    var needToReduxKoefY = (int)Math.Ceiling((reduceStepY - residueY) / (double)convolShape[1]);
            //    var needToReduceX = residueX != 0 ? (needToReduxKoefX * convolShape[0]) + residueX : residueX;
            //    var needToReduceY = residueY != 0 ? (needToReduxKoefY * convolShape[1]) + residueY : residueY;
            //    if (needToReduceX % reduceStepX != 0 || needToReduceY % reduceStepY != 0)
            //        throw new Exception("Convolution step is not possible select different filter size");
            //    var stride = new int[] { convolShape[0] - (needToReduceX / reduceStepX), convolShape[1] - (needToReduceY / reduceStepY) };
            //    strides[i] = stride;
            //}

            //for (int i = 1; i <= 3; i++)
            //{
            //    var cd = Util.Convolution2D(convolution, 1, convolShape, device, false, useBias, strides[i - 1], null, $"Convol2^{i}");
            //    shapes[i - 1] = convolution.Output.Shape.Dimensions.ToArray();
            //    cd = bn_with_relu(cd, device, normScale, activation, $"Convol2^{i}Normalized");
            //    layers.Add(cd);
            //    convolution = cd;
            //}

            //for (int i = 1; i <= 3; i++)
            //{
            //    var cd = Util.ConvolutionTranspose(convolution, device, convolShape, 1, null, false, strides[3 - i], true, shapes[3 - i].Take(2).ToArray(), name: $"Deconvol2^{i}");
            //    if (i == 3)
            //    {
            //        cd = bn_with_relu(cd, device, normScale, activation);
            //        cd = CC.Tanh(cd, $"Deconvol2^{i}Normalized");
            //    }
            //    else
            //        cd = bn_with_leaky_relu(cd, device, name: $"Deconvol2^{i}Normalized");
            //    layers.Add(cd);
            //    convolution = cd;
            //}
            //#endregion

            //#region ConcatPointAndConvolution
            //var comb = CC.ElementTimes(convolution, pointsDetection, "CombConvAndPoints");
            //comb = bn_with_relu(comb, device, normScale, activation, "CombConvAndPointsNorm");
            //layers.Add(comb);
            //#endregion

            #region Output
            var output = Util.Convolution2D(pointsDetection, 1, filterShape, device, true, useBias, stride1, null, "Output");
            output = bn_with_relu(output, device, normScale, activation);
            output = bn_with_relu(output, device, normScale, CC.Tanh, "OutputNormalized");
            //output = CC.Tanh(output, "OutputNormalized");
            layers.Add(output);
            #endregion

            #endregion
            //NETWORK BUILD COMPLETED
            #region SaveAndStats
            Util.log_number_of_parameters(output);
            output.Save("test.cnn");

            foreach (var l in layers)
            {
                Log.LogInfo($"{l.Name} shape: {l.Output.Shape.AsString()}");
            }
            #endregion

            #region SetHowToLearn
            C.Variable originalResult = CNTK.Variable.InputVariable(labelsShape, CNTK.DataType.Float, "OriginalResult");
            var originalResultScaled = CC.ElementDivide(originalResult, C.Constant.Scalar(C.DataType.Float, 4), "OriginalResultScaled");

            //originalResultScaled = CC.Reshape(originalResultScaled, C.NDShape.CreateNDShape(new int[] { w, h, 1 }));
            //originalResultScaled = Util.Convolution2D(originalResultScaled, 1, filterShape, device, true, useBias, stride1, null, "OrigResultBlend");
            //originalResultScaled = bn_with_relu(originalResultScaled, device, normScale, activation);
            //originalResultScaled = CC.Tanh(originalResultScaled, "OriginalResultNormalized");

            layers.Add(originalResultScaled);

            var maxSum = C.Constant.Scalar(C.DataType.Float, ioDim);


            var diff = CC.Abs(CC.Minus(output, originalResultScaled, "Diff"), "DiffAbs");
            layers.Add(diff);

            var summDiff = CC.ReduceSum(diff, C.Axis.AllAxes(), "SumDiff");
            var lossByNotBeSameAsOriginal = CC.ElementDivide(summDiff, maxSum, "LossBadDiff");
            layers.Add(lossByNotBeSameAsOriginal);

            var outputSum = CC.ReduceSum(output,C.Axis.AllStaticAxes(), "OutputSum");
            var outputSumNormalized = CC.ElementDivide(outputSum, maxSum,"OutputSumNormalized");
            //layers.Add(outputSumNormalized);
            var lossByTooMuchBlack = CC.Minus(C.Constant.Scalar(C.DataType.Float, 1), outputSumNormalized, "LossTooBlack");
            layers.Add(lossByTooMuchBlack);



            lossByNotBeSameAsOriginal = CC.ElementTimes(lossByNotBeSameAsOriginal, C.Constant.Scalar(C.DataType.Float, 3));

            //Func<C.Function,C.Function> log = (C.Function z) => CC.Negate(CC.Minus(C.Constant.Scalar(C.DataType.Float, 1),z));
            //var lossFunction = CC.Plus(lossByNotBeSameAsOriginal, lossByTooMuchBlack,"SumOfLossFunctions");
            var lossFunction = CC.Plus(lossByNotBeSameAsOriginal, lossByTooMuchBlack, "SumOfLossFunctions");
            layers.Add(lossFunction);
            //var lossFunction = lossByNotBeSameAsOriginal;

            var parameterVector = new CNTK.ParameterVector((System.Collections.ICollection)output.Parameters());
            var learner = CC.SGDLearner(parameterVector, new C.TrainingParameterScheduleDouble(learningRate, (uint)batchSize));
            //var learner = CC.AdamLearner(parameterVector, new C.TrainingParameterScheduleDouble(learningRate, (uint)batchSize), CC.MomentumAsTimeConstantSchedule(momentum));
            var trainer = CC.CreateTrainer(output, lossFunction, new CNTK.LearnerVector() { learner });
            #endregion


            #region Train
            for (int currentEpoch = 0; currentEpoch < epochs; currentEpoch++)
            {
                Log.LogInfo(string.Format("Epoch {0}/{1}", currentEpoch + 1, epochs));

                var trainIndices = Util.shuffled_indices(images.Count);


                Task.Run(() => startLoadingImages(trainIndices, images));
                System.Threading.Thread.Sleep(5);//load some images to queue


                var pos = 0;
                while (pos < trainIndices.Length)
                {
                    var pos_end = Math.Min(pos + batchSize, trainIndices.Length);

                    var selImages = new ImageAndExposure<MinimalImageF, float>[pos_end - pos];
                    var imagesData = new float[pos_end - pos][];
                    var resultData = new float[pos_end - pos][];
                    for (int i = 0; i < pos_end - pos; i++)
                    {
                        (selImages[i], imagesData[i], resultData[i]) = _imagesQueue.Consume();
                    }

                    var minibatchImages = Util.get_tensors(inputShape, imagesData, 0, pos_end - pos, device);
                    var minibatchResults = Util.get_tensors(labelsShape, resultData, 0, pos_end - pos, device);

                    var feedDictionary = new Dictionary<CNTK.Variable, CNTK.Value>() { { imageInput, minibatchImages }, { originalResult, minibatchResults } };
                    trainer.TrainMinibatch(feedDictionary, false, device);


                    var training_loss = double.NaN;
                    //var eval_error = double.NaN;

                    if ((pos / batchSize) % logFrequency == 0)
                    {
                        training_loss = trainer.PreviousMinibatchLossAverage();
                        //eval_error = trainer.PreviousMinibatchEvaluationAverage();
                        //Log.LogInfo(string.Format("Minibatch: {0}, Loss: {1}, Error: {2}", (pos / batch_size), training_loss, eval_error * 100));
                        Log.LogInfo(string.Format("Minibatch: {0}, Loss: {1}", (pos / batchSize), training_loss));


                        //var outputBuffer = new float[ioDim];
                        for (int i = 0; i < Math.Min(numberOfPrinted, pos_end - pos); i++)
                        {
                            int imgCounter = 0;
                            var img = selImages[i];

                            //NoNeeded result from inside of network are okey
                            //lock (Context.Output)
                            //{
                            //    Context.Output.UpdateImage($"IMG{i}.{getChars(imgCounter++)} Original", new MinimalImageF(img.Image.ID, img.Image.Width, img.Image.Height, imagesData[i]));
                            //    Context.Output.UpdateImage($"IMG{i}.{getChars(imgCounter++)} Original Result", new MinimalImageF(img.Image.ID, img.Image.Width, img.Image.Height, resultData[i]));
                            //}

                            var imagesVal = new C.Value(new C.NDArrayView(inputShape, imagesData[i], device));
                            var resultVal = new C.Value(new C.NDArrayView(labelsShape, resultData[i], device));

                            var inD = new Dictionary<CNTK.Variable, CNTK.Value>() { { imageInput, imagesVal }, { originalResult, resultVal } };

                            var outD = new Dictionary<CNTK.Variable, CNTK.Value>();

                            foreach (var l in layers)
                            {
                                outD.Add(l.Output, null);
                            }

                            var start = DateTime.Now;
                            lossFunction.Evaluate(inD, outD, device);
                            var end = DateTime.Now;
                            Context.Output.UpdateData("MakeMapsNNTiming", "OnlyMakeMaps", pos + i, (end - start).TotalMilliseconds, "Frame", "Time [ms]");

                            foreach (var l in outD)
                            {
                                var data = l.Value.GetDenseData<float>(l.Key);
                                var imgData = data[0].ToArray();

                                var imgWidth = 1;
                                var imgHeight = 1;
                                if (l.Key.Shape.Dimensions.Count > 0)
                                    imgWidth = l.Key.Shape.Dimensions[0];
                                if (l.Key.Shape.Dimensions.Count > 1)
                                    imgHeight = l.Key.Shape.Dimensions[1];

                                if (l.Value.Shape.Dimensions.Count > 2 && l.Value.Shape.Dimensions[2] > 1)
                                {
                                    int currentOffset = 0;
                                    int step = l.Key.Shape.Dimensions[0] * l.Key.Shape.Dimensions[1];
                                    for (int ii = 0; ii < l.Value.Shape.Dimensions[2]; ii++)
                                    {
                                        var imgName = $"IMG{i}.{getChars(imgCounter)} {l.Key.Name}".Trim() + $".{getChars(ii)}".Trim();
                                        Context.Output.UpdateImage(imgName, new MinimalImageF(l.Key.Name, imgWidth, imgHeight, imgData.Skip(currentOffset).Take(step).ToArray()));
                                        currentOffset += step;
                                    }
                                    imgCounter++;
                                }
                                else
                                {
                                    var imgName = $"IMG{i}.{getChars(imgCounter++)} {l.Key.Name}".Trim();
                                    if (imgWidth == 1 && imgHeight == 1)
                                    {
                                        Log.LogInfo($"{imgName}: {imgData[0]}");
                                        Context.Output.UpdateData("Learning", "Loss", int.Parse(img.Image.ID), imgData[0], "Id", "Loss");
                                        //var bData = imgData.Select(x => (byte)x).ToArray();
                                        //Context.Output.UpdateImage(imgName, new MinimalImageB(l.Key.Name, imgWidth, imgHeight, bData));
                                    }
                                    else
                                        Context.Output.UpdateImage(imgName, new MinimalImageF(l.Key.Name, imgWidth, imgHeight, imgData));
                                }
                                l.Value.Dispose();
                            }

                        }
                    }
                    pos = pos_end;
                }
            }
            #endregion
        }


        private void startLoadingImages(int[] trainIndices, IList<ImageAndExposure<MinimalImageF, float>> images)
        {
            //foreach (var i in trainIndices)
            Parallel.ForEach(trainIndices, (i) =>
            {
                ImageAndExposure<MinimalImageF, float> image;
                float[] resultData;
                float[] imageData;
                image = images[i];
                //image = images[trainIndices[i]];
                resultData = getLabel(image);
                imageData = image.Image.Data;
                //Normalize
                float max = imageData.Max();
                imageData = imageData.Select(x => x / max).ToArray();
                //-----------------
                _imagesQueue.Produce((image, imageData, resultData));
            }
            );
        }

        private string getChars(int i, int width = -1)
        {
            StringBuilder sb = new StringBuilder();
            int l = i;
            int ii = 0;
            while (true)
            {
                l = l % 26;
                char ch = (char)('a' + l);
                sb.Append(ch);
                l = l / 26;
                ii++;
                if ((width < 0 && l == 0) || width == ii) break;
            }
            return new string(sb.ToString().Reverse().ToArray());
        }

        private float[] getLabel(ImageAndExposure<MinimalImageF, float> image)
        {
            var frameHessian = new FrameHessian(Context);
            FrameShell shell = new FrameShell();
            shell.Timestamp = image.TimeStamp;
            shell.IncomingId = image.Image.ID;
            shell.Image = image;
            frameHessian.Shell = shell;
            // =========================== make Images / derivatives etc. =========================
            frameHessian.AbExposure = image.ExposureTime; //no needed values hold by context
            frameHessian.MakeImages(image.Image, false);

            float[] statusMap = new float[Context.w[0] * Context.h[0]];
            float[] densities = new float[5] { 0.03f, 0.05f, 0.15f, 0.5f, 1f };
            _origSelector.CurrentPotential = 3;
            _origSelector.MakeMaps(frameHessian, statusMap, 0.03f * Context.w[0] * Context.h[0], 1, false, 2);
            return statusMap;
        }

        private void WriteAllNodes(C.Variable node)
        {
            WriteAllNodes(new List<C.Variable>() { node });
        }
        private void WriteAllNodes(List<C.Variable> nodes)
        {
            if (nodes == null) return;
            List<C.Variable> currentLayerParents = new List<C.Variable>();
            foreach (var node in nodes)
            {
                var parents = node.Owner?.Inputs;
                if (parents != null && parents.Count > 0)
                {
                    currentLayerParents.AddRange(parents);
                }
            }

            if (currentLayerParents.Count > 0)
            {
                WriteAllNodes(currentLayerParents);
                Log.LogInfo("");
            }

            Log.LogInfo("---");
            foreach (var node in nodes)
            {

                if (node.Kind == C.VariableKind.Output)
                {
                    Log.LogInfo($"F:{node.Uid}:{node.Name}");
                    Log.LogInfo($"D:{string.Join(":", node.Shape.Dimensions)}");

                    //var val = new C.Value(node.GetValue());
                    //var data = val.GetDenseData<float>(node).First();

                }
                else if (node.Kind == C.VariableKind.Parameter)
                {
                    Log.LogInfo($"P:{node.Uid}:{node.Name}");
                    Log.LogInfo($"D:{string.Join(":", node.Shape.Dimensions)}");

                    //var val = new C.Value(node.GetValue());
                    //var data = val.GetDenseData<float>(node).First();
                    //string log = "";
                    //for (int i = 0; i < node.Shape.Dimensions.Aggregate(1,(a,v)=> { return a * v; }); i++)
                    //{
                    //    log += data[i].ToString("F2") + " ";
                    //    if ((i+1) % node.Shape.Dimensions.First() == 0)
                    //        log += "\nDebug: ";
                    //}
                    //Log.LogInfo(log, false);
                }
                else
                {
                    Log.LogInfo("Unknown");
                }
                Log.LogInfo("---");
            }
        }

























        private void makeAll(IList<ImageAndExposure<MinimalImageF, float>> images)
        {
            //architectural parameters
            int img_h = 192, img_w = 256;
            var kernel = new int[] { 5, 5 };
            var stride = new int[] { 1, 1 };

            //Input / Output parameter of Generator and Discriminator
            var g_input_dim = new int[] { img_w, img_h, 1 };
            int g_output_dim = img_h * img_w;
            // We expect the kernel shapes to be square in this tutorial and
            // the strides to be of the same length along each data dimension
            if (kernel[0] != kernel[1])
                throw new Exception("This tutorial needs square shaped kernel");

            if (stride[0] != stride[1])
                throw new Exception("This tutorial needs same stride in all dims");
            //------------------------------------------------------------------------

            //training config
            int minibatch_size = 2;//128;
            int num_minibatches = 10;//00;
            double lr = 0.0002;
            double momentum = 0.5; //equivalent to beta1

            int out_freq = 1;
            int num_of_outs = 2;
            //------------------------------------------------------------------------





            //var pimages = get_images(minibatch_size, num_minibatches);
            (var sources, var pimages) = get_images1(images, minibatch_size, num_minibatches);

            (var G_input, var G_output, var G_trainer_loss) = train(sources, pimages, out_freq, num_of_outs, minibatch_size, num_minibatches, g_input_dim, g_output_dim, lr, momentum, img_w, img_h, kernel, stride, kernel, stride, C.DeviceDescriptor.CPUDevice);
        }

        private ValueTuple<IList<float[][]>, IList<float[][]>> get_images1(IList<ImageAndExposure<MinimalImageF, float>> images, int batchSize, int numberOfBatches = -1)
        {
            Context.Output.DisableUpdates = true;
            var retA = new List<float[][]>();
            var retB = new List<float[][]>();
            var currentBatchA = new List<float[]>();
            var currentBatchB = new List<float[]>();
            int it = 0;
            foreach (var i in images)
            {
                var bImage = getLabel(i);
                currentBatchA.Add(i.Image.Data);
                currentBatchB.Add(bImage);

                if (currentBatchA.Count == batchSize)
                {
                    retA.Add(currentBatchA.ToArray());
                    retB.Add(currentBatchB.ToArray());
                    currentBatchA = new List<float[]>();
                    currentBatchB = new List<float[]>();
                    if (numberOfBatches != -1 && retA.Count == numberOfBatches)
                        break;
                }
                Log.LogInfo("Loaded " + i.Image.ID);
                it++;
            }
            if (currentBatchA.Count > 0)
                retA.Add(currentBatchA.ToArray());

            if (currentBatchB.Count > 0)
                retB.Add(currentBatchB.ToArray());

            Context.Output.DisableUpdates = false;
            return (retA, retB);
        }

        private IList<float[][]> get_images(int batchSize, int numberOfBatches = -1)
        {
            var ret = new List<float[][]>();
            var currentBatch = new List<float[]>();
            int it = 0;
            using (var stream = new System.IO.FileStream(@"F:\Data\Skola_Prace\CVUT\Magistr\V. Semestr\MI-MPR,DIP\Datasets\CNTKTest\CNTK-master\Examples\Image\DataSets\MNIST\Train-28x28_cntk_text.txt", System.IO.FileMode.Open))
            {
                using (var reader = new System.IO.StreamReader(stream))
                {
                    while (!reader.EndOfStream)
                    {
                        var l = reader.ReadLine();
                        int i = 38;
                        l = l.Substring(i, l.Length - i);
                        var values = l.Split(' ').Select(x => (float)int.Parse(x)).ToArray();
                        currentBatch.Add(values);
                        if (currentBatch.Count == batchSize)
                        {
                            ret.Add(currentBatch.ToArray());
                            currentBatch = new List<float[]>();
                            if (numberOfBatches != -1 && ret.Count == numberOfBatches)
                                break;
                        }
                        it++;
                    }
                }
            }
            if (currentBatch.Count > 0)
                ret.Add(currentBatch.ToArray());
            return ret;
        }

        private ValueTuple<C.Variable, C.Function, double> train(IList<float[][]> sources, IList<float[][]> images, int out_freq, int num_of_outs, int minibatch_size, int num_minibatches, int[] g_input_dim, int g_output_dim, double lr, double momentum, int w, int h, int[] gkernel, int[] gstrides, int[] dkernel, int[] dstrides, C.DeviceDescriptor device)
        {
            var sourceShape = C.NDShape.CreateNDShape(g_input_dim);
            (var X_real, var X_fake, var Z, var G_trainer, var D_trainer) = build_graph(sourceShape, lr, momentum, w, h, gkernel, gstrides, dkernel, dstrides, device);

            Log.LogInfo("First row is Generator loss, second row is Discriminator loss");

            var k = 2;

            double G_trainer_loss = -1;
            int pos = 0;

            //var testVector = Enumerable.Repeat((float)0.7f, g_input_dim).ToArray();
            while (true)
                for (int i = 0; i < sources.Count; i++)
                {
                    var batchS = sources[i];
                    var batchI = images[i];

                    //train the discriminator model for k steps
                    for (int ii = 0; ii < k; ii++)
                    {
                        var Z_val = Util.get_tensors(sourceShape, batchS, 0, batchS.Length, device);
                        var X_val = Util.get_tensors(C.NDShape.CreateNDShape(new int[] { g_output_dim }), batchI, 0, batchI.Length, device);

                        var ibatch_inputs = new Dictionary<C.Variable, C.Value>() { { X_real, X_val }, { Z, Z_val } };
                        D_trainer.TrainMinibatch(ibatch_inputs, false, device);
                    }

                    //# train the generator model for a single step
                    var val = Util.get_tensors(sourceShape, batchS, 0, batchS.Length, device);
                    var batch_inputs = new Dictionary<C.Variable, C.Value>() { { Z, val } };

                    G_trainer.TrainMinibatch(batch_inputs, false, device);
                    G_trainer.TrainMinibatch(batch_inputs, false, device);

                    var training_loss = G_trainer.PreviousMinibatchLossAverage();
                    Log.LogInfo(string.Format("G_trainer, minibatch: {0}, Loss: {1}", pos, training_loss));
                    training_loss = D_trainer.PreviousMinibatchLossAverage();
                    Log.LogInfo(string.Format("D_trainer, minibatch: {0}, Loss: {1}", pos, training_loss));
                    G_trainer_loss = G_trainer.PreviousMinibatchLossAverage();

                    for (int bi = 0; bi < num_of_outs; bi++)
                    {
                        if (pos % out_freq == 0)
                        {
                            Context.Output.UpdateImage($"01 Originals.{bi}", new MinimalImageF(bi.ToString(), w, h, batchS[bi]));
                            Context.Output.UpdateImage($"02 Label.{bi}", new MinimalImageF(bi.ToString(), w, h, batchI[bi]));

                            var inputVal = Util.get_tensors(sourceShape, batchS[bi], 0, batchS[bi].Length, device);
                            var inD = new Dictionary<CNTK.Variable, CNTK.Value>() { { Z, inputVal } };
                            var outD = new Dictionary<CNTK.Variable, CNTK.Value>() { { X_fake.Output, null } };
                            X_fake.Evaluate(inD, outD, device);
                            var dataOut = outD.Values.First().GetDenseData<float>(X_fake.Output)[0].ToArray();
                            Context.Output.UpdateImage($"02 Learned.{bi}", new MinimalImageF(bi.ToString(), w, h, dataOut));
                            inputVal.Dispose(); outD.Values.First().Dispose();
                            //if (bi == 0)
                            {
                                var min = dataOut.Min();
                                var max = dataOut.Max();
                                Log.LogInfo($"Min: {min}, Max: {max}, Diff: {max - min}");
                            }
                        }
                        if (pos % out_freq != 0) break;
                    }
                    pos++;
                }
            return (Z, X_fake, G_trainer_loss);
        }




        private ValueTuple<C.Variable, C.Function, C.Variable, C.Trainer, C.Trainer> build_graph(C.NDShape source_shape, double lr, double momentum, int w, int h, int[] gkernel, int[] gstrides, int[] dkernel, int[] dstrides, C.DeviceDescriptor device)
        {
            //var input_dynamic_axes = new C.AxisVector() { C.Axis.DefaultBatchAxis() };

            //C.input_variable(noise_shape, dynamic_axes = input_dynamic_axes)
            var z = CC.InputVariable(source_shape, C.DataType.Float);
            var z_scaled = CC.ElementDivide(z, C.Constant.Scalar(C.DataType.Float, 255));

            //C.input_variable(image_shape, dynamic_axes = input_dynamic_axes)
            var X_real = CC.InputVariable(C.NDShape.CreateNDShape(new int[] { w * h }), C.DataType.Float);
            var X_real_scaled = CC.ElementDivide(X_real, C.Constant.Scalar(C.DataType.Float, 4));

            //Create the model function for the generator and discriminator models
            var X_fake = convolutional_generator(z_scaled, w, h, device, gkernel, gstrides);
            //X_fake.Save("x_fake.cnn");
            var D_real = convolutional_discriminator(X_real_scaled, w, h, device, dkernel, dstrides);
            //X_fake.Save("d_real.cnn");
            var D_fake = D_real.Clone(C.ParameterCloningMethod.Share, new Dictionary<C.Variable, C.Variable>() { { X_real_scaled.Output, X_fake.Output } });
            //X_fake.Save("d_fake.cnn");

            var one = C.Constant.Scalar(C.DataType.Float, 1.0);
            //Create loss functions and configure optimazation algorithms
            var G_loss = CC.Minus(one, CC.Log(D_fake), "GLoss");
            //-( C.log(D_real) + C.log(1.0 - D_fake) )
            var D_loss = CC.Negate(CC.Plus(CC.Log(D_real), CC.Log(CC.Minus(one, D_fake))));

            var GParsVect = new C.ParameterVector();
            foreach (var i in X_fake.Parameters())
                GParsVect.Add(i);
            var G_learner = CC.AdamLearner(GParsVect, new C.TrainingParameterScheduleDouble(lr, 1), CC.MomentumAsTimeConstantSchedule(momentum));

            var DParsVect = new C.ParameterVector();
            foreach (var i in D_real.Parameters())
                DParsVect.Add(i);
            var D_learner = CC.AdamLearner(DParsVect, new C.TrainingParameterScheduleDouble(lr, 1), CC.MomentumAsTimeConstantSchedule(momentum));

            //Instantiate the trainers
            var G_trainer = CC.CreateTrainer(X_fake, G_loss, new C.LearnerVector() { G_learner });
            var D_trainer = CC.CreateTrainer(D_real, D_loss, new C.LearnerVector() { D_learner });

            return (X_real, X_fake, z, G_trainer, D_trainer);
        }

        private C.Function convolutional_generator(C.Variable z, int w, int h, C.DeviceDescriptor device, int[] gkernel, int[] gstrides)
        {
            Log.LogInfo($"Generator input shape: {z.Shape}");

            int s_h2 = h / 2; int s_w2 = w / 2; //Input shape (14,14)
            int s_h4 = h / 4; int s_w4 = w / 4; //Input shape (7,7)
            int gfc_dim = 2;// 1024;
            int gf_dim = 1;// 64;

            var h0 = Util.Convolution2D(z, gfc_dim, gkernel, device, true, true, gstrides);
            h0 = bn_with_relu(h0, device);
            Log.LogInfo($"h0 shape {h0.Output.Shape.AsString()}");

            var h1 = Util.Convolution2D(z, gf_dim, gkernel, device, true, true, gstrides);
            h1 = bn_with_relu(h1, device);
            Log.LogInfo($"h1 shape {h1.Output.Shape.AsString()}");

            //var h2 = Util.ConvolutionTranspose(h1, device, gkernel,
            //                          gf_dim * 2,
            //                          null,
            //                          true,
            //                          gstrides,
            //                          true,
            //                          new int[] { s_w2, s_h2 });
            //h2 = bn_with_relu(h2, device);
            //Log.LogInfo($"h2 shape {h2.Output.Shape.AsString()}");

            var h3 = Util.Convolution2D(h1, 1, gkernel, device, true, true, gstrides);

            //h3 = CC.Plus(h3,C.Constant.Scalar(C.DataType.Float,1));
            h3 = Util.batchNormalization(h3, device, 1);
            h3 = CC.Sigmoid(h3);
            //h3 = CC.Times(h3, C.Constant.Scalar(C.DataType.Float, 4));
            //h3 = bn_with_relu(h3, device,1);


            Log.LogInfo($"h3 shape {h3.Output.Shape.AsString()}");
            return CC.Reshape(h3, C.NDShape.CreateNDShape(new int[] { w * h }));
        }

        private C.Function convolutional_discriminator(C.Variable x, int w, int h, C.DeviceDescriptor device, int[] dkernel, int[] dstrides)
        {
            int dfc_dim = 2;// 1024;
            int df_dim = 1;// 64;

            Log.LogInfo($"Discriminator convolution input shape {x.Shape.AsString()}");
            var x1 = CC.Reshape(x, C.NDShape.CreateNDShape(new int[] { w, h, 1 }));

            //C.layers.Convolution2D(dkernel, 1, strides = dstride)(x)
            var h0 = Util.Convolution2D(x1, 1, dkernel, device, false, true, dstrides);
            h0 = bn_with_leaky_relu(h0, device, 0.2);
            Log.LogInfo($"h0 shape : {h0.Output.Shape.AsString()}");

            //C.layers.Convolution2D(dkernel, df_dim, strides = dstride)(h0)
            var h1 = Util.Convolution2D(h0, df_dim, dkernel, device, false, true, dstrides);

            h1 = bn_with_leaky_relu(h1, device, 0.2);
            Log.LogInfo($"h1 shape : {h1.Output.Shape.AsString()}");

            //C.layers.Dense(dfc_dim, activation = None)(h1)
            var h2 = Util.Dense(h1, dfc_dim, device);
            h2 = bn_with_leaky_relu(h2, device, 0.2);
            Log.LogInfo($"h2 shape : {h2.Output.Shape.AsString()}");

            //C.layers.Dense(1, activation = C.sigmoid)(h2)
            var h3 = Util.Dense(h2, 1, device);

            h3 = CC.Sigmoid(h3);

            Log.LogInfo($"h3 shape : {h3.Output.Shape.AsString()}");
            return h3;
        }

        //Helper functions
        private C.Function bn_with_relu(C.Function x, C.DeviceDescriptor device, double scale = 0.02, Func<CNTK.Variable, string, CNTK.Function> activation = null, string name = "")
        {
            if (activation == null)
                activation = CC.ReLU;

            var h = Util.batchNormalization(x, device, 1, scale, name: name);
            h = activation(h, name + "Activation");
            return h;
        }

        //We use param-relu function to use a leak=0.2 since CNTK implementation 
        //of Leaky ReLU is fixed to 0.01
        private C.Function bn_with_leaky_relu(C.Function x, C.DeviceDescriptor device, double leak = 0.2, string name = "")
        {
            var h = Util.batchNormalization(x, device, 1, 0.02, name: name);
            var r = CC.LeakyReLU(h, leak, name + "ReLU");
            return r;
        }

        private float[][] noiseSample(int numSamples, int vectSize)
        {
            Random r = new Random(Guid.NewGuid().GetHashCode());
            var ret = new float[numSamples][];
            for (int i = 0; i < numSamples; i++)
            {
                var c = new float[vectSize];
                ret[i] = c;
                for (int y = 0; y < vectSize; y++)
                {
                    c[y] = (float)(r.NextDouble() * 2) - 1;
                }
            }
            return ret;
        }













    }
}
