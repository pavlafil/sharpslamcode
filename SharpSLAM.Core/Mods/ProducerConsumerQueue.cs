﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace SharpSLAM.Core
{
    public class ProducerConsumerQueue<T>
    {
        Queue<T> queue = new Queue<T>();

        public void Produce(T o)
        {
            lock (queue)
            {
                queue.Enqueue(o);

                // We always need to pulse, even if the queue wasn't
                // empty before. Otherwise, if we add several items
                // in quick succession, we may only pulse once, waking
                // a single thread up, even if there are multiple threads
                // waiting for items.      

                if ((queue.Count > 200))
                {
                    Monitor.Exit(queue);
                    while (queue.Count > 200)
                    {
                        System.Threading.Thread.Sleep(500);
                    }
                    Monitor.Enter(queue);
                }
                Monitor.Pulse(queue);
            }
        }

        public T Consume()
        {
            lock (queue)
            {
                // If the queue is empty, wait for an item to be added
                // Note that this is a while loop, as we may be pulsed
                // but not wake up before another thread has come in and
                // consumed the newly added object. In that case, we'll
                // have to wait for another pulse.
                while (queue.Count == 0)
                {
                    // This releases listLock, only reacquiring it
                    // after being woken up by a call to Pulse
                    Monitor.Wait(queue);
                }
                return queue.Dequeue();
            }
        }
    }
}
