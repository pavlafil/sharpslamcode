﻿using SharpSLAM.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace SharpSLAM.Core
{
    public partial class Core
    {
        public void Train(IList<ImageAndExposure<MinimalImageF, float>> images)
        {
            if (_pixelSelector is PixelSelectorNN psNN)
                psNN.Train(images);

            
        }
    }
}
