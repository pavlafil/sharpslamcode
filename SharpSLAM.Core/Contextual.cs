﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharpSLAM.Core
{
    public class Contextual
    {
        protected CoreContext Context { get; private set; }

        public Contextual(CoreContext context)
        {
            Context = context;
        }
    }
}
