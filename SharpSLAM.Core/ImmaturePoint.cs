﻿using System;
using SharpSLAM.Core.Unmanaged;
using System.Linq;
using SharpSLAM.Shared;
using System.Diagnostics;

namespace SharpSLAM.Core
{
    public enum ImmaturePointStatus
    {
        IPS_GOOD = 0,                   // traced well and good
        IPS_OOB,                    // OOB: end tracking & marginalize!
        IPS_OUTLIER,                // energy too high: if happens again: outlier!
        IPS_SKIPPED,                // traced well and good (but not actually traced).
        IPS_BADCONDITION,           // not traced because of bad condition.
        IPS_UNINITIALIZED           // not even traced once.
    };
    public class ImmaturePoint : Contextual
    {
        public FrameHessian Host { get; set; }

        public ImmaturePointStatus LastTraceStatus { get; set; }
        public float[] LastTraceUV { get; set; }
        public float LastTracePixelInterval { get; set; }

        public float[] Color { get; set; }
        public float[] Weights { get; set; }
        public Mat22f GradH { get; set; }
        public float U { get; set; }
        public float V { get; set; }
        public float Type { get; set; }
        public float EnergyTh { get; set; }
        public int IdepthGt { get; set; }
        public float Quality { get; set; }
        public float IdepthMin { get; set; }
        public float IdepthMax { get; set; }

        public int IdxInImmaturePoints { get; set; }

        public ImmaturePoint(CoreContext context, int u, int v, FrameHessian host, float type) : base(context)
        {
            U = u;
            V = v;
            Type = type;
            Host = host;

            LastTraceStatus = ImmaturePointStatus.IPS_UNINITIALIZED;

            GradH = new Mat22f();
            GradH.SetZero();

            Color = new float[Context.Config.Core.PatternNumber];
            Weights = new float[Context.Config.Core.PatternNumber];
            for (int idx = 0; idx < Context.Config.Core.PatternNumber; idx++)
            {
                int dx = Configuration.Core.StaticPatterns[Context.Config.Core.PatternIndex, idx, 0];
                int dy = Configuration.Core.StaticPatterns[Context.Config.Core.PatternIndex, idx, 1];

                float[] ptc = host.dI.GetInterpolatedElement33BiLin(U + dx, V + dy, Context.w[0]);

                Color[idx] = ptc[0];
                if (float.IsInfinity(Color[idx]) || float.IsNaN(Color[idx])) { EnergyTh = float.NaN; return; }


                //GradH += ptc.tail < 2 > () * ptc.tail < 2 > ().transpose();
                //TODO make sure that this is correct
                GradH[0, 0] += ptc[0] * ptc[0];
                GradH[0, 1] += ptc[0] * ptc[1];
                GradH[1, 0] += ptc[1] * ptc[0];
                GradH[1, 1] += ptc[1] * ptc[1];

                Weights[idx] = (float)Math.Sqrt(Context.Config.Core.OutlierTHSumComponent / (Context.Config.Core.OutlierTHSumComponent + ptc.Skip(1).SquaredNorm()));
            }

            EnergyTh = Context.Config.Core.PatternNumber * Context.Config.Core.OutlierTH;
            EnergyTh *= Context.Config.Core.OverallEnergyTHWeight * Context.Config.Core.OverallEnergyTHWeight;

            IdepthGt = 0;
            Quality = 10000;

            IdepthMin = 0;
            IdepthMax = float.NaN;
        }

        /*
    * returns
    * * OOB -> point is optimized and marginalized
    * * UPDATED -> point has been updated.
    * * SKIP -> point has not been updated.
    */
        public ImmaturePointStatus TraceOn(FrameHessian frame, Mat33f hostToFrameKRKi, float[] hostToFrameKt, float[] hostToFrameAffine, CalibHessian hCalib, bool debugPrint)
        {
            if (LastTraceStatus == ImmaturePointStatus.IPS_OOB) return LastTraceStatus;

            float maxPixSearch = (Context.w[0] + Context.h[0]) * Context.Config.Core.MaxPixSearch;

            //if (debugPrint)
            //    debugPrint = Context.Rand.Next() % 100 == 0;

            if (debugPrint)
                Log.LogDebug(string.Format("trace pt ({0} {1}) from frame {2} to {3}. Range {4} -> {5}. t {6} {7} {8}!",
                        U, V,
                        Host.Shell.Id, frame.Shell.Id,
                        IdepthMin, IdepthMax,
                        hostToFrameKt[0], hostToFrameKt[1], hostToFrameKt[2]));

            ////	const float stepsize = 1.0;				// stepsize for initial discrete search.
            ////	const int GNIterations = 3;				// max # GN iterations
            ////	const float GNThreshold = 0.1;				// GN stop after this stepsize.
            ////	const float extraSlackOnTH = 1.2;			// for energy-based outlier check, be slightly more relaxed by this factor.
            ////	const float slackInterval = 0.8;			// if pixel-interval is smaller than this, leave it be.
            ////	const float minImprovementFactor = 2;		// if pixel-interval is smaller than this, leave it be.
            //// ============== project min and max. return if one of them is OOB ===================
            float[] pr = hostToFrameKRKi * new float[3] { U, V, 1 };
            float[] ptpMin = pr.Plus(hostToFrameKt.Mul(IdepthMin));
            float uMin = ptpMin[0] / ptpMin[2];
            float vMin = ptpMin[1] / ptpMin[2];

            if (!(uMin > 4 && vMin > 4 && uMin < Context.w[0] - 5 && vMin < Context.h[0] - 5))
            {
                if (debugPrint)
                    Log.LogDebug(string.Format("OOB uMin {0} {1} - {2} {3} {4} (id {5}-{6})!",
                         U, V, uMin, vMin, ptpMin[2], IdepthMin, IdepthMax));
                LastTraceUV = new float[2] { -1, -1 };
                LastTracePixelInterval = 0;
                return LastTraceStatus = ImmaturePointStatus.IPS_OOB;
            }

            float dist;
            float uMax;
            float vMax;
            float[] ptpMax;
            if (!float.IsInfinity(IdepthMax) && !float.IsNaN(IdepthMax))
            {
                ptpMax = pr.Plus(hostToFrameKt.Mul(IdepthMax));
                uMax = ptpMax[0] / ptpMax[2];
                vMax = ptpMax[1] / ptpMax[2];


                if (!(uMax > 4 && vMax > 4 && uMax < Context.w[0] - 5 && vMax < Context.h[0] - 5))
                {
                    if (debugPrint) Log.LogDebug(string.Format("OOB uMax  {0} {1} - {0} {1}!", U, V, uMax, vMax));
                    LastTraceUV = new float[2] { -1, -1 };
                    LastTracePixelInterval = 0;
                    return LastTraceStatus = ImmaturePointStatus.IPS_OOB;
                }



                // ============== check their distance. everything below 2px is OK (-> skip). ===================
                dist = (uMin - uMax) * (uMin - uMax) + (vMin - vMax) * (vMin - vMax);
                dist = (float)Math.Sqrt(dist);
                if (dist < Context.Config.Core.TraceSlackInterval)
                {
                    if (debugPrint)
                        Log.LogDebug(string.Format("TOO CERTAIN ALREADY (dist {0})!", dist));

                    LastTraceUV = new float[2] { (uMax + uMin) * 0.5f, (vMax + vMin) * 0.5f };
                    LastTracePixelInterval = dist;
                    return LastTraceStatus = ImmaturePointStatus.IPS_SKIPPED;
                }
                Debug.Assert(dist > 0);
            }
            else
            {
                dist = maxPixSearch;

                // project to arbitrary depth to get direction.
                ptpMax = pr.Plus(hostToFrameKt).Mul(0.01f);
                uMax = ptpMax[0] / ptpMax[2];
                vMax = ptpMax[1] / ptpMax[2];


                // direction.
                float ddx = uMax - uMin;
                float ddy = vMax - vMin;
                float d = 1.0f / (float)Math.Sqrt(ddx * ddx + ddy * ddy);

                // set to [setting_maxPixSearch].
                uMax = uMin + dist * ddx * d;
                vMax = vMin + dist * ddy * d;

                // may still be out!
                if (!(uMax > 4 && vMax > 4 && uMax < Context.w[0] - 5 && vMax < Context.h[0] - 5))
                {
                    if (debugPrint) Log.LogDebug(string.Format("OOB uMax-coarse {0} {2} {2}!", uMax, vMax, ptpMax[2]));
                    LastTraceUV = new float[2] { -1, -1 };
                    LastTracePixelInterval = 0;
                    return LastTraceStatus = ImmaturePointStatus.IPS_OOB;
                }
                Debug.Assert(dist > 0);
            }


            // set OOB if scale change too big.
            if (!(IdepthMin < 0 || (ptpMin[2] > 0.75 && ptpMin[2] < 1.5)))
            {
                if (debugPrint) Log.LogDebug(string.Format("OOB SCALE {0} {1} {2}!", uMax, vMax, ptpMin[2]));
                LastTraceUV = new float[2] { -1, -1 };
                LastTracePixelInterval = 0;
                return LastTraceStatus = ImmaturePointStatus.IPS_OOB;
            }


            // ============== compute error-bounds on result in pixel. if the new interval is not at least 1/2 of the old, SKIP ===================
            float dx = Context.Config.Core.TraceStepsize * (uMax - uMin);
            float dy = Context.Config.Core.TraceStepsize * (vMax - vMin);

            var dxdy = new float[2] { dx, dy };
            var dymdx = new float[2] { dx, dy };
            float a = (dxdy * GradH).Mul(dxdy);
            float b = (dymdx * GradH).Mul(dymdx);
            float errorInPixel = 0.2f + 0.2f * (a + b) / a;

            if (errorInPixel * Context.Config.Core.TraceMinImprovementFactor > dist && !float.IsInfinity(IdepthMax) && !float.IsNaN(IdepthMax))
            {
                if (debugPrint)
                    Log.LogDebug(string.Format("NO SIGNIFICANT IMPROVMENT ({0})!", errorInPixel));
                LastTraceUV = new float[2] { (uMax + uMin) * 0.5f, (vMax + vMin) * 0.5f };
                LastTracePixelInterval = dist;
                return LastTraceStatus = ImmaturePointStatus.IPS_BADCONDITION;
            }

            if (errorInPixel > 10) errorInPixel = 10;



            // ============== do the discrete search ===================
            dx /= dist;
            dy /= dist;

            if (debugPrint)
                Log.LogDebug(string.Format("trace pt ({0} {1}) from frame {2} to {3}. Range {4} ({5} {6}) -> {7} ({8} {9})! ErrorInPixel {10}!",
                         U, V,
                         Host.Shell.Id, frame.Shell.Id,
                         IdepthMin, uMin, vMin,
                         IdepthMax, uMax, vMax,
                         errorInPixel
                         ));


            if (dist > maxPixSearch)
            {
                uMax = uMin + maxPixSearch * dx;
                vMax = vMin + maxPixSearch * dy;
                dist = maxPixSearch;
            }

            int numSteps = (int)(1.9999f + (dist / Context.Config.Core.TraceStepsize));
            Mat22f Rplane = new Mat22f();
            Rplane[0, 0] = hostToFrameKRKi[0, 0];
            Rplane[0, 1] = hostToFrameKRKi[0, 1];
            Rplane[1, 0] = hostToFrameKRKi[1, 0];
            Rplane[1, 1] = hostToFrameKRKi[1, 1];

            float randShift = uMin * 1000 - (float)Math.Floor(uMin * 1000);
            float ptx = uMin - randShift * dx;
            float pty = vMin - randShift * dy;


            float[][] rotatetPattern = new float[Context.Config.Core.MaxResPerPoint][];
            for (int idx = 0; idx < Context.Config.Core.PatternNumber; idx++)
                rotatetPattern[idx] = Rplane * new float[2] { Configuration.Core.StaticPatterns[Context.Config.Core.PatternIndex, idx, 0], Configuration.Core.StaticPatterns[Context.Config.Core.PatternIndex, idx, 1] };

            if (float.IsInfinity(dx) || float.IsInfinity(dy) || float.IsNaN(dx) || float.IsNaN(dy))
            {
                //printf("COUGHT INF / NAN dxdy (%f %f)!\n", dx, dx);

                LastTracePixelInterval = 0;
                LastTraceUV = new float[2] { -1, -1 };
                return LastTraceStatus = ImmaturePointStatus.IPS_OOB;
            }



            if (numSteps >= 100) numSteps = 99;
            float[] errors = new float[numSteps];
            float bestU = 0, bestV = 0, bestEnergy = 1e10f;
            int bestIdx = -1;

            for (int i = 0; i < numSteps; i++)
            {
                float energy = 0;
                for (int idx = 0; idx < Context.Config.Core.PatternNumber; idx++)
                {
                    float hitColor = frame.dI.GetInterpolatedElement31((ptx + rotatetPattern[idx][0]),
                                                (pty + rotatetPattern[idx][1]),
                                                Context.w[0]);

                    if (float.IsInfinity(hitColor) || float.IsNaN(hitColor)) { energy += 1e5f; continue; }
                    float residual = hitColor - (float)(hostToFrameAffine[0] * Color[idx] + hostToFrameAffine[1]);
                    float hw = Math.Abs(residual) < Context.Config.Core.HuberTH ? 1 : Context.Config.Core.HuberTH / Math.Abs(residual);
                    energy += hw * residual * residual * (2 - hw);
                }

                if (debugPrint)
                    Log.LogDebug(string.Format("Step {0} {1} (id {2}): energy = {3}!",
                            ptx, pty, 0.0f, energy));


                errors[i] = energy;
                if (energy < bestEnergy)
                {
                    bestU = ptx; bestV = pty; bestEnergy = energy; bestIdx = i;
                }

                ptx += dx;
                pty += dy;
            }


            // find best score outside a +-2px radius.
            float secondBest = 1e10f;
            for (int i = 0; i < numSteps; i++)
            {
                if ((i < bestIdx - Context.Config.Core.MinTraceTestRadius || i > bestIdx + Context.Config.Core.MinTraceTestRadius) && errors[i] < secondBest)
                    secondBest = errors[i];
            }
            float newQuality = secondBest / bestEnergy;
            if (newQuality < Quality || numSteps > 10) Quality = newQuality;


            // ============== do GN optimization ===================
            float uBak = bestU, vBak = bestV, gnstepsize = 1, stepBack = 0;
            if (Context.Config.Core.TraceGNIterations > 0) bestEnergy = 1e5f;
            int gnStepsGood = 0, gnStepsBad = 0;
            for (int it = 0; it < Context.Config.Core.TraceGNIterations; it++)
            {
                b = 0f;
                float H = 1, energy = 0f;
                for (int idx = 0; idx < Context.Config.Core.PatternNumber; idx++)
                {
                    float[] hitColor = frame.dI.GetInterpolatedElement33(
                            (float)(bestU + rotatetPattern[idx][0]),
                            (float)(bestV + rotatetPattern[idx][1]), Context.w[0]);

                    if (float.IsInfinity(hitColor[0]) || float.IsNaN(hitColor[0])) { energy += 1e5f; continue; }
                    float residual = hitColor[0] - (hostToFrameAffine[0] * Color[idx] + hostToFrameAffine[1]);
                    float dResdDist = dx * hitColor[1] + dy * hitColor[2];
                    float hw = Math.Abs(residual) < Context.Config.Core.HuberTH ? 1 : Context.Config.Core.HuberTH / Math.Abs(residual);

                    H += hw * dResdDist * dResdDist;
                    b += hw * residual * dResdDist;
                    energy += Weights[idx] * Weights[idx] * hw * residual * residual * (2 - hw);
                }

                if (energy > bestEnergy)
                {
                    gnStepsBad++;

                    // do a smaller step from old point.
                    stepBack *= 0.5f;
                    bestU = uBak + stepBack * dx;
                    bestV = vBak + stepBack * dy;
                    if (debugPrint)
                        Log.LogDebug(string.Format("GN BACK {0}: E {1}, H {2}, b {3}. id-step {4}. UV {5} {6} -> {7} {8}.",
                                it, energy, H, b, stepBack,
                                uBak, vBak, bestU, bestV));
                }
                else
                {
                    gnStepsGood++;

                    float step = -gnstepsize * b / H;
                    if (step < -0.5) step = -0.5f;
                    else if (step > 0.5) step = 0.5f;

                    if (float.IsInfinity(step) || float.IsNaN(step)) step = 0;

                    uBak = bestU;
                    vBak = bestV;
                    stepBack = step;

                    bestU += step * dx;
                    bestV += step * dy;
                    bestEnergy = energy;

                    if (debugPrint)
                        Log.LogDebug(string.Format("GN step {0}: E {1}, H {2}, b {3}. id-step {4}. UV {5} {6} -> {7} {8}.",
                                it, energy, H, b, step,
                                uBak, vBak, bestU, bestV));
                }

                if (Math.Abs(stepBack) < Context.Config.Core.TraceGNThreshold) break;
            }


            //// ============== detect energy-based outlier. ===================
            ////	float absGrad0 = getInterpolatedElement(frame->absSquaredGrad[0],bestU, bestV, wG[0]);
            ////	float absGrad1 = getInterpolatedElement(frame->absSquaredGrad[1],bestU*0.5-0.25, bestV*0.5-0.25, wG[1]);
            ////	float absGrad2 = getInterpolatedElement(frame->absSquaredGrad[2],bestU*0.25-0.375, bestV*0.25-0.375, wG[2]);
            if (!(bestEnergy < EnergyTh * Context.Config.Core.TraceExtraSlackOnTH))
            ////			|| (absGrad0*areaGradientSlackFactor < host->frameGradTH
            ////		     && absGrad1*areaGradientSlackFactor < host->frameGradTH*0.75f
            ////			 && absGrad2*areaGradientSlackFactor < host->frameGradTH*0.50f))
            {
                if (debugPrint)
                    Log.LogDebug("OUTLIER!");

                LastTracePixelInterval = 0;
                LastTraceUV = new float[] { -1, -1 };
                if (LastTraceStatus == ImmaturePointStatus.IPS_OUTLIER)
                    return LastTraceStatus = ImmaturePointStatus.IPS_OOB;
                else
                    return LastTraceStatus = ImmaturePointStatus.IPS_OUTLIER;
            }


            // ============== set new interval ===================
            if (dx * dx > dy * dy)
            {
                IdepthMin = (pr[2] * (bestU - errorInPixel * dx) - pr[0]) / (hostToFrameKt[0] - hostToFrameKt[2] * (bestU - errorInPixel * dx));
                IdepthMax = (pr[2] * (bestU + errorInPixel * dx) - pr[0]) / (hostToFrameKt[0] - hostToFrameKt[2] * (bestU + errorInPixel * dx));
            }
            else
            {
                IdepthMin = (pr[2] * (bestV - errorInPixel * dy) - pr[1]) / (hostToFrameKt[1] - hostToFrameKt[2] * (bestV - errorInPixel * dy));
                IdepthMax = (pr[2] * (bestV + errorInPixel * dy) - pr[1]) / (hostToFrameKt[1] - hostToFrameKt[2] * (bestV + errorInPixel * dy));
            }
            if (IdepthMin > IdepthMax)
            {
                ////std::swap<float>(idepth_min, idepth_max);
                var tmp = IdepthMax;
                IdepthMax = IdepthMin;
                IdepthMin = tmp;
            }

            if (float.IsInfinity(IdepthMin) || float.IsInfinity(IdepthMax) || float.IsNaN(IdepthMin)|| float.IsNaN(IdepthMax) || (IdepthMax < 0))
            {
                if (debugPrint)
                    Log.LogDebug(string.Format("COUGHT INF / NAN minmax depth ({0} {1})!", IdepthMin, IdepthMax));

                LastTracePixelInterval = 0;
                LastTraceUV = new float[2] { -1, -1 };
                return LastTraceStatus = ImmaturePointStatus.IPS_OUTLIER;
            }

            LastTracePixelInterval = 2 * errorInPixel;
            LastTraceUV = new float[2] { bestU, bestV };
            return LastTraceStatus = ImmaturePointStatus.IPS_GOOD;
        }

        public double LinearizeResidual(CalibHessian hCalib, float outlierTHSlack, ImmaturePointTemporaryResidual tmpRes, ref float hdd, ref float bd, float idepth)
        {
            if (tmpRes.StateState == ResState.OOB)
            { tmpRes.StateNewState = ResState.OOB; return tmpRes.StateEnergy; }

            FramePrecalc precalc = Host.TargetPrecalc[tmpRes.Target.Idx];

            // check OOB due to scale angle change.
            float energyLeft = 0;
            var dIl = tmpRes.Target.dI;
            Mat33f PRE_RTll = precalc.PRE_RTll;
            float[] PRE_tTll = precalc.PRE_tTll;
            //const float * const Il = tmpRes->target->I;

            float[] affLL = precalc.PRE_aff_mode;

            for (int idx = 0; idx < Context.Config.Core.PatternNumber; idx++)
            {
                int dx = Configuration.Core.StaticPatterns[Context.Config.Core.PatternNumber, idx, 0];
                int dy = Configuration.Core.StaticPatterns[Context.Config.Core.PatternNumber, idx, 1];

                float drescale, u, v, newIdepth;
                float Ku, Kv;
                float[] KliP;

                if (!Extensions.ProjectPoint(Context, U, V, idepth, dx, dy, hCalib,
                        PRE_RTll, PRE_tTll, out drescale, out u, out v, out Ku, out Kv, out KliP, out newIdepth))
                { tmpRes.StateNewState = ResState.OOB; return tmpRes.StateEnergy; }


                float[] hitColor = dIl.GetInterpolatedElement33(Ku, Kv, Context.w[0]);

                if (float.IsInfinity(hitColor[0]) || float.IsNaN(hitColor[0])) { tmpRes.StateNewState = ResState.OOB; return tmpRes.StateEnergy; }
                float residual = hitColor[0] - (affLL[0] * Color[idx] + affLL[1]);

                float hw = Math.Abs(residual) < Context.Config.Core.HuberTH ? 1 : Context.Config.Core.HuberTH / Math.Abs(residual);
                energyLeft += Weights[idx] * Weights[idx] * hw * residual * residual * (2 - hw);

                // depth derivatives.
                float dxInterp = hitColor[1] * hCalib.fxl;
                float dyInterp = hitColor[2] * hCalib.fyl;
                float d_idepth = (dxInterp * drescale * (PRE_tTll[0] - PRE_tTll[2] * u)
                                 + dyInterp * drescale * (PRE_tTll[1] - PRE_tTll[2] * v)) * Context.Config.Core.ScaleIdepth; //DeriveIdepth(PRE_tTll, u, v, dx, dy, dxInterp, dyInterp, drescale);

                hw *= Weights[idx] * Weights[idx];

                hdd += (hw * d_idepth) * d_idepth;
                bd += (hw * residual) * d_idepth;
            }


            if (energyLeft > EnergyTh * outlierTHSlack)
            {
                energyLeft = EnergyTh * outlierTHSlack;
                tmpRes.StateNewState = ResState.OUTLIER;
            }
            else
            {
                tmpRes.StateNewState = ResState.IN;
            }

            tmpRes.StateNewEnergy = energyLeft;
            return energyLeft;
        }
    }
}