﻿using SharpSLAM.Core.Unmanaged;
using SharpSLAM.Shared;
using System;
using System.Diagnostics;
using System.Linq;

namespace SharpSLAM.Core
{
    public enum ResState { IN = 0, OOB, OUTLIER };

    public class PointFrameResidual : Contextual
    {
        public PointHessian Point { get; }
        public FrameHessian Host { get; }
        public FrameHessian Target { get; }

        public EFResidual EfResidual { get; set; }
        public RawResidualJacobian J { get; set; }
        public bool IsNew { get; set; }
        public double StateNewEnergy { get; set; }
        public double StateEnergy { get; set; }
        public float StateNewEnergyWithOutlier { get; set; }

        public ResState StateNewState { get; set; }
        public ResState StateState { get; set; }

        public float[] CenterProjectedTo { get; set; }

        public float[][] ProjectedTo { get; set; }

        public PointFrameResidual(CoreContext context, PointHessian point, FrameHessian host, FrameHessian target) : base(context)
        {
            EfResidual = null;
            //instanceCounter++;
            ResetOOB();
            J = new RawResidualJacobian(Context);
            //alig//assert(((long)J) % 16 == 0);

            IsNew = true;

            Point = point;
            Host = host;
            Target = target;

            ProjectedTo = new float[Context.Config.Core.MaxResPerPoint][];
            for (int i = 0; i < ProjectedTo.Length; i++)
            {
                ProjectedTo[i] = new float[2];
            }
        }



        public void ResetOOB()
        {
            StateNewEnergy = StateEnergy = 0;
            StateNewState = ResState.OUTLIER;
            StateState = ResState.IN;
        }

        public double Linearize(CalibHessian hCalib)//, Shared.MinimalImageF3[] imgs=null)
        {
            StateNewEnergyWithOutlier = -1;

            if (StateState == ResState.OOB)
            {
                Log.CountAdd("OOB State OOB");
                StateNewState = ResState.OOB; return StateEnergy;
            }

            FramePrecalc precalc = Host.TargetPrecalc[Target.Idx];
            float energyLeft = 0;
            var dIl = Target.dI;
            ////const float* const Il = target->I;
            Mat33f PRE_KRKiTll = precalc.PRE_KRKiTll;
            var PRE_KtTll = precalc.PRE_KtTll;
            var PRE_RTll_0 = precalc.PRE_RTll_0;
            var PRE_tTll_0 = precalc.PRE_tTll_0;
            var color = Point.Color;
            var weights = Point.Weights;

            var affLL = precalc.PRE_aff_mode;
            float b0 = (float)precalc.PRE_b0_mode;


            float[] d_xi_x = new float[6], d_xi_y = new float[6]; //6
            float[] d_C_x = new float[4], d_C_y = new float[4];   //4
            float d_d_x, d_d_y;
            {
                float drescale, u, v, new_idepth;
                float Ku, Kv;
                float[] KliP; //3

                if (!Extensions.ProjectPoint(Context, Point.U, Point.V, Point.IdepthZeroScaled, 0, 0, hCalib,
                        PRE_RTll_0, PRE_tTll_0, out drescale, out u, out v, out Ku, out Kv, out KliP, out new_idepth))
                { StateNewState = ResState.OOB;
                    Log.CountAdd("OOB Project point fail");

                    return StateEnergy; }

                CenterProjectedTo = new float[3] { Ku, Kv, new_idepth };


                // diff d_idepth
                d_d_x = drescale * (PRE_tTll_0[0] - PRE_tTll_0[2] * u) * Context.Config.Core.ScaleIdepth * hCalib.fxl;
                d_d_y = drescale * (PRE_tTll_0[1] - PRE_tTll_0[2] * v) * Context.Config.Core.ScaleIdepth * hCalib.fyl;


                // diff calib
                d_C_x[2] = drescale * (PRE_RTll_0[2, 0] * u - PRE_RTll_0[0, 0]);
                d_C_x[3] = hCalib.fxl * drescale * (PRE_RTll_0[2, 1] * u - PRE_RTll_0[0, 1]) * hCalib.fyli;
                d_C_x[0] = KliP[0] * d_C_x[2];
                d_C_x[1] = KliP[1] * d_C_x[3];

                d_C_y[2] = hCalib.fyl * drescale * (PRE_RTll_0[2, 0] * v - PRE_RTll_0[1, 0]) * hCalib.fxli;
                d_C_y[3] = drescale * (PRE_RTll_0[2, 1] * v - PRE_RTll_0[1, 1]);
                d_C_y[0] = KliP[0] * d_C_y[2];
                d_C_y[1] = KliP[1] * d_C_y[3];

                d_C_x[0] = (d_C_x[0] + u) * Context.Config.Core.ScaleF;
                d_C_x[1] *= Context.Config.Core.ScaleF;
                d_C_x[2] = (d_C_x[2] + 1) * Context.Config.Core.ScaleC;
                d_C_x[3] *= Context.Config.Core.ScaleC;

                d_C_y[0] *= Context.Config.Core.ScaleF;
                d_C_y[1] = (d_C_y[1] + v) * Context.Config.Core.ScaleF;
                d_C_y[2] *= Context.Config.Core.ScaleC;
                d_C_y[3] = (d_C_y[3] + 1) * Context.Config.Core.ScaleC;


                d_xi_x[0] = new_idepth * hCalib.fxl;
                d_xi_x[1] = 0;
                d_xi_x[2] = -new_idepth * u * hCalib.fxl;
                d_xi_x[3] = -u * v * hCalib.fxl;
                d_xi_x[4] = (1 + u * u) * hCalib.fxl;
                d_xi_x[5] = -v * hCalib.fxl;

                d_xi_y[0] = 0;
                d_xi_y[1] = new_idepth * hCalib.fyl;
                d_xi_y[2] = -new_idepth * v * hCalib.fyl;
                d_xi_y[3] = -(1 + v * v) * hCalib.fyl;
                d_xi_y[4] = u * v * hCalib.fyl;
                d_xi_y[5] = u * hCalib.fyl;
            }


            {
                J.Jpdxi[0] = d_xi_x;
                J.Jpdxi[1] = d_xi_y;

                J.Jpdc[0] = d_C_x;
                J.Jpdc[1] = d_C_y;

                J.Jpdd[0] = d_d_x;
                J.Jpdd[1] = d_d_y;

            }




            float JIdxJIdx_00 = 0, JIdxJIdx_11 = 0, JIdxJIdx_10 = 0;
            float JabJIdx_00 = 0, JabJIdx_01 = 0, JabJIdx_10 = 0, JabJIdx_11 = 0;
            float JabJab_00 = 0, JabJab_01 = 0, JabJab_11 = 0;

            float wJI2_sum = 0;

            for (int idx = 0; idx < Context.Config.Core.PatternNumber; idx++)
            {
                float Ku, Kv;

                var dx = Configuration.Core.StaticPatterns[Context.Config.Core.PatternIndex, idx, 0];
                var dy = Configuration.Core.StaticPatterns[Context.Config.Core.PatternIndex, idx, 1];

                if (!Extensions.ProjectPoint(Context, Point.U + dx, Point.V + dy, Point.IdepthScaled, PRE_KRKiTll, PRE_KtTll, out Ku, out Kv))
                { StateNewState = ResState.OOB;
                    Log.CountAdd("OOB Project point fail in pattern");
                    return StateEnergy; }

                ProjectedTo[idx][0] = Ku;
                ProjectedTo[idx][1] = Kv;


                var hitColor = dIl.GetInterpolatedElement33(Ku, Kv, Context.w[0]);
                float residual = hitColor[0] - (float)(affLL[0] * color[idx] + affLL[1]);

                float drdA = (color[idx] - b0);
                if (float.IsInfinity(hitColor[0]) || float.IsNaN(hitColor[0]))
                { StateNewState = ResState.OOB;
                    Log.CountAdd("OOB Hit color out of range");
                    return StateEnergy; }


                float w = (float)Math.Sqrt(Context.Config.Core.OutlierTHSumComponent / (Context.Config.Core.OutlierTHSumComponent + new float[2] { hitColor[1], hitColor[2] }.SquaredNorm()));
                w = 0.5f * (w + weights[idx]);



                float hw = Math.Abs(residual) < Context.Config.Core.HuberTH ? 1 : Context.Config.Core.HuberTH / Math.Abs(residual);
                energyLeft += w * w * hw * residual * residual * (2 - hw);

                {
                    if (hw < 1) hw = (float)Math.Sqrt(hw);
                    hw = hw * w;

                    hitColor[1] *= hw;
                    hitColor[2] *= hw;

                    J.ResF[idx] = residual * hw;

                    J.JIdx[0][idx] = hitColor[1];
                    J.JIdx[1][idx] = hitColor[2];
                    J.JabF[0][idx] = drdA * hw;
                    J.JabF[1][idx] = hw;

                    JIdxJIdx_00 += hitColor[1] * hitColor[1];
                    JIdxJIdx_11 += hitColor[2] * hitColor[2];
                    JIdxJIdx_10 += hitColor[1] * hitColor[2];

                    JabJIdx_00 += drdA * hw * hitColor[1];
                    JabJIdx_01 += drdA * hw * hitColor[2];
                    JabJIdx_10 += hw * hitColor[1];
                    JabJIdx_11 += hw * hitColor[2];

                    JabJab_00 += drdA * drdA * hw * hw;
                    JabJab_01 += drdA * hw * hw;
                    JabJab_11 += hw * hw;


                    wJI2_sum += hw * hw * (hitColor[1] * hitColor[1] + hitColor[2] * hitColor[2]);

                    if (Context.Config.Core.AffineOptModeA < 0) J.JabF[0][idx] = 0;
                    if (Context.Config.Core.AffineOptModeB < 0) J.JabF[1][idx] = 0;

                }
            }





            //if (imgs != null)
            //{
            //    for (int idx = 0; idx < Context.Config.Core.PatternNumber; idx++)
            //    {
            //        float Ku, Kv;

            //        var dx = Configuration.Core.StaticPatterns[Context.Config.Core.PatternIndex, idx, 0];
            //        var dy = Configuration.Core.StaticPatterns[Context.Config.Core.PatternIndex, idx, 1];

            //        Extensions.ProjectPoint(Context, Point.U + dx, Point.V + dy, Point.IdepthScaled, PRE_KRKiTll, PRE_KtTll, out Ku, out Kv);

            //        //Log.LogDebug($"{Point.U + dx} {Point.V + dy} => {Ku} {Kv}");

            //        var hitColor = dIl.GetInterpolatedElement33(Ku, Kv, Context.w[0]);
            //        float residual = hitColor[0] - (float)(affLL[0] * color[idx] + affLL[1]);

            //        imgs[2].SetPixel((int)(Point.U + dx + 0.5f), (int)(Point.V + dy + 0.5f), new float[] { 255, 0, 0 });
            //        imgs[2].SetPixel((int)(Ku + 0.5f), (int)(Kv + 0.5f), new float[] { 0, 0, 255 });


            //        if ((energyLeft > Host.FrameEnergyTh && energyLeft > Target.FrameEnergyTh) || wJI2_sum < 2)
            //        {
            //            imgs[0].At((int)(Ku + 0.5f), (int)(Kv + 0.5f), 0) = 0;
            //            imgs[0].At((int)(Ku + 0.5f), (int)(Kv + 0.5f), 1) = 0;
            //            imgs[0].At((int)(Ku + 0.5f), (int)(Kv + 0.5f), 2) = 255;

            //            //imgs[0].At((int)(Point.U + dx + 0.5f), (int)(Point.V + dy + 0.5f), 0) = 0;
            //            //imgs[0].At((int)(Point.U + dx + 0.5f), (int)(Point.V + dy + 0.5f), 1) = 255;
            //            //imgs[0].At((int)(Point.U + dx + 0.5f), (int)(Point.V + dy + 0.5f), 2) = 0;

            //            imgs[1].At((int)(Point.U + dx + 0.5f), (int)(Point.V + dy + 0.5f), 0) = 0;
            //            imgs[1].At((int)(Point.U + dx + 0.5f), (int)(Point.V + dy + 0.5f), 1) = 0;
            //            imgs[1].At((int)(Point.U + dx + 0.5f), (int)(Point.V + dy + 0.5f), 2) = 255;

            //        }
            //        else
            //        {
            //            imgs[0].At((int)(Ku + 0.5f), (int)(Kv + 0.5f), 0) = 255;
            //            imgs[0].At((int)(Ku + 0.5f), (int)(Kv + 0.5f), 1) = 0;
            //            imgs[0].At((int)(Ku + 0.5f), (int)(Kv + 0.5f), 2) = 0;

            //            //imgs[0].At((int)(Point.U + dx + 0.5f), (int)(Point.V + dy + 0.5f), 0) = 0;
            //            //imgs[0].At((int)(Point.U + dx + 0.5f), (int)(Point.V + dy + 0.5f), 1) = 255;
            //            //imgs[0].At((int)(Point.U + dx + 0.5f), (int)(Point.V + dy + 0.5f), 2) = 0;

            //            imgs[1].At((int)(Point.U + dx + 0.5f), (int)(Point.V + dy + 0.5f), 0) = 255;
            //            imgs[1].At((int)(Point.U + dx + 0.5f), (int)(Point.V + dy + 0.5f), 1) = 0;
            //            imgs[1].At((int)(Point.U + dx + 0.5f), (int)(Point.V + dy + 0.5f), 2) = 0;
            //        }

            //    }

            //    Context.Output.UpdateImage("02 Residuals coresspodence.Target", imgs[0]);
            //    Context.Output.UpdateImage("02 Residuals coresspodence.Host", imgs[1]);
            //    Context.Output.UpdateImage("02 Residuals coresspodence.Diff", imgs[2]);
            //    //Console.WriteLine("\n====PAUSE!");
            //    //Console.ReadKey();
            //}




            J.JIdx2[0, 0] = JIdxJIdx_00;
            J.JIdx2[0, 1] = JIdxJIdx_10;
            J.JIdx2[1, 0] = JIdxJIdx_10;
            J.JIdx2[1, 1] = JIdxJIdx_11;
            J.JabJIdx[0, 0] = JabJIdx_00;
            J.JabJIdx[0, 1] = JabJIdx_01;
            J.JabJIdx[1, 0] = JabJIdx_10;
            J.JabJIdx[1, 1] = JabJIdx_11;
            J.Jab2[0, 0] = JabJab_00;
            J.Jab2[0, 1] = JabJab_01;
            J.Jab2[1, 0] = JabJab_01;
            J.Jab2[1, 1] = JabJab_11;

            StateNewEnergyWithOutlier = energyLeft;

            if ((energyLeft > Host.FrameEnergyTh && energyLeft > Target.FrameEnergyTh) || wJI2_sum < 2)
            {
                if (energyLeft > Host.FrameEnergyTh && energyLeft > Target.FrameEnergyTh)
                    Log.CountAdd("Energy too hight");
                if (wJI2_sum < 2)
                    Log.CountAdd("wJI2_sum too low");

                energyLeft = Math.Max(Host.FrameEnergyTh, Target.FrameEnergyTh);
                StateNewState = ResState.OUTLIER;

            }
            else
            {
                StateNewState = ResState.IN;
                Log.CountAdd("OK - in");
            }
            StateNewEnergy = energyLeft;
            return energyLeft;
        }

        internal void ApplyRes(bool copyJacobians)
        {
            if (copyJacobians)
            {
                if (StateState == ResState.OOB)
                {
                    Debug.Assert(!EfResidual.IsActiveAndIsGoodNEW);
                    return; // can never go back from OOB
                }
                if (StateNewState == ResState.IN)// && )
                {
                    EfResidual.IsActiveAndIsGoodNEW = true;
                    EfResidual.TakeDataF();
                }
                else
                {
                    EfResidual.IsActiveAndIsGoodNEW = false;
                }
            }

            StateState = StateNewState;
            StateEnergy = StateNewEnergy;
        }
    }
}