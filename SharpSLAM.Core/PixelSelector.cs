﻿using SharpSLAM.Shared;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace SharpSLAM.Core
{
    public class PixelSelector : Contextual
    {
        public int CurrentPotential { get; set; } = 3;

        private FrameHessian _gradHistFrame;
        private int _thsStep;
        private float[] _ths;
        private float[] _thsSmoothed;
        private byte[] _randomPattern;

        public PixelSelector(CoreContext context) : base(context)
        {
            var size = (Context.w[0] / 32) * (Context.h[0] / 32) + 100;
            _ths = new float[size];
            _thsSmoothed = new float[size];

            _randomPattern = new byte[Context.w[0] * Context.h[0]];
            var r = new Random(3141592);    // want to be deterministic.
            r.NextBytes(_randomPattern);
            //Context.Output.UpdateImage("Random pattern", new Shared.MinimalImageB(Context.wG[0], Context.hG[0], _randomPattern));
        }

        public int MakeMaps(FrameHessian frameHessian, float[] mapOut, float density)
        {
            var start = DateTime.Now;
            var map = MakeMaps(frameHessian, mapOut, density, 1, false, 1);
            var end = DateTime.Now;
            Context.Output.UpdateData("MakeMapsTiming", "OnlyMakeMaps", frameHessian.Shell.Id, (end - start).TotalMilliseconds, "Frame", "Time [ms]");
            return map;
        }
        public int MakeMaps(FrameHessian frameHessian, float[] mapOut, float density, int recursionsLeft, bool plot, float thFactor)
        {

            float numHave = 0;
            float numWant = density;
            float quotia;
            int idealPotential = CurrentPotential;


            //if(setting_pixelSelectionUseFast>0 && allowFast)
            //if(true)
            //{
            //memset(map_out, 0, sizeof(float)*wG[0]*hG[0]);
            //var pts = new List<float[]>();
            //std::vector<cv::KeyPoint> pts;
            //cv::Mat img8u(hG[0],wG[0],CV_8U);
            //for(int i=0;i<wG[0]*hG[0];i++)
            //{
            //float v = fh->dI[i][0]*0.8;
            //img8u.at<uchar>(i) = (!std::isfinite(v) || v>255) ? 255 : v;
            //}
            //cv::FAST(img8u, pts, setting_pixelSelectionUseFast, true);
            //for(unsigned int i=0;i<pts.size();i++)
            //{
            //int x = pts[i].pt.x+0.5;
            //int y = pts[i].pt.y+0.5;
            //map_out[x+y*wG[0]]=1;
            //numHave++;
            //}

            //printf("FAST selection: got %f / %f!\n", numHave, numWant);
            //quotia = numWant / numHave;
            //}
            //else
            {

                // the number of selected pixels behaves approximately as
                // K / (pot+1)^2, where K is a scene-dependent constant.
                // we will allow sub-selecting pixels by up to a quotia of 0.25, otherwise we will re-select.

                if (frameHessian != _gradHistFrame)
                    MakeHist(frameHessian,plot);

                // select!
                var n = Select(frameHessian, mapOut, CurrentPotential, thFactor);

                // sub-select!
                numHave = n[0] + n[1] + n[2];
                quotia = numWant / numHave;

                // by default we want to over-sample by 40% just to be sure.
                float K = numHave * (CurrentPotential + 1) * (CurrentPotential + 1);
                idealPotential = (int)Math.Sqrt(K / numWant) - 1;    // round down.
                if (idealPotential < 1) idealPotential = 1;

                if (recursionsLeft > 0 && quotia > 1.25 && CurrentPotential > 1)
                {
                    //re-sample to get more points!
                    // potential needs to be smaller
                    if (idealPotential >= CurrentPotential)
                        idealPotential = CurrentPotential - 1;

                    //		printf("PixelSelector: have %.2f%%, need %.2f%%. RESAMPLE with pot %d -> %d.\n",
                    //				100*numHave/(float)(wG[0]*hG[0]),
                    //				100*numWant/(float)(wG[0]*hG[0]),
                    //				currentPotential,
                    //				idealPotential);
                    CurrentPotential = idealPotential;
                    return MakeMaps(frameHessian, mapOut, density, recursionsLeft - 1, plot, thFactor);
                }
                else if (recursionsLeft > 0 && quotia < 0.25)
                {
                    // re-sample to get less points!

                    if (idealPotential <= CurrentPotential)
                        idealPotential = CurrentPotential + 1;

                    //		printf("PixelSelector: have %.2f%%, need %.2f%%. RESAMPLE with pot %d -> %d.\n",
                    //				100*numHave/(float)(wG[0]*hG[0]),
                    //				100*numWant/(float)(wG[0]*hG[0]),
                    //				currentPotential,
                    //				idealPotential);
                    CurrentPotential = idealPotential;
                    return MakeMaps(frameHessian, mapOut, density, recursionsLeft - 1, plot, thFactor);
                }
            }

            int numHaveSub = (int)numHave;
            if (quotia < 0.95)
            {
                int wh = Context.w[0] * Context.h[0];
                int rn = 0;
                byte charTH = (byte)(255 * quotia);
                for (int i = 0; i < wh; i++)
                {
                    if (mapOut[i] != 0)
                    {
                        if (_randomPattern[rn] > charTH)
                        {
                            mapOut[i] = 0;
                            numHaveSub--;
                        }
                        rn++;
                    }
                }
            }

            //	printf("PixelSelector: have %.2f%%, need %.2f%%. KEEPCURR with pot %d -> %d. Subsampled to %.2f%%\n",
            //			100*numHave/(float)(wG[0]*hG[0]),
            //			100*numWant/(float)(wG[0]*hG[0]),
            //			currentPotential,
            //			idealPotential,
            //			100*numHaveSub/(float)(wG[0]*hG[0]));
            CurrentPotential = idealPotential;


            if (plot)
            {
                int w = Context.w[0];
                int h = Context.h[0];

                var img = new MinimalImageB3(frameHessian.Shell.IncomingId, w, h);

                for (int i = 0; i < w * h; i++)
                {
                    byte c = (byte)(frameHessian.dI[i, 0] * 0.7f);
                    if (c > 255) c = 255;
                    img.At(i, 0) = c;
                    img.At(i, 1) = c;
                    img.At(i, 2) = c;
                }

                for (int y = 0; y < h; y++)
                    for (int x = 0; x < w; x++)
                    {
                        int i = x + y * w;
                        if (mapOut[i] == 1)
                            img.SetPixelCirc(x, y, 255, 1); //G
                        else if (mapOut[i] == 2)
                            img.SetPixelCirc(x, y, 255, 0); //B
                        else if (mapOut[i] == 4)
                            img.SetPixelCirc(x, y, 255, 2); //R
                    }
                Context.Output?.UpdateImage("01 CoarseInit.04 Grad plotting points", img);
            }

            return numHaveSub;
        }

        private int[] Select(FrameHessian frameHessian, float[] mapOut, int pot, float thFactor)
        {
            var map0 = frameHessian.dI;

            float[] mapmax0 = frameHessian.AbsSquaredGrad[0];
            float[] mapmax1 = frameHessian.AbsSquaredGrad[1];
            float[] mapmax2 = frameHessian.AbsSquaredGrad[2];


            int w = Context.w[0];
            int w1 = Context.w[1];
            int w2 = Context.w[2];
            int h = Context.h[0];


            var directions = new float[16, 2]{
             {0,          1.0000f },
             {0.3827f,    0.9239f},
             {0.1951f,    0.9808f},
             {0.9239f,    0.3827f},
             {0.7071f,    0.7071f},
             {0.3827f,   -0.9239f},
             {0.8315f,    0.5556f},
             {0.8315f,   -0.5556f},
             {0.5556f,   -0.8315f},
             {0.9808f,    0.1951f},
             {0.9239f,   -0.3827f},
             {0.7071f,   -0.7071f},
             {0.5556f,    0.8315f},
             {0.9808f,   -0.1951f},
             {1.0000f,    0.0000f},
             {0.1951f,   -0.9808f}};

            Array.Clear(mapOut, 0, mapOut.Length);

            float dw1 = Context.Config.Core.GradDownWeightPerLevel;
            float dw2 = dw1 * dw1;

            int n3 = 0, n2 = 0, n4 = 0;
            for (int y4 = 0; y4 < h; y4 += (4 * pot)) for (int x4 = 0; x4 < w; x4 += (4 * pot))
                {
                    int my3 = Math.Min((4 * pot), h - y4);
                    int mx3 = Math.Min((4 * pot), w - x4);
                    int bestIdx4 = -1; float bestVal4 = 0;
                    var dir40 = directions[(int)(_randomPattern[n2] & 0xF), 0];
                    var dir41 = directions[(int)(_randomPattern[n2] & 0xF), 1];
                    for (int y3 = 0; y3 < my3; y3 += (2 * pot)) for (int x3 = 0; x3 < mx3; x3 += (2 * pot))
                        {
                            int x34 = x3 + x4;
                            int y34 = y3 + y4;
                            int my2 = Math.Min((2 * pot), h - y34);
                            int mx2 = Math.Min((2 * pot), w - x34);
                            int bestIdx3 = -1; float bestVal3 = 0;
                            var dir30 = directions[(int)(_randomPattern[n2] & 0xF), 0];
                            var dir31 = directions[(int)(_randomPattern[n2] & 0xF), 1];
                            for (int y2 = 0; y2 < my2; y2 += pot) for (int x2 = 0; x2 < mx2; x2 += pot)
                                {
                                    int x234 = x2 + x34;
                                    int y234 = y2 + y34;
                                    int my1 = Math.Min(pot, h - y234);
                                    int mx1 = Math.Min(pot, w - x234);
                                    int bestIdx2 = -1; float bestVal2 = 0;
                                    var dir20 = directions[(int)(_randomPattern[n2] & 0xF), 0];
                                    var dir21 = directions[(int)(_randomPattern[n2] & 0xF), 1];
                                    for (int y1 = 0; y1 < my1; y1 += 1) for (int x1 = 0; x1 < mx1; x1 += 1)
                                        {
                                            System.Diagnostics.Debug.Assert(x1 + x234 < w);
                                            System.Diagnostics.Debug.Assert(y1 + y234 < h);
                                            int idx = x1 + x234 + w * (y1 + y234);
                                            int xf = x1 + x234;
                                            int yf = y1 + y234;

                                            if (xf < 4 || xf >= w - 5 || yf < 4 || yf > h - 4) continue;

                                            float pixelTH0 = _thsSmoothed[(xf >> 5) + (yf >> 5) * _thsStep];
                                            float pixelTH1 = pixelTH0 * dw1;
                                            float pixelTH2 = pixelTH1 * dw2;


                                            float ag0 = mapmax0[idx];
                                            if (ag0 > pixelTH0 * thFactor)
                                            {
                                                //dot product direction and grad vector
                                                var dirNorm = Math.Abs(map0[idx, 1] * dir20 + map0[idx, 2] * dir21);
                                                if (!Context.Config.Core.SelectDirectionDistribution) dirNorm = ag0;

                                                if (dirNorm > bestVal2)
                                                { bestVal2 = dirNorm; bestIdx2 = idx; bestIdx3 = -2; bestIdx4 = -2; }
                                            }
                                            if (bestIdx3 == -2) continue;

                                            float ag1 = mapmax1[(int)(xf * 0.5f + 0.25f) + (int)(yf * 0.5f + 0.25f) * w1];
                                            if (ag1 > pixelTH1 * thFactor)
                                            {
                                                //dot product direction and grad vector
                                                var dirNorm = Math.Abs(map0[idx, 1] * dir30 + map0[idx, 2] * dir31);
                                                if (!Context.Config.Core.SelectDirectionDistribution) dirNorm = ag1;

                                                if (dirNorm > bestVal3)
                                                { bestVal3 = dirNorm; bestIdx3 = idx; bestIdx4 = -2; }
                                            }
                                            if (bestIdx4 == -2) continue;

                                            float ag2 = mapmax2[(int)(xf * 0.25f + 0.125) + (int)(yf * 0.25f + 0.125) * w2];
                                            if (ag2 > pixelTH2 * thFactor)
                                            {
                                                //dot product direction and grad vector
                                                var dirNorm = Math.Abs(map0[idx, 1] * dir40 + map0[idx, 2] * dir41);
                                                if (!Context.Config.Core.SelectDirectionDistribution) dirNorm = ag2;

                                                if (dirNorm > bestVal4)
                                                { bestVal4 = dirNorm; bestIdx4 = idx; }
                                            }
                                        }

                                    if (bestIdx2 > 0)
                                    {
                                        mapOut[bestIdx2] = 1;
                                        bestVal3 = 1e10f;
                                        n2++;
                                    }
                                }

                            if (bestIdx3 > 0)
                            {
                                mapOut[bestIdx3] = 2;
                                bestVal4 = 1e10f;
                                n3++;
                            }
                        }

                    if (bestIdx4 > 0)
                    {
                        mapOut[bestIdx4] = 4;
                        n4++;
                    }
                }
            return new int[3] { n2, n3, n4 };
        }

        private int computeHistQuantil(int[] hist, float below)
        {
            int th = (int)(hist[0] * below + 0.5f);
            for (int i = 0; i < 48; i++)
            {
                th -= hist[i + 1];
                if (th < 0) return i;
            }
            return 50;
        }

        public void MakeHist(FrameHessian frameHessian, bool plot = true)
        {
            _gradHistFrame = frameHessian;
            var mapmax0 = frameHessian.AbsSquaredGrad[0];

            int w = Context.w[0];
            int h = Context.h[0];

            int w32 = w / 32;
            int h32 = h / 32;

            _thsStep = w32;

            for (int y = 0; y < h32; y++)
                for (int x = 0; x < w32; x++)
                {
                    float map0 = mapmax0[32 * x + 32 * y * w];
                    int[] hist0 = new int[50];// gradHist;// + 50*(x+y*w32);
                                              //memset(hist0, 0, sizeof(int) * 50);
                    var offsetIndex = 32 * x + 32 * y * w;
                    for (int j = 0; j < 32; j++) for (int i = 0; i < 32; i++)
                        {
                            int it = i + 32 * x;
                            int jt = j + 32 * y;
                            if (it > w - 2 || jt > h - 2 || it < 1 || jt < 1) continue;
                            int g = (int)Math.Sqrt(mapmax0[(offsetIndex) + (i + j * w)]);// map0[i + j * w]); //navigate inside big box and then inside pixels inside this box
                            if (g > 48) g = 48; //what why? maybe because histogram array is finite?
                            hist0[g + 1]++;
                            hist0[0]++;
                        }
                    _ths[x + y * w32] = computeHistQuantil(hist0, Context.Config.Core.MinGradHistCut) + Context.Config.Core.MinGradHistAdd; //not sure but it seems that it only find max gradient
                }

            if (plot)
                Context.Output?.UpdateImage("01 CoarseInit.01 Gradient per 32 pixels.Orig", new Shared.MinimalImageF(frameHessian.Shell.IncomingId, w32, h32, _ths));

            //squared average 3x3 of grad
            for (int y = 0; y < h32; y++)
            {
                for (int x = 0; x < w32; x++)
                {
                    float sum = 0, num = 0;
                    if (x > 0)
                    {
                        if (y > 0) { num++; sum += _ths[x - 1 + (y - 1) * w32]; }
                        if (y < h32 - 1) { num++; sum += _ths[x - 1 + (y + 1) * w32]; }
                        num++; sum += _ths[x - 1 + (y) * w32];
                    }

                    if (x < w32 - 1)
                    {
                        if (y > 0) { num++; sum += _ths[x + 1 + (y - 1) * w32]; }
                        if (y < h32 - 1) { num++; sum += _ths[x + 1 + (y + 1) * w32]; }
                        num++; sum += _ths[x + 1 + (y) * w32];
                    }

                    if (y > 0) { num++; sum += _ths[x + (y - 1) * w32]; }
                    if (y < h32 - 1) { num++; sum += _ths[x + (y + 1) * w32]; }
                    num++; sum += _ths[x + y * w32];

                    _thsSmoothed[x + y * w32] = (sum / num) * (sum / num);
                }
            }

            if (plot)
                Context.Output?.UpdateImage("01 CoarseInit.01 Gradient per 32 pixels.Smooth", new Shared.MinimalImageF(frameHessian.Shell.IncomingId, w32, h32, _thsSmoothed));
        }
    }
}
