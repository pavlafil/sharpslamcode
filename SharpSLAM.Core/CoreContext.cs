﻿using SharpSLAM.Core.Unmanaged;
using SharpSLAM.Configuration;
using SharpSLAM.Output.Abstractions;
using SharpSLAM.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace SharpSLAM.Core
{
    public class CoreContext
    {
        public  Random Rand { get; private set; }

        public int PyrLevelsUsed { get; set; }

        public int[] w { get; set; }
        public int[] h { get; set; }

        public double[] fx { get; set; }
        public double[] fy { get; set; }
        public double[] cx { get; set; }
        public double[] cy { get; set; }
        public Mat33f[] K { get; set; }

        public double[] fxi { get; set; }
        public double[] fyi { get; set; }
        public double[] cxi { get; set; }
        public double[] cyi { get; set; }
        public Mat33f[] Ki { get; set; }


        public Configuration.Configuration Config { get; set; }
        public Output.Abstractions.Output Output { get; set; }

        public CoreContext(Configuration.Configuration config, Output.Abstractions.Output output=null)
        {
            Rand = new Random();

            Config = config;
            Output = output;

            int wlvl = Config.Input.GeometricCalibration.OutputWidth;
            int hlvl = Config.Input.GeometricCalibration.OutputHeight;
            PyrLevelsUsed = 1;
            while (wlvl % 2 == 0 && hlvl % 2 == 0 && wlvl * hlvl > 5000 && PyrLevelsUsed < Config.Core.MaxPyrLevels)
            {
                wlvl /= 2;
                hlvl /= 2;
                PyrLevelsUsed++;
            }
            Shared.Log.LogInfo($"Using pyramid levels 0 to {PyrLevelsUsed}. Coarsest resolution: {wlvl} x {hlvl}!");
            if (wlvl > 100 && hlvl > 100)
            {
                Shared.Log.LogInfo(@"\n===============WARNING!===================\n
                                    Using not enough pyramid levels.\n
                  Consider scaling to a resolution that is a multiple of a power of 2.\n");
            }
            if (PyrLevelsUsed < 3)
            {
                Shared.Log.LogInfo(@"\n===============WARNING!===================\n
                                     I need higher resolution.\n
                                      I will probably segfault.\n");
            }

            w = new int[PyrLevelsUsed];
            h = new int[PyrLevelsUsed];

            fx = new double[PyrLevelsUsed];
            fy = new double[PyrLevelsUsed];
            cx = new double[PyrLevelsUsed];
            cy = new double[PyrLevelsUsed];
            K = new Mat33f[PyrLevelsUsed];

            fxi = new double[PyrLevelsUsed];
            fyi = new double[PyrLevelsUsed];
            cxi = new double[PyrLevelsUsed];
            cyi = new double[PyrLevelsUsed];
            Ki = new Mat33f[PyrLevelsUsed];

            w[0] = Config.Input.GeometricCalibration.OutputWidth; //w;
            h[0] = Config.Input.GeometricCalibration.OutputHeight; //h;

            fx[0] = Config.Input.GeometricCalibration.OutputCameraModel.fx;//K(0, 0);
            fy[0] = Config.Input.GeometricCalibration.OutputCameraModel.fy;//K(1, 1);
            cx[0] = Config.Input.GeometricCalibration.OutputCameraModel.cx;//K(0, 2);
            cy[0] = Config.Input.GeometricCalibration.OutputCameraModel.cy;//K(1, 2);

            for (int level = 1; level < PyrLevelsUsed; ++level)
            {
                w[level] = w[0] >> level;
                h[level] = h[0] >> level;
                fx[level] = fx[level - 1] * 0.5;
                fy[level] = fy[level - 1] * 0.5;
                cx[level] = (cx[0] + 0.5) / ((int)1 << level) - 0.5;
                cy[level] = (cy[0] + 0.5) / ((int)1 << level) - 0.5;
            }

            for (int level = 0; level < PyrLevelsUsed; ++level)
            {
                K[level] = new Mat33f();
                K[level][0, 0] = (float)fx[level];
                K[level][0, 1] = 0f;
                K[level][0, 2] = (float)cx[level];
                K[level][1, 0] = 0f;
                K[level][1, 1] = (float)fy[level];
                K[level][1, 2] = (float)cy[level];
                K[level][2, 0] = 0f;
                K[level][2, 1] = 0f;
                K[level][2, 2] = 1f;

                Ki[level] = K[level].Inverse();

                fxi[level] = Ki[level][0, 0];
                fyi[level] = Ki[level][1, 1];
                cxi[level] = Ki[level][0, 2];
                cyi[level] = Ki[level][1, 2];
            }
        }
    }
}
