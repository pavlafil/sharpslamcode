﻿using System;
using System.Collections.Generic;

namespace SharpSLAM.Core
{
    public class EFFrame
    {
        public int FrameID { get { return Data.FrameID; } }
        public int Idx { get; set; }

        public FrameHessian Data { get; private set; }


        public double[] Prior { get; set; } = new double[8];// Vec8 prior; // prior hessian (diagonal)
        [Obsolete]
        public  double[] DeltaPrior { get; set; } = new double[8];//Vec8 delta_prior;// = state-state_prior (E_prior = (delta_prior)' * diag(prior) * (delta_prior)
        [Obsolete]
        public double[] Delta { get; set; } = new double[8];//Vec8 delta// state - state_zero.

        public List<EFPoint> Points { get; set; } = new List<EFPoint>();

        public EFFrame(FrameHessian fh)
        {
            Data = fh;

            var fhPrior = Data.GetPrior();

            for (int i = 0; i < 8; i++)
            {
                Prior[i] = fhPrior[i];
            }

            //delta = data->get_state_minus_stateZero().head < 8 > ();
            //delta_prior = (data->get_state() - data->getPriorZero()).head < 8 > ();

        }

    }
}