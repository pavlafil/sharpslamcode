﻿using SharpSLAM.Core.Unmanaged;
using SharpSLAM.Shared;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static SharpSLAM.Configuration.Core;

namespace SharpSLAM.Core
{
    public class FrameHessian : Contextual
    {
        public EFFrame EfFrame { get; set; }

        public float AbExposure { get; set; }

        // precalc values
        public SE3 PREWorldToCam { get; set; }
        public SE3 PRECamToWorld { get; set; }
        public List<FramePrecalc> TargetPrecalc { get; set; }

        public List<PointHessian> PointHessians { get; set; }               // contains all ACTIVE points.
        public List<PointHessian> PointHessiansMarginalized { get; set; }   // contains all MARGINALIZED points (= fully marginalized, usually because point went OOB.)
        public List<PointHessian> PointHessiansOut { get; set; }            // contains all OUTLIER points (= discarded.).
        public List<ImmaturePoint> ImmaturePoints { get; set; }		     // contains all OUTLIER points (= discarded.).

        public int FrameID { get; set; }						// incremental ID for keyframes only!
        public int Idx { get; set; }

        public double[][] NullspacesPose { get; set; }
        public double[][] NullspacesAffine { get; set; }
        public double[] NullspacesScale { get; set; }

        public SE3 WorldToCamEvalPT { get; set; }

        public double[] Step { get; set; }
        public double[] StepBackup { get; set; }
        public double[] StateBackup { get; set; }


        public double[] State { get; set; }	// [0-5: worldToCam-leftEps. 6-7: a,b]
        public double[] StateScaled { get; set; }
        public double[] StateZero { get; set; }
        public double[] StateMinusStateZero { get { return State.Zip(StateZero, (a, b) => { return a - b; }).ToArray(); } }


        public float[,] dI;     // trace, fine tracking. Used for direction select (not for gradient histograms etc.)
        public float[][,] dIp;    // coarse tracking / coarse initializer. NAN in [0] only.
        public float[][] AbsSquaredGrad { get; set; } // only used for pixel select (histograms etc.). no NAN.
        public FrameShell Shell { get; set; }
        public bool FlaggedForMarginalization { get; set; }

        // Photometric Calibration Stuff
        public float FrameEnergyTh { get; set; }    // set dynamically depending on tracking residual
        //float ab_exposure;

        public AffLight Aff_g2l()
        {
            return new AffLight(StateScaled[6], StateScaled[7]);
        }
        public AffLight Aff_g2l_0()
        {
            return new AffLight(StateZero[6] * Context.Config.Core.ScaleA, StateZero[7] * Context.Config.Core.ScaleB);
        }

        public IEnumerable<double> W2cLeftEps()
        {
            return StateScaled.Take(6);
        }

        public FrameHessian(CoreContext context) : base(context)
        {
            AbsSquaredGrad = new float[context.PyrLevelsUsed][];
            dIp = new float[context.PyrLevelsUsed][,];

            for (int i = 0; i < Context.PyrLevelsUsed; i++)
            {
                dIp[i] = new float[Context.w[i] * Context.h[i], 3];
                AbsSquaredGrad[i] = new float[Context.w[i] * Context.h[i]];
                if (i == 0)
                {
                    dI = dIp[i];
                }
            }

            TargetPrecalc = new List<FramePrecalc>();

            WorldToCamEvalPT = new SE3();
            PRECamToWorld = new SE3();
            PREWorldToCam = new SE3();

            Step = new double[10];

            State = new double[10];
            StateScaled = new double[10];
            StateScaled = new double[10];
            StateZero = new double[10];


            PointHessians = new List<PointHessian>();
            PointHessiansMarginalized = new List<PointHessian>();
            PointHessiansOut = new List<PointHessian>();
            ImmaturePoints = new List<ImmaturePoint>();

            FlaggedForMarginalization = false;

            FrameID = -1;
            FrameEnergyTh = 8 * 8 * Context.Config.Core.PatternNumber;
        }

        public void SetEvalPT(SE3 worldToCamEvalPT, double[] state)
        {
            WorldToCamEvalPT = worldToCamEvalPT;
            SetState(state);
            SetStateZero(state);
        }

        public void SetEvalPTScaled(SE3 worldToCamEvalPT, AffLight aff_g2l)
        {
            var initialState = new double[10];
            initialState[6] = aff_g2l.a;
            initialState[7] = aff_g2l.b;
            WorldToCamEvalPT = worldToCamEvalPT;
            SetStateScaled(initialState);
            SetStateZero(State);
        }

        private void SetStateZero(double[] stateZero)
        {
            Debug.Assert(StateZero.Take(6).Aggregate(0d, (a, i) => { return a += (i * i); }) < 1e-20);

            StateZero = stateZero;

            NullspacesPose = new double[6][];
            for (int i = 0; i < 6; i++)
            {
                double[] eps = new double[6];
                eps[i] = 1e-3;

                SE3 EepsP = SE3.Exp(eps);

                eps[i] = -eps[i];
                SE3 EepsM = SE3.Exp(eps);
                eps[i] = -eps[i];

                SE3 lw2c_leftEps_P_x0 = (WorldToCamEvalPT * EepsP) * WorldToCamEvalPT.Inverse();
                SE3 lw2c_leftEps_M_x0 = (WorldToCamEvalPT * EepsM) * WorldToCamEvalPT.Inverse();
                NullspacesPose[i] = (lw2c_leftEps_P_x0.Log().Zip(lw2c_leftEps_M_x0.Log(), (a, b) => { return (a - b) / 2e-3; }).ToArray());
            }
            ////nullspaces_pose.topRows<3>() *= SCALE_XI_TRANS_INVERSE;
            ////nullspaces_pose.bottomRows<3>() *= SCALE_XI_ROT_INVERSE;

            // scale change
            SE3 w2c_leftEps_P_x0 = WorldToCamEvalPT.Clone();
            w2c_leftEps_P_x0.TranslationMul(1.00001);
            w2c_leftEps_P_x0 = w2c_leftEps_P_x0 * WorldToCamEvalPT.Inverse();
            SE3 w2c_leftEps_M_x0 = WorldToCamEvalPT.Clone();
            w2c_leftEps_M_x0.TranslationDivide(1.00001);
            w2c_leftEps_M_x0 = w2c_leftEps_M_x0 * WorldToCamEvalPT.Inverse();
            NullspacesScale = w2c_leftEps_P_x0.Log().Zip(w2c_leftEps_M_x0.Log(), (a, b) => { return (a - b) / 2e-3; }).ToArray();

            NullspacesAffine = new double[2][];
            NullspacesAffine[0] = new double[2] { 1, 0 };
            Debug.Assert(AbExposure > 0);
            NullspacesAffine[1] = new double[2] { 0, Math.Exp(StateZero[6] * Context.Config.Core.ScaleA) * AbExposure };
        }

        public void SetState(double[] state)
        {
            Debug.Assert(state.Length == 10);
            State = state;
            for (int i = 0; i < 3; i++)
            {
                StateScaled[i] = Context.Config.Core.ScaleXITRANS * state[i];
            }
            for (int i = 3; i < 6; i++)
            {
                StateScaled[i] = Context.Config.Core.ScaleXIROT * state[i];
            }

            StateScaled[6] = Context.Config.Core.ScaleA * State[6];
            StateScaled[7] = Context.Config.Core.ScaleB * State[7];
            StateScaled[8] = Context.Config.Core.ScaleA * State[8];
            StateScaled[9] = Context.Config.Core.ScaleB * State[9];

            PREWorldToCam = SE3.Exp(StateScaled) * WorldToCamEvalPT;
            PRECamToWorld = PREWorldToCam.Inverse();
        }

        private void SetStateScaled(double[] stateScaled)
        {
            Debug.Assert(stateScaled.Length == 10);
            StateScaled = stateScaled;
            for (int i = 0; i < 3; i++)
            {
                State[i] = Context.Config.Core.ScaleXITRANSInverse * stateScaled[i];
            }
            for (int i = 3; i < 6; i++)
            {
                State[i] = Context.Config.Core.ScaleXIROTInverse * stateScaled[i];
            }

            State[6] = Context.Config.Core.ScaleAInverse * StateScaled[6];
            State[7] = Context.Config.Core.ScaleBInverse * StateScaled[7];
            State[8] = Context.Config.Core.ScaleAInverse * StateScaled[8];
            State[9] = Context.Config.Core.ScaleBInverse * StateScaled[9];

            PREWorldToCam = SE3.Exp(StateScaled) * WorldToCamEvalPT;
            PRECamToWorld = PREWorldToCam.Inverse();
        }

        public float[] GetPrior()
        {
            float[] p = new float[10];
            if (FrameID == 0)
            {
                if (!((Context.Config.Core.SolverMode & (int)SOLVERTYPE.REMOVE_POSEPRIOR) != 0))
                {
                    for (int i = 0; i < 3; i++)
                    {
                        p[i] = Context.Config.Core.InitialTransPrior;
                    }

                    for (int i = 3; i < 6; i++)
                    {
                        p[i] = Context.Config.Core.InitialRotPrior;
                    }
                }

                p[6] = Context.Config.Core.InitialAffAPrior;
                p[7] = Context.Config.Core.InitialAffBPrior;
            }
            else
            {
                if (Context.Config.Core.AffineOptModeA < 0)
                    p[6] = Context.Config.Core.InitialAffAPrior;
                else
                    p[6] = Context.Config.Core.AffineOptModeA;

                if (Context.Config.Core.AffineOptModeB < 0)
                    p[7] = Context.Config.Core.InitialAffBPrior;
                else
                    p[7] = Context.Config.Core.AffineOptModeB;
            }
            p[8] = Context.Config.Core.InitialAffAPrior;
            p[9] = Context.Config.Core.InitialAffBPrior;
            return p;
        }

        public void MakeImages(MinimalImageF image, bool plot = true)
        {
            // make d0
            int w = Context.w[0];
            int h = Context.h[0];
            for (int i = 0; i < w * h; i++)
            {
                dI[i, 0] = image.Data[i];
            }

            for (int lvl = 0; lvl < Context.PyrLevelsUsed; lvl++)
            {
                int wl = Context.w[lvl], hl = Context.h[lvl];
                var dI_l = dIp[lvl];

                var dabs_l = AbsSquaredGrad[lvl];
                if (lvl > 0)
                {
                    int lvlm1 = lvl - 1;
                    int wlm1 = Context.w[lvlm1];
                    var dI_lm = dIp[lvlm1];

                    for (int y = 0; y < hl; y++)
                        for (int x = 0; x < wl; x++)
                        {
                            dI_l[x + y * wl, 0] = 0.25f * (dI_lm[2 * x + 2 * y * wlm1, 0] +
                                                        dI_lm[2 * x + 1 + 2 * y * wlm1, 0] +
                                                        dI_lm[2 * x + 2 * y * wlm1 + wlm1, 0] +
                                                        dI_lm[2 * x + 1 + 2 * y * wlm1 + wlm1, 0]);
                        }
                }

                for (int idx = wl; idx < wl * (hl - 1); idx++)
                {
                    //if (idx % wl == 0 || (idx + 1) % wl == 0)
                    //    continue;

                    float dx = 0.5f * (dI_l[idx + 1, 0] - dI_l[idx - 1, 0]);
                    float dy = 0.5f * (dI_l[idx + wl, 0] - dI_l[idx - wl, 0]);


                    if (float.IsInfinity(dx) || float.IsNaN(dx))
                        dx = 0;
                    if (float.IsInfinity(dy) || float.IsNaN(dy))
                        dy = 0;

                    dI_l[idx, 1] = dx;
                    dI_l[idx, 2] = dy;


                    dabs_l[idx] = dx * dx + dy * dy;

                    if (Context.Config?.Input?.PhotometricCalibration?.Gamma != null)
                    {
                        int c = (int)(dI_l[idx, 0] + 0.5f);
                        if (c < 5) c = 5;
                        if (c > 250) c = 250;
                        float gw = Context.Config.Input.PhotometricCalibration.Gamma.InvertedValues[c + 1] - Context.Config.Input.PhotometricCalibration.Gamma.InvertedValues[c];
                        dabs_l[idx] *= gw * gw; // convert to gradient of original color space (before removing response).
                    }
                }
                if (plot)
                    Context.Output?.UpdateImage($"01 CoarseInit.02 Abs squared grad.Lvl {lvl}", new MinimalImageF(image.ID, Context.w[lvl], Context.h[lvl], AbsSquaredGrad[lvl]));
            }
        }


    }
}
