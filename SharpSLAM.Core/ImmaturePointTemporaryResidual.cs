﻿namespace SharpSLAM.Core
{
    public class ImmaturePointTemporaryResidual
    {
        public ResState StateState { get; set; }
        public double StateEnergy { get; set; }
        public ResState StateNewState { get; set; }
        public double StateNewEnergy { get; set; }
        public FrameHessian Target { get; set; }
    }
}