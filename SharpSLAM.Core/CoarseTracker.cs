﻿using SharpSLAM.Core.Unmanaged;
using SharpSLAM.Shared;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace SharpSLAM.Core
{
    public class CoarseTracker : Contextual
    {
        private CoarseTrackerU _coarseTrackerU { get; set; }

        public AffLight LastRefAffG2l { get; set; }
        public double FirstCoarseRMSE { get; set; }
        public FrameHessian LastRef { get; set; }
        public int RefFrameId { get; set; }

        public FrameHessian NewFrame { get; set; }

        public bool DebugPlot { get; set; }
        public bool DebugPrint { get; set; }

        // act as pure ouptut
        public double[] LastResiduals { get; set; }
        public double[] LastFlowIndicators { get; set; }

        //public int[] W { get; set; }
        //public int[] H { get; set; }


        private float[][] _idepth { get; set; }

        private float[][] _weightSums { get; set; }
        private float[][] _weightSumsBak { get; set; }

        private float[][] _pcU { get; set; }
        private float[][] _pcV { get; set; }
        private float[][] _pcIdepth { get; set; }
        private float[][] _pcColor { get; set; }
        private int[] _pcN { get; set; }

        private float[] _bufWarpedIdepth { get; set; }
        private float[] _bufWarpedU { get; set; }
        private float[] _bufWarpedV { get; set; }
        private float[] _bufWarpedDx { get; set; }
        private float[] _bufWarpedDy { get; set; }
        private float[] _bufWarpedResidual { get; set; }
        private float[] _bufWarpedWeight { get; set; }
        private float[] _bufWarpedRefColor { get; set; }
        private int _bufWarpedN { get; set; }


        private Mat33f[] _K;
        private Mat33f[] _Ki;
        private float[] _fx;
        private float[] _fy;
        private float[] _fxi;
        private float[] _fyi;
        private float[] _cx;
        private float[] _cy;
        private float[] _cxi;
        private float[] _cyi;

        private bool _already222=false;

        public CoarseTracker(CoreContext context) : base(context)
        {
            _coarseTrackerU = new CoarseTrackerU();

            LastRefAffG2l = new AffLight(0, 0);

            _K = new Mat33f[Context.PyrLevelsUsed];
            _Ki = new Mat33f[Context.PyrLevelsUsed];
            _fx = new float[Context.PyrLevelsUsed];
            _fy = new float[Context.PyrLevelsUsed];
            _fxi = new float[Context.PyrLevelsUsed];
            _fyi = new float[Context.PyrLevelsUsed];
            _cx = new float[Context.PyrLevelsUsed];
            _cy = new float[Context.PyrLevelsUsed];
            _cxi = new float[Context.PyrLevelsUsed];
            _cyi = new float[Context.PyrLevelsUsed];

            _idepth = new float[Context.PyrLevelsUsed][];
            _weightSums = new float[Context.PyrLevelsUsed][];
            _weightSumsBak = new float[Context.PyrLevelsUsed][];

            _pcU = new float[Context.PyrLevelsUsed][];
            _pcV = new float[Context.PyrLevelsUsed][];
            _pcIdepth = new float[Context.PyrLevelsUsed][];
            _pcColor = new float[Context.PyrLevelsUsed][];
            _pcN = new int[Context.PyrLevelsUsed];

            _bufWarpedIdepth = new float[Context.PyrLevelsUsed];
            _bufWarpedU = new float[Context.PyrLevelsUsed];
            _bufWarpedV = new float[Context.PyrLevelsUsed];
            _bufWarpedDx = new float[Context.PyrLevelsUsed];
            _bufWarpedDy = new float[Context.PyrLevelsUsed];
            _bufWarpedResidual = new float[Context.PyrLevelsUsed];
            _bufWarpedWeight = new float[Context.PyrLevelsUsed];
            _bufWarpedRefColor = new float[Context.PyrLevelsUsed];



            int ww = Context.w[0]; int hh = Context.h[0];

            // make coarse tracking templates.
            for (int lvl = 0; lvl < Context.PyrLevelsUsed; lvl++)
            {
                int wl = ww >> lvl;
                int hl = hh >> lvl;

                _idepth[lvl] = new float[wl * hl]; //allocAligned<4,float>(wl * hl, ptrToDelete);
                _weightSums[lvl] = new float[wl * hl]; //allocAligned < 4,float> (wl * hl, ptrToDelete);
                _weightSumsBak[lvl] = new float[wl * hl]; //allocAligned < 4,float> (wl * hl, ptrToDelete);

                _pcU[lvl] = new float[wl * hl]; //allocAligned < 4,float> (wl * hl, ptrToDelete);
                _pcV[lvl] = new float[wl * hl]; //allocAligned < 4,float> (wl * hl, ptrToDelete);
                _pcIdepth[lvl] = new float[wl * hl]; //allocAligned < 4,float> (wl * hl, ptrToDelete);
                _pcColor[lvl] = new float[wl * hl]; //allocAligned < 4,float> (wl * hl, ptrToDelete);
            }

            // warped buffers
            _bufWarpedIdepth = new float[ww * hh]; //allocAligned < 4,float> (ww * hh, ptrToDelete);
            _bufWarpedU = new float[ww * hh]; //allocAligned < 4,float> (ww * hh, ptrToDelete);
            _bufWarpedV = new float[ww * hh]; //allocAligned < 4,float> (ww * hh, ptrToDelete);
            _bufWarpedDx = new float[ww * hh]; //allocAligned < 4,float> (ww * hh, ptrToDelete);
            _bufWarpedDy = new float[ww * hh]; //allocAligned < 4,float> (ww * hh, ptrToDelete);
            _bufWarpedResidual = new float[ww * hh]; //allocAligned < 4,float> (ww * hh, ptrToDelete);
            _bufWarpedWeight = new float[ww * hh]; //allocAligned < 4,float> (ww * hh, ptrToDelete);
            _bufWarpedRefColor = new float[ww * hh]; //allocAligned < 4,float> (ww * hh, ptrToDelete);

            //W = new int[Context.PyrLevelsUsed];
            //H = new int[Context.PyrLevelsUsed];
            //W[0] = H[0] = 0;
            RefFrameId = -1;

            LastResiduals = new double[5];
            LastFlowIndicators = new double[3];
        }

        public void SetCoarseTrackingRef(List<FrameHessian> frameHessians)
        {
            Debug.Assert(frameHessians.Count > 0);
            LastRef = frameHessians[frameHessians.Count - 1];
            makeCoarseDepthL0(frameHessians);



            RefFrameId = LastRef.Shell.Id;
            LastRefAffG2l = LastRef.Aff_g2l();

            FirstCoarseRMSE = -1;
        }

        private void makeCoarseDepthL0(List<FrameHessian> frameHessians)
        {
            // make coarse tracking templates for latstRef.
            Array.Clear(_idepth[0], 0, _idepth[0].Length);
            Array.Clear(_weightSums[0], 0, _weightSums[0].Length);

            foreach (FrameHessian fh in frameHessians)
            {
                foreach (PointHessian ph in fh.PointHessians)
                {
                    if (ph.LastResiduals[0].Item1 != null && ph.LastResiduals[0].Item2 == ResState.IN)
                    {
                        PointFrameResidual r = ph.LastResiduals[0].Item1;
                        Debug.Assert(r.EfResidual.IsActiveAndIsGoodNEW && r.Target == LastRef);
                        int u = (int)(r.CenterProjectedTo[0] + 0.5f);
                        int v = (int)(r.CenterProjectedTo[1] + 0.5f);
                        float new_idepth = r.CenterProjectedTo[2];
                        float weight = (float)Math.Sqrt(1e-3 / (ph.EfPoint.HdiF + 1e-12));

                        _idepth[0][u + Context.w[0] * v] += new_idepth * weight;
                        _weightSums[0][u + Context.w[0] * v] += weight;
                    }
                }
            }


            for (int lvl = 1; lvl < Context.PyrLevelsUsed; lvl++)
            {
                int lvlm1 = lvl - 1;
                int wl = Context.w[lvl], hl = Context.h[lvl], wlm1 = Context.w[lvlm1];

                float[] idepth_l = _idepth[lvl];
                float[] weightSums_l = _weightSums[lvl];

                float[] idepth_lm = _idepth[lvlm1];
                float[] weightSums_lm = _weightSums[lvlm1];

                for (int y = 0; y < hl; y++)
                    for (int x = 0; x < wl; x++)
                    {
                        int bidx = 2 * x + 2 * y * wlm1;
                        idepth_l[x + y * wl] = idepth_lm[bidx] +
                                                    idepth_lm[bidx + 1] +
                                                    idepth_lm[bidx + wlm1] +
                                                    idepth_lm[bidx + wlm1 + 1];

                        weightSums_l[x + y * wl] = weightSums_lm[bidx] +
                                                    weightSums_lm[bidx + 1] +
                                                    weightSums_lm[bidx + wlm1] +
                                                    weightSums_lm[bidx + wlm1 + 1];
                    }
            }


            // dilate idepth by 1.
            for (int lvl = 0; lvl < 2; lvl++)
            {
                int numIts = 1;


                for (int it = 0; it < numIts; it++)
                {
                    int wh = Context.w[lvl] * Context.h[lvl] - Context.w[lvl];
                    int wl = Context.w[lvl];
                    float[] weightSumsl = _weightSums[lvl];
                    float[] weightSumsl_bak = _weightSumsBak[lvl];
                    weightSumsl.CopyTo(weightSumsl_bak, 0);
                    float[] idepthl = _idepth[lvl];   // dotnt need to make a temp copy of depth, since I only
                                                      // read values with weightSumsl>0, and write ones with weightSumsl<=0.
                    for (int i = Context.w[lvl]; i < wh; i++)
                    {
                        if (weightSumsl_bak[i] <= 0)
                        {
                            float sum = 0, num = 0, numn = 0;
                            var idx = i + 1 + wl;
                            if (idx >= 0 && idx < weightSumsl_bak.Length && weightSumsl_bak[idx] > 0) { sum += idepthl[idx]; num += weightSumsl_bak[idx]; numn++; }
                            idx = i - 1 - wl;
                            if (idx >= 0 && idx < weightSumsl_bak.Length && weightSumsl_bak[idx] > 0) { sum += idepthl[idx]; num += weightSumsl_bak[idx]; numn++; }
                            idx = i + wl - 1;
                            if (idx >= 0 && idx < weightSumsl_bak.Length && weightSumsl_bak[idx] > 0) { sum += idepthl[idx]; num += weightSumsl_bak[idx]; numn++; }
                            idx = i - wl + 1;
                            if (idx >= 0 && idx < weightSumsl_bak.Length && weightSumsl_bak[idx] > 0) { sum += idepthl[idx]; num += weightSumsl_bak[idx]; numn++; }
                            if (numn > 0) { idepthl[i] = sum / numn; weightSumsl[i] = num / numn; }
                        }
                    }
                }
            }


            // dilate idepth by 1 (2 on lower levels).
            for (int lvl = 2; lvl < Context.PyrLevelsUsed; lvl++)
            {
                int wh = Context.w[lvl] * Context.h[lvl] - Context.w[lvl];
                int wl = Context.w[lvl];
                float[] weightSumsl = _weightSums[lvl];
                float[] weightSumsl_bak = _weightSumsBak[lvl];
                weightSumsl.CopyTo(weightSumsl_bak, 0);
                float[] idepthl = _idepth[lvl];   // dotnt need to make a temp copy of depth, since I only
                                                  // read values with weightSumsl>0, and write ones with weightSumsl<=0.
                for (int i = Context.w[lvl]; i < wh; i++)
                {
                    if (weightSumsl_bak[i] <= 0)
                    {
                        float sum = 0, num = 0, numn = 0;
                        var idx = i + 1;
                        if (idx >= 0 && idx < weightSumsl_bak.Length && weightSumsl_bak[idx] > 0) { sum += idepthl[idx]; num += weightSumsl_bak[idx]; numn++; }
                        idx = i - 1;
                        if (idx >= 0 && idx < weightSumsl_bak.Length && weightSumsl_bak[idx] > 0) { sum += idepthl[idx]; num += weightSumsl_bak[idx]; numn++; }
                        idx = i + wl;
                        if (idx >= 0 && idx < weightSumsl_bak.Length && weightSumsl_bak[idx] > 0) { sum += idepthl[idx]; num += weightSumsl_bak[idx]; numn++; }
                        idx = i - wl;
                        if (idx >= 0 && idx < weightSumsl_bak.Length && weightSumsl_bak[idx] > 0) { sum += idepthl[idx]; num += weightSumsl_bak[idx]; numn++; }
                        if (numn > 0) { idepthl[i] = sum / numn; weightSumsl[i] = num / numn; }
                    }
                }
            }


            // normalize idepths and weights.
            for (int lvl = 0; lvl < Context.PyrLevelsUsed; lvl++)
            {
                float[] weightSumsl = _weightSums[lvl];
                float[] idepthl = _idepth[lvl];
                var dIRefl = LastRef.dIp[lvl];

                int wl = Context.w[lvl], hl = Context.h[lvl];

                int lpc_n = 0;
                float[] lpc_u = _pcU[lvl];
                float[] lpc_v = _pcV[lvl];
                float[] lpc_idepth = _pcIdepth[lvl];
                float[] lpc_color = _pcColor[lvl];


                for (int y = 2; y < hl - 2; y++)
                    for (int x = 2; x < wl - 2; x++)
                    {
                        int i = x + y * wl;

                        if (weightSumsl[i] > 0)
                        {
                            idepthl[i] /= weightSumsl[i];
                            lpc_u[lpc_n] = x;
                            lpc_v[lpc_n] = y;
                            lpc_idepth[lpc_n] = idepthl[i];
                            lpc_color[lpc_n] = dIRefl[i, 0];



                            if (float.IsInfinity(lpc_color[lpc_n]) || float.IsNaN(lpc_color[lpc_n]) || !(idepthl[i] > 0))
                            {
                                idepthl[i] = -1;
                                continue;   // just skip if something is wrong.
                            }
                            lpc_n++;
                        }
                        else
                            idepthl[i] = -1;

                        weightSumsl[i] = 1;
                    }

                _pcN[lvl] = lpc_n;
            }
        }

        public void debugPlotIDepthMap(ref float minID_pt, ref float maxID_pt)
        {
            //SOME ERROR HERE
            if (Context.w[1] == 0) return;


            int lvl = 0;

            {
                List<float> allID = new List<float>();
                for (int i = 0; i < Context.h[lvl] * Context.w[lvl]; i++)
                {
                    if (_idepth[lvl][i] > 0)
                        allID.Add(_idepth[lvl][i]);
                }
                allID.Sort();
                int n = allID.Count - 1;

                float minID_new = allID[(int)(n * 0.05)];
                float maxID_new = allID[(int)(n * 0.95)];

                float minID, maxID;
                minID = minID_new;
                maxID = maxID_new;
                if (minID_pt != 0 && maxID_pt != 0)
                {
                    if (minID_pt < 0 || maxID_pt < 0)
                    {
                        maxID_pt = maxID;
                        minID_pt = minID;
                    }
                    else
                    {

                        // slowly adapt: change by maximum 10% of old span.
                        float maxChange = 0.3f * (maxID_pt - minID_pt);

                        if (minID < minID_pt - maxChange)
                            minID = minID_pt - maxChange;
                        if (minID > minID_pt + maxChange)
                            minID = minID_pt + maxChange;


                        if (maxID < maxID_pt - maxChange)
                            maxID = maxID_pt - maxChange;
                        if (maxID > maxID_pt + maxChange)
                            maxID = maxID_pt + maxChange;

                        maxID_pt = maxID;
                        minID_pt = minID;
                    }
                }


                MinimalImageB3 mf = new MinimalImageB3(LastRef.Shell.IncomingId, Context.w[lvl], Context.h[lvl]);
                mf.SetBlack();
                for (int i = 0; i < Context.h[lvl] * Context.w[lvl]; i++)
                {
                    int c = (int)(LastRef.dIp[lvl][i, 0] * 0.9f);
                    if (c > 255) c = 255;
                    mf.At(i, 0) = (byte)c;
                    mf.At(i, 1) = (byte)c;
                    mf.At(i, 2) = (byte)c;
                }
                int wl = Context.w[lvl];
                for (int y = 3; y < Context.h[lvl] - 3; y++)
                    for (int x = 3; x < wl - 3; x++)
                    {
                        int idx = x + y * wl;
                        float sid = 0, nid = 0;
                        float[] bp = _idepth[lvl];

                        if (bp[idx + 0] > 0) { sid += bp[idx + 0]; nid++; }
                        if (bp[idx + 1] > 0) { sid += bp[idx + 1]; nid++; }
                        if (bp[idx - 1] > 0) { sid += bp[idx - 1]; nid++; }
                        if (bp[idx + wl] > 0) { sid += bp[idx + wl]; nid++; }
                        if (bp[idx - wl] > 0) { sid += bp[idx - wl]; nid++; }

                        if (bp[0] > 0 || nid >= 3)
                        {
                            float id = ((sid / nid) - minID) / ((maxID - minID));
                            mf.SetPixelCirc(x, y, mf.GetJetColor(id));
                            ////mf.at(idx) = makeJet3B(id);
                        }
                    }
                //IOWrap::displayImage("coarseDepth LVL0", &mf, false);

                Context.Output.UpdateImage("Predicted", mf);

                //TODO add if debug output to file but probably will be sovled by otupt layer
                //if (debugSaveImages)
                //{
                //    char buf[1000];
                //    snprintf(buf, 1000, "images_out/predicted_%05d_%05d.png", lastRef->shell->id, refFrameID);
                //    IOWrap::writeImage(buf, &mf);
                //}

            }
        }

        internal void debugPlotIDepthMapFloat()
        {
            if (Context.w[1] == 0) return;
            int lvl = 0;
            MinimalImageF mim = new MinimalImageF(LastRef.Shell.IncomingId, Context.w[lvl], Context.h[lvl], _idepth[lvl]);
            Context.Output.UpdateImage("IdepthMapPlot", mim);
        }

        public bool TrackNewestCoarse(FrameHessian newFrameHessian, ref SE3 lastToNew_out, ref AffLight aff_g2l_out, int coarsestLvl, double[] minResForAbort)
        {
            DebugPlot = Context.Config.Core.RenderDisplayCoarseTrackingFull;
            DebugPrint = Context.Config.Output.Log.Debug;

            Debug.Assert(coarsestLvl < 5 && coarsestLvl < Context.PyrLevelsUsed);

            for (int i = 0; i < LastResiduals.Length; i++)
            {
                LastResiduals[i] = double.NaN;
            }
            for (int i = 0; i < LastFlowIndicators.Length; i++)
            {
                LastFlowIndicators[i] = 1000;
            }

            NewFrame = newFrameHessian;
            int[] maxIterations = new int[] { 10, 20, 50, 50, 50 };
            float lambdaExtrapolationLimit = 0.001f;

            SE3 refToNew_current = lastToNew_out.Clone();
            AffLight aff_g2l_current = aff_g2l_out;

            bool haveRepeated = false;



            //OPTIMIZE START==============================================================================


            for (int lvl = coarsestLvl; lvl >= 0; lvl--)
            {
                Mat88 H = null; double[] b = null;
                float levelCutoffRepeat = 1;
                double[] resOld = calcRes(lvl, refToNew_current, ref aff_g2l_current, Context.Config.Core.CoarseCutoffTH * levelCutoffRepeat);
                while (resOld[5] > 0.6 && levelCutoffRepeat < 50)
                {
                    levelCutoffRepeat *= 2;
                    resOld = calcRes(lvl, refToNew_current, ref aff_g2l_current, Context.Config.Core.CoarseCutoffTH * levelCutoffRepeat);

                    Log.LogDebug(string.Format("INCREASING cutoff to {0} (ratio is {1})!", Context.Config.Core.CoarseCutoffTH * levelCutoffRepeat, resOld[5]));
                }

                //unmanaged
                _coarseTrackerU.CalcGSSSE(_fx[lvl], _fy[lvl], _bufWarpedN, LastRefAffG2l.b, AffLight.FromToVecExposure(LastRef.AbExposure, NewFrame.AbExposure, LastRefAffG2l, aff_g2l_current)[0], ref H, ref b, ref refToNew_current, aff_g2l_current.a, aff_g2l_current.b, _bufWarpedDx, _bufWarpedDy, _bufWarpedU, _bufWarpedV, _bufWarpedIdepth, _bufWarpedRefColor, _bufWarpedResidual, _bufWarpedWeight, Context.Config.Core.ScaleXIROT, Context.Config.Core.ScaleXITRANS, Context.Config.Core.ScaleA, Context.Config.Core.ScaleB);

                float lambda = 0.01f;

                if (DebugPrint)
                {
                    var relAffI = AffLight.FromToVecExposure(LastRef.AbExposure, NewFrame.AbExposure, LastRefAffG2l, aff_g2l_current).Cast<double, float>().ToArray();
                    Log.LogDebug(string.Format("lvl{0}, it {1} (l={2} / {3}) {4}: {5}->{6} ({7} -> {8}) (|inc| = {9})!",
                            lvl, -1, lambda, 1.0f,
                            "INITIA",
                            0.0f,
                            resOld[0] / resOld[1],
                             0, (int)resOld[1],
                            0.0f));
                    Log.LogDebug($"{string.Join(",", refToNew_current.Log())} AFF {aff_g2l_current.a}, {aff_g2l_current.b} (rel { relAffI })");
                }


                #region Stats
                if (Context.Config.Core.GeneticAlg.EnableStateBrute && newFrameHessian.Shell.IncomingId == Context.Config.Core.GeneticAlg.InterestFrameID && lvl == 0)
                {
                    int interationsFrom = -5;
                    int interationsTo = 5;
                    int fac = 100;

                    Mat88 Hl = H;
                    for (int i = 0; i < 8; i++) Hl[i, i] *= (1 + lambda);
                    var inc = Hl.LdltSolve(b);

                    int cnt = 0;


                    Action<string, int, int, int, int, int, int> generateStats = (name, it0, it1, it2, it3, it4, it5) =>
                    {
                        var lInc = (double[])inc.Clone();

                        //var diff = lInc[dim] / 10; //10%
                        //lInc[dim] += it * diff;

                        lInc[0] += it0 * (lInc[0] * fac / 100); //10%;
                        lInc[1] += it1 * (lInc[1] * fac / 100); //10%;
                        lInc[2] += it2 * (lInc[2] * fac / 100); //10%;
                        lInc[3] += it3 * (lInc[3] * fac / 100); //10%;
                        lInc[4] += it4 * (lInc[4] * fac / 100); //10%;
                        lInc[5] += it5 * (lInc[5] * fac / 100); //10%;


                        float extrapFac = 1;
                        if (lambda < lambdaExtrapolationLimit) extrapFac = (float)Math.Sqrt(Math.Sqrt(lambdaExtrapolationLimit / lambda));
                        lInc = lInc.Mul(extrapFac);

                        var incScaled = lInc; //Vec8
                                              //incScaled.segment < 3 > (0) *= SCALE_XI_ROT;
                        for (int i = 0; i < 3; i++)
                        {
                            incScaled[i] *= Context.Config.Core.ScaleXIROT;
                        }
                        //incScaled.segment < 3 > (3) *= SCALE_XI_TRANS;
                        for (int i = 3; i < 6; i++)
                        {
                            incScaled[i] *= Context.Config.Core.ScaleXITRANS;
                        }
                        //incScaled.segment < 1 > (6) *= SCALE_A;
                        incScaled[6] *= Context.Config.Core.ScaleA;
                        //incScaled.segment < 1 > (7) *= SCALE_B;
                        incScaled[7] *= Context.Config.Core.ScaleB;

                        var sum = incScaled.Sum();
                        if (double.IsNaN(sum) || double.IsInfinity(sum))
                        {
                            for (int i = 0; i < incScaled.Length; i++)
                            {
                                incScaled[i] = 0;
                            }
                        }

                        SE3 refToNew_new = SE3.Exp(incScaled.Take(6).ToArray()) * refToNew_current;
                        AffLight aff_g2l_new = aff_g2l_current;
                        aff_g2l_new.a += incScaled[6];
                        aff_g2l_new.b += incScaled[7];

                        var resNew = calcRes(lvl, refToNew_new, ref aff_g2l_new, Context.Config.Core.CoarseCutoffTH * levelCutoffRepeat);

                        bool accept = (resNew[0] / resNew[1]) < (resOld[0] / resOld[1]);

                        Context.Output.Flush = false;
                        Context.Output.UpdateData(name, $"Trans X", cnt, it0 * fac, "Iteration", "Relative energy");
                        Context.Output.UpdateData(name, $"Trans Y", cnt, it1 * fac, "Iteration", "Relative energy");
                        Context.Output.UpdateData(name, $"Trans Z", cnt, it2 * fac, "Iteration", "Relative energy");

                        Context.Output.UpdateData(name, $"Rot X", cnt, it3 * fac, "Iteration", "Relative energy");
                        Context.Output.UpdateData(name, $"Rot Y", cnt, it4 * fac, "Iteration", "Relative energy");
                        Context.Output.UpdateData(name, $"Rot Z", cnt, it5 * fac, "Iteration", "Relative energy");

                        Context.Output.Flush = true;
                        Context.Output.UpdateData(name, $"", cnt, resNew[0] / resNew[1], "Iteration", "Relative energy");

                        cnt++;
                    };



                    for (int dim = 0; dim < 6; dim++)
                    {
                        for (int it = interationsFrom; it <= interationsTo; it++)
                        {

                            switch (dim)
                            {
                                case 0:
                                    generateStats("TrackNewCoarseFixed", it, 0, 0, 0, 0, 0);
                                    break;
                                case 1:
                                    generateStats("TrackNewCoarseFixed", 0, it, 0, 0, 0, 0);
                                    break;
                                case 2:
                                    generateStats("TrackNewCoarseFixed", 0, 0, it, 0, 0, 0);
                                    break;
                                case 3:
                                    generateStats("TrackNewCoarseFixed", 0, 0, 0, it, 0, 0);
                                    break;
                                case 4:
                                    generateStats("TrackNewCoarseFixed", 0, 0, 0, 0, it, 0);
                                    break;
                                case 5:
                                    generateStats("TrackNewCoarseFixed", 0, 0, 0, 0, 0, it);
                                    break;
                            }
                        }
                    }

                    for (int it3 = interationsFrom; it3 <= interationsTo; it3++)
                        for (int it4 = interationsFrom; it4 <= interationsTo; it4++)
                            for (int it5 = interationsFrom; it5 <= interationsTo; it5++)
                            {
                                generateStats("TrackNewCoarseFixTransRot", 0, 0, 0, it3, it4, it5);
                            }

                    for (int it0 = interationsFrom; it0 <= interationsTo; it0++)
                        for (int it1 = interationsFrom; it1 <= interationsTo; it1++)
                            for (int it2 = interationsFrom; it2 <= interationsTo; it2++)
                            {
                                generateStats("TrackNewCoarseFixRotTrans", it0, it1, it2, 0, 0, 0);
                            }













                }
                #endregion


                if (Context.Config.Core.GeneticAlg.Enable && (newFrameHessian.Shell.IncomingId == Context.Config.Core.GeneticAlg.InterestFrameID || (Context.Config.Core.GeneticAlg.ContinueAfterStarterFrame && _already222)))//temp disable
                {
                    _already222 = true;
                    #region NewOptimize
                    Log.LogDebug("Evolution start");

                    var maxVar = 800;//max init variance in %
                    var popSize = 20;
                    var genLength = 6;
                    var maxGenerations = 50;// 100;
                    var stillCountToQuit = 10;
                    var toCrossOverP = 0.5;
                    //var crossSwitchP = 0.4;
                    var mutateP = 0.5;
                    var mutateVar = 800;
                    var imunne = 3;
                    var baseStep = new double[genLength];
                    var baseStepChangeRate = 0.97;

                    Mat88 Hl = H;
                    for (int i = 0; i < 8; i++) Hl[i, i] *= (1 + lambda);
                    var initGen = Hl.LdltSolve(b);

                    for (int i = 0; i < genLength; i++)
                    {
                        baseStep[i] = initGen[i] / 100;
                    }

                    var population = new ValueTuple<ValueTuple<double, double, double, SE3, AffLight>, double[]>[popSize];
                    var newPopulation = new ValueTuple<ValueTuple<double, double, double, SE3, AffLight>, double[]>[popSize];

                    Func<double[], ValueTuple<double, double, double, SE3, AffLight>> evalFitness = (gen) =>
                    {
                        var incScaled = (double[])gen.Clone(); //Vec8
                                                               //incScaled.segment < 3 > (0) *= SCALE_XI_ROT;
                        for (int i = 0; i < 3; i++)
                        {
                            incScaled[i] *= Context.Config.Core.ScaleXIROT;
                        }
                        //incScaled.segment < 3 > (3) *= SCALE_XI_TRANS;
                        for (int i = 3; i < 6; i++)
                        {
                            incScaled[i] *= Context.Config.Core.ScaleXITRANS;
                        }
                        //incScaled.segment < 1 > (6) *= SCALE_A;
                        incScaled[6] *= Context.Config.Core.ScaleA;
                        //incScaled.segment < 1 > (7) *= SCALE_B;
                        incScaled[7] *= Context.Config.Core.ScaleB;

                        var sum = incScaled.Sum();
                        if (double.IsNaN(sum) || double.IsInfinity(sum))
                        {
                            for (int i = 0; i < incScaled.Length; i++)
                            {
                                incScaled[i] = 0;
                            }
                        }

                        SE3 refToNew_new = SE3.Exp(incScaled.Take(6).ToArray()) * refToNew_current;
                        AffLight aff_g2l_new = aff_g2l_current;
                        aff_g2l_new.a += incScaled[6];
                        aff_g2l_new.b += incScaled[7];

                        var resNew = calcRes(lvl, refToNew_new, ref aff_g2l_new, Context.Config.Core.CoarseCutoffTH * levelCutoffRepeat);

                        var relEnergy = resNew[0] / resNew[1];
                        return new ValueTuple<double, double, double, SE3, AffLight>(relEnergy, 100 / relEnergy, 0, refToNew_new, aff_g2l_new);

                    };

                    Comparison<ValueTuple<ValueTuple<double, double, double, SE3, AffLight>, double[]>> compare = 
                        (y, z) => {
                            var diff = z.Item1.Item2 - y.Item1.Item2;
                            if (diff < 0) return -1;
                            else if (diff > 0) return 1;
                            else return 0;
                        };

                    //Generate init population
                    for (int i = 0; i < popSize; i++)
                    {
                        var l = (double[])initGen.Clone();
                        for (int gi = 0; gi < genLength; gi++)
                        {
                                var diff = baseStep[gi] * ((Context.Rand.NextDouble() * maxVar) - (maxVar / 2));
                                l[gi] += diff;
                        }
                        population[i] = new ValueTuple<ValueTuple<double, double, double, SE3, AffLight>, double[]>(evalFitness(l), l);
                    }


                    int stillCount = 0;
                    double lastTop=double.NaN;

                    for (int geni = 0; geni < maxGenerations; geni++)
                    {
                        Array.Sort(population, compare);
                        var sumOfFitness = population.Select(x => x.Item1.Item2).Sum();
                        //make % from fitness and aggregate
                        for (int i = 0; i < population.Length; i++)
                        {
                            population[i].Item1.Item3 = population[i].Item1.Item2 / sumOfFitness;
                            if (i != 0)
                                population[i].Item1.Item3 += population[i - 1].Item1.Item3;

                            if (i < 10)
                            {
                                Log.LogDebug($"Gen {geni} Error: {population[i].Item1.Item1} Fitness: {population[i].Item1.Item2}");
                            }
                            else if (i < 20)
                            {
                                var newI = Context.Rand.Next(10, popSize);
                                Log.LogDebug($"Gen {geni} Error: {population[newI].Item1.Item1} Fitness: {population[newI].Item1.Item2}");
                            }
                        }
                        Log.LogDebug("====");
                        //get new population
                        for (int i = 0; i < popSize; i++)
                        {
                            if (i < imunne)
                            {
                                newPopulation[i] = population[i];
                            }
                            else
                            {
                                var selected = Context.Rand.NextDouble();
                                for (int si = 0; si < popSize; si++)
                                {
                                    if (selected < population[si].Item1.Item3)
                                    {
                                        newPopulation[i] = population[si];
                                        newPopulation[i].Item2 = (double[])newPopulation[i].Item2.Clone(); //in case same is selected multiple times - so everyone is able to cross and mutate independently
                                        break;
                                    }
                                }
                            }
                        }
                        //crossover
                        int firstParent = -1;
                        int newAdded = -1;
                        for (int i = imunne; i < popSize; i++)
                        {
                            if (i >= newAdded) break; //do not mess with new added to end

                            if (Context.Rand.NextDouble() < toCrossOverP)
                            {
                                if (firstParent == -1)
                                { firstParent = i; }
                                else
                                {
                                    bool switched = false;
                                    var idxToSwitch = Context.Rand.Next(0, genLength);

                                    if (newAdded == -1)
                                    {
                                        newAdded = popSize - 2;
                                    }
                                    else
                                    {
                                        newAdded -= 2;
                                    }


                                    for (int gi = 0; gi < genLength; gi++)
                                    {
                                        if (gi == idxToSwitch) switched = true; //one time cross

                                        if (switched)
                                        {
                                            //cross gens
                                            var tmp = newPopulation[firstParent].Item2[gi];
                                            newPopulation[newAdded].Item2[gi] = newPopulation[i].Item2[gi];
                                            newPopulation[newAdded + 1].Item2[gi] = tmp;
                                        }
                                        else {
                                            newPopulation[newAdded].Item2[gi] = newPopulation[firstParent].Item2[gi];
                                            newPopulation[newAdded + 1].Item2[gi] = newPopulation[i].Item2[gi];
                                        }
                                    }
                                    firstParent = -1;
                                }
                            }
                        }
                        //mutate
                        for (int i = imunne; i < popSize; i++)
                        {
                            if (Context.Rand.NextDouble() < mutateP)
                            {
                                var dim = Context.Rand.Next(0, genLength);
                                var diff = baseStep[dim] * ((Context.Rand.NextDouble() * mutateVar) - (mutateVar / 2));
                                newPopulation[i].Item2[dim] += diff;
                            }
                        }

                        //compute new fitness
                        //Parallel.For(0, popSize, (i) =>
                        //{
                        for (int i = 0; i < popSize; i++)
                        {
                            newPopulation[i].Item1 = evalFitness(newPopulation[i].Item2);
                        }
                        //});

                        //change base Step
                        for (int i = 0; i < genLength; i++)
                        {
                            baseStep[i] *= baseStepChangeRate;
                        }
                        //quit if still too long
                        if (newPopulation[0].Item1.Item1 == lastTop)
                        {
                            stillCount++;
                        }
                        else
                        {
                            stillCount = 0;
                            lastTop = newPopulation[0].Item1.Item1;
                        }
                        if (stillCount >= stillCountToQuit)
                        {
                            Log.LogDebug($"Still for {stillCount} iterations - quiting");
                            break;
                        }


                        population = newPopulation;

                        if (newFrameHessian.Shell.IncomingId == Context.Config.Core.GeneticAlg.InterestFrameID)
                        {
                            var relE = population.Skip(0).Take(1).Select(x => x.Item1.Item1).Average();
                            Context.Output.UpdateData($"TrackNewCoarseGen", $"Lvl{lvl}", geni, relE, "Iteration", "Relative energy");

                            //for (int i = 0; i < popSize; i++)
                            //{
                            //    Context.Output.UpdateData($"TrackNewCoarseGenAll", $"{i}", geni, population[i].Item1.Item1, "Generation", "");
                            //}
                        }
                    }

                    Array.Sort(population, compare);
                    resOld = population[0].Item2;
                    aff_g2l_current = population[0].Item1.Item5;
                    refToNew_current = population[0].Item1.Item4;

                    #endregion
                }
                else
                {

                    #region OriginalOptimize
                    for (int iteration = 0; iteration < maxIterations[lvl]; iteration++)
                    {
                        Mat88 Hl = H;
                        for (int i = 0; i < 8; i++) Hl[i, i] *= (1 + lambda);

                        var inc = Hl.LdltSolve(b);

                        if (Context.Config.Core.AffineOptModeA < 0 && Context.Config.Core.AffineOptModeB < 0)   // fix a, b
                        {
                            var incHead = Hl.LdltSolve(b, 6);//inc.head < 6 > () = Hl.topLeftCorner < 6,6 > ().ldlt().solve(-b.head < 6 > ());
                            for (int i = 0; i < 6; i++)
                            {
                                inc[i] = incHead[i];
                            }
                            inc[inc.Length - 2] = 0; //inc.tail < 2 > ().setZero();
                            inc[inc.Length - 1] = 0;
                        }
                        if (!(Context.Config.Core.AffineOptModeA < 0) && Context.Config.Core.AffineOptModeB < 0)    // fix b
                        {
                            var incHead = Hl.LdltSolve(b, 7);//inc.head < 7 > () = Hl.topLeftCorner < 7,7 > ().ldlt().solve(-b.head < 7 > ());
                            inc[inc.Length] = 0;//inc.tail < 1 > ().setZero();
                        }
                        if (Context.Config.Core.AffineOptModeA < 0 && !(Context.Config.Core.AffineOptModeB < 0))    // fix a
                        {
                            Mat88 HlStitch = Hl;
                            var bStitch = b;
                            HlStitch.CpyCol(7, 6); //HlStitch.col(6) = HlStitch.col(7);
                            HlStitch.CpyRow(7, 6);//HlStitch.row(6) = HlStitch.row(7);
                            bStitch[6] = bStitch[7];
                            var incStitch = HlStitch.LdltSolve(bStitch, 7); //Vec7 incStitch = HlStitch.topLeftCorner < 7,7 > ().ldlt().solve(-bStitch.head < 7 > ());

                            for (int i = 0; i < 6; i++)//inc.setZero();//inc.head < 6 > () = incStitch.head < 6 > ();
                            {
                                inc[i] = incStitch[i];
                            }

                            inc[6] = 0;
                            inc[7] = incStitch[6];
                        }




                        float extrapFac = 1;
                        if (lambda < lambdaExtrapolationLimit) extrapFac = (float)Math.Sqrt(Math.Sqrt(lambdaExtrapolationLimit / lambda));
                        inc = inc.Mul(extrapFac);

                        var incScaled = inc; //Vec8
                                             //incScaled.segment < 3 > (0) *= SCALE_XI_ROT;
                        for (int i = 0; i < 3; i++)
                        {
                            incScaled[i] *= Context.Config.Core.ScaleXIROT;
                        }
                        //incScaled.segment < 3 > (3) *= SCALE_XI_TRANS;
                        for (int i = 3; i < 6; i++)
                        {
                            incScaled[i] *= Context.Config.Core.ScaleXITRANS;
                        }
                        //incScaled.segment < 1 > (6) *= SCALE_A;
                        incScaled[6] *= Context.Config.Core.ScaleA;
                        //incScaled.segment < 1 > (7) *= SCALE_B;
                        incScaled[7] *= Context.Config.Core.ScaleB;

                        var sum = incScaled.Sum();
                        if (double.IsNaN(sum) || double.IsInfinity(sum))
                        {
                            for (int i = 0; i < incScaled.Length; i++)
                            {
                                incScaled[i] = 0;
                            }
                        }

                        SE3 refToNew_new = SE3.Exp(incScaled.Take(6).ToArray()) * refToNew_current;
                        AffLight aff_g2l_new = aff_g2l_current;
                        aff_g2l_new.a += incScaled[6];
                        aff_g2l_new.b += incScaled[7];

                        var resNew = calcRes(lvl, refToNew_new, ref aff_g2l_new, Context.Config.Core.CoarseCutoffTH * levelCutoffRepeat);

                        if (newFrameHessian.Shell.IncomingId == Context.Config.Core.GeneticAlg.InterestFrameID)
                        {
                            Context.Output.UpdateData($"TrackNewCoarse", $"Lvl{lvl}", iteration, resNew[0] / resNew[1], "Iteration", "Relative energy");
                        }

                        bool accept = (resNew[0] / resNew[1]) < (resOld[0] / resOld[1]);

                        if (DebugPrint)
                        {
                            var relAffI = AffLight.FromToVecExposure(LastRef.AbExposure, NewFrame.AbExposure, LastRefAffG2l, aff_g2l_new);
                            Log.LogDebug(string.Format("lvl {0}, it {1} (l={2} / {3}) {4}: {5}->{6} ({7} -> {8}) (|inc| = {9})!",
                                    lvl, iteration, lambda,
                                    extrapFac,
                                    (accept ? "ACCEPT" : "REJECT"),
                                    resOld[0] / resOld[1],
                                    resNew[0] / resNew[1],
                                    (int)resOld[1], (int)resNew[1],
                                    inc.Norm()));
                            Log.LogDebug($"{refToNew_new.Log()} AFF {aff_g2l_new.a}, {aff_g2l_new.b} (rel {relAffI})");
                        }
                        if (accept)
                        {
                            _coarseTrackerU.CalcGSSSE(_fx[lvl], _fy[lvl], _bufWarpedN, LastRefAffG2l.b, AffLight.FromToVecExposure(LastRef.AbExposure, NewFrame.AbExposure, LastRefAffG2l, aff_g2l_current)[0], ref H, ref b, ref refToNew_new, aff_g2l_new.a, aff_g2l_new.b, _bufWarpedDx, _bufWarpedDy, _bufWarpedU, _bufWarpedV, _bufWarpedIdepth, _bufWarpedRefColor, _bufWarpedResidual, _bufWarpedWeight, Context.Config.Core.ScaleXIROT, Context.Config.Core.ScaleXITRANS, Context.Config.Core.ScaleA, Context.Config.Core.ScaleB);
                            resOld = resNew;
                            aff_g2l_current = aff_g2l_new;
                            refToNew_current = refToNew_new;
                            lambda *= 0.5f;
                        }
                        else
                        {
                            lambda *= 4;
                            if (lambda < lambdaExtrapolationLimit) lambda = lambdaExtrapolationLimit;
                        }

                        if (!(inc.Norm() > 1e-3))
                        {
                            if (DebugPrint)
                                Log.LogDebug("Inc too small, break!");
                            break;
                        }
                    }
                    #endregion
                }

                // set last residual for that level, as well as flow indicators.
                LastResiduals[lvl] = Math.Sqrt(resOld[0] / resOld[1]);
                for (int i = 2; i < 5; i++)// LastFlowIndicators = resOld.segment < 3 > (2);
                {
                    LastFlowIndicators[i - 2] = resOld[i];
                }

                if (LastResiduals[lvl] > 1.5 * minResForAbort[lvl]) return false;


                if (levelCutoffRepeat > 1 && !haveRepeated)
                {
                    lvl++;
                    haveRepeated = true;
                    Log.LogDebug("REPEAT LEVEL!");
                }
            }




            //OPTIMIZE END==============================================================================




            // set!
            lastToNew_out = refToNew_current;
            aff_g2l_out = aff_g2l_current;


            if ((Context.Config.Core.AffineOptModeA != 0 && (Math.Abs(aff_g2l_out.a) > 1.2))
            || (Context.Config.Core.AffineOptModeB != 0 && (Math.Abs(aff_g2l_out.b) > 200)))
                return false;

            var relAff = AffLight.FromToVecExposure(LastRef.AbExposure, NewFrame.AbExposure, LastRefAffG2l, aff_g2l_out);

            if ((Context.Config.Core.AffineOptModeA == 0 && (Math.Abs(Math.Log(relAff[0])) > 1.5))
            || (Context.Config.Core.AffineOptModeB == 0 && (Math.Abs(relAff[1]) > 200)))
                return false;



            if (Context.Config.Core.AffineOptModeA < 0) aff_g2l_out.a = 0;
            if (Context.Config.Core.AffineOptModeB < 0) aff_g2l_out.b = 0;

            return true;
        }

        private double[] calcRes(int lvl, SE3 refToNew, ref AffLight aff_g2l, float cutoffTH)
        {
            float E = 0;
            int numTermsInE = 0;
            int numTermsInWarped = 0;
            int numSaturated = 0;

            int wl = Context.w[lvl];
            int hl = Context.h[lvl];
            var dINewl = NewFrame.dIp[lvl];
            float fxl = _fx[lvl];
            float fyl = _fy[lvl];
            float cxl = _cx[lvl];
            float cyl = _cy[lvl];


            Mat33f RKi = (refToNew.RotationMatrixF() * _Ki[lvl]);
            var t = refToNew.TranslationF();
            var affLL = AffLight.FromToVecExposure(LastRef.AbExposure, NewFrame.AbExposure, LastRefAffG2l, aff_g2l).Cast<double, float>().ToArray();


            float sumSquaredShiftT = 0;
            float sumSquaredShiftRT = 0;
            float sumSquaredShiftNum = 0;

            float maxEnergy = 2 * Context.Config.Core.HuberTH - Context.Config.Core.HuberTH * Context.Config.Core.HuberTH;   // energy for r=setting_coarseCutoffTH.


            MinimalImageB3 resImage = null;
            MinimalImageB3 resImage1 = null;
            MinimalImageB3 resImage2 = null;
            if (DebugPlot)
            {
                resImage = new MinimalImageB3(NewFrame.Shell.IncomingId, wl, hl);
                resImage.SetConst(255, 0);
                resImage.SetConst(255, 1);
                resImage.SetConst(255, 2);

                resImage1 = new MinimalImageB3(NewFrame.Shell.IncomingId, wl, hl);
                resImage2 = new MinimalImageB3(NewFrame.Shell.IncomingId, wl, hl);
            }

            int nl = _pcN[lvl];
            float[] lpc_u = _pcU[lvl];
            float[] lpc_v = _pcV[lvl];
            float[] lpc_idepth = _pcIdepth[lvl];
            float[] lpc_color = _pcColor[lvl];


            for (int i = 0; i < nl; i++)
            {
                float id = lpc_idepth[i];
                float x = lpc_u[i];
                float y = lpc_v[i];

                var pt = (RKi * new float[3] { x, y, 1 }).Plus(t.Mul(id));
                float u = pt[0] / pt[2];
                float v = pt[1] / pt[2];
                float Ku = fxl * u + cxl;
                float Kv = fyl * v + cyl;
                float new_idepth = id / pt[2];

                if (lvl == 0 && i % 32 == 0)
                {
                    // translation only (positive)
                    var ptT = (_Ki[lvl] * new float[3] { x, y, 1 }).Plus(t.Mul(id));
                    float uT = ptT[0] / ptT[2];
                    float vT = ptT[1] / ptT[2];
                    float KuT = fxl * uT + cxl;
                    float KvT = fyl * vT + cyl;

                    // translation only (negative)
                    var ptT2 = (_Ki[lvl] * new float[3] { x, y, 1 }).Minus(t.Mul(id));
                    float uT2 = ptT2[0] / ptT2[2];
                    float vT2 = ptT2[1] / ptT2[2];
                    float KuT2 = fxl * uT2 + cxl;
                    float KvT2 = fyl * vT2 + cyl;

                    //translation and rotation (negative)
                    var pt3 = (RKi * new float[3] { x, y, 1 }).Minus(t.Mul(id));
                    float u3 = pt3[0] / pt3[2];
                    float v3 = pt3[1] / pt3[2];
                    float Ku3 = fxl * u3 + cxl;
                    float Kv3 = fyl * v3 + cyl;

                    //translation and rotation (positive)
                    //already have it.

                    sumSquaredShiftT += (KuT - x) * (KuT - x) + (KvT - y) * (KvT - y);
                    sumSquaredShiftT += (KuT2 - x) * (KuT2 - x) + (KvT2 - y) * (KvT2 - y);
                    sumSquaredShiftRT += (Ku - x) * (Ku - x) + (Kv - y) * (Kv - y);
                    sumSquaredShiftRT += (Ku3 - x) * (Ku3 - x) + (Kv3 - y) * (Kv3 - y);
                    sumSquaredShiftNum += 2;
                }

                if (!(Ku > 2 && Kv > 2 && Ku < wl - 3 && Kv < hl - 3 && new_idepth > 0)) continue;



                float refColor = lpc_color[i];
                var hitColor = Extensions.GetInterpolatedElement33(dINewl, Ku, Kv, wl);
                if (float.IsInfinity(hitColor[0]) || float.IsNaN(hitColor[0])) continue;
                float residual = hitColor[0] - (float)(affLL[0] * refColor + affLL[1]);
                float hw = Math.Abs(residual) < Context.Config.Core.HuberTH ? 1 : Context.Config.Core.HuberTH / Math.Abs(residual);


                if (Math.Abs(residual) > cutoffTH)
                {
                    if (DebugPlot)
                    {
                        resImage.SetPixel4((int)lpc_u[i], (int)lpc_v[i], new byte[3] { 0, 0, 255 });
                        resImage1.SetPixel((int)lpc_u[i], (int)lpc_v[i], Enumerable.Repeat((byte)((float)(affLL[0] * refColor + affLL[1])), 3).ToArray());
                        resImage2.SetPixel((int)Ku, (int)Kv, Enumerable.Repeat((byte)(hitColor[0]), 3).ToArray());

                        resImage1.SetPixelCirc((int)lpc_u[i], (int)lpc_v[i], new byte[] { 0, 0, 255 });
                        resImage2.SetPixelCirc((int)Ku, (int)Kv, new byte[] { 0, 0, 255 });
                    }

                    E += maxEnergy;
                    numTermsInE++;
                    numSaturated++;
                }
                else
                {
                    if (DebugPlot)
                    {
                        var val = (byte)(residual + 128);
                        resImage.SetPixel4((int)lpc_u[i], (int)lpc_v[i], new byte[3] { val, val, val });
                        resImage1.SetPixel((int)lpc_u[i], (int)lpc_v[i], Enumerable.Repeat((byte)((float)(affLL[0] * refColor + affLL[1])), 3).ToArray());
                        resImage2.SetPixel((int)Ku, (int)Kv, Enumerable.Repeat((byte)(hitColor[0]), 3).ToArray());
                    }

                    E += hw * residual * residual * (2 - hw);
                    numTermsInE++;

                    _bufWarpedIdepth[numTermsInWarped] = new_idepth;
                    _bufWarpedU[numTermsInWarped] = u;
                    _bufWarpedV[numTermsInWarped] = v;
                    _bufWarpedDx[numTermsInWarped] = hitColor[1];
                    _bufWarpedDy[numTermsInWarped] = hitColor[2];
                    _bufWarpedResidual[numTermsInWarped] = residual;
                    _bufWarpedWeight[numTermsInWarped] = hw;
                    _bufWarpedRefColor[numTermsInWarped] = lpc_color[i];
                    numTermsInWarped++;
                }
            }


            if (DebugPlot)
            {
                string catName = $"CoarseTrackerRes";
                Context.Output.UpdateImage(catName, resImage);
                Context.Output.UpdateImage(catName + "1", resImage1);
                Context.Output.UpdateImage(catName + "2", resImage2);
                if (Context.Output is Output.Abstractions.OutputViewer ov)
                {
                    if ((ov.IsCategoryActive(catName) || ov.IsCategoryActive(catName + "1") || ov.IsCategoryActive(catName + "2")) && ((Context.Config.Core.PausingMode & Configuration.PausingMode.Debugging) != Configuration.PausingMode.NoPause))
                    {
                        Console.WriteLine("PAUSE for debugging");
                        Console.ReadKey();
                    }
                }
            }


            while (numTermsInWarped % 4 != 0)
            {
                _bufWarpedIdepth[numTermsInWarped] = 0;
                _bufWarpedU[numTermsInWarped] = 0;
                _bufWarpedV[numTermsInWarped] = 0;
                _bufWarpedDx[numTermsInWarped] = 0;
                _bufWarpedDy[numTermsInWarped] = 0;
                _bufWarpedResidual[numTermsInWarped] = 0;
                _bufWarpedWeight[numTermsInWarped] = 0;
                _bufWarpedRefColor[numTermsInWarped] = 0;
                numTermsInWarped++;
            }
            _bufWarpedN = numTermsInWarped;




            var rs = new double[6];
            rs[0] = E;
            rs[1] = numTermsInE;
            rs[2] = sumSquaredShiftT / (sumSquaredShiftNum + 0.1);
            rs[3] = 0;
            rs[4] = sumSquaredShiftRT / (sumSquaredShiftNum + 0.1);
            rs[5] = numSaturated / (float)numTermsInE;

            return rs;
        }

        internal void MakeK(CalibHessian hCalib)
        {
            _fx[0] = hCalib.fxl;
            _fy[0] = hCalib.fyl;
            _cx[0] = hCalib.cxl;
            _cy[0] = hCalib.cyl;

            for (int level = 1; level < Context.PyrLevelsUsed; ++level)
            {
                _fx[level] = _fx[level - 1] * 0.5f;
                _fy[level] = _fy[level - 1] * 0.5f;
                _cx[level] = (_cx[0] + 0.5f) / ((int)1 << level) - 0.5f;
                _cy[level] = (_cy[0] + 0.5f) / ((int)1 << level) - 0.5f;
            }

            for (int level = 0; level < Context.PyrLevelsUsed; ++level)
            {
                _K[level] = new Mat33f(new float[9] { _fx[level], 0.0f, _cx[level], 0.0f, _fy[level], _cy[level], 0.0f, 0.0f, 1.0f });
                _Ki[level] = _K[level].Inverse();
                _fxi[level] = _Ki[level][0, 0];
                _fyi[level] = _Ki[level][1, 1];
                _cxi[level] = _Ki[level][0, 2];
                _cyi[level] = _Ki[level][1, 2];
            }
        }
    }
}