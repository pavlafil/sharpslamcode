﻿using SharpSLAM.Core.Unmanaged;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace SharpSLAM.Core
{

    public class CoarseDistanceMap:Contextual
    {
        public float[] FwdWarpedIDDistFinal { get; set; }
        public int[][] _bfsList1 { get; set; }
        public int[][] _bfsList2 { get; set; }
        //public int[] w { get;  set; }
        //public int[] h { get; set; }

        public CoarseDistanceMap(CoreContext context):base(context)
        {
            var ww = Context.w[0];
            var hh = Context.h[0];
            FwdWarpedIDDistFinal = new float[ww * hh / 4];

            _bfsList1 = new int[ww * hh / 4][];
            _bfsList2 = new int[ww * hh / 4][];

            //int fac = 1 << (Context.PyrLevelsUsed - 1);
            //notused??//coarseProjectionGrid = new PointFrameResidual*[2048 * (ww * hh / (fac * fac))];
            //notused??//coarseProjectionGridNum = new int[ww * hh / (fac * fac)];

            //should be already in context
            //w = new int[Context.PyrLevelsUsed];
            //h = new int[Context.PyrLevelsUsed];
            //w[0] = h[0] = 0;
        }

        internal void MakeDistanceMap(List<FrameHessian> frameHessians, FrameHessian frame)
        {
            int w1 = Context.w[1];
            int h1 = Context.h[1];
            int wh1 = w1 * h1;
            for (int i = 0; i < wh1; i++)
                FwdWarpedIDDistFinal[i] = 1000;


            // make coarse tracking templates for latstRef.
            int numItems = 0;

            foreach (FrameHessian fh in frameHessians)
            {
                if (frame == fh) continue;

                SE3 fhToNew = frame.PREWorldToCam * fh.PRECamToWorld;
                Mat33f KRKi = (Context.K[1] * fhToNew.RotationMatrixF() * Context.Ki[0]);
                float[] Kt = (Context.K[1] * fhToNew.TranslationF());

                foreach (PointHessian ph in fh.PointHessians)
                {
                    Debug.Assert(ph.Status == PointHessianStatus.ACTIVE);
                    var ptp = (KRKi * new float[3] { ph.U, ph.V, 1 }).Plus(Kt.Mul(ph.IdepthScaled));
                    int u = (int)(ptp[0] / ptp[2] + 0.5f);
                    int v = (int)(ptp[1] / ptp[2] + 0.5f);
                    if (!(u > 0 && v > 0 && u < Context.w[1] && v < Context.h[1])) continue;
                    FwdWarpedIDDistFinal[u + w1 * v] = 0;
                    _bfsList1[numItems] = new int[2] { u, v };
                    numItems++;
                }
            }
            growDistBFS(numItems);
        }

        private void growDistBFS(int bfsNum)
        {
            Debug.Assert(Context.w[0] != 0);
            int w1 =Context.w[1], h1 = Context.h[1];
            for (int k = 1; k < 40; k++)
            {
                int bfsNum2 = bfsNum;

                ////std::swap<Eigen::Vector2i*>(bfsList1,bfsList2);
                var tmp = _bfsList1;
                _bfsList1 = _bfsList2;
                _bfsList2 = tmp;

                bfsNum = 0;

                if (k % 2 == 0)
                {
                    for (int i = 0; i < bfsNum2; i++)
                    {
                        int x = _bfsList2[i][0];
                        int y = _bfsList2[i][1];
                        if (x == 0 || y == 0 || x == w1 - 1 || y == h1 - 1) continue;
                        int idx = x + y * w1;

                        if (FwdWarpedIDDistFinal[idx + 1] > k)
                        {
                            FwdWarpedIDDistFinal[idx + 1] = k;
                            _bfsList1[bfsNum] = new int[2] { x + 1, y }; bfsNum++;
                        }
                        if (FwdWarpedIDDistFinal[idx - 1] > k)
                        {
                            FwdWarpedIDDistFinal[idx - 1] = k;
                            _bfsList1[bfsNum] = new int[2] { x - 1, y }; bfsNum++;
                        }
                        if (FwdWarpedIDDistFinal[idx + w1] > k)
                        {
                            FwdWarpedIDDistFinal[idx + w1] = k;
                            _bfsList1[bfsNum] = new int[2] { x, y + 1 }; bfsNum++;
                        }
                        if (FwdWarpedIDDistFinal[idx - w1] > k)
                        {
                            FwdWarpedIDDistFinal[idx - w1] = k;
                            _bfsList1[bfsNum] = new int[2] { x, y - 1 }; bfsNum++;
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < bfsNum2; i++)
                    {
                        int x = _bfsList2[i][0];
                        int y = _bfsList2[i][1];
                        if (x == 0 || y == 0 || x == w1 - 1 || y == h1 - 1) continue;
                        int idx = x + y * w1;

                        if (FwdWarpedIDDistFinal[idx + 1] > k)
                        {
                            FwdWarpedIDDistFinal[idx + 1] = k;
                            _bfsList1[bfsNum] = new int[2] { x + 1, y }; bfsNum++;
                        }
                        if (FwdWarpedIDDistFinal[idx - 1] > k)
                        {
                            FwdWarpedIDDistFinal[idx - 1] = k;
                            _bfsList1[bfsNum] = new int[2] { x - 1, y }; bfsNum++;
                        }
                        if (FwdWarpedIDDistFinal[idx + w1] > k)
                        {
                            FwdWarpedIDDistFinal[idx + w1] = k;
                            _bfsList1[bfsNum] = new int[2] { x, y + 1 }; bfsNum++;
                        }
                        if (FwdWarpedIDDistFinal[idx - w1] > k)
                        {
                            FwdWarpedIDDistFinal[idx - w1] = k;
                            _bfsList1[bfsNum] = new int[2] { x, y - 1 }; bfsNum++;
                        }

                        if (FwdWarpedIDDistFinal[idx + 1 + w1] > k)
                        {
                            FwdWarpedIDDistFinal[idx + 1 + w1] = k;
                            _bfsList1[bfsNum] = new int[2] { x + 1, y + 1 }; bfsNum++;
                        }
                        if (FwdWarpedIDDistFinal[idx - 1 + w1] > k)
                        {
                            FwdWarpedIDDistFinal[idx - 1 + w1] = k;
                            _bfsList1[bfsNum] = new int[2] { x - 1, y + 1 }; bfsNum++;
                        }
                        if (FwdWarpedIDDistFinal[idx - 1 - w1] > k)
                        {
                            FwdWarpedIDDistFinal[idx - 1 - w1] = k;
                            _bfsList1[bfsNum] = new int[2] { x - 1, y - 1 }; bfsNum++;
                        }
                        if (FwdWarpedIDDistFinal[idx + 1 - w1] > k)
                        {
                            FwdWarpedIDDistFinal[idx + 1 - w1] = k;
                            _bfsList1[bfsNum] = new int[2] { x + 1, y - 1 }; bfsNum++;
                        }
                    }
                }
            }
        }

        public void AddIntoDistFinal(int u, int v)
        {
            if (Context.w[0] == 0) return;
            _bfsList1[0] = new int[2] { u, v };
            FwdWarpedIDDistFinal[u + Context.w[1] * v] = 0;
            growDistBFS(1);
        }
    }
}