﻿using SharpSLAM.Core.Unmanaged;
using System;

namespace SharpSLAM.Core
{
    public class EFResidual:Contextual
    {
        private EFResidualU _efResidualU { get; set; }

        public EFFrame Host { get; set; }
        public EFFrame Target { get; set; }
        public bool IsLinearized { get; set; }
        public bool IsActiveAndIsGoodNEW { get; set; }
        public RawResidualJacobian J { get; set; }

        public int HostIdx { get { return Host.Idx; } }
        public int TargetIdx { get { return Target.Idx; } }

        public PointFrameResidual Data { get; set; }
        public EFPoint Point { get; set; }
        public int IdxInAll { get; set; }

        public float[] ResToZeroF { get; set; }
        public float[] JpJdF { get; set; }

        public EFResidual(CoreContext context, PointFrameResidual org, EFPoint point, EFFrame host, EFFrame target):base(context)
        {
            _efResidualU = new EFResidualU();

            Data = org;
            Point = point;
            Host = host;
            Target = target;

            IsLinearized = false;
            IsActiveAndIsGoodNEW = false;
            J = new RawResidualJacobian(Context);
            //align check //assert(((long)this) % 16 == 0);
            //align check //assert(((long)J) % 16 == 0);

            ResToZeroF = new float[Context.Config.Core.MaxResPerPoint];
            JpJdF = new float[8];
        }

        public void TakeDataF()
        {
            //std::swap<RawResidualJacobian*>(J, data->J);
            var tmp = J;
            J = Data.J;
            Data.J = tmp;

            var JI_JI_Jd = J.JIdx2 * J.Jpdd;

            for (int i = 0; i < 6; i++)
                JpJdF[i] = J.Jpdxi[0][i] * JI_JI_Jd[0] + J.Jpdxi[1][i] * JI_JI_Jd[1];

            var tail = J.JabJIdx * J.Jpdd;
            JpJdF[6] = tail[0];
            JpJdF[7] = tail[1];
        }

        internal void FixLinearizationF(EnergyFunctional ef)
        {
            //unmanaged
            _efResidualU.FixLinearizationF(Context.Config.Core.PatternNumber,ef._adHTdeltaF[HostIdx + ef.nFrames * TargetIdx],J.Jpdxi[0],J.Jpdxi[1],J.Jpdc[0],J.Jpdc[1],J.Jpdd,ef._cDeltaF,J.ResF,ResToZeroF,J.JIdx[0],J.JIdx[1],J.JabF[0],J.JabF[1],Point.DeltaF);
            IsLinearized = true;
        }
    }
}