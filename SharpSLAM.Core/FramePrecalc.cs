﻿using SharpSLAM.Core.Unmanaged;
using System.Linq;

namespace SharpSLAM.Core
{
    public class FramePrecalc:Contextual
    {
        private readonly CalibHessian _hCalib;

        public FrameHessian Host  {get;set;}     // defines row
        public FrameHessian Target { get; set; }    // defines column


        // precalc values
        public Mat33f PRE_RTll { get; set; }
        public Mat33f PRE_KRKiTll { get; set; }
        public Mat33f PRE_RKiTll { get; set; }
        public Mat33f PRE_RTll_0 { get; set; }

        public float[] PRE_aff_mode { get; set; }
        public double PRE_b0_mode { get; set; }


        public float[] PRE_tTll { get;  set; }
        public float[] PRE_KtTll { get; set; }
        public float[] PRE_tTll_0 { get;  set; }

        public float DistanceLL { get;  set; }

        public FramePrecalc(CoreContext context, CalibHessian hCalib):base(context)
        {
            _hCalib = hCalib;
        }

        public void Set(FrameHessian host, FrameHessian target)
        {
            Host = host;
            Target = target;

            SE3 leftToLeft_0 = target.WorldToCamEvalPT * host.WorldToCamEvalPT.Inverse();
            PRE_RTll_0 = leftToLeft_0.RotationMatrixF();
            PRE_tTll_0 = leftToLeft_0.TranslationF();



            SE3 leftToLeft = target.PREWorldToCam * host.PRECamToWorld;
            PRE_RTll = leftToLeft.RotationMatrixF();
            PRE_tTll = leftToLeft.TranslationF();
            DistanceLL = leftToLeft.TranslationF().Norm();


            Mat33f K = Mat33f.Zero();
            K[0, 0] = _hCalib.fxl; //HCalib->fxl();
            K[1, 1] = _hCalib.fyl; //HCalib->fyl();
            K[0, 2] = _hCalib.cxl; //HCalib->cxl();
            K[1, 2] = _hCalib.cyl; //HCalib->cyl();
            K[2, 2] = 1;
            PRE_KRKiTll = K * PRE_RTll * K.Inverse();
            PRE_RKiTll = PRE_RTll * K.Inverse();
            PRE_KtTll = K * PRE_tTll;


            PRE_aff_mode = AffLight.FromToVecExposure(host.AbExposure, target.AbExposure, host.Aff_g2l(), target.Aff_g2l()).Cast<double,float>().ToArray();
            PRE_b0_mode = host.Aff_g2l_0().b;
        }
    }
}