﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharpSLAM.Core
{
    public class CalibHessian : Contextual
    {
        public float fxl { get { return (float)ValueScaled[0]; } }
        public float fyl { get{return   (float)ValueScaled[1]; }}
        public float cxl { get{return   (float)ValueScaled[2]; }}
        public float cyl { get{return   (float)ValueScaled[3]; }}

        public float fxli {get{ return  (float)ValueScaledi[0];} }
        public float fyli {get{ return  (float)ValueScaledi[1];} }
        public float cxli {get{ return  (float)ValueScaledi[2];} }
        public float cyli {get{ return  (float)ValueScaledi[3];} }


        public double[] Step { get; set; }
        public double[] StepBackup { get; set; }
        public double[] ValueBackup { get; set; }

        public double[] Value { get; set; }
        public double[] ValueScaled { get; set; }
        public double[] ValueScaledi { get; set; }

        public double[] ValueZero { get; set; }
        public double[] ValueMinusValueZero { get { return Value.Minus(ValueZero); } }

        public CalibHessian(CoreContext context):base(context)
        {
            SetValueScaled(new double[] { Context.fx[0], Context.fy[0], Context.cx[0], Context.cy[0] });
            ValueZero = (double[])Value.Clone();
        }

        public void SetValue(double[] value)
        {
            //// [0-3: Kl, 4-7: Kr, 8-12: l2r]


            Value = value;
            ValueScaled = new double[4];
            ValueScaled[0] = Context.Config.Core.ScaleF * value[0];
            ValueScaled[1] = Context.Config.Core.ScaleF * value[1];
            ValueScaled[2] = Context.Config.Core.ScaleC * value[2];
            ValueScaled[3] = Context.Config.Core.ScaleC * value[3];


            //this->ValueScaledf = this->ValueScaled.cast<float>();
            ValueScaledi = new double[4];
            ValueScaledi[0] = 1.0 / ValueScaled[0];
            ValueScaledi[1] = 1.0 / ValueScaled[1];
            ValueScaledi[2] = -ValueScaled[2] / ValueScaled[0];
            ValueScaledi[3] = -ValueScaled[3] / ValueScaled[1];
            //this->value_minus_value_zero = this->value - this->value_zero;
        }

        public void SetValueScaled(double[] valueScaled)
        {
            ValueScaled = valueScaled;
            //this->value_scaledf = this->value_scaled.cast<float>();
            Value = new double[4];
            Value[0] = Context.Config.Core.ScaleFInverse * ValueScaled[0];
            Value[1] = Context.Config.Core.ScaleFInverse * ValueScaled[1];
            Value[2] = Context.Config.Core.ScaleCInverse * ValueScaled[2];
            Value[3] = Context.Config.Core.ScaleCInverse * ValueScaled[3];

            //this->value_minus_value_zero = this->value - this->value_zero;
            ValueScaledi = new double[4];
            ValueScaledi[0] = 1.0 / ValueScaled[0];
            ValueScaledi[1] = 1.0 / ValueScaled[1];
            ValueScaledi[2] = -ValueScaled[2] / ValueScaled[0];
            ValueScaledi[3] = -ValueScaled[3] / ValueScaled[1];
        }
    }
}
