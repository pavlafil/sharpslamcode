﻿using SharpSLAM.Core.Unmanaged;
using SharpSLAM.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace SharpSLAM.Core
{
    public class FrameShell
    {
        public ImageAndExposure<MinimalImageF, float> Image { get; set; }

        public int Id { get; set; }             // INTERNAL ID, starting at zero.
        public string IncomingId { get; set; }    // ID passed into DSO
        public double Timestamp { get; set; }		// timestamp passed into DSO.

        public SE3 GroundTruth { get; set; }
        public SE3 CamToWorld { get; set; }			// Write: TRACKING, while frame is still fresh; MAPPING: only when locked [shellPoseMutex].
        public AffLight Aff_g2l { get; set; }
        public bool PoseValid { get; set; }

        // set once after tracking
        public SE3 CamToTrackingRef { get;  set; }
        public FrameShell TrackingRef { get; set; }

        // statisitcs
        public int StatisticsOutlierResOnThis { get; set; }
        public int StatisticsGoodResOnThis { get; set; }
        public int MarginalizedAt { get; set; }
        public double MovedByOpt { get; set; }

        public FrameShell()
        {
            PoseValid = true;
            CamToWorld = new SE3();
            MarginalizedAt = -1;
            CamToTrackingRef = new SE3();
        }
    }
}
