﻿using SharpSLAM.Core.Unmanaged;
using System;
using System.Diagnostics;

namespace SharpSLAM.Core
{
    public class AccumulatedSCHessian : Contextual
    {
        private AccumulatedSCHessianU _accumulatedSCHessianU { get; set; }

        public int[] Nframes { get; set; }


        public AccumulatedSCHessian(CoreContext context,int numThreads):base(context)
        {
            _accumulatedSCHessianU = new AccumulatedSCHessianU(numThreads);
            Nframes = new int[numThreads];
        }

        public void SetZero(int nFrames, int min = 0, int max = 1, int tid = 0)
        {
            _accumulatedSCHessianU.SetZero(nFrames, Nframes[tid], tid);

            Nframes[tid] = nFrames;
        }

        public void AddPoint(EFPoint p, bool shiftPriorToZero, int tid)
        {
            int ngoodres = 0;
            foreach (EFResidual r in p.ResidualsAll) if (r.IsActiveAndIsGoodNEW) ngoodres++;
            if (ngoodres == 0)
            {
                p.HdiF = 0;
                p.BdSumF = 0;
                p.Data.IdepthHessian = 0;
                p.Data.MaxRelBaseline = 0;
                return;
            }

            float H = p.HddAccAF + p.HddAccLF + p.PriorF;
            if (H < 1e-10) H = 1e-10f;

            p.Data.IdepthHessian = H;

            p.HdiF = 1.0f / H;
            p.BdSumF = p.BdAccAF + p.BdAccLF;
            if (shiftPriorToZero) p.BdSumF += p.PriorF * p.DeltaF;
            var Hcd = p.HcdAccAF.Plus(p.HcdAccLF);

            //unmanaged
            _accumulatedSCHessianU.AddPoint1(Hcd, p.HdiF, p.BdSumF, tid);

            Debug.Assert(!float.IsInfinity(p.HdiF) && !float.IsNaN(p.HdiF));

            int nFrames2 = Nframes[tid] * Nframes[tid];
            foreach (EFResidual r1 in p.ResidualsAll)
            {
                if (!r1.IsActiveAndIsGoodNEW) continue;
                int r1ht = r1.HostIdx + r1.TargetIdx * Nframes[tid];

                foreach (EFResidual r2 in p.ResidualsAll)
                {
                    if (!r2.IsActiveAndIsGoodNEW) continue;

                    //unmanaged
                    _accumulatedSCHessianU.AddPoint2(r1ht + r2.TargetIdx * nFrames2, r1.JpJdF, r2.JpJdF, p.HdiF, tid);
                }

                //unmanaged
                _accumulatedSCHessianU.AddPoint3(r1ht, r1.JpJdF, Hcd, p.HdiF, p.BdSumF, tid);
            }
        }

        public void StitchDoubleMT(out MatXX H, out double[] b, EnergyFunctional energyFunctional)
        {
            // sum up, splitting by bock in square.
#if PAR
            throw new NotImplementedException();
                MatXX Hs[NUM_THREADS];
                VecX bs[NUM_THREADS];
                for (int i = 0; i < NUM_THREADS; i++)
                {
                    assert(nframes[0] == nframes[i]);
                    Hs[i] = MatXX::Zero(nframes[0] * 8 + CPARS, nframes[0] * 8 + CPARS);
                    bs[i] = VecX::Zero(nframes[0] * 8 + CPARS);
                }

                red->reduce(boost::bind(&AccumulatedSCHessianSSE::stitchDoubleInternal,
                    this, Hs, bs, EF, _1, _2, _3, _4), 0, nframes[0] * nframes[0], 0);

                // sum up results
                H = Hs[0];
                b = bs[0];

                for (int i = 1; i < NUM_THREADS; i++)
                {
                    H.noalias() += Hs[i];
                    b.noalias() += bs[i];
                }

    
#else
            H = MatXX.Zero(Nframes[0] * 8 + Context.Config.Core.Cpars, Nframes[0] * 8 + Context.Config.Core.Cpars);
            b = new double[Nframes[0] * 8 + Context.Config.Core.Cpars];
            stitchDoubleInternal(H, b, energyFunctional, 0, Nframes[0] * Nframes[0], -1);
#endif

            // make diagonal by copying over parts.
            for (int h = 0; h < Nframes[0]; h++)
            {
                int hIdx = Context.Config.Core.Cpars + h * 8;

                //unmanaged
                _accumulatedSCHessianU.StitchDoubleMTIter(hIdx, H);
            }
        }

        private void stitchDoubleInternal(MatXX H, double[] b, EnergyFunctional energyFunctional, int min, int max, int tid)
        {
            int toAggregate = _accumulatedSCHessianU.NumThreads;
            if (tid == -1) { toAggregate = 1; tid = 0; }    // special case: if we dont do multithreading, dont aggregate.
            if (min == max) return;

            //unmanaged
            _accumulatedSCHessianU.StitchDoubleInternal(Nframes[0],min,max,H,b,energyFunctional._adHost,energyFunctional._adTarget);

            ////	// ----- new: copy transposed parts for calibration only.
            ////	for(int h=0;h<nf;h++)
            ////	{
            ////		int hIdx = 4+h*8;
            ////		H.block<4,8>(0,hIdx).noalias() = H.block<8,4>(hIdx,0).transpose();
            ////	}
        }
    }
}