﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Collections;

namespace SharpSLAM.Shared
{
    public class MinimalImage<Scalar> : OutputContextMedium
    {
        private readonly int _w;
        private readonly int _h;
        private readonly int _channles;
        private Scalar[] _data;

        public int Width { get { return _w; } }
        public int Height { get { return _h; } }
        public int ChannelsCount { get { return _channles; } }
        public Scalar[] Data { get { return _data; } }

        public override bool Is3D => false;

        private void checkCahnnel(int channel)
        {
            if (channel >= _channles || channel < 0)
                throw new IndexOutOfRangeException($"Image has #{_channles} channels, you try access to channel of index {channel}");
        }

        private bool isIn(int x, int y)
        {
            return x >= 0 && x < Width && y >= 0 && y < Height;
        }

        public MinimalImage(string id, int w, int h, Scalar[] data, int channels = 1):base(id)
        {
            _w = w;
            _h = h;
            _channles = channels;
            _data = data;
        }

        public MinimalImage(string id, int w, int h, int channels = 1):base(id)
        {
            _w = w; _h = h; _channles = channels;
            _data = new Scalar[w * h * channels];
        }

        public MinimalImage<Scalar> GetClone()
        {
            Scalar[] cloneData = cloneData = new Scalar[_data.Length];
            _data.CopyTo(cloneData, 0);
            return new MinimalImage<Scalar>(ID, _w, _h, cloneData, _channles);
        }

        private void setAllChanels(int u, int v, Scalar[] val, Action<int, int, Scalar, Nullable<int>> action)
        {
            if (val.Length == ChannelsCount)
            {
                for (int i = 0; i < ChannelsCount; i++)
                {
                    action(u, v, val[i], i);
                }
            }
            else
            {
                throw new Exception("Dimension must match");
            }
        }
        private ref Scalar at(int x, int y, int channel = 0) { return ref _data[channel + (x * _channles + (y * _w * _channles))]; }
        private ref Scalar at(int i, int channel = 0) { return ref _data[channel + (i * _channles)]; }
        public ref Scalar At(int x, int y, int channel = 0) { checkCahnnel(channel); return ref at(x, y, channel); }
        public ref Scalar At(int i, int channel = 0) { checkCahnnel(channel); return ref at(i, channel); }

        public void SetBlack()
        {
            Array.Clear(_data, 0, _data.Length);
        }

        public void SetConst(Scalar val, int? channel = null)
        {
            if (channel.HasValue)
            {
                checkCahnnel(channel.Value);
                for (int i = 0; i < _w * _h; i++) _data[channel.Value + (i * _channles)] = val;
            }
            else
            {
                for (int i = 0; i < _w * _h * _channles; i++) _data[i] = val;
            }

        }

        /// <summary>
        /// Threshold is excluded
        /// </summary>
        /// <returns></returns>
        public Scalar[] GetRainbowColor(object value, object maxThreshold = default)
        {
            if (value is float valf)
            {
                bool thOut = false;
                if (maxThreshold != default && maxThreshold is float threshF)
                {
                    thOut = valf >= threshF;
                }

                var ret = new Scalar[ChannelsCount];
                if (valf >= 0 && !thOut)
                {
                    int icP = (int)valf;
                    float ifP = valf - icP;
                    icP = icP % ChannelsCount;

                    //B-G G-R R-B
                    for (int i = 0; i < ChannelsCount; i++)
                    {
                        if (icP == i)
                            ret[i] = (Scalar)Convert.ChangeType(255f * (1 - ifP), typeof(Scalar));
                        else if ((icP + 1) % ChannelsCount == i)
                            ret[i] = (Scalar)Convert.ChangeType(255f * ifP, typeof(Scalar));
                        else
                            ret[i] = (Scalar)Convert.ChangeType(0f, typeof(Scalar));
                    }
                }
                else
                {
                    for (int i = 0; i < ChannelsCount; i++)
                    {
                        ret[i] = (Scalar)Convert.ChangeType(255f, typeof(Scalar));
                    }
                }
                return ret;
            }
            else
                throw new NotImplementedException();
        }

        public Scalar[] GetJetColor(object value)
        {
            if (value is float valf)
            {
                var ret = new Scalar[_channles];


                if (valf <= 0)
                {
                    ret[0] = (Scalar)Convert.ChangeType(128, typeof(Scalar));
                    return ret;
                }
                if (valf >= 1)
                {
                    ret[ret.Length - 1] = (Scalar)Convert.ChangeType(128, typeof(Scalar));
                    return ret;
                }

                int icP = (int)(valf * 8);
                float ifP = (valf * 8) - icP;

                if (icP == 0)
                {
                    ret[0] = (Scalar)Convert.ChangeType(255 * (0.5 + 0.5 * ifP), typeof(Scalar));
                    ret[1] = (Scalar)Convert.ChangeType(0, typeof(Scalar));
                    ret[2] = (Scalar)Convert.ChangeType(0, typeof(Scalar));
                    return ret;
                }
                if (icP == 1)
                {
                    ret[0] = (Scalar)Convert.ChangeType(255, typeof(Scalar));
                    ret[1] = (Scalar)Convert.ChangeType(255 * (0.5 * ifP), typeof(Scalar));
                    ret[2] = (Scalar)Convert.ChangeType(0, typeof(Scalar));
                    return ret;
                }
                if (icP == 2)
                {
                    ret[0] = (Scalar)Convert.ChangeType(255, typeof(Scalar));
                    ret[1] = (Scalar)Convert.ChangeType(255 * (0.5 + 0.5 * ifP), typeof(Scalar));
                    ret[2] = (Scalar)Convert.ChangeType(0, typeof(Scalar));
                    return ret;
                }
                if (icP == 3)
                {
                    ret[0] = (Scalar)Convert.ChangeType(255 * (1 - 0.5 * ifP), typeof(Scalar));
                    ret[1] = (Scalar)Convert.ChangeType(255, typeof(Scalar));
                    ret[2] = (Scalar)Convert.ChangeType(255 * (0.5 * ifP), typeof(Scalar));
                    return ret;
                }
                if (icP == 4)
                {
                    ret[0] = (Scalar)Convert.ChangeType(255 * (0.5 - 0.5 * ifP), typeof(Scalar));
                    ret[1] = (Scalar)Convert.ChangeType(255, typeof(Scalar));
                    ret[2] = (Scalar)Convert.ChangeType(255 * (0.5 + 0.5 * ifP), typeof(Scalar));
                    return ret;
                }
                if (icP == 5)
                {
                    ret[0] = (Scalar)Convert.ChangeType(0, typeof(Scalar));
                    ret[1] = (Scalar)Convert.ChangeType(255 * (1 - 0.5 * ifP), typeof(Scalar));
                    ret[2] = (Scalar)Convert.ChangeType(255, typeof(Scalar));
                    return ret;
                }
                if (icP == 6)
                {
                    ret[0] = (Scalar)Convert.ChangeType(0, typeof(Scalar));
                    ret[1] = (Scalar)Convert.ChangeType(255 * (0.5 - 0.5 * ifP), typeof(Scalar));
                    ret[2] = (Scalar)Convert.ChangeType(255, typeof(Scalar));
                    return ret;
                }
                if (icP == 7)
                {
                    ret[0] = (Scalar)Convert.ChangeType(0, typeof(Scalar));
                    ret[1] = (Scalar)Convert.ChangeType(0, typeof(Scalar));
                    ret[2] = (Scalar)Convert.ChangeType(255 * (1 - 0.5 * ifP), typeof(Scalar));
                    return ret;
                }

                for (int i = 0; i < _channles; i++)
                {
                    ret[i] = (Scalar)Convert.ChangeType(255, typeof(Scalar));
                }
                return ret;
            }
            else
                throw new NotImplementedException();
        }

        public void SetPixel(int u, int v, Scalar[] val)
        {
            setAllChanels(u, v, val, SetPixel);
        }

        public void SetPixel(int u, int v, Scalar val, int? channel = null)
        {
            int from = 0; int to = ChannelsCount;
            if (channel.HasValue)
            {
                from = channel.Value;
                to = from + 1;
                checkCahnnel(channel.Value);
            }

            for (int i = from; i < to; i++)
            {
                at(u, v, i) = val;
            }
        }

        public void SetPixel4(int u, int v, Scalar[] val)
        {
            setAllChanels(u, v, val, SetPixel4);
        }

        public void SetPixel4(int u, int v, Scalar val, int? channel = null)
        {
            int from = 0; int to = ChannelsCount;
            if (channel.HasValue)
            {
                from = channel.Value;
                to = from + 1;
                checkCahnnel(channel.Value);
            }

            for (int i = from; i < to; i++)
            {
                at((int)(u + 1.0f), (int)(v + 1.0f), i) = val;
                at((int)(u + 1.0f), (int)(v), i) = val;
                at((int)(u), (int)(v + 1.0f), i) = val;
                at((int)(u), (int)(v), i) = val;
            }
        }

        public void SetPixel9(int u, int v, Scalar[] val)
        {
            setAllChanels(u, v, val, SetPixel9);
        }

        public void SetPixel9(int u, int v, Scalar val, int? channel = null)
        {
            int from = 0; int to = ChannelsCount;
            if (channel.HasValue)
            {
                from = channel.Value;
                to = from + 1;
                checkCahnnel(channel.Value);
            }

            for (int i = from; i < to; i++)
            {
                int cx, cy;
                cx = u + 1; cy = v - 1;
                if (isIn(cx, cy)) at(cx, cy, i) = val;
                cx = u + 1; cy = v;
                if (isIn(cx, cy)) at(cx, cy, i) = val;
                cx = u + 1; cy = v + 1;
                if (isIn(cx, cy)) at(cx, cy, i) = val;
                cx = u; cy = v - 1;
                if (isIn(cx, cy)) at(cx, cy, i) = val;
                cx = u; cy = v;
                if (isIn(cx, cy)) at(cx, cy, i) = val;
                cx = u; cy = v + 1;
                if (isIn(cx, cy)) at(cx, cy, i) = val;
                cx = u - 1; cy = v - 1;
                if (isIn(cx, cy)) at(cx, cy, i) = val;
                cx = u - 1; cy = v;
                if (isIn(cx, cy)) at(cx, cy, i) = val;
                cx = u - 1; cy = v + 1;
            }
        }

        public void SetPixelCirc(int u, int v, Scalar[] val)
        {
            setAllChanels(u, v, val, SetPixelCirc);
        }

        public void SetPixelCirc(int u, int v, Scalar val, int? channel = null)
        {
            int from = 0; int to = ChannelsCount;
            if (channel.HasValue)
            {
                from = channel.Value;
                to = from + 1;
                checkCahnnel(channel.Value);
            }

            for (int i = from; i < to; i++)
            {
                for (int ii = -3; ii <= 3; ii++)
                {
                    int cx, cy;

                    cx = u + 3; cy = v + ii;
                    if (isIn(cx, cy)) at(cx, cy, i) = val;
                    cx = u - 3; cy = v + ii;
                    if (isIn(cx, cy)) at(cx, cy, i) = val;
                    cx = u + 2; cy = v + ii;
                    if (isIn(cx, cy)) at(cx, cy, i) = val;
                    cx = u - 2; cy = v + ii;
                    if (isIn(cx, cy)) at(cx, cy, i) = val;

                    cx = u + ii; cy = v - 3;
                    if (isIn(cx, cy)) at(cx, cy, i) = val;
                    cx = u + ii; cy = v + 3;
                    if (isIn(cx, cy)) at(cx, cy, i) = val;
                    cx = u + ii; cy = v - 2;
                    if (isIn(cx, cy)) at(cx, cy, i) = val;
                    cx = u + ii; cy = v + 2;
                    if (isIn(cx, cy)) at(cx, cy, i) = val;
                }
            }
        }
    };


    //public struct MinimalVectorF3
    //{
    //    private float _a; private float _b; private float _c;
    //    public MinimalVectorF3(float a, float b, float c)
    //    {
    //        _a = a; _b = b; _c = c;
    //    }

    //    public float this[int i]
    //    {
    //        get
    //        {
    //            return i == 0 ? _a : i == 1 ? _b : _c;
    //        }
    //        set
    //        {
    //            if (i == 0) { _a = value; } else if (i == 1) { _b = value; } else { _c = value; }
    //        }
    //    }
    //}

    //public struct MinimalVectorB3
    //{
    //    private byte _a; private byte _b; private byte _c;
    //    public MinimalVectorB3(byte a, byte b, byte c)
    //    {
    //        _a = a;_b = b;_c = c;
    //    }

    //    public byte this[int i]
    //    {
    //        get
    //        {
    //            return i==0?_a:i==1?_b:_c;
    //        }
    //        set
    //        {
    //            if (i == 0) { _a = value; } else if (i == 1) { _b = value; } else { _c = value; }
    //        }
    //    }
    //}

    //public struct MinimalVector<T> where T : struct
    //{
    //    private T[] _data;
    //    public MinimalVector(int size)
    //    {
    //        _data = new T[size];
    //    }

    //    public MinimalVector(T[] data)
    //    {
    //        _data = data;
    //    }

    //    public MinimalVector(IEnumerable<T> data)
    //    {
    //        _data = data.ToArray();
    //    }

    //    public T this[int i]
    //    {
    //        get
    //        {
    //            return _data[i];
    //        }
    //        set {
    //            _data[i] = value;
    //        }
    //    }
    //}

    public class MinimalImageB3 : MinimalImage<byte> { public MinimalImageB3(string id, int w, int h) : base(id, w, h, 3) { } public MinimalImageB3(string id, int w, int h, byte[] data) : base(id, w, h, data, 3) { } };
    public class MinimalImageF3 : MinimalImage<float> { public MinimalImageF3(string id, int w, int h) : base(id, w, h, 3) { } public MinimalImageF3(string id, int w, int h, float[] data) : base(id, w, h, data, 3) { } };
    public class MinimalImageB : MinimalImage<byte> { public MinimalImageB(string id, int w, int h) : base(id, w, h) { } public MinimalImageB(string id, int w, int h, byte[] data) : base(id, w, h, data) { } };
    public class MinimalImageF : MinimalImage<float> { public MinimalImageF(string id, int w, int h) : base(id, w, h) { } public MinimalImageF(string id, int w, int h, float[] data) : base(id, w, h, data) { } };
    public class MinimalImageB16 : MinimalImage<ushort> { public MinimalImageB16(string id, int w, int h) : base(id, w, h) { } public MinimalImageB16(string id, int w, int h, ushort[] data) : base(id, w, h, data) { } };
}
