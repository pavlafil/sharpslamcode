﻿namespace SharpSLAM.Shared
{
    public static class LogConfig
    {
        private static bool _debugOutput = true;
        private static bool _infoOutput = true;
        private static bool _errorOutput = true;

        public static bool LogOutput   { get; set; }   = true;
        public static bool DebugOutput { get { return LogOutput && _debugOutput; } set { _debugOutput = value; } }
        public static bool InfoOutput  { get { return LogOutput && _infoOutput; }  set { _infoOutput = value;  } } 
        public static bool ErrorOutput { get { return LogOutput && _errorOutput; } set { _errorOutput = value; } }
    }
}