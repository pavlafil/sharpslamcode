﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharpSLAM.Shared
{
    public class PointsCloud : OutputContextMedium
    {
        public override string ID
        {
            get
            {
                return Frames.Values.Last().ID;
            }
            set
            { }
        }

        public PointsCloud()
        {
            Frames = new Dictionary<int, PointsCloudFrame>();
        }
        public override bool Is3D => true;

        public Dictionary<int, PointsCloudFrame> Frames { get; set; }
    }

    public class PointsCloudFrame
    {
        public PointsCloudFrame(string id)
        {
            ID = id;
        }
        public string ID { get; set; }
        public PointsCloudPoint[] Points { get; set; }
    }

    public class PointsCloudPoint
    {
        public double[] Coords { get; set; }
        public double[] Color { get; set; }


    }
}
