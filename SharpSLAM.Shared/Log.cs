﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharpSLAM.Shared
{
    public static class Log
    {
        private static Dictionary<string, int> _counts = new Dictionary<string, int>();

        public static void CountClear(string key)
        {
            if (LogConfig.LogOutput)
            {
                if (_counts.ContainsKey(key))
                    _counts.Remove(key);
            }
        }

        public static void CountAdd(string key)
        {
            if (LogConfig.LogOutput)
            {
                if (!_counts.ContainsKey(key))
                    _counts.Add(key, 0);
                _counts[key]++;
            }
        }

        public static void CountLog(string key, bool clearAfter = true, Action<string, bool> handler = null)
        {
            if (LogConfig.LogOutput)
            {
                if (handler == null)
                    handler = LogDebug;

                int count = 0;
                if (_counts.ContainsKey(key))
                {
                    count = _counts[key];
                    if (clearAfter)
                        CountClear(key);
                }

                handler($"Count of: \"{key}\": {count}", true);

            }
        }

        public static void CountLogDebug(string key, bool clearAfter = true)
        {
            CountLog(key, clearAfter, LogDebug);
        }
        public static void CountLogInfo(string key, bool clearAfter = true)
        {
            CountLog(key, clearAfter, LogInfo);
        }
        public static void CountLogError(string key, bool clearAfter = true)
        {
            CountLog(key, clearAfter, LogError);
        }

        public static void LogDebug(string message, bool breakLine = true)
        {
            if (LogConfig.DebugOutput)
            {
                if (breakLine)
                    Console.WriteLine("Debug: " + message);
                else
                    Console.Write("Debug: " + message);
            }
        }
        public static void LogInfo(string message, bool breakLine = true)
        {
            if (LogConfig.InfoOutput)
                if (breakLine)
                    Console.WriteLine(message);
                else
                    Console.Write(message);
        }
        public static void LogError(string message, bool breakLine = true)
        {
            if (LogConfig.ErrorOutput)
                if (breakLine)
                    Console.WriteLine(message);
                else
                    Console.Write(message);
        }
    }
}
