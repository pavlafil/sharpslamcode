﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharpSLAM.Shared
{
    public class CamPose : OutputContextMedium
    {
        public CamPose(string id):base(id)
        {
            Points = new double[5][];
            //for (int i = 0; i < 5; i++)
            //{
            //    Points[i] = new double[3];
            //}
        }

        public override bool Is3D => true;

        public double[][] Points { get; private set; }

        public bool RenderAll { get; set; }

        public bool IsKeyFrame { get; set; }

        public byte[] Color { get; set; }
    }
}
