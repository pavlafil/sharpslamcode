﻿namespace SharpSLAM.Shared
{
    public class ImageAndExposure<T,S> where T : MinimalImage<S> where S : struct
    {
        public double TimeStamp { get; set; }
        public float ExposureTime { get; set; }
        public double[] GroundTruth { get; set; }
        public T Image { get; set; }
    }
}