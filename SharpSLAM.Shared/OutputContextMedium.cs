﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharpSLAM.Shared
{
    public abstract class OutputContextMedium
    {
        public OutputContextMedium()
        {
        }

        public OutputContextMedium(string id)
        {
            ID = id;
        }

        public virtual string ID { get; set; }

        public abstract bool Is3D { get;}
    }
}
