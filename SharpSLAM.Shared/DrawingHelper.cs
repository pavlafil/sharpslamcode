﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Text;

namespace SharpSLAM.Shared
{
    public static class DrawingHelper
    {
        public static Bitmap MakeGrayscale3(Bitmap original)
        {
            //create a blank bitmap the same size as original
            Bitmap newBitmap = new Bitmap(original.Width, original.Height);

            //get a graphics object from the new image
            Graphics g = Graphics.FromImage(newBitmap);

            //create the grayscale ColorMatrix
            ColorMatrix colorMatrix = new ColorMatrix(
               new float[][]
               {
                 new float[] {.3f, .3f, .3f, 0, 0},
                 new float[] {.59f, .59f, .59f, 0, 0},
                 new float[] {.11f, .11f, .11f, 0, 0},
                 new float[] {0, 0, 0, 1, 0},
                 new float[] {0, 0, 0, 0, 1}
               });

            //create some image attributes
            ImageAttributes attributes = new ImageAttributes();

            //set the color matrix attribute
            attributes.SetColorMatrix(colorMatrix);

            //draw the original image on the new image
            //using the grayscale color matrix
            g.DrawImage(original, new Rectangle(0, 0, original.Width, original.Height),
               0, 0, original.Width, original.Height, GraphicsUnit.Pixel, attributes);

            //dispose the Graphics object
            g.Dispose();
            return newBitmap;
        }

        public static byte[] MakeArray(Bitmap original)
        {
            Bitmap clone = new Bitmap(original.Width, original.Height,
                System.Drawing.Imaging.PixelFormat.Format24bppRgb);

            using (Graphics gr = Graphics.FromImage(clone))
            {
                gr.DrawImage(original, new Rectangle(0, 0, clone.Width, clone.Height));
            }

            var data = clone.LockBits(new Rectangle(Point.Empty, clone.Size), ImageLockMode.ReadWrite, clone.PixelFormat);
            var pixelSize = 3; // only works with 32 or 24 pixel-size bitmap!
            var bytes = new byte[data.Height * data.Width * pixelSize];
            // copy the bytes from bitmap to array
            Marshal.Copy(data.Scan0, bytes, 0, bytes.Length);
            clone.UnlockBits(data);
            return bytes;
        }
        public static Bitmap MakeBitmap(byte[] original, int width, int height, PixelFormat format = PixelFormat.Format24bppRgb)
        {
            Bitmap clone = new Bitmap(width, height,format);

            var data = clone.LockBits(new Rectangle(Point.Empty, clone.Size), ImageLockMode.ReadWrite, clone.PixelFormat);
            // copy the bytes from bitmap to array
            Marshal.Copy(original, 0, data.Scan0, original.Length);
            clone.UnlockBits(data);
            return clone;
        }
    }
}
