﻿using Newtonsoft.Json.Linq;
using SharpSLAM.Configuration;
using SharpSLAM.Core;
using SharpSLAM.Input;
using SharpSLAM.Output.Abstractions;
using SharpSLAM.Shared;
using System;
using System.IO;
using System.Threading;

namespace SharpSLAM.App.NetCore
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("To fully work remake unmanaged code to basic invoke mthods. Mixed dlls are not supported in coreCLR yet. https://github.com/dotnet/coreclr/issues/659!!");

            string configuration = null;
#if PUBLISH
            configuration=@".\configurationPublish.json";
#else
            configuration = @".\configurationOthers.json";
#endif

            //Load configuration
            var conff = Configuration.Configuration.FromFile(configuration);

            //Disable debug logging outputs if it is not debug
#if !DEBUG
            conff.Output.Log.Log = false;
#endif

            //Create output
            var output = new Output.Files.OutputFiles();
            output.SetCurrentConfiguration(conff);
            output.Initialize();

            //Create image reader
            var imr = new ImageReader(conff, output);

            //Create core context
            var coreContext = new CoreContext(conff, output);
            //Prepare core - solving system
            var core = new Core.Core(coreContext);

            var dt = DateTime.Now;

            int start = conff.Input.Start;
            int end = conff.Input.End.HasValue ? conff.Input.End.Value : imr.Count - 1;
            if (end < start || end >= imr.Count)
                end = imr.Count - 1;

            if (conff.Core.FramesPerSecondCompute <= 0)
            {
                for (int i = start; i <= end;)
                {
                    OneFrame(ref i, output, imr, core, ref dt);
                }
            }
            else
            {
                var time = 1000 / conff.Core.FramesPerSecondCompute;
                int i = start;
                Timer t = null;
                t = new Timer((o) => {
                    if (i <= end)
                    {
                        OneFrame(ref i, output, imr, core, ref dt);
                    }
                    else
                    {
                        t.Dispose();
                    }
                }, null, 0, time);
            }
        }

        private static void OneFrame(ref int i, Output.Abstractions.Output output, ImageReader imr, Core.Core core, ref DateTime dt)
        {
            output.CategoryPrefix = "01 Preprocessing";
            //Get one frame
            var image = imr.GetImageAndExposureF(i);

            output.CategoryPrefix = "02 Core";
            core.AddActiveFrame(image);
            output.CategoryPrefix = null;

            var dtn = DateTime.Now;
            Console.WriteLine($"\n=== Frame {i} {dtn - dt}\n");
            dt = dtn;

            //GC.Collect();
            //GC.WaitForPendingFinalizers();
            i++;
        }
    }
}
